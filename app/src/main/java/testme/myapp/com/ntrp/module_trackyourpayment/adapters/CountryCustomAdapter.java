package testme.myapp.com.ntrp.module_trackyourpayment.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import testme.myapp.com.ntrp.R;

import testme.myapp.com.ntrp.module_trackyourpayment.models.CountryNameList;


/**
 * Created by prakhar.s on 4/13/2017.
 */

public class CountryCustomAdapter extends BaseAdapter{
    private List<CountryNameList> listModelList;
    private static LayoutInflater inflater=null;

    CountryNameList listModel=null;
    public CountryCustomAdapter(Activity a, List<CountryNameList> listModelList) {

        this.listModelList = listModelList;
        inflater = ( LayoutInflater )a.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return listModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public static class MyViewHolder{

        public TextView textView;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        MyViewHolder holder;
        if(convertView==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            view = inflater.inflate(R.layout.single_row_spinner, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new MyViewHolder();
            holder.textView = (TextView) view.findViewById(R.id.cust_view);


            /************  Set holder with LayoutInflater ************/
            view.setTag( holder );
        }
        else
            holder=(MyViewHolder)view.getTag();
        if(listModelList.size()<=0)
        {
            holder.textView.setText("No Data");

        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            listModel=null;
            listModel = (CountryNameList) listModelList.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.textView.setText( listModel.CountryName );



            /******** Set Item Click Listner for LayoutInflater for each row *******/


        }
        return view;
    }


}