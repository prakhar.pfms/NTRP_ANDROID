package testme.myapp.com.ntrp.module_nonregisteredusers.adapters;

import android.app.Dialog;
import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_nonregisteredusers.events.DeletePurposeEvent;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsersFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.FormDetail;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.PurposeFunctionalMinisty;
import testme.myapp.com.ntrp.utils.MessageEvent;

public class AddFormAdaptor extends RecyclerView.Adapter<AddFormAdaptor.MyViewHolder> {

    private List<FormDetail> purposeLists;
    private Context context;
    private Fragment nonRegisteredUsersFragment;

    public AddFormAdaptor(Context context, Fragment nonRegisteredUsersFragment, List<FormDetail> purposeList)
    {
        this.context=context;
        this.nonRegisteredUsersFragment=nonRegisteredUsersFragment;
        this.purposeLists=purposeList;

    }

    @Override
    public AddFormAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_view_non_registered_user_deposit_detail,parent, false);
        return new AddFormAdaptor.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AddFormAdaptor.MyViewHolder holder, int position) {

        FormDetail purposeList=purposeLists.get(position);


        holder.pao_name_textview_vlue.setText(purposeList.getPAOName());
        holder.ddo_name_textview_vlue.setText(purposeList.getDDOName());
        holder.ministry_textview_vlue.setText(purposeList.getMinistryDepartment());
        holder.amount_textview_vlue.setText(""+purposeList.getAmountValue());
        holder.payment_frequency_textview_vlue.setText(""+purposeList.getPeriod());

        SpannableString str = new SpannableString(purposeList.getPurposeDescription());
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        holder.purpose_textview_vlue.setText(str);


        holder.delete_purpose_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new DeletePurposeEvent(
                        purposeList.getPurposeListId().toString()
                ));
            }
        });
    }

    @Override
    public int getItemCount() {
        return purposeLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        public LinearLayout delete_purpose_layout;
        public TextView purpose_textview_vlue,pao_name_textview_vlue,ddo_name_textview_vlue,ministry_textview_vlue,amount_textview_vlue,payment_frequency_textview_vlue;
        public MyViewHolder(View itemView) {
            super(itemView);
            delete_purpose_layout= (LinearLayout) itemView.findViewById(R.id.delete_purpose_layout);
            purpose_textview_vlue= (TextView) itemView.findViewById(R.id.purpose_textview_vlue);
            pao_name_textview_vlue= (TextView) itemView.findViewById(R.id.pao_name_textview_vlue);
            ddo_name_textview_vlue= (TextView) itemView.findViewById(R.id.ddo_name_textview_vlue);
            ministry_textview_vlue= (TextView) itemView.findViewById(R.id.ministry_textview_vlue);
            amount_textview_vlue= (TextView) itemView.findViewById(R.id.amount_textview_vlue);
            payment_frequency_textview_vlue= (TextView) itemView.findViewById(R.id.payment_frequency_textview_vlue);

        }
    }
}
