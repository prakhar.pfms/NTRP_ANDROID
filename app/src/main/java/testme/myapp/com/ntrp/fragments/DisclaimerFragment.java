package testme.myapp.com.ntrp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.fragment.app.Fragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import testme.myapp.com.ntrp.R;

public class DisclaimerFragment extends Fragment {

    private String html_content=" <!DOCTYPE html> <html lang=\"en\"> <div class=\"container container-l footer-page-heading\"> <h3 class=\"pageHeading\">Disclaimer</h3> <hr> <p><p>This Portal is designed, developed and hosted by O/o Controller General of Accounts, Ministry of Finance, Government of India with sole objective of collections for various Departments/ Ministries in Government of India and others as decided from time to time to promote better service through e-Governance principles. </p> <p>Users are advised to verify/check any information with the relevant Government department(s) and/or other source(s), and to obtain any details about process of delivery of service by related Ministry/Department from them. Portal provides access to Ministry and departments for contents hosted and it is their responsibility to update any changes from time to time. </p> <p>In no event will the Government or O/o Controller General of Accounts be liable for any expense, loss or damage including, without limitation, indirect or consequential loss or damage, or any expense, loss or damage whatsoever arising from use, or loss of use, of data, arising out of or in connection with the use of this Portal. Despite all effort O/o Controller General of Accounts will not be responsible for availability of its portal services beyond its controls and means. </p> <p>Links to other websites that have been included on this Portal are provided for public convenience only. O/o Controller General of Accounts is not responsible for the contents or reliability of linked websites and does not necessarily endorse the view expressed within them. This portal does not guarantee the availability of such linked pages at all times which is beyond its control. </p> <p>Material featured on this Portal may be reproduced after taking proper permission/with condition as may be attached by the competent authority by sending a request in writing to us. The material if permitted has to be reproduced accurately and not to be used in a derogatory manner or in a misleading context. Wherever the material is being published or issued to others, the source must be prominently acknowledged. However, the permission to reproduce this material shall not extend to any material which is identified as being copyright of a third party. Authorisation to reproduce such material must be obtained from the respective Ministry/Departments/Copyright Holders as the case may be. </p> <p>These terms and conditions shall be governed by and construed in accordance with the Indian Laws. Any dispute arising under these terms and conditions shall be subject to the exclusive jurisdiction of the Courts of India. </p></p> </div> </section> </div> <div class=\"clearfix\"></div> </body> </html>";
    @BindView(R.id.disclaimer_web_view)
    public WebView disclaimer_web_view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_disclaimer, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle("Disclaimer");

        disclaimer_web_view.setWebViewClient(new WebViewClient());
        WebSettings set = disclaimer_web_view.getSettings();
        set.setJavaScriptEnabled(true);
        set.setBuiltInZoomControls(true);
        disclaimer_web_view.loadData(html_content, "text/html", "UTF-8");

        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle("Disclaimer");
    }
}
