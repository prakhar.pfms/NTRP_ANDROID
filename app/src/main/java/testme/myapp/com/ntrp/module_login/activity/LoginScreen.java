package testme.myapp.com.ntrp.module_login.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_login.fragments.LoginFragment;



/**
 * Created by deepika.s on 05-05-2017.
 */

public class LoginScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login_screen);
        changeFragment_LoginFragment();
    }
    private void changeFragment_LoginFragment() {

        Fragment loginFragment = new LoginFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_frame, loginFragment);
        ft.commit();
    }


}
