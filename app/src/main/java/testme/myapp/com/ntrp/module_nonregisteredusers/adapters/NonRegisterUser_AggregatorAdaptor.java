package testme.myapp.com.ntrp.module_nonregisteredusers.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.adaptors.AggregatorAdaptor;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.PaymentFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.AggregatorListModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.BankListCorrespondingToSelectedAggregatorModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_PaymentFragment;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

public class NonRegisterUser_AggregatorAdaptor extends RecyclerView.Adapter<NonRegisterUser_AggregatorAdaptor.MyViewHolder> {

    private AggregatorListModel aggregatorListModel;
    private NonRegisteredUsers_PaymentFragment fragment;
    private Context context;
    private RadioButton lastCheckedRB = null;

    public NonRegisterUser_AggregatorAdaptor(Context context, NonRegisteredUsers_PaymentFragment fragment, AggregatorListModel aggregatorListModel) {
        this.context = context;
        this.fragment = fragment;
        this.aggregatorListModel = aggregatorListModel;

    }

    @Override
    public NonRegisterUser_AggregatorAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_view_aggregator_list, parent, false);
        return new NonRegisterUser_AggregatorAdaptor.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NonRegisterUser_AggregatorAdaptor.MyViewHolder holder, int position) {



        holder.aggregator_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton checked_rb = (RadioButton) group.findViewById(checkedId);
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }

                //store the clicked radiobutton
                lastCheckedRB = checked_rb;

            }
        });
        Picasso.with(context)
                .load("https://training.pfms.gov.in/Bharatkosh/App_Themes/Receipt/images/"+aggregatorListModel.getAggregatorList().get(position).getAggregatorLogoName())
                .placeholder(R.drawable.loading_error_image)
                .error(R.drawable.loading_error_image)
                .into(holder.payment_gateway_image);

        holder.successrate_payment_text.setText("Success Rate: "+ aggregatorListModel.getAggregatorList().get(position).getAggregatorwiseSuccessRatio()+" %");

        holder.aggregator_radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Hit_API_GetBankList(fragment.getActivity(),fragment, String.valueOf(aggregatorListModel.getAggregatorList().get(holder.getAdapterPosition()).getAggregatorID()));

            }
        });




    }

    public void Hit_API_GetChannelList(final Activity activity, final PaymentFragment fragment, final String Aggregatorid) {


        HandleHttpsRequest.setSSL();


        System.out.println("Send Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getBankListForSelectedPaymentGatewayURL;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                BankListCorrespondingToSelectedAggregatorModel sendOTPModel=gson.fromJson(response,BankListCorrespondingToSelectedAggregatorModel.class);






                if (sendOTPModel.getAggregatorBankList().size()>0) {
                    MyToken.setToken(sendOTPModel.getRefreshTokenValue());
                    MyToken.setAuthorization(sendOTPModel.getAcsessTokenValue());



                    //  return;
                } else {


                    //  showDialog_ServerError("Some Error occured !");
                    //  return;
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Aggregatorid", Aggregatorid);
                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }

        };




        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    public void Hit_API_GetBankList(final Activity activity, final NonRegisteredUsers_PaymentFragment fragment, final String Aggregatorid) {


        HandleHttpsRequest.setSSL();


        System.out.println("Send Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getBankListForSelectedPaymentGatewayURL;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                BankListCorrespondingToSelectedAggregatorModel sendOTPModel=gson.fromJson(response,BankListCorrespondingToSelectedAggregatorModel.class);






                if (sendOTPModel.getAggregatorBankList().size()>0) {
                    MyToken.setToken(sendOTPModel.getRefreshTokenValue());
                    MyToken.setAuthorization(sendOTPModel.getAcsessTokenValue());


                    fragment.setBankDataToRecyclerView(sendOTPModel,Aggregatorid);

                    fragment.HitGetListForChannelTypeAPI(Aggregatorid);   //debit card



                    //  return;
                } else {


                    //  showDialog_ServerError("Some Error occured !");
                    //  return;
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Aggregatorid", Aggregatorid);
                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }

        };




        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @Override
    public int getItemCount() {
        return aggregatorListModel.getAggregatorList().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView successrate_payment_text;
        public RadioButton aggregator_radio_button;
        public RadioGroup aggregator_radio_group;

        public ImageView payment_gateway_image;
        public MyViewHolder(View itemView) {
            super(itemView);

            successrate_payment_text = (TextView) itemView.findViewById(R.id.successrate_payment_text);
            aggregator_radio_button = (RadioButton) itemView.findViewById(R.id.aggregator_radio_button);
            payment_gateway_image = (ImageView) itemView.findViewById(R.id.payment_gateway_image);
            aggregator_radio_group = (RadioGroup) itemView.findViewById(R.id.aggregator_radio_group);


        }
    }


}


