package testme.myapp.com.ntrp.module_nonregisteredusers.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.activity.MainScreen;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.ConfirmInfoFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.DepositorDetailsFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.ConfirmInfoModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.Country;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.CountryModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.DepositorDetailsSubmitModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.District;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.DistrictModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.SalutaionModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.Salution;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.State;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.StateModel;
import testme.myapp.com.ntrp.module_login.models.LogedInUserDetailsModels;
import testme.myapp.com.ntrp.module_nonregisteredusers.events.BackPressEvent;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.BankAccountValidationModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.DisplayPopUpForPaymentModeModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.OffLinePaymentModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.UserInfoModel;
import testme.myapp.com.ntrp.module_registeration.adapters.BankListCustomAdapter;
import testme.myapp.com.ntrp.module_registeration.models.BankArrayList;
import testme.myapp.com.ntrp.module_registeration.models.BankListModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

/**
 * Created by deepika.s on 31-05-2017.
 */

public class NonRegisteredUsers_DepositorDetailsFragment extends Fragment {

    private View localView;
    private TextView Name_TextView,Address_Line1_TextView,Country_TextView,State_TextView,District_TextView,City_TextView,Email_TextView,wrong_credntial_textview,wrong_bank_details_textview;
    private EditText Name_EditText,Email_EditText,Address_Line1_EditText,Address_Line2_EditText,City_EditText,Pincode_Zipcode_EditText,tin_edittext,tan_edittext;
    private Spinner Spinner_Marital_Status,Spinner_Country,Spinner_State,Spinner_District,Spinner_MobileNo,spinner_bankname;
    private Button nextButton;
    private String EmailText,NameText,AddressLine1Text,SelectedStateName,SelectedDistrictName;
    private int selectedCountryID,selectedStateID,selectedDistrictID,selectedSalutationId;
    private ArrayList<Country> countryArrayList;
    private ArrayList<State> stateArrayList;
    private ArrayList<District> districtArrayList;
    private ArrayList<Salution> salutionArrayList= new ArrayList<>();;
    private int Country_check = 2, State_check = 0, District_check = 0,DepositorCategoryId;
    private String UserNameText;
    private boolean isOnlinePaymentSelected=false,UserLogedInSuccessfull=false;

    private UserInfoModel userInfoModel;

    private Dialog LoginDialog;

    private BankListModel bankListModel;

    private BankAccountValidationModel bankAccountValidationModel;

    @BindView(R.id.onile_payment_radio_button)
    public RadioButton onile_payment_radio_button;

    @BindView(R.id.swift_payment_radio_button)
    public RadioButton swift_payment_radio_button;




    public static String MobileNumber,ReceiptEntryId,UniqueUserDeviceId;

    private  String selectedBankId=null;

    public static boolean OfflineWithLogin,  OfflineWithAccountValidation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_non_registered_users_depositor_details, container, false);
        ButterKnife.bind(this,view);
        localView=view;
        Country_check = 2; State_check = 0; District_check = 0;
        SelectedStateName=null;SelectedDistrictName=null;
        initializeView();
        CloseKeyBoard();

       Bundle bundle = getArguments();
        if (bundle != null) {
            ReceiptEntryId = bundle.getString("ReceiptEntryId");
            MobileNumber = bundle.getString("MobileNumber");
            UniqueUserDeviceId = bundle.getString("UniqueUserDeviceId");
            DepositorCategoryId= bundle.getInt("DepositorCategoryId");
        }
        PreFillDataIfUserLogin();

        return view;
    }

    private void PreFillDataIfUserLogin() {
        if (MainActivity.IsLogin) {
            Gson gson = new GsonBuilder().setLenient().create();
            LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);

            Name_EditText.setText("" + userInfoModel.getFirstName() != null ? userInfoModel.getFirstName() : "");

            Email_EditText.setText("" + userInfoModel.getEmail() != null ? userInfoModel.getEmail() : "");

            City_EditText.setText("" + userInfoModel.getCity() != null ? userInfoModel.getCity() : "");

            Address_Line1_EditText.setText("" + userInfoModel.getAddress1() != null ? userInfoModel.getAddress1() : "");

            Address_Line2_EditText.setText("" + userInfoModel.getAddress2() != null ? userInfoModel.getAddress2() : "");

            Pincode_Zipcode_EditText.setText("" + userInfoModel.getPincode() != null ? userInfoModel.getPincode() : "");

            UserNameText=userInfoModel.getUserName();
        }
    }
    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
    private void initializeView() {

        selectedCountryID=101;
        countryArrayList=new ArrayList<>();
        selectedStateID=0;
        stateArrayList=new ArrayList<>();
        selectedDistrictID=0;
        districtArrayList= new ArrayList<>();

        initialize_TextViews();
        initialize_EditTextViews();
        initializeSpinnerViews();
        initialize_Spinner_Country();
        initialize_Spinner_State();
        initialize_Spinner_District();
        handle_NextButton();
        //set_validation_on_Marital_Status_Dropdown();
        set_Validation_Name();
        set_Validation_Address();
        set_Validation_Address2();
        //System.out.println("77777777777");

    }
    private void initialize_TextViews()
    {
        Name_TextView= (TextView) localView.findViewById(R.id.name_textview);
        Address_Line1_TextView= (TextView) localView.findViewById(R.id.addressline1_textview);
        Country_TextView= (TextView) localView.findViewById(R.id.country_textview);
        State_TextView= (TextView) localView.findViewById(R.id.state_textview);
        District_TextView= (TextView) localView.findViewById(R.id.district_textview);
        City_TextView= (TextView) localView.findViewById(R.id.city_textview);
        Email_TextView= (TextView) localView.findViewById(R.id.email_textview);
        putRedAsterisk(localView, Name_TextView, R.id.name_textview);
        putRedAsterisk(localView, Address_Line1_TextView, R.id.addressline1_textview);
        putRedAsterisk(localView, Country_TextView, R.id.country_textview);
        putRedAsterisk(localView, State_TextView, R.id.state_textview);
        putRedAsterisk(localView, District_TextView, R.id.district_textview);
        putRedAsterisk(localView, City_TextView, R.id.city_textview);
        //putRedAsterisk(localView, Mobile_No_TextView, R.id.mobile_no_textview);
        putRedAsterisk(localView, Email_TextView, R.id.email_textview);
    }
    private void initialize_EditTextViews()
    {
        Name_EditText= (EditText) localView.findViewById(R.id.name_edittext);
        Address_Line1_EditText = (EditText) localView.findViewById(R.id.addressline1_edittext);
        Address_Line2_EditText= (EditText) localView.findViewById(R.id.addressline2_edittext);
        City_EditText= (EditText) localView.findViewById(R.id.city_edittext);
        Pincode_Zipcode_EditText= (EditText) localView.findViewById(R.id.pincode_zipcode_edittext);
        tin_edittext= (EditText) localView.findViewById(R.id.tin_edittext);
        Email_EditText= (EditText) localView.findViewById(R.id.email_edittext);
        tan_edittext= (EditText) localView.findViewById(R.id.tan_edittext);
    }
    private void initializeSpinnerViews()
    {
        Spinner_Marital_Status = (Spinner) localView.findViewById(R.id.spinner_maritalstatus);
       /* String[] items = new String[]{"-Select-", "Mr.", "Ms./Mrs."};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
        Spinner_Marital_Status.setAdapter(adapter);*/
        Spinner_Marital_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                selectedSalutationId = salutionArrayList.get(position).getSalutationId();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }

        });


        Spinner_Country= (Spinner) localView.findViewById(R.id.spinner_country);



        Spinner_State= (Spinner) localView.findViewById(R.id.spinner_state);
        Spinner_District= (Spinner) localView.findViewById(R.id.spinner_district);
        Spinner_MobileNo= (Spinner) localView.findViewById(R.id.spinner_countrypnone_code);

        Spinner_Country.setSelection(selectedCountryID);
        Spinner_State.setSelection(selectedStateID);
        Spinner_District.setSelection(selectedDistrictID);
        HitSalutationAPI();

    }
    private void handle_NextButton()
    {
        nextButton= (Button) localView.findViewById(R.id.next_button);
        setOnClick_NextButton();
    }

    private void changeFragment_ConfirmInfoFragment()
    {
        Bundle bundle=new Bundle();
        bundle.putString("Name",Name_EditText.getText().toString());
        bundle.putString("Address1",Address_Line1_EditText.getText().toString());
        bundle.putString("Address2",Address_Line2_EditText.getText().toString());
        bundle.putString("Country",Spinner_Country.getSelectedItem().toString());
        bundle.putString("State",Spinner_State.getSelectedItem().toString());
        bundle.putString("District",Spinner_District.getSelectedItem().toString());
        bundle.putString("City",City_EditText.getText().toString());
        bundle.putString("Pincode",Pincode_Zipcode_EditText.getText().toString());
        bundle.putString("Mobile", MobileNumber);
        bundle.putString("Email",Email_EditText.getText().toString());
        bundle.putString("ReceiptEntryId", ReceiptEntryId);
        bundle.putString("UserNameText", UserNameText);
        if (OfflineWithAccountValidation)
        {
            bundle.putString("BankAccount", bankAccountValidationModel.getBankAccount());
            bundle.putString("BankIfscCode", bankAccountValidationModel.getBankIfscCode());
            bundle.putString("PaymentPurposeID", String.valueOf(bankAccountValidationModel.getPaymentPurposeID()));
            bundle.putString("BankId", String.valueOf(bankAccountValidationModel.getBankId()));

        }

        Fragment confirmInfoFragment = new NonRegisteredUsers_ConfirmInfoFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();


        confirmInfoFragment.setArguments(bundle);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, confirmInfoFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void changeFragment_OfflineConfirmInfoFragment(String receiptEntryId, String challanNumber, String amount)
    {
        Bundle bundle=new Bundle();

        bundle.putString("amount", amount);
        bundle.putString("challanNumber",challanNumber);
        bundle.putString("ReceiptEntryId",receiptEntryId);



        Fragment confirmInfoFragment = new NonRegisteredUsers_OfflinePaymentFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();


        confirmInfoFragment.setArguments(bundle);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, confirmInfoFragment);
        ft.addToBackStack(null);
        ft.commit();
    }



    private void setOnClick_NextButton()
    {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailText=Email_EditText.getText().toString();
                NameText=Name_EditText.getText().toString();
                AddressLine1Text= Address_Line1_EditText.getText().toString();


                if(NameText.matches("")) {
                    Name_EditText.setError("Please Enter Name!");
                    Toast.makeText(getActivity(),"Please Enter Name!",Toast.LENGTH_SHORT).show();
                    return;
                }
                else if (AddressLine1Text.matches("")) {
                    Address_Line1_EditText.setError("Please Enter Address!");
                    Toast.makeText(getActivity(),"Please Enter Address!",Toast.LENGTH_SHORT).show();
                    return;
                }
                else if (EmailText.matches("")) {
                    Email_EditText.setError("Please Enter Email!");
                    Toast.makeText(getActivity(),"Please Enter Email!",Toast.LENGTH_LONG).show();
                    return;
                }
                else if (City_EditText.getText().toString().matches("")) {
                    City_EditText.setError("Please Enter City!");
                    Toast.makeText(getActivity(),"Please Enter City!",Toast.LENGTH_LONG).show();
                    return;
                }

                else if (!isValidEmail(EmailText)) {
                    Email_EditText.setError("Invalid Email!");
                    Toast.makeText(getActivity(),"Invalid Email",Toast.LENGTH_LONG).show();
                }
                else if (SelectedStateName == null  ) {
                    ((TextView)Spinner_State.getSelectedView()).setError("Select State");
                    Toast.makeText(getActivity(),"Select State",Toast.LENGTH_LONG).show();
                }
                else if (SelectedDistrictName == null  ) {
                    ((TextView)Spinner_District.getSelectedView()).setError("Select District");
                    Toast.makeText(getActivity(),"Select District",Toast.LENGTH_LONG).show();
                }
                else if (UserNameText == null &&swift_payment_radio_button.isChecked() && OfflineWithLogin ) {

                    Toast.makeText(getActivity(),"Please login again for SWIFT/NEFT payment",Toast.LENGTH_LONG).show();
                }
                else if (swift_payment_radio_button.isChecked() && OfflineWithAccountValidation &&bankAccountValidationModel==null) {

                    Toast.makeText(getActivity(),"Please submit your bank details again",Toast.LENGTH_LONG).show();
                }
                else
                {

                    HitSubmitDepositorDetailsAPI();
                }
            }
        });
    }

    private void HitSubmitDepositorDetailsAPI() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_User_Depositor_URL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                DepositorDetailsSubmitModel depositorCategoryModel = gson.fromJson(response, DepositorDetailsSubmitModel.class);


                if (depositorCategoryModel.getReceiptEntryId()!=null&&depositorCategoryModel.getAcsessTokenValue()!=null&&depositorCategoryModel.getRefreshTokenValue()!=null)
                {
                    MyToken.setToken(depositorCategoryModel.getRefreshTokenValue());
                    MyToken.setAuthorization(depositorCategoryModel.getAcsessTokenValue());

                    /*if (UserLogedInSuccessfull&&onile_payment_radio_button.isChecked()==false)
                    {
                        if (MainActivity.IsLogin)
                        {

                            LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);

                            Hit_Offline_Details_API(UserNameText,userInfoModel.getPaymentPurposeID().toString(),  userInfoModel.getBankId().toString(),userInfoModel.getAccountNumber(),userInfoModel.getIFSCCode(),Name_EditText.getText().toString());

                        }
                        else
                        {
                            Hit_Offline_Details_API(UserNameText,userInfoModel.getPaymentPurposeID().toString(),  userInfoModel.getBankId().toString(),userInfoModel.getBankAccount(),userInfoModel.getBankIfscCode(),Name_EditText.getText().toString());

                        }




                    }
                    else if (UserLogedInSuccessfull==false&& onile_payment_radio_button.isChecked())
                    {

                    }*/

                    changeFragment_ConfirmInfoFragment();

                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryID", ReceiptEntryId);
                params.put("DepositorName", Name_EditText.getText().toString());
                params.put("Address1", Address_Line1_EditText.getText().toString());
                params.put("Address2", Address_Line2_EditText.getText().toString());
                params.put("City", City_EditText.getText().toString());
                params.put("Pincode", Pincode_Zipcode_EditText.getText().toString());
                params.put("Email", Email_EditText.getText().toString());
                params.put("MobileNo", MobileNumber);
                params.put("StateId", String.valueOf(selectedStateID));
                params.put("DistrictId", String.valueOf(selectedDistrictID));
                params.put("CountryId", String.valueOf(selectedCountryID));
                params.put("CountryCode", "91");
                params.put("SalutationId", String.valueOf(selectedSalutationId));
                params.put("PaymentMode",  onile_payment_radio_button.isChecked() ? "O" : "SWIFT/NEFT/RTGS");
                params.put("Flag", "0");
                params.put("Tin", tin_edittext.getText().toString());
                params.put("Tan", tan_edittext.getText().toString());
                params.put("RefreshTokenValue", MyToken.getToken());
                if (MainActivity.IsLogin)
                {
                    params.put("UserId",  MainActivity.UserID );
                    params.put("UserName", MainActivity.UserName);
                }
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }
    private void HitSalutationAPI() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getSalutationListURL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                SalutaionModel salutaionModel = gson.fromJson(response, SalutaionModel.class);

                MyToken.setToken(salutaionModel.getRefreshTokenValue());
                MyToken.setAuthorization(salutaionModel.getAcsessTokenValue());

                if (salutaionModel.getErrorCodes()!=null&&salutaionModel.getErrorCodes().matches("Success")&&salutaionModel.getSalutionList()!=null&&salutaionModel.getSalutionList().size()>0)
                {


                    salutionArrayList.clear();

                    salutionArrayList= (ArrayList<Salution>) salutaionModel.getSalutionList();

                    String[] salutationList=new String[salutaionModel.getSalutionList().size()];
                    for(int i=0;i<salutaionModel.getSalutionList().size();i++)
                    {
                        salutationList[i]=salutaionModel.getSalutionList().get(i).getSalutationName();
                    }




                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, salutationList);
                    Spinner_Marital_Status.setAdapter(adapter);
                    Spinner_Marital_Status.setSelection(0);
                    Hit_CountryList_API();
                }
                else
                {
                    showDialog_ServerError("Server Error !");
                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showDialog_ServerError("Server Error !");
                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("DepositorCategoryId", String.valueOf(DepositorCategoryId ));
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("parentControlId2","0");


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //  strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }
    private void putRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }
    private void set_validation_on_Marital_Status_Dropdown()
    {
        Spinner_Marital_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String interesting = Spinner_Marital_Status.getItemAtPosition(position).toString();
                if(interesting.equals("-Select-"))
                {
                    Name_EditText.setEnabled(false);
                }
                else
                {
                    Name_EditText.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void set_Validation_Name()
    {
        InputFilter filter= new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String checkMe = String.valueOf(source.charAt(i));

                    Pattern pattern = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz.0123456789]");
                    Matcher matcher = pattern.matcher(checkMe);
                    boolean valid = matcher.matches();
                    if(!valid){
                        Log.d("", "invalid");
                        Name_EditText.setError("Invalid Entry");
                        return "";
                    }
                }
                return null;
            }
        };

        Name_EditText.setFilters(new InputFilter[]{filter});
    }
    private void set_Validation_Address()
    {
        InputFilter filter= new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String checkMe = String.valueOf(source.charAt(i));

                    Pattern pattern = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz0123456789'.,-/]");
                    Matcher matcher = pattern.matcher(checkMe);
                    boolean valid = matcher.matches();
                    if(!valid){
                        Log.d("", "invalid");
                        Address_Line1_EditText.setError("Invalid Entry");
                        return "";
                    }
                }
                return null;
            }
        };

        Address_Line1_EditText.setFilters(new InputFilter[]{filter});
    }
    private void set_Validation_Address2()
    {
        InputFilter filter= new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String checkMe = String.valueOf(source.charAt(i));

                    Pattern pattern = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz0123456789'.,-/]");
                    Matcher matcher = pattern.matcher(checkMe);
                    boolean valid = matcher.matches();
                    if(!valid){
                        Log.d("", "invalid");
                        Address_Line2_EditText.setError("Invalid Entry");
                        return "";
                    }
                }
                return null;
            }
        };

        Address_Line2_EditText.setFilters(new InputFilter[]{filter});
    }
    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    /*private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", text)) {
            if(phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
                txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }*/

    private void initialize_Spinner_Country()
    {

        Spinner_Country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

           //     String item = Spinner_Country.getItemAtPosition(position).toString();
                selectedCountryID = countryArrayList.get(position).getCountryId();
                Hit_StateList_API();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }

        });

    }


    private void initialize_Spinner_State()
    {

        Spinner_State.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // if(++State_check > 1) {

                    SelectedStateName = Spinner_State.getItemAtPosition(position).toString();
                    selectedStateID=stateArrayList.get(position).getStateId();
                   // selectedStateID = stateArrayList.get(Spinner_State.getSelectedItemPosition()).getStateId();

                    Hit_DistrictList_API();
               // }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void initialize_Spinner_District()
    {

        Spinner_District.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(++District_check > 1) {
                    SelectedDistrictName = Spinner_District.getItemAtPosition(position).toString();
                    selectedDistrictID=districtArrayList.get(position).getDistrictId();
                   // selectedDistrictID = dUserInfoModelistrictArrayList.get(Spinner_District.getSelectedItemPosition()).getDistrictId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @OnClick({R.id.swift_payment_radio_button, R.id.onile_payment_radio_button})
    public void onRadioButtonClicked(RadioButton radioButton) {

        boolean checked = radioButton.isChecked();


        switch (radioButton.getId()) {
            case R.id.swift_payment_radio_button:
                if (checked) {
                    isOnlinePaymentSelected=false;

                    OfflineWithLogin=false;
                    OfflineWithAccountValidation=false;

                    Hit_CheckIfNEFTAvailable_API();

                    nextButton.setVisibility(View.GONE);
                }
                break;
            case R.id.onile_payment_radio_button:
                if (checked) {
                    isOnlinePaymentSelected=true;
                    OfflineWithLogin=false;
                    OfflineWithAccountValidation=false;
                    nextButton.setVisibility(View.VISIBLE);
                }
                break;
        }
    }


    private void showDialog_LoginPreview() {
        UserLogedInSuccessfull=false;
        LoginDialog = new Dialog(getActivity());
        LoginDialog.setContentView(R.layout.dialog_nonregisteredusers_validateaccount);
        LoginDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LoginDialog.show();

        EditText user_name_edittext=(EditText) LoginDialog.findViewById(R.id.user_name_edittext);
        EditText user_password_edittext=(EditText) LoginDialog.findViewById(R.id.user_password_edittext);

        EditText bank_account_no_edittext=(EditText) LoginDialog.findViewById(R.id.bank_account_no_edittext);
        EditText bank_ifsc_code_edittext=(EditText) LoginDialog.findViewById(R.id.bank_ifsc_code_edittext);

        wrong_bank_details_textview=(TextView) LoginDialog.findViewById(R.id.wrong_bank_details_textview);




        spinner_bankname=(Spinner) LoginDialog.findViewById(R.id.spinner_bankname);
        spinner_bankname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedBankId=bankListModel.BankList.get(position).BankId;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        wrong_credntial_textview =(TextView) LoginDialog.findViewById(R.id.wrong_credntial_textview);


        Button CrossDialogButton= (Button) LoginDialog.findViewById(R.id.cross_button);
        CrossDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OfflineWithLogin=false;
                OfflineWithAccountValidation=false;
                UserLogedInSuccessfull=false;
                LoginDialog.dismiss();
            }
        });



        Button submit_button= (Button) LoginDialog.findViewById(R.id.submit_button);
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (user_name_edittext.getText().toString().matches(""))
                {
                    user_name_edittext.setError("This field can not be blank");
                }
                if (user_password_edittext.getText().toString().matches(""))
                {
                    user_password_edittext.setError("This field can not be blank");
                }
                if (user_name_edittext.getText().toString().matches("")==false&&user_password_edittext.getText().toString().matches("")==false)
                {
                    wrong_credntial_textview.setVisibility(View.GONE);
                    wrong_bank_details_textview.setVisibility(View.GONE);
                    Hit_Submit_User_Login_Details_API(user_name_edittext.getText().toString(),user_password_edittext.getText().toString());

                }
                return;
            }
        });


        Button submit_validate_button= (Button) LoginDialog.findViewById(R.id.submit_validate_button);
        submit_validate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bank_account_no_edittext.getText().toString().matches(""))
                {
                    bank_account_no_edittext.setError("This field can not be blank");
                }
                if (bank_ifsc_code_edittext.getText().toString().matches(""))
                {
                    bank_ifsc_code_edittext.setError("This field can not be blank");
                }
                if (selectedBankId==null)
                {

                    ((TextView)spinner_bankname.getSelectedView()).setError("Select Bank");
                    Toast.makeText(getContext(),"Select Bank",Toast.LENGTH_LONG).show();
                }

                if (selectedBankId!=null&&bank_account_no_edittext.getText().toString().matches("")==false&&bank_ifsc_code_edittext.getText().toString().matches("")==false)
                {
                    wrong_credntial_textview.setVisibility(View.GONE);
                    wrong_bank_details_textview.setVisibility(View.GONE);
                    Hit_Account_Validation_API(bank_account_no_edittext.getText().toString(), bank_ifsc_code_edittext.getText().toString());

                }
            }
        });


        Hit_API_BankList();

    }

    private void Hit_Submit_User_Login_Details_API(String UserName, String UserPassword) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_User_Submit_User_Details_URL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                userInfoModel = gson.fromJson(response, UserInfoModel.class);

                MyToken.setToken(userInfoModel.getRefreshTokenValue());
                MyToken.setAuthorization(userInfoModel.getAcsessTokenValue());


                if (userInfoModel.getResponseStatus()!=null&&userInfoModel.getAcsessTokenValue()!=null&&userInfoModel.getRefreshTokenValue()!=null)
                {
                    UserLogedInSuccessfull=true;
                    OfflineWithLogin=true;
                    OfflineWithAccountValidation=false;
                    LoginDialog.dismiss();
                    UserNameText=UserName;
                    nextButton.setVisibility(View.VISIBLE);
                    wrong_credntial_textview.setVisibility(View.GONE);
                    Prefs.putString("userBankData", new Gson().toJson(userInfoModel));
                    showDialog("Login successfull !");
                    //Hit_Offline_Details_API(UserName,userInfoModel.getPaymentPurposeID().toString(),  userInfoModel.getBankId().toString(),userInfoModel.getBankAccount(),userInfoModel.getBankIfscCode(),userInfoModel.getResponseFirstName()+" "+userInfoModel.getResponseLastName());

                }
                else if (userInfoModel.getResponseStatus().matches("Fail")&&userInfoModel.getErrorCodes()!=null)
                {
                    OfflineWithLogin=false;
                    OfflineWithAccountValidation=false;
                    wrong_credntial_textview.setVisibility(View.VISIBLE);
                    wrong_credntial_textview.setText(""+userInfoModel.getErrorCodes()+" ");

                }
                else
                {
                    OfflineWithLogin=false;
                    OfflineWithAccountValidation=false;
                    wrong_credntial_textview.setVisibility(View.VISIBLE);
                    wrong_credntial_textview.setText("Wrong Credentials !");
                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                OfflineWithLogin=false;
                OfflineWithAccountValidation=false;
                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("LoginUserName", UserName);


                String password_hashed=HashingSha256(UserPassword);

                 String RandomString=getRandomNumberString();

                String password_hashed_hexa=HashingSha256(RandomString+password_hashed.toUpperCase());

                params.put("Password", password_hashed_hexa);

                params.put("newRno", RandomString);

                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }

    private  String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        SecureRandom rnd = new SecureRandom();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }


    private void Hit_Offline_Details_API(String UserName, String paymentPurposeID, String BankId, String BankACC, String BankIFC, String DepositorName) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_OfflinePaymentMode_URL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                OffLinePaymentModel depositorCategoryModel = gson.fromJson(response, OffLinePaymentModel.class);

               // MyToken.setToken(depositorCategoryModel.getRefreshTokenValue());
               // MyToken.setAuthorization(depositorCategoryModel.getAcsessTokenValue());


                if (depositorCategoryModel.getChallanNumber()!=null&&depositorCategoryModel.getAmount()!=null)
                {

                    changeFragment_OfflineConfirmInfoFragment(ReceiptEntryId,depositorCategoryModel.getChallanNumber(),depositorCategoryModel.getAmount());

                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("LoginUserName", MainActivity.IsLogin? MainActivity.UserName : UserName);


                params.put("paymentPurposeID", paymentPurposeID);

                params.put("LoggedInUserName",   MainActivity.IsLogin? MainActivity.UserName : UserName);

                params.put("ReceiptEntryId", ReceiptEntryId);

                params.put("BankId", BankId);

                params.put("BankACC", BankACC);

                params.put("BankIFC", BankIFC);

                params.put("Salutation", "Mr");

                params.put("DepositorName", DepositorName);

                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }



    private void Hit_CountryList_API() {

        System.out.println("Hit_CountryList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getCountryListURL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                CountryModel countryModel = gson.fromJson(response, CountryModel.class);
                countryArrayList.clear();

                countryArrayList= (ArrayList<Country>) countryModel.getCountryList();
                String[] countryItems=new String[countryArrayList.size()];
                for(int i=0;i<countryArrayList.size();i++)
                {
                    countryItems[i]=countryArrayList.get(i).getCountryName().toString();
                }




                ArrayAdapter<String> adapter = new ArrayAdapter<String>(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, countryItems);
                Spinner_Country.setAdapter(adapter);
                Spinner_Country.setSelection(99);

                if (MainActivity.IsLogin) {

                    LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);


                    for(int i=0;i<countryModel.getCountryList().size();i++)
                    {
                        if (userInfoModel.getCountryId().equals(countryModel.getCountryList().get(i).getCountryId()))
                        {
                            Spinner_Country.setSelection(i);
                        }
                    }


                   }
                }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private void Hit_StateList_API() {

        System.out.println("Hit_StateList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getStateListURL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                StateModel stateModel = gson.fromJson(response, StateModel.class);
                stateArrayList.clear();

                stateArrayList= stateModel.getStateList();
                String[] stateItems=new String[stateArrayList.size()+1];
                for(int i=0;i<stateArrayList.size();i++)
                {
                    stateItems[i]=stateArrayList.get(i).getStateName().toString();
                }
               // stateItems[stateArrayList.size()]="Select";

                //  ArrayAdapter<String> adapter = new ArrayAdapter<String>(DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, stateItems);

                NonRegisteredUsers_DepositorDetailsFragment.HintAdapter<String> adapter = new NonRegisteredUsers_DepositorDetailsFragment.HintAdapter<String>(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, stateItems);



                Spinner_State.setAdapter(adapter);

                if (MainActivity.IsLogin) {

                    LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);


                    for(int i=0;i<stateModel.getStateList().size();i++)
                    {
                        if (userInfoModel.getStateId().equals(stateModel.getStateList().get(i).getStateId()))
                        {
                            Spinner_State.setSelection(i);
                        }
                    }


                }

               // Spinner_State.setSelection(adapter.getCount());

              //  initialize_Spinner_State();

                //  selectedStateID=stateArrayList.get(Spinner_State.getSelectedItemPosition()).getStateId();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ParentControlId", ""+selectedCountryID);

                //System.out.println(LoginScreen.selectedBankID + "==" + MyToken.getToken() + "==" + LoginScreen.SelectedSchemeId+"=="+LoginScreen.selectedSchemeName);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_DistrictList_API() {

        System.out.println("Hit_DistrictList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDistrictListURL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                DistrictModel districtModel = gson.fromJson(response, DistrictModel.class);
                districtArrayList.clear();

                districtArrayList= districtModel.getDistrictList();
                String[] districtItems=new String[districtArrayList.size()+1];
                for(int i=0;i<districtArrayList.size();i++)
                {
                    districtItems[i]=districtArrayList.get(i).getDistrictName().toString();
                }

              //  districtItems[districtArrayList.size()]="Select";

                NonRegisteredUsers_DepositorDetailsFragment.HintAdapter<String> adapter = new NonRegisteredUsers_DepositorDetailsFragment.HintAdapter<String>(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, districtItems);
                Spinner_District.setAdapter(adapter);

                Spinner_District.setSelection(0);

                if (MainActivity.IsLogin) {

                    LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);

                    for(int i=0;i<districtModel.getDistrictList().size();i++)
                    {
                        if (userInfoModel.getDistrictId().equals(districtModel.getDistrictList().get(i).getDistrictId()))
                        {
                            Spinner_District.setSelection(i);
                        }
                    }


                }


                SelectedDistrictName = Spinner_District.getItemAtPosition(Spinner_District.getSelectedItemPosition()).toString();


                for(int i=0;i<districtModel.getDistrictList().size();i++)
                {
                    if (SelectedDistrictName.equals(districtModel.getDistrictList().get(i).getDistrictName()))
                    {
                        selectedDistrictID= districtArrayList.get(i).getDistrictId();
                    }
                }


               // Spinner_District.setSelection(adapter.getCount());

                // selectedDistrictID=districtArrayList.get(Spinner_District.getSelectedItemPosition()).getDistrictId();




                /*System.out.println("***** "+Spinner_District.getSelectedItem().toString());
                System.out.println(Spinner_State.getSelectedItem().toString());
                System.out.println(Spinner_Country.getSelectedItem().toString());*/


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ParentControlId", ""+selectedStateID);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private void Hit_CheckIfNEFTAvailable_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_CheckPaymentMode_URL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                DisplayPopUpForPaymentModeModel depositorCategoryModel = gson.fromJson(response, DisplayPopUpForPaymentModeModel.class);


                if (depositorCategoryModel.getAcsessTokenValue()!=null&&depositorCategoryModel.getRefreshTokenValue()!=null)
                {
                    MyToken.setToken(depositorCategoryModel.getRefreshTokenValue());
                    MyToken.setAuthorization(depositorCategoryModel.getAcsessTokenValue());

                    if (depositorCategoryModel.getDisplayPopup()!=null&&depositorCategoryModel.getDisplayPopup().matches("true"))
                    {
                        showDialog_LoginPreview();
                    }
                    else if (depositorCategoryModel.getErrorMessage()!=null&&depositorCategoryModel.getErrorMessage().matches("true")==false)
                    {
                        Toast.makeText(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(),"Error: "+depositorCategoryModel.getErrorMessage(),Toast.LENGTH_LONG);
                    }
                    else if (depositorCategoryModel.getErrorMessage()!=null&&depositorCategoryModel.getErrorMessage().matches("true"))
                    {
                        UserLogedInSuccessfull=true;
                        OfflineWithLogin=true;
                        nextButton.setVisibility(View.VISIBLE);
                        //Toast.makeText(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(),"Error: "+depositorCategoryModel.getErrorMessage(),Toast.LENGTH_LONG);
                    }


                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("LoggedInUserName",  MainActivity.IsLogin ? MainActivity.UserName : "");
                params.put("ReceiptEntryId", ReceiptEntryId);
                params.put("UniqueUserDeviceId", UniqueUserDeviceId);

                params.put("RefreshTokenValue", MyToken.getToken());



                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }
    private void Hit_API_BankList() {


        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getBankListURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                bankListModel=gson.fromJson(response,BankListModel.class);

                MyToken.setToken(bankListModel.RefreshTokenValue);
                MyToken.setAuthorization(bankListModel.AcsessTokenValue);

                ArrayList<String> bank_names=new ArrayList<>();

                for(int i=0;i<bankListModel.BankList.size();i++)
                {
                    bank_names.add(bankListModel.BankList.get(i).BankName);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, bank_names);

                spinner_bankname.setAdapter(adapter);



            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
               // showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_Account_Validation_API(String bankAccountNumber, String bankIfscCode)
    {
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_getBankVlidation_URL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                bankAccountValidationModel=gson.fromJson(response,BankAccountValidationModel.class);

                MyToken.setToken(bankAccountValidationModel.getRefreshTokenValue());
                MyToken.setAuthorization(bankAccountValidationModel.getAcsessTokenValue());


                if (bankAccountValidationModel.getIsValidNeft()!=null&&bankAccountValidationModel.getIsValidNeft().matches("True"))
                {
                    UserLogedInSuccessfull=true;
                    OfflineWithLogin=false;
                    OfflineWithAccountValidation=true;
                    LoginDialog.dismiss();
                    nextButton.setVisibility(View.VISIBLE);
                    wrong_credntial_textview.setVisibility(View.GONE);
                    wrong_bank_details_textview.setVisibility(View.GONE);
                    Prefs.putString("userBankValidationData", new Gson().toJson(bankAccountValidationModel));
                   // Toast.makeText(NonRegisteredUsers_DepositorDetailsFragment.this.getActivity(),"Account validated successfully",Toast.LENGTH_LONG);
                    showDialog("Account validation successfull !");
                }
                else if (bankAccountValidationModel.getIsValidNeft()==null&&bankAccountValidationModel.getErrorCodes()!=null)
                {
                    OfflineWithLogin=false;
                    OfflineWithAccountValidation=false;
                    wrong_credntial_textview.setVisibility(View.GONE);
                    wrong_bank_details_textview.setVisibility(View.VISIBLE);
                    wrong_bank_details_textview.setText(""+bankAccountValidationModel.getErrorCodes()+" ");
                }
                else
                {
                    OfflineWithLogin=false;
                    OfflineWithAccountValidation=false;
                    wrong_credntial_textview.setVisibility(View.GONE);
                    wrong_bank_details_textview.setVisibility(View.VISIBLE);
                    wrong_bank_details_textview.setText("Wrong Credentials !");
                }




            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                OfflineWithLogin=false;
                OfflineWithAccountValidation=false;
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                // showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());

                params.put("BankId",selectedBankId);
                params.put("BankAccount", bankAccountNumber);


                params.put("BankIfscCode", bankIfscCode);
                params.put("loginUserId", MainActivity.IsLogin ? MainActivity.UserID : "0");
                params.put("OfflineUserId", "0");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    public class HintAdapter<String>
            extends ArrayAdapter<String> {


        public HintAdapter(@NonNull Context context, int resource, @NonNull String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public int getCount() {
            // don't display last item. It is used as hint.
            int count = super.getCount();
            return count > 0 ? count - 1 : count;
        }
    }

    private String HashingSha256(String Input) {
        final HashFunction hashFunction = Hashing.sha256();
        final HashCode hc = hashFunction
                .newHasher()
                .putString(Input, Charsets.UTF_8)
                .hash();
        final String sha256 = hc.toString();
       return sha256;
    }
    private void showDialog(String warningText) {

        final Dialog dialog = new Dialog(this.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_success);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(isRemoving()){
            EventBus.getDefault().post(new BackPressEvent());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(isRemoving()){
            EventBus.getDefault().post(new BackPressEvent());
        }
    }
    public void showDialog_ServerError(String errorText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // getActivity().finish();
                dialog.dismiss();
                //System.exit(0);
            }
        });
    }

}
