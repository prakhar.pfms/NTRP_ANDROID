package testme.myapp.com.ntrp.module_forgotpassword.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.MyCustomToast;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.extra.RepeatSafeToast;
import testme.myapp.com.ntrp.extra.UserModel;
import testme.myapp.com.ntrp.fragments.NewHomeFragmentTwo;
import testme.myapp.com.ntrp.module_forgotpassword.model.ChangePasswordModel;
import testme.myapp.com.ntrp.module_forgotpassword.model.CheckUserDetailsModel;
import testme.myapp.com.ntrp.module_registeration.models.SendOTPModel_Registration;
import testme.myapp.com.ntrp.module_registeration.models.VerifyModel;
import testme.myapp.com.ntrp.module_trackyourpayment.mycaptcha.TextCaptcha;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.utils.AllUrls;

/**
 * Created by deepika.s on 18-05-2017.
 */

public class ForgotPasswordFragment extends Fragment {

    private View localView;
    private TextCaptcha textCaptcha;
    private ImageView captcha_ImageView;
    private Button Captcha_Retry_Button,Continue_Button,Reset_Button;
    private TextView User_Name_Textview,Email_Id_Textview,Enter_Security_Code_Textview,Send_OTP_On_Textview,Enter_OTP_Code_Textview;
    private View User_Name_Linear_Layout,OR_Linear_Layout,Email_Id_Linear_Layout,Captcha_Linear_Layout,Enter_Security_Code_linear_Layout;
    private View Reset_And_Submit_Buttons_Layout;
    private View Recover_Radio_Buttons_Linear_Layout,Recover_UserName_Linear_Layout,Recover_Password_Linear_Layout,Send_OTP_On_Linear_Layout;
    private View OTP_Hint_Linear_Layout,Enter_OTP_Code_Linear_Layout;
    private EditText User_Name_EditText,Email_Id_EditText,Security_Code_EditText,Enter_OTP_Code_EditText;
    private RadioGroup recover_radio_buttons_radiogroup;
    private int indexRadioBtn;
    private CheckBox Email_CheckBox,SMS_CheckBox,Recover_Through_OTP_UserName_CheckBox,Recover_Through_OTP_Password_CheckBox;
    private boolean set_Continue_Text_Flag,set_SendOTP_Text_Flag,set_Verify_Text_Flag,set_SendVerifyLast_Text_Flag;
    private boolean FirstPage;
    private String usernameInputString, emailIDInputString, securityCodeInputString;
    private boolean usernameSelected,passwordSelected;
    private RadioButton recoverUsernameRadioButton, recoverPasswordRadioButton;

    private CheckUserDetailsModel checkUserDetailsModel;

    private SendOTPModel_Registration sendOTPModel_registration;

    @BindView(R.id.change_passowrd_parent_layout)
    public RelativeLayout change_passowrd_parent_layout;

    @BindView(R.id.new_password_edittext)
    public EditText new_password_edittext;

    @BindView(R.id.confirm_new_password_edittext)
    public EditText confirm_new_password_edittext;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        localView=view;
        ButterKnife.bind(this,view);
        FirstPage=true;
        usernameSelected=false;
        passwordSelected=false;
        getActivity().setTitle("Forget Password");
        initializeView();
        generate_Captcha();
        reload_Captcha_Button();
        showView_Start();
        hideView_Start();
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle("Forget Password");
    }
    private void initializeView()
    {
        initialize_TextViews();
        initialize_Layouts();
        initialize_EditTextViews();
        initialize_CheckBoxViews();
        handle_ContinueButton();
        handle_ResetButton();
        handleRadioGroup();
        handleCheckBox();
        change_passowrd_parent_layout.setVisibility(View.GONE);
    }
    private void initialize_TextViews()
    {
        User_Name_Textview= (TextView) localView.findViewById(R.id.user_name_textview);
        Email_Id_Textview= (TextView) localView.findViewById(R.id.email_id_textview);
        Enter_Security_Code_Textview= (TextView) localView.findViewById(R.id.enter_security_code_textview);
        Send_OTP_On_Textview= (TextView) localView.findViewById(R.id.send_OTP_on_textview);
        Enter_OTP_Code_Textview= (TextView) localView.findViewById(R.id.enter_otp_code_textview);
        putRedAsterisk(localView,User_Name_Textview,R.id.user_name_textview);
        putRedAsterisk(localView,Email_Id_Textview,R.id.email_id_textview);
        putRedAsterisk(localView,Enter_Security_Code_Textview,R.id.enter_security_code_textview);
        putRedAsterisk(localView,Send_OTP_On_Textview,R.id.send_OTP_on_textview);
        putRedAsterisk(localView,Enter_OTP_Code_Textview,R.id.enter_otp_code_textview);
    }
    private void initialize_Layouts()
    {
        User_Name_Linear_Layout=localView.findViewById(R.id.user_name_linear_layout);
        OR_Linear_Layout=localView.findViewById(R.id.or_linear_layout);
        Email_Id_Linear_Layout=localView.findViewById(R.id.email_id_linear_layout);
        Captcha_Linear_Layout=localView.findViewById(R.id.captcha_linear_layout);
        Enter_Security_Code_linear_Layout=localView.findViewById(R.id.enter_security_code_linear_layout);
        Reset_And_Submit_Buttons_Layout=localView.findViewById(R.id.reset_and_submit_buttons_layout);
        Recover_Radio_Buttons_Linear_Layout=localView.findViewById(R.id.recover_radio_buttons_radiogroup);
        Recover_UserName_Linear_Layout=localView.findViewById(R.id.recover_username_linear_layout);
        Recover_Password_Linear_Layout=localView.findViewById(R.id.recover_password_linear_layout);
        Send_OTP_On_Linear_Layout=localView.findViewById(R.id.send_OTP_on_linear_layout);
        OTP_Hint_Linear_Layout=localView.findViewById(R.id.otp_hint_linear_layout);
        Enter_OTP_Code_Linear_Layout=localView.findViewById(R.id.enter_otp_code_linear_layout);
    }
    private void initialize_EditTextViews() {
        User_Name_EditText= (EditText) localView.findViewById(R.id.user_name_edittext);
        Email_Id_EditText= (EditText) localView.findViewById(R.id.email_id_edittext);
        Security_Code_EditText= (EditText) localView.findViewById(R.id.security_code_edittext);
        Enter_OTP_Code_EditText= (EditText) localView.findViewById(R.id.enter_otp_code_edittext);

    }
    private void initialize_CheckBoxViews() {
        Email_CheckBox= (CheckBox) localView.findViewById(R.id.email_checkbox);
        SMS_CheckBox= (CheckBox) localView.findViewById(R.id.sms_checkbox);
        Recover_Through_OTP_UserName_CheckBox= (CheckBox) localView.findViewById(R.id.recover_username_through_otp_checkbox);
        Recover_Through_OTP_Password_CheckBox= (CheckBox) localView.findViewById(R.id.recover_password_through_otp_checkbox);
    }
    private void handle_ContinueButton() {
        Continue_Button = (Button) localView.findViewById(R.id.continue_button);
        Continue_Button.setText("Continue");
        set_Continue_Text_Flag=true;
        set_SendOTP_Text_Flag=false;
        set_Verify_Text_Flag=false;
        set_SendVerifyLast_Text_Flag=false;

        Continue_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (set_Continue_Text_Flag == true && set_SendOTP_Text_Flag == false && set_Verify_Text_Flag == false && set_SendVerifyLast_Text_Flag == false) {
                    System.out.println("here........111111111");
                    check_validation();
                    return;
                } else if (set_Continue_Text_Flag == false && set_SendOTP_Text_Flag == true && set_Verify_Text_Flag == false && set_SendVerifyLast_Text_Flag == false) {
                    if (Email_CheckBox.isChecked() == false && SMS_CheckBox.isChecked() == false) {
                        showDialog_Error("Please select options for send otp");
                        return;
                    } else if (Email_CheckBox.isChecked() == true || SMS_CheckBox.isChecked() == true) {
                        //cal API


                     Hit_API_SENDOTP();



                    }
                } else if (set_Continue_Text_Flag == false && set_SendOTP_Text_Flag == false && set_Verify_Text_Flag == true && set_SendVerifyLast_Text_Flag == false) {

                    if (Enter_OTP_Code_EditText.getText().toString().matches("") == false) {
                        if (usernameSelected == true && passwordSelected == false) {
                           // Continue_Button.setText("Send UserName");
                        } else if (usernameSelected == false && passwordSelected == true) {
                           // Continue_Button.setText("Send Password");
                        }
                        Hit_API_VeriFyRefisterUserOTP();
                    } else {
                        showDialog_Error("Please Enter OTP");
                        return;
                    }
                } else if (set_Continue_Text_Flag == false && set_SendOTP_Text_Flag == false && set_Verify_Text_Flag == false && set_SendVerifyLast_Text_Flag == true) {


                }
            }
        });
    }
    private void handle_ResetButton()
    {
        Reset_Button= (Button) localView.findViewById(R.id.reset_button);
        Reset_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(FirstPage==false)
                {
                    changeFragment_ForgotPasswordFragment();
                    return;
                }
                else if(FirstPage)
                {
                    User_Name_EditText.setText("");
                    Email_Id_EditText.setText("");
                    Security_Code_EditText.setText("");
                    generate_Captcha();
                    return;
                }

            }
        });
    }






    private void check_validation()
    {
        usernameInputString =User_Name_EditText.getText().toString().trim();
        emailIDInputString =Email_Id_EditText.getText().toString().trim();
        securityCodeInputString =Security_Code_EditText.getText().toString();


         if(securityCodeInputString.matches("")==false) {

            if (usernameInputString.matches("") && emailIDInputString.matches("")) {
                showDialog_Error("Please enter at least one Login Id or Email Id to recover your password.");
                return;
            } else {
                if (securityCodeInputString.matches(textCaptcha.input_captcha_string)) {

                    if (emailIDInputString.matches("") == false && !isValidEmail(emailIDInputString)) {
                        showDialog_Error("Invalid Email Id");
                        return;
                    } else {
                        switchPage();
                    }

                } else if (!securityCodeInputString.matches(textCaptcha.input_captcha_string)) {
                    generate_Captcha();
                    showDialog_Error("The text you entered does not matches with the word verification. Please try again.");
                }
            }

        }
         else if(securityCodeInputString.matches(""))
        {
            Security_Code_EditText.setError("Please Enter Captcha");
            return;
        }
    }
    private void switchPage() {
        if (set_Continue_Text_Flag) {
            Hit_API_CheckUserIfExits();

            return;
        }
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void handleCheckBox()
    {
        Recover_Through_OTP_UserName_CheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    Send_OTP_On_Linear_Layout.setVisibility(View.VISIBLE);
                    Send_OTP_On_Linear_Layout.setEnabled(true);
                    Continue_Button.setVisibility(View.VISIBLE);
                    Continue_Button.setEnabled(true);
                    Continue_Button.setText("Send OTP");
                    set_Continue_Text_Flag=false;
                    set_SendOTP_Text_Flag=true;
                    set_Verify_Text_Flag=false;
                    set_SendVerifyLast_Text_Flag=false;
                }
                else
                {
                    Send_OTP_On_Linear_Layout.setVisibility(View.GONE);
                    Continue_Button.setVisibility(View.GONE);
                    Continue_Button.setText("Continue");
                    set_Continue_Text_Flag=true;
                    set_SendOTP_Text_Flag=false;
                    set_Verify_Text_Flag=false;
                    set_SendVerifyLast_Text_Flag=false;

                }
            }
        });

        Recover_Through_OTP_Password_CheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    Send_OTP_On_Linear_Layout.setVisibility(View.VISIBLE);
                    Send_OTP_On_Linear_Layout.setEnabled(true);
                    Continue_Button.setVisibility(View.VISIBLE);
                    Continue_Button.setEnabled(true);
                    Continue_Button.setText("Send OTP");
                    set_Continue_Text_Flag=false;
                    set_SendOTP_Text_Flag=true;
                    set_Verify_Text_Flag=false;
                    set_SendVerifyLast_Text_Flag=false;

                }
                else
                {
                    Send_OTP_On_Linear_Layout.setVisibility(View.GONE);
                    Continue_Button.setVisibility(View.GONE);
                    Continue_Button.setText("Continue");
                    set_Continue_Text_Flag=true;
                    set_SendOTP_Text_Flag=false;
                    set_Verify_Text_Flag=false;
                    set_SendVerifyLast_Text_Flag=false;
                }
            }
        });


    }
    private void handleRadioGroup() {
        recover_radio_buttons_radiogroup = (RadioGroup) localView.findViewById(R.id.recover_radio_buttons_radiogroup);
        recoverUsernameRadioButton = (RadioButton) recover_radio_buttons_radiogroup.findViewById(R.id.recover_username_radiobutton);
        recoverPasswordRadioButton = (RadioButton) recover_radio_buttons_radiogroup.findViewById(R.id.recover_password_radiobutton);


            recover_radio_buttons_radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    View radioButton = recover_radio_buttons_radiogroup.findViewById(checkedId);
                    indexRadioBtn = recover_radio_buttons_radiogroup.indexOfChild(radioButton);

                    System.out.println(indexRadioBtn);
                    switch (indexRadioBtn) {
                        case 0:
                            show_Username();
                            hide_Username();
                            break;
                        case 1:
                            show_Password();
                            hide_Password();
                            break;
                    }
                }
            });

    }
    private void generate_Captcha() {
        textCaptcha = new TextCaptcha(1500, 400, 6, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
        captcha_ImageView = (ImageView) localView.findViewById(R.id.captcha_imageview);
        captcha_ImageView.setImageBitmap(textCaptcha.getImage());
        Security_Code_EditText.setText("");
    }

    private void reload_Captcha_Button()
    {
        Captcha_Retry_Button = (Button) localView.findViewById(R.id.captcha_retry_button);
        Captcha_Retry_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                generate_Captcha();
                return;
            }
        });
    }
    private void putRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = " *";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }
    private void showView_Start()
    {
        User_Name_Linear_Layout.setVisibility(View.VISIBLE);
        User_Name_Linear_Layout.setEnabled(true);
        OR_Linear_Layout.setVisibility(View.VISIBLE);
        OR_Linear_Layout.setEnabled(true);
        Email_Id_Linear_Layout.setVisibility(View.VISIBLE);
        Email_Id_Linear_Layout.setEnabled(true);
        Captcha_Linear_Layout.setVisibility(View.VISIBLE);
        Captcha_Linear_Layout.setEnabled(true);
        Enter_Security_Code_linear_Layout.setVisibility(View.VISIBLE);
        Enter_Security_Code_linear_Layout.setEnabled(true);
        Reset_And_Submit_Buttons_Layout.setVisibility(View.VISIBLE);
        Reset_And_Submit_Buttons_Layout.setEnabled(true);
    }
    private void hideView_Start()
    {
        Recover_Radio_Buttons_Linear_Layout.setVisibility(View.GONE);
        Recover_UserName_Linear_Layout.setVisibility(View.GONE);
        Recover_Password_Linear_Layout.setVisibility(View.GONE);
        Send_OTP_On_Linear_Layout.setVisibility(View.GONE);
        OTP_Hint_Linear_Layout.setVisibility(View.GONE);
        Enter_OTP_Code_Linear_Layout.setVisibility(View.GONE);
    }
    private void showView_Continue()
    {
        FirstPage=false;
        User_Name_Linear_Layout.setVisibility(View.VISIBLE);
        User_Name_Linear_Layout.setEnabled(true);
        User_Name_EditText.setEnabled(false);
        User_Name_EditText.setBackgroundResource(R.drawable.unfocusededittext);
        OR_Linear_Layout.setVisibility(View.VISIBLE);
        OR_Linear_Layout.setEnabled(true);
        Email_Id_Linear_Layout.setVisibility(View.VISIBLE);
        Email_Id_Linear_Layout.setEnabled(true);
        Email_Id_EditText.setEnabled(false);
        Email_Id_EditText.setBackgroundResource(R.drawable.unfocusededittext);
        Recover_Radio_Buttons_Linear_Layout.setVisibility(View.VISIBLE);
        Recover_Radio_Buttons_Linear_Layout.setEnabled(true);
        recover_radio_buttons_radiogroup.setEnabled(true);
        recoverUsernameRadioButton.setEnabled(false);
        recoverPasswordRadioButton.setEnabled(true);

    }
    private void hideView_Continue()
    {
        Captcha_Linear_Layout.setVisibility(View.GONE);
        Enter_Security_Code_linear_Layout.setVisibility(View.GONE);
        Recover_UserName_Linear_Layout.setVisibility(View.GONE);
        Recover_Password_Linear_Layout.setVisibility(View.GONE);
        Send_OTP_On_Linear_Layout.setVisibility(View.GONE);
        Continue_Button.setVisibility(View.GONE);
        OTP_Hint_Linear_Layout.setVisibility(View.GONE);
        Enter_OTP_Code_Linear_Layout.setVisibility(View.GONE);
        Continue_Button.setVisibility(View.GONE);
    }
    private void show_Username()
    {
        Recover_UserName_Linear_Layout.setVisibility(View.VISIBLE);
        Recover_UserName_Linear_Layout.setEnabled(true);
    }
    private void hide_Username()
    {
        Recover_Password_Linear_Layout.setVisibility(View.GONE);
        Send_OTP_On_Linear_Layout.setVisibility(View.GONE);
        Continue_Button.setVisibility(View.GONE);
        Recover_Through_OTP_Password_CheckBox.setChecked(false);
        Email_CheckBox.setChecked(false);
        SMS_CheckBox.setChecked(false);
    }
    private void show_Password()
    {
        Recover_Password_Linear_Layout.setVisibility(View.VISIBLE);
        Recover_Password_Linear_Layout.setEnabled(true);
    }
    private void hide_Password()
    {
        Recover_UserName_Linear_Layout.setVisibility(View.GONE);
        Send_OTP_On_Linear_Layout.setVisibility(View.GONE);
        Continue_Button.setVisibility(View.GONE);
        Recover_Through_OTP_UserName_CheckBox.setChecked(false);
        Email_CheckBox.setChecked(false);
        SMS_CheckBox.setChecked(false);
    }
    private void showDialog_Error(String errorText) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.error_textview);
        error_textview.setText(errorText);

        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    private void changeFragment_ForgotPasswordFragment() {

        Fragment forgotPasswordFragment = new ForgotPasswordFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, forgotPasswordFragment);
        ft.addToBackStack(null);
        ft.commit();
    }
    private void show_SendOTP()
    {
        User_Name_Linear_Layout.setVisibility(View.VISIBLE);
        User_Name_Linear_Layout.setEnabled(true);
        User_Name_EditText.setEnabled(false);
        User_Name_EditText.setBackgroundResource(R.drawable.unfocusededittext);
        OR_Linear_Layout.setVisibility(View.VISIBLE);
        OR_Linear_Layout.setEnabled(true);
        Email_Id_Linear_Layout.setVisibility(View.VISIBLE);
        Email_Id_Linear_Layout.setEnabled(true);
        Email_Id_EditText.setEnabled(false);
        Email_Id_EditText.setBackgroundResource(R.drawable.unfocusededittext);
        Recover_Radio_Buttons_Linear_Layout.setVisibility(View.VISIBLE);
        Recover_Radio_Buttons_Linear_Layout.setEnabled(false);
        recoverUsernameRadioButton.setEnabled(false);
        recoverPasswordRadioButton.setEnabled(false);
        if(usernameSelected&&passwordSelected==false)
        {
            Recover_UserName_Linear_Layout.setVisibility(View.VISIBLE);
            Recover_UserName_Linear_Layout.setEnabled(false);
            Recover_Password_Linear_Layout.setVisibility(View.GONE);
        }
        else if(usernameSelected==false&&passwordSelected==true)
        {
            Recover_Password_Linear_Layout.setVisibility(View.VISIBLE);
            Recover_Password_Linear_Layout.setEnabled(false);
            Recover_UserName_Linear_Layout.setVisibility(View.GONE);
        }
        Send_OTP_On_Linear_Layout.setVisibility(View.VISIBLE);
        Send_OTP_On_Linear_Layout.setEnabled(false);
        Email_CheckBox.setEnabled(false);
        SMS_CheckBox.setEnabled(false);
        OTP_Hint_Linear_Layout.setVisibility(View.VISIBLE);
        OTP_Hint_Linear_Layout.setEnabled(true);
        Enter_OTP_Code_Linear_Layout.setVisibility(View.VISIBLE);
        Enter_OTP_Code_Linear_Layout.setEnabled(true);
        Recover_Through_OTP_UserName_CheckBox.setEnabled(false);
        Recover_Through_OTP_Password_CheckBox.setEnabled(false);
    }
    private void hide_SendOTP()
    {

    }
    private void showView_Verify()
    {
        User_Name_Linear_Layout.setVisibility(View.VISIBLE);
        User_Name_Linear_Layout.setEnabled(true);
        User_Name_EditText.setEnabled(false);
        User_Name_EditText.setBackgroundResource(R.drawable.unfocusededittext);
        OR_Linear_Layout.setVisibility(View.VISIBLE);
        OR_Linear_Layout.setEnabled(true);
        Email_Id_Linear_Layout.setVisibility(View.VISIBLE);
        Email_Id_Linear_Layout.setEnabled(true);
        Email_Id_EditText.setEnabled(false);
        Email_Id_EditText.setBackgroundResource(R.drawable.unfocusededittext);
        Recover_Radio_Buttons_Linear_Layout.setVisibility(View.VISIBLE);
        Recover_Radio_Buttons_Linear_Layout.setEnabled(false);
        recoverUsernameRadioButton.setEnabled(false);
        recoverPasswordRadioButton.setEnabled(false);
        if(usernameSelected&&passwordSelected==false)
        {
            Recover_UserName_Linear_Layout.setVisibility(View.VISIBLE);
            Recover_UserName_Linear_Layout.setEnabled(false);
            Recover_Password_Linear_Layout.setVisibility(View.GONE);
        }
        else if(usernameSelected==false&&passwordSelected==true)
        {
            Recover_Password_Linear_Layout.setVisibility(View.VISIBLE);
            Recover_Password_Linear_Layout.setEnabled(false);
            Recover_UserName_Linear_Layout.setVisibility(View.GONE);
        }
        Send_OTP_On_Linear_Layout.setVisibility(View.VISIBLE);
        Send_OTP_On_Linear_Layout.setEnabled(false);
        Email_CheckBox.setEnabled(false);
        SMS_CheckBox.setEnabled(false);

    }
    private void hideView_Verify()
    {
        OTP_Hint_Linear_Layout.setVisibility(View.GONE);
        Enter_OTP_Code_Linear_Layout.setVisibility(View.GONE);
    }

    private boolean checkNewPasswordValidation()
    {
        if (new_password_edittext.getText().toString().matches(""))
        {
            new_password_edittext.setError("This field can not be blank");
            return false;
        }
        else if(new_password_edittext.getText().toString().length()<8 &&!isValidPassword(new_password_edittext.getText().toString())&&new_password_edittext.getText().toString().length()>15){
            new_password_edittext.setError("Not valid password");
            return false;
        }
        else if (confirm_new_password_edittext.getText().toString().matches(""))
        {
            confirm_new_password_edittext.setError("This field can not be blank");
            return false;
        }
        else if (confirm_new_password_edittext.getText().toString().matches(new_password_edittext.getText().toString())==false)
        {
            confirm_new_password_edittext.setError("This password in not same as above");
            return false;
        }
        else if(confirm_new_password_edittext.getText().toString().length()<8 &&!isValidPassword(confirm_new_password_edittext.getText().toString())&&confirm_new_password_edittext.getText().toString().length()>15){
            confirm_new_password_edittext.setError("Not valid password");
            return false;
        }
        else if (IsHavingSpecialCharector(new_password_edittext.getText().toString()))
        {
            new_password_edittext.setError("Must contain special charactor");
            return false;
        }

        else if (IsHavingSpecialCharector(confirm_new_password_edittext.getText().toString()))
        {
            confirm_new_password_edittext.setError("Must contain special charactor");
            return false;
        }


       return true;
    }


    public void Hit_API_ChangePassword()
    {
        //System.out.println("VeriFyRefisterUserOTP  API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.CHANGE_PASSWORD_URL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                ChangePasswordModel verifyModel=gson.fromJson(response,ChangePasswordModel.class);
                //System.out.println(verifyModel.RefreshTokenValue);
                //System.out.println(verifyModel.ResponseStatus);

                MyToken.setToken(verifyModel.getRefreshTokenValue());
                MyToken.setAuthorization(verifyModel.getAcsessTokenValue());

                if (verifyModel.getResponseStatus()!=null&&verifyModel.getResponseStatus().matches("Success"))
                {
                    showDialog("Password changed Successfully");


                }
                else if (verifyModel.getResponseStatus()!=null&&verifyModel.getResponseStatus().matches("Fail")&&verifyModel.getErrorCodes()!=null)
                {
                    showDialog_Error(verifyModel.getErrorCodes());
                }
                else
                {
                    showDialog_Error("Some error occured !");
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_Error("Server Error, Please Try again later");
                change_passowrd_parent_layout.setVisibility(View.GONE);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                //System.out.println(User_EmailId_Text+" "+User_CountryCode+" "+User_MobileNo+" "+User_OTP_Text);
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put(
                        "newPassword", HashingSha256(new_password_edittext.getText().toString()));

                params.put(
                        "ConfirmPasswrd", HashingSha256(confirm_new_password_edittext.getText().toString()));


                params.put("LoginUserName",  User_Name_EditText.getText().toString() );

                params.put(
                        "Password", HashingSha256(new_password_edittext.getText().toString()));

                params.put("Flag",  "ForgetPassword");


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();     //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

        // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }

    @OnClick(R.id.change_password_button)
    public void click_ChangePassword()
    {

        if (checkNewPasswordValidation())
        {
            Hit_API_ChangePassword();
        }

    }
    public void Hit_API_CheckUserIfExits() {

        //System.out.println("Send Otp API Registration..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.CHECK_USER_EXIST_URL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                checkUserDetailsModel  =gson.fromJson(response,CheckUserDetailsModel.class);




                if(checkUserDetailsModel !=null)
                {
                    MyToken.setToken(checkUserDetailsModel.getRefreshTokenValue());
                    MyToken.setAuthorization(checkUserDetailsModel.getAcsessTokenValue());

                    if(checkUserDetailsModel.getResponseStatus()!=null&&checkUserDetailsModel.getResponseStatus().matches("Success"))
                    {

                        // Spinner_CountryCode.setEnabled(false);
                        User_Name_EditText.setEnabled(false);
                        User_Name_EditText.setBackgroundResource(R.drawable.unfocusededittext);
                        Email_Id_EditText.setEnabled(false);
                        Email_Id_EditText.setBackgroundResource(R.drawable.unfocusededittext);


                        Email_CheckBox.setText(""+checkUserDetailsModel.getMaskedEmail());
                        SMS_CheckBox.setText(""+checkUserDetailsModel.getMaskedMobileNo());
                        // setTimer(OTPExpires_TextView,millisToGo);
                        // IsCountDownStart=true;
                        showView_Continue();
                        hideView_Continue();
                    }
                    else if(checkUserDetailsModel.getErrorCodes()!=null)
                    {
                        showDialog_ShowError(""+checkUserDetailsModel.getErrorCodes());
                        return;
                    }

                    else
                    {
                        showDialog_ShowError("Some error occured ! Try again later.");
                        return;
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ShowError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                //System.out.println(User_MobileNo+" "+ User_EmailId_Text +" "+User_CountryCode);
                params.put("UserName",User_Name_EditText.getText().toString());
                params.put("Email",  Email_Id_EditText.getText().toString());


                Gson gson = new GsonBuilder().setLenient().create();
                UserModel userModel = gson.fromJson(Prefs.getString("userData"), UserModel.class);

                params.put("RefreshTokenValue", userModel.getRefreshToken());
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                Gson gson = new GsonBuilder().setLenient().create();
                UserModel userModel = gson.fromJson(Prefs.getString("userData"), UserModel.class);

                headers.put("Authorization", "Bearer "+userModel.getAccessToken());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        //   strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public void Hit_API_SENDOTP() {

        //System.out.println("Send Otp API Registration..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.sendOTPRegisterUserURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                 sendOTPModel_registration =gson.fromJson(response,SendOTPModel_Registration.class);




                if(sendOTPModel_registration !=null)
                {
                    MyToken.setToken(sendOTPModel_registration.getRefreshTokenValue());
                    MyToken.setAuthorization(sendOTPModel_registration.getAcsessTokenValue());

                    if(sendOTPModel_registration.getResponseStatus()!=null&&sendOTPModel_registration.getResponseStatus().matches("Success"))
                    {

                        if (Recover_Through_OTP_UserName_CheckBox.isChecked()) {
                            usernameSelected = true;
                            passwordSelected = false;
                        } else {
                            usernameSelected = false;
                            passwordSelected = true;
                        }
                        show_SendOTP();

                        Continue_Button.setText("Verify");
                        set_Continue_Text_Flag = false;
                        set_SendOTP_Text_Flag = false;
                        set_Verify_Text_Flag = true;
                        set_SendVerifyLast_Text_Flag = false;
                    }
                    else if(sendOTPModel_registration.getErrorCodes()!=null)
                    {
                        showDialog_ShowError(""+sendOTPModel_registration.getErrorCodes());
                        return;
                    }
                    else  if(sendOTPModel_registration.getResponseStatus()!=null&&sendOTPModel_registration.getResponseStatus().matches("OTP failed"))
                    {
                        showDialog_ShowError("OTP failed! Try again later.");
                        return;
                    }
                    else
                    {
                        showDialog_ShowError("OTP failed! Try again later.");
                        return;
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ShowError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                //System.out.println(User_MobileNo+" "+ User_EmailId_Text +" "+User_CountryCode);
                params.put("MobileNumber",checkUserDetailsModel.getMobileNo());
                params.put("EmailID",  checkUserDetailsModel.getEmail());
                params.put("countryCode", "91" );
                params.put("Flag", "ForgetPassword");


                params.put("RefreshTokenValue", MyToken.getToken());
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
        //   strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public  void Hit_API_VeriFyRefisterUserOTP() {

        //System.out.println("VeriFyRefisterUserOTP  API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.VeriFyRefisterUserOTPURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                VerifyModel verifyModel=gson.fromJson(response,VerifyModel.class);
                //System.out.println(verifyModel.RefreshTokenValue);
                //System.out.println(verifyModel.ResponseStatus);

                MyToken.setToken(verifyModel.RefreshTokenValue);
                MyToken.setAuthorization(verifyModel.AcsessTokenValue);

                if (verifyModel.ResponseStatus.matches("Verified Success"))
                {
                    if (CheckNetworkConnection.getInstance(getActivity()).isOnline())
                    {
                      //  Hit_API_DepositorList();
                        showView_Verify();
                        hideView_Verify();

                        set_Continue_Text_Flag = false;
                        set_SendOTP_Text_Flag = false;
                        set_Verify_Text_Flag = false;
                        set_SendVerifyLast_Text_Flag = true;
                        change_passowrd_parent_layout.setVisibility(View.VISIBLE);
                        Continue_Button.setVisibility(View.GONE);
                        Reset_Button.setVisibility(View.GONE);
                    }
                    else {
                        change_passowrd_parent_layout.setVisibility(View.GONE);
                        MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                        return;
                    }
                }
                else if (verifyModel.ResponseStatus.matches("Verification Fail"))
                {
                    change_passowrd_parent_layout.setVisibility(View.GONE);
                    RepeatSafeToast.show(getActivity(),verifyModel.ErrorCodes);
                    return;
                }
                else if (verifyModel.ResponseStatus.matches("EmailId  Should not be blank"))
                {
                    change_passowrd_parent_layout.setVisibility(View.GONE);
                    RepeatSafeToast.show(getActivity(),"EmailId  Should not be blank");
                    return;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_Error("Server Error, Please Try again later");
                change_passowrd_parent_layout.setVisibility(View.GONE);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                //System.out.println(User_EmailId_Text+" "+User_CountryCode+" "+User_MobileNo+" "+User_OTP_Text);
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("EmailID", checkUserDetailsModel.getEmail());

                    params.put("countryCode", "91");


                    params.put("MobileNumber",  checkUserDetailsModel.getMobileNo() );


                params.put("OTP", Enter_OTP_Code_EditText.getText().toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();     //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

        // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public void showDialog_ShowError(String errorText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Email_Id_EditText.setText("");
                Security_Code_EditText.setText("");
                User_Name_EditText.setText("");
               // Spinner_CountryCode.setSelection(0);
                generate_Captcha();
                dialog.dismiss();
                return;

            }
        });
    }

    private String HashingSha256(String Input) {
        final HashFunction hashFunction = Hashing.sha256();
        final HashCode hc = hashFunction
                .newHasher()
                .putString(Input, Charsets.UTF_8)
                .hash();
        final String sha256 = hc.toString();
        return sha256;
    }

    private boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    private boolean IsHavingSpecialCharector(String s){


            Pattern p = Pattern.compile("[^A-Za-z0-9]");
            Matcher m = p.matcher(s);

            boolean b = m.find();
            if (b)
                System.out.println("There is a special character in my string ");
            else
                System.out.println("There is no special char.");
            return b;

    }

    private void showDialog(String warningText) {

        final Dialog dialog = new Dialog(this.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_success);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                changeScreen();
            }
        });

    }

    private void changeScreen() {

        NewHomeFragmentTwo newFragment = new NewHomeFragmentTwo();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_frame, newFragment);
        transaction.commit();
    }
}
