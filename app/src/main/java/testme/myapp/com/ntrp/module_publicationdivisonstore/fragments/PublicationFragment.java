package testme.myapp.com.ntrp.module_publicationdivisonstore.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pixplicity.easyprefs.library.Prefs;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyCustomToast;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_publicationdivisonstore.IItemListener;
import testme.myapp.com.ntrp.module_publicationdivisonstore.adapter.MenuAdapter;
import testme.myapp.com.ntrp.module_publicationdivisonstore.adapter.PublicationAdapter;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.BaseResponse;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.CartData;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.MenuModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.MenuResponse;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ProductCartListModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ProductListModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.utils.AllUrls;


public class PublicationFragment extends BaseFragment {
    private View mView;
    private Spinner spList;
    private MenuAdapter menuAdapter;
    private RecyclerView rvData;
    private ArrayList<MenuModel> menuModelList;
    private List<ProductListModel> itemList;
    private PublicationAdapter publicationAdapter;
    private LinearLayout llCheckout;
    private TextView tvValue, tvQtyAmt;
    private Button btnCheckout;
    private EditText etSearch;
    private String type;
    public static String ModuleType = "", AddToCardType = "";
    public static String pTitle;
    public static final String UNIQUE_ID = String.valueOf(UUID.randomUUID());
    public static String checkoutString = "";
    public static ArrayList<ProductCartListModel> productCartList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_publication, container, false);
        init();
        return mView;
    }

    @Override
    public void onStart() {
         getActivity().setTitle(pTitle);
        super.onStart();
    }

    @Override
    protected void initView() {
        menuModelList = new ArrayList<>();
        productCartList = new ArrayList<>();
        itemList = new ArrayList<>();
        spList = mView.findViewById(R.id.spList);
        rvData = mView.findViewById(R.id.rvData);
        llCheckout = mView.findViewById(R.id.llCheckout);
        tvValue = mView.findViewById(R.id.tvValue);
        tvQtyAmt = mView.findViewById(R.id.tvQtyAmt);
        btnCheckout = mView.findViewById(R.id.btnCheckout);
        etSearch = mView.findViewById(R.id.etSearch);
    }

    @Override
    protected void initListener() {
        spList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {
                    // CloseKeyBoard();
                    System.out.println("cancelled timer....");
                    HitGetProductListApi(menuModelList.get(position).getiD());

                } else {
                    MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                    return;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckoutFragment newFragment = new CheckoutFragment();
                replaceFragment(newFragment, R.id.container_frame, true);
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString().toLowerCase());
            }
        });
    }

    @Override
    protected void bindDataWithUi() {
        Bundle bundle = this.getArguments();
        if (null != bundle) {
            type = bundle.getString("type");
            ModuleType = type;
        }
        if (!TextUtils.isEmpty(checkoutString)) {
            llCheckout.setVisibility(View.VISIBLE);
            tvQtyAmt.setText(checkoutString);
        }
//        if (CartData.cartDataModelList.size() > 0) {
//            int totalQty = 0, totalPrice = 0;
//            for (CartData item : CartData.cartDataModelList) {
//                totalQty += item.getQty();
//                totalPrice += item.getTotalPrice();
//            }
//            llCheckout.setVisibility(View.VISIBLE);
//            tvQtyAmt.setText(totalQty + " Item(s) | ₹ " + totalPrice);
//        }
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        rvData.setLayoutManager(mLayoutManager);
        publicationAdapter = new PublicationAdapter(getContext(), itemList, new IItemListener<ProductListModel>() {
            @Override
            public void onItemClick(ProductListModel productListModel, int position) {
                Bundle args = new Bundle();
                args.putParcelable("data", productListModel);
                ProductDescFragment newFragment = new ProductDescFragment();
                newFragment.setArguments(args);
                replaceFragment(newFragment, R.id.container_frame, true);
            }
        });
        rvData.setAdapter(publicationAdapter);
        UserModel model = new UserModel();
        model = new Gson().fromJson(Prefs.getString("userData"), UserModel.class);
        MyToken.setToken(model.refreshToken);
        MyToken.setAuthorization(model.accessToken);
        MyToken.setMobileNumber(model.mobileNumber);
        Hit_API_Token(MyToken.getMobileNumber());
    }

    public void Hit_API_Token(String mobileNumber) {
        showLoadingDialog();
        //System.out.println("Token API for registration..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                TokenModel tokenModel = gson.fromJson(response, TokenModel.class);
                MyToken.setToken(tokenModel.refresh_token);
                MyToken.setAuthorization(tokenModel.access_token);
                if (MyToken.getToken() != null) {
                    HitGetBasketApi();


                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                //showDialog_ServerError("Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserName", mobileNumber);
                params.put("grant_type", "password");
                params.put("Operator", "airtel");
                params.put("DeviceID", "123");
                params.put("AppType", "NTRP-APP");
                params.put("AppVersion", "1.0");
                params.put("OS", "android");
                // params.put("UserName",mobileNumber );
                //params.put("password", "123456");
                params.put("Flag", "5");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }


        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public void HitGetProductMenuXMLApi() {
        //System.out.println("Token API for registration..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getProductMenuXMLURL;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                // Gson gson = new GsonBuilder().setLenient().create();
                Gson gson = new Gson();
                Type type = new TypeToken<MenuResponse>() {
                }.getType();
                MenuResponse menuResponse = gson.fromJson(response, type);
                if (menuResponse.getErrorCodes().matches("Success")) {
                    MyToken.setToken(menuResponse.getRefreshTokenValue());
                    MyToken.setAuthorization(menuResponse.getAcsessTokenValue());
                    MenuResponse m = gson.fromJson(menuResponse.getResponseStatus(), type);
                    ArrayList<MenuModel> dataList = new ArrayList<>();
                    dataList = new ArrayList<>(m.getMenus().getMenu());
                    for (int i = 0; i < dataList.size(); i++) {
                        if (!dataList.get(i).getParentID().equalsIgnoreCase("0")) {
                            menuModelList.add(dataList.get(i));
                        }
                    }
                    menuAdapter = new MenuAdapter(getContext(), R.layout.single_row_spinner, menuModelList);
                    spList.setAdapter(menuAdapter);
                } else {
                    MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");

                }

                //.setData(itemList);
                // publicationAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("CategoryId", type);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public void HitGetProductListApi(String catId) {
        itemList = new ArrayList<>();
        //System.out.println("Token API for registration..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getProductListURL;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                // Gson gson = new GsonBuilder().setLenient().create();
                Gson gson = new Gson();
                Type type = new TypeToken<ProductListModel>() {
                }.getType();
                ProductListModel responseData = gson.fromJson(response, type);
                if (responseData.getErrorCodes().matches("Success")) {
                    MyToken.setToken(responseData.getRefreshTokenValue());
                    MyToken.setAuthorization(responseData.getAcsessTokenValue());
                    itemList.addAll(responseData.getProductList());
                    publicationAdapter.setData(itemList);
                } else {
                    MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("Flag", "P");
                params.put("CategoryId", catId); //2
                //params.put("ProductId", productId);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    void filter(String text) {
        List<ProductListModel> temp = new ArrayList<>();
        if (TextUtils.isEmpty(text)) {
            publicationAdapter.setData(itemList);
        } else {

            for (ProductListModel d : itemList) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.getProductName().toLowerCase().trim().contains(text.trim())) {
                    temp.add(d);
                }
            }
            if (temp.size() > 0)
                publicationAdapter.setData(temp);
            else {
                MyCustomToast.show(getContext(), "No record found");
                publicationAdapter.setData(new ArrayList<>());
            }

            // Toast.makeText(getContext(), "No record found", Toast.LENGTH_SHORT).show();
        }


        //update recyclerview

    }

    public class UserModel {
        private String accessToken, refreshToken, mobileNumber;

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getRefreshToken() {
            return refreshToken;
        }

        public void setRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }


    }

    private void HitGetBasketApi() {
        productCartList.clear();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getBasket;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    if (responseData.getErrorCodes().matches("Success") || responseData.getErrorCodes().matches("Record Not Found !")) {
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        HitGetProductMenuXMLApi();
                        if (null != responseData.getProductCartList() && responseData.getProductCartList().size() > 0) {
                            productCartList.addAll(responseData.getProductCartList());

                        } else {
                            llCheckout.setVisibility(View.GONE);
                            productCartList.clear();
                            checkoutString = "";
                        }
                    } else {
                        MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
                    }
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
}