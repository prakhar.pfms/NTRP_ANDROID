package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.events.MakeUserLogout;
import testme.myapp.com.ntrp.extra.ConvertNumberToWords;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.DepositorCategoryModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.RegisterUserModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.SwachhBharatKoshModel;
import testme.myapp.com.ntrp.module_login.models.LogedInUserDetailsModels;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;


/**
 * Created by deepika.s on 3/24/2017.
 */

public class DonateToSwachhBharatKoshFragment extends Fragment {

    private View localView;
    private TextView DepositorCategory_Textview,Purpose_Textview,Amount_Textview,AmountValueInText_Textview,mobile_no_textview_payment_purpose;
    private EditText Amount_EditText,Remarks_EditText,mobileno_edittext_payment_purpose;
    private Spinner DepositorCategory_Spinner,Currency_Spinner,spinner_countrypnone_code_payment_purpose;
    private Button NextButton;
    private boolean convertToText;
    private String amountValue,tempAmountValue;
    public static double FinalAmount;
    public static String FinalAmountText;
    private DepositorCategoryModel depositorCategoryModel;
    private int selectedDepositorId;
    @BindView(R.id.remarks_textview_)
    public TextView remarks_textview;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_donate_to_sbk_payment_purpose, container, false);
        localView=view;
        FinalAmount=0;
        convertToText=false;
        ButterKnife.bind(this,view);
        getActivity().setTitle("Swachh Bharat Kosh");
        initializeView();
        CloseKeyBoard();
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle("Swachh Bharat Kosh");
    }
    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    private void initializeView() {

        initialize_TextViews();
        initialize_EditTextViews();
        initialize_SpinnersViews();
        initialize_NextButtonViews();
        setOnTouchListener_RemarksEdittext();
        setOnTouchListener_Amount_EditText();
        DisbaleAllExceptMobileNumberView();
    }

    private void DisbaleAllExceptMobileNumberView()
    {
        NextButton.setVisibility(View.GONE);
        mobileno_edittext_payment_purpose.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                     if (charSequence.length()==10&&spinner_countrypnone_code_payment_purpose.getSelectedItem().toString().equals("91"))
                     {
                        // HitDepositorCategoryAPI();
                         Hit_RegisterUser_API();
                     }
                     else
                     {
                         NextButton.setVisibility(View.GONE);
                     }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        PreFillDataIfUserLogin();
    }

    private void PreFillDataIfUserLogin() {
        if (MainActivity.IsLogin) {
            Gson gson = new GsonBuilder().setLenient().create();
            LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);

            mobileno_edittext_payment_purpose.setText("" + userInfoModel.getMobileNo() != null ? userInfoModel.getMobileNo() : "");

        }
        else
        {

            EventBus.getDefault().post(new MakeUserLogout());

        }
    }

    private void Hit_RegisterUser_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.registerAPIUserURL;
        final ProgressDialog pDialog = new ProgressDialog(DonateToSwachhBharatKoshFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                RegisterUserModel registerUserModel = gson.fromJson(response, RegisterUserModel.class);

                if(registerUserModel.getErrorCodes().toString().matches("User Already Exists")||
                        registerUserModel.getErrorCodes().toString().matches("User Registered Successfully"))
                {
                    Hit_API_Token();
                }
                else
                {

                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("MobileNo", ""+mobileno_edittext_payment_purpose.getText().toString());
                params.put("Flag", "5");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_API_Token() {
        HandleHttpsRequest.setSSL();
        System.out.println("Token API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(DonateToSwachhBharatKoshFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                TokenModel tokenModel=gson.fromJson(response,TokenModel.class);
                MyToken.setToken(tokenModel.refresh_token);
                MyToken.setAuthorization(tokenModel.access_token);
                if(MyToken.getToken()!=null)
                {

                    if (CheckNetworkConnection.getInstance(DonateToSwachhBharatKoshFragment.this.getActivity()).isOnline())
                    {

                        Prefs.putString("LoggedInMobileNumber", mobileno_edittext_payment_purpose.getText().toString());
                        Hit_Depositor_category_API();

                    }
                    else {
                        Toast.makeText(DonateToSwachhBharatKoshFragment.this.getActivity(), "Some error occured!",
                                Toast.LENGTH_LONG).show();

                    }
                }
                else
                {
                    Toast.makeText(DonateToSwachhBharatKoshFragment.this.getActivity(), "Some error occured!",
                            Toast.LENGTH_LONG).show();
                }




            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("UserName", mobileno_edittext_payment_purpose.getText().toString());
                params.put("AppType", "NTRP-APP");
                params.put("DeviceID", "123");
                params.put("AppVersion", "1.0");
                params.put("password", "123456");
                params.put("OS", "android");
                params.put("Operator", "airtel");
                params.put("grant_type", "password");
                params.put("Flag", "5");
                return params;
            }


        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    private void Hit_Depositor_category_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDepositorListURL;
        final ProgressDialog pDialog = new ProgressDialog(DonateToSwachhBharatKoshFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                depositorCategoryModel = gson.fromJson(response, DepositorCategoryModel.class);


                if (depositorCategoryModel.getResponseStatus().matches("Success")&&depositorCategoryModel.getAcsessTokenValue()!=null&&depositorCategoryModel.getRefreshTokenValue()!=null)
                {
                    MyToken.setToken(depositorCategoryModel.getRefreshTokenValue());
                    MyToken.setAuthorization(depositorCategoryModel.getAcsessTokenValue());
                    NextButton.setVisibility(View.VISIBLE);
                    ArrayList<String> items = new ArrayList<String>();

                    for(int i=0;i<depositorCategoryModel.getDepositorList().size();i++)
                    {
                        items.add(depositorCategoryModel.getDepositorList().get(i).getDepositorName());
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
                    DepositorCategory_Spinner.setAdapter(adapter);
                    DepositorCategory_Spinner.setSelection(0);

                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("PurposeId", "1");
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
               // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void putSingleRedStar() {
        putRedAsterisk(localView, mobile_no_textview_payment_purpose, R.id.mobile_no_textview_payment_purpose);
        putRedAsterisk(localView, DepositorCategory_Textview, R.id.depositorcategory_textview);
        putRedAsterisk(localView, Amount_Textview, R.id.amount_textview);

    }

    private void initialize_TextViews()
    {
        mobile_no_textview_payment_purpose= (TextView) localView.findViewById(R.id.mobile_no_textview_payment_purpose);
        DepositorCategory_Textview= (TextView) localView.findViewById(R.id.depositorcategory_textview);
        Purpose_Textview= (TextView) localView.findViewById(R.id.purpose_textview);
        Amount_Textview = (TextView) localView.findViewById(R.id.amount_textview);
        AmountValueInText_Textview = (TextView) localView.findViewById(R.id.amountvalueintext_textview);
        /*putRedAsterisk(localView, DepositorCategory_Textview, R.id.depositorcategory_textview);
        putRedAsterisk(localView, Purpose_Textview, R.id.purpose_textview);
        putRedAsterisk(localView, Amount_Textview, R.id.amount_textview);*/
        putSingleRedStar();
    }
    private void initialize_EditTextViews()
    {
        mobileno_edittext_payment_purpose= (EditText) localView.findViewById(R.id.mobileno_edittext_payment_purpose);
        Amount_EditText = (EditText) localView.findViewById(R.id.amountvalue_edittext);
        Remarks_EditText = (EditText) localView.findViewById(R.id.remarks_edittext);
        Amount_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(Amount_EditText.getText().length() == 1 && Amount_EditText.getText().toString().trim().equals("0")){
                    Amount_EditText.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        Remarks_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int charleft=60-s.length();

                remarks_textview.setText(charleft+ "character left");

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    private void initialize_SpinnersViews()
    {
        initialize_DepositorCategorySpinnerViews();
        initialize_CurrencySpinnerViews();
        initialze_Mobile_Country_Code_Spinner();
    }

    private void initialze_Mobile_Country_Code_Spinner() {
        spinner_countrypnone_code_payment_purpose = (Spinner) localView.findViewById(R.id.spinner_countrypnone_code_payment_purpose);
         String[] country_mobile_number_codes=new String[1];
        country_mobile_number_codes[0]="91";
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(DonateToSwachhBharatKoshFragment.this.getActivity(), R.layout.single_row_spinner, country_mobile_number_codes);
        spinner_countrypnone_code_payment_purpose.setAdapter(adapter);

    }

    private void initialize_DepositorCategorySpinnerViews()
    {
        DepositorCategory_Spinner = (Spinner) localView.findViewById(R.id.spinner_depositorcategory);

        DepositorCategory_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*if(position==0)
                {
                    View selectedView = DepositorCategory_Spinner.getSelectedView();
                    if (selectedView != null && selectedView instanceof TextView) {
                        TextView selectedTextView = (TextView) selectedView;
                        selectedTextView.setError("Error");
                    }
                }*/

                selectedDepositorId=depositorCategoryModel.getDepositorList().get(position).getDepositorId();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void initialize_CurrencySpinnerViews()
    {
        Currency_Spinner = (Spinner) localView.findViewById(R.id.spinner_inr);
        String[] currency = new String[]{"INR"};
        final ArrayAdapter<String> adapterCurrency = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, currency);
        Currency_Spinner.setAdapter(adapterCurrency);
        if (currency.length == 1) {
            Currency_Spinner.setClickable(false);
            Currency_Spinner.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        //Toast.makeText(getActivity(), "Catch it!", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });
        }
    }

    private void initialize_NextButtonViews()
    {
        NextButton = (Button) localView.findViewById(R.id.next_button);
        NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                amountValue = Amount_EditText.getText().toString();

                /*if(DepositorCategory_Spinner.getSelectedItemPosition()==0)
                {
                    Toast.makeText(getActivity(), "Please Select Depositor Category!", Toast.LENGTH_SHORT).show();
                    return;
                }*/
                if (amountValue.matches("")) {
                    convertToText=false;
                    AmountValueInText_Textview.setText("");
                    Toast.makeText(getActivity(), "Please Enter Amount!", Toast.LENGTH_SHORT).show();
                    return;
                } else if (amountValue.length() < 10 && Integer.valueOf(amountValue) == 0) {
                    convertToText=false;
                    Amount_EditText.setError("Please Enter Numerics only,0 is not allowed at first position. Must be greater than 0\n");
                    return;
                }

                else {

                    if(convertToText==false)
                    {
                        AmountValueInText_Textview.setText(set_validation_On_Input_Amount_String(amountValue));
                        CloseKeyBoard();
                        convertToText=true;
                        return;
                    }
                    if(convertToText) {
                        if(amountValue.matches(tempAmountValue))
                        {
                            FinalAmount= Double.parseDouble(amountValue);
                            HitSwachhBharatKoshAPI();
                          //  changeFragment_DepositorDetailsFragment();
                        }
                        else
                        {
                            convertToText=false;
                            return;
                        }
                    }
                }

            }
        });
    }

    private void HitSwachhBharatKoshAPI() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getSwachhBharatKoshURL;
        final ProgressDialog pDialog = new ProgressDialog(DonateToSwachhBharatKoshFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                SwachhBharatKoshModel swachhBharatKoshModel = gson.fromJson(response, SwachhBharatKoshModel.class);


                if (swachhBharatKoshModel.getReceiptEntryId()!=null&&swachhBharatKoshModel.getResponseStatus().matches("SUCCESS")&&swachhBharatKoshModel.getAcsessTokenValue()!=null&&swachhBharatKoshModel.getRefreshTokenValue()!=null)
                {
                    MyToken.setToken(swachhBharatKoshModel.getRefreshTokenValue());
                    MyToken.setAuthorization(swachhBharatKoshModel.getAcsessTokenValue());

                    Prefs.putInt("ReceiptEntryId", swachhBharatKoshModel.getReceiptEntryId());
                    Prefs.putInt("selectedDepositorId", selectedDepositorId);
                    changeFragment_DepositorDetailsFragment();

                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("PurposeId", "1");
                params.put("UserId", MainActivity.IsLogin ? MainActivity.UserID :"");
                params.put("UserName", MainActivity.IsLogin ? MainActivity.UserName :"");
                params.put("Amount", String.valueOf(FinalAmount));
                params.put("paymentTypeId", "0");
                params.put("CurrencyCode", "INR");
                params.put("PurposeRemarks", Remarks_EditText.getText().toString());
                params.put("DepositorCategory", String.valueOf(selectedDepositorId));
                params.put("RefreshTokenValue", MyToken.getToken());



                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

      //  strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    private void changeFragment_DepositorDetailsFragment()
    {
        Fragment depositorDetailsFragment = new DepositorDetailsFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, depositorDetailsFragment);
        ft.addToBackStack(null);
        ft.commit();
    }
    private void setOnTouchListener_Amount_EditText()
    {
        Amount_EditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    if(convertToText)
                    {
                        convertToText=false;
                    }
                }
                return false;
            }
        });
    }
    private void setOnTouchListener_RemarksEdittext()
    {

    }

    private String set_validation_On_Input_Amount_String(String bigString) {
        tempAmountValue=bigString;
        String finalString = "RUPEES ";
        int length = bigString.length();
        if (length == 10) {
            String first = bigString.substring(0, 3);
            String last = bigString.substring(3, length);
            int firstvalue = Integer.parseInt(first);
            int lastvalue = Integer.parseInt(last);
            finalString = finalString + ConvertNumberToWords.convertToWords(firstvalue);
            finalString = finalString + " CRORE(S) ";
            finalString = finalString + ConvertNumberToWords.convertToWords(lastvalue);
        } else if (length == 11) {
            String first = bigString.substring(0, 4);
            String last = bigString.substring(4, length);
            int firstvalue = Integer.parseInt(first);
            int lastvalue = Integer.parseInt(last);
            finalString = finalString + ConvertNumberToWords.convertToWords(firstvalue);
            finalString = finalString + " CRORE(S) ";
            finalString = finalString + ConvertNumberToWords.convertToWords(lastvalue);
        } else if (length < 10) {
            finalString = finalString + ConvertNumberToWords.convertToWords(Integer.parseInt(bigString));
        } else {
            finalString = finalString + "";
        }
        finalString = finalString + "ONLY";

        FinalAmountText=finalString;
        return finalString;

    }
    private void putRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }





}

