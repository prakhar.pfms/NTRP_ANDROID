package testme.myapp.com.ntrp.module_nonregisteredusers.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsersFragment;


/**
 * Created by deepika.s on 05-05-2017.
 */

public class NonRegisteredUsersScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_nonregisteredusers_screen);
        changeFragment_NonRegisteredUsersFragment();

    }
    private void changeFragment_NonRegisteredUsersFragment() {

        Fragment nonRegisteredUsersFragment = new NonRegisteredUsersFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_frame, nonRegisteredUsersFragment);
        ft.commit();
    }


}
