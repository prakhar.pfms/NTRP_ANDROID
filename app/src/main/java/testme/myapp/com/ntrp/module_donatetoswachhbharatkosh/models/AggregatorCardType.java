
package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AggregatorCardType {

    @SerializedName("AggregatorID")
    @Expose
    private Integer aggregatorID;
    @SerializedName("AggregatorBankName")
    @Expose
    private String aggregatorBankName;
    @SerializedName("AggregatorLogoName")
    @Expose
    private String aggregatorLogoName;

    public Integer getAggregatorID() {
        return aggregatorID;
    }

    public void setAggregatorID(Integer aggregatorID) {
        this.aggregatorID = aggregatorID;
    }

    public String getAggregatorBankName() {
        return aggregatorBankName;
    }

    public void setAggregatorBankName(String aggregatorBankName) {
        this.aggregatorBankName = aggregatorBankName;
    }

    public String getAggregatorLogoName() {
        return aggregatorLogoName;
    }

    public void setAggregatorLogoName(String aggregatorLogoName) {
        this.aggregatorLogoName = aggregatorLogoName;
    }

}
