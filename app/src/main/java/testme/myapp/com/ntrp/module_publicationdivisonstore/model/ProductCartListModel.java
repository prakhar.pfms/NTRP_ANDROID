package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import com.google.gson.annotations.SerializedName;

public class ProductCartListModel {
    @SerializedName("TotalQty")
    public int totalQty;
    @SerializedName("ItemPrice")
    public double itemPrice;
    @SerializedName("TotalPrice")
    public double totalPrice;
    @SerializedName("ProductImage")
    public String productImage;
    @SerializedName("ProductName")
    public String productName;
    @SerializedName("ProductId")
    public int productId;
    @SerializedName("ProductCode")
    public String productCode;
    @SerializedName("DeliveryCharges")
    public double deliveryCharges;
    @SerializedName("ProductDiscount")
    public double productDiscount;
    @SerializedName("ProductType")
    public Object productType;
    @SerializedName("VatCharge")
    public double vatCharge;
    @SerializedName("ProductAttributeMappingId")
    public int productAttributeMappingId;
    @SerializedName("ProductIssues")
    public Object productIssues;
    @SerializedName("ProductLanguage")
    public Object productLanguage;
    @SerializedName("ProductCountry")
    public Object productCountry;
    @SerializedName("UserId")
    public int userId;
    @SerializedName("SessionUniqueUserId")
    public Object sessionUniqueUserId;
    @SerializedName("DeliveryChargesId")
    public int deliveryChargesId;
    @SerializedName("Stock")
    public int stock;
    @SerializedName("CategoryId")
    public int categoryId;
    @SerializedName("TotalAmount")
    public double totalAmount;
    @SerializedName("TotalGrandAmount")
    public double totalGrandAmount;
    @SerializedName("ProductShippingAmount")
    public double productShippingAmount;
    @SerializedName("DeliveryType")
    public Object deliveryType;
    @SerializedName("TotalPriceShippingChargesSum")
    public double totalPriceShippingChargesSum;
    @SerializedName("ProductAttribute")
    public Object productAttribute;
    @SerializedName("ReceiptProductItemId")
    public int receiptProductItemId;
    @SerializedName("CategoryReceiptAccountId")
    public int categoryReceiptAccountId;
    @SerializedName("ParentCategoryName")
    public String parentCategoryName;

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public double getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(double deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public double getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(double productDiscount) {
        this.productDiscount = productDiscount;
    }

    public Object getProductType() {
        return productType;
    }

    public void setProductType(Object productType) {
        this.productType = productType;
    }

    public double getVatCharge() {
        return vatCharge;
    }

    public void setVatCharge(double vatCharge) {
        this.vatCharge = vatCharge;
    }

    public int getProductAttributeMappingId() {
        return productAttributeMappingId;
    }

    public void setProductAttributeMappingId(int productAttributeMappingId) {
        this.productAttributeMappingId = productAttributeMappingId;
    }

    public Object getProductIssues() {
        return productIssues;
    }

    public void setProductIssues(Object productIssues) {
        this.productIssues = productIssues;
    }

    public Object getProductLanguage() {
        return productLanguage;
    }

    public void setProductLanguage(Object productLanguage) {
        this.productLanguage = productLanguage;
    }

    public Object getProductCountry() {
        return productCountry;
    }

    public void setProductCountry(Object productCountry) {
        this.productCountry = productCountry;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Object getSessionUniqueUserId() {
        return sessionUniqueUserId;
    }

    public void setSessionUniqueUserId(Object sessionUniqueUserId) {
        this.sessionUniqueUserId = sessionUniqueUserId;
    }

    public int getDeliveryChargesId() {
        return deliveryChargesId;
    }

    public void setDeliveryChargesId(int deliveryChargesId) {
        this.deliveryChargesId = deliveryChargesId;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getTotalGrandAmount() {
        return totalGrandAmount;
    }

    public void setTotalGrandAmount(double totalGrandAmount) {
        this.totalGrandAmount = totalGrandAmount;
    }

    public double getProductShippingAmount() {
        return productShippingAmount;
    }

    public void setProductShippingAmount(double productShippingAmount) {
        this.productShippingAmount = productShippingAmount;
    }

    public Object getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(Object deliveryType) {
        this.deliveryType = deliveryType;
    }

    public double getTotalPriceShippingChargesSum() {
        return totalPriceShippingChargesSum;
    }

    public void setTotalPriceShippingChargesSum(double totalPriceShippingChargesSum) {
        this.totalPriceShippingChargesSum = totalPriceShippingChargesSum;
    }

    public Object getProductAttribute() {
        return productAttribute;
    }

    public void setProductAttribute(Object productAttribute) {
        this.productAttribute = productAttribute;
    }

    public int getReceiptProductItemId() {
        return receiptProductItemId;
    }

    public void setReceiptProductItemId(int receiptProductItemId) {
        this.receiptProductItemId = receiptProductItemId;
    }

    public int getCategoryReceiptAccountId() {
        return categoryReceiptAccountId;
    }

    public void setCategoryReceiptAccountId(int categoryReceiptAccountId) {
        this.categoryReceiptAccountId = categoryReceiptAccountId;
    }

    public String getParentCategoryName() {
        return parentCategoryName;
    }

    public void setParentCategoryName(String parentCategoryName) {
        this.parentCategoryName = parentCategoryName;
    }
}
