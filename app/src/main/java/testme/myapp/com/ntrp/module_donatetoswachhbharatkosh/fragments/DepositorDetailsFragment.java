package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.ConfirmInfoModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.Country;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.CountryModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.DepositorDetailsSubmitModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.District;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.DistrictModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.SalutaionModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.Salution;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.State;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.StateModel;

import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.SwachhBharatKoshModel;
import testme.myapp.com.ntrp.module_login.models.LogedInUserDetailsModels;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

/**
 * Created by deepika.s on 3/28/2017.
 */

public class DepositorDetailsFragment extends Fragment {

    private View localView;
    private TextView Name_TextView,Address_Line1_TextView,Country_TextView,State_TextView,District_TextView,City_TextView,Email_TextView;
    private EditText Name_EditText,Email_EditText,Address_Line1_EditText,Address_Line2_EditText,City_EditText,Pincode_Zipcode_EditText;
    private Spinner Spinner_Marital_Status,Spinner_Country,Spinner_State,Spinner_District,Spinner_MobileNo;
    private Button nextButton;
    private String EmailText,NameText,AddressLine1Text,SelectedStateName,SelectedDistrictName;
    private int selectedCountryID,selectedStateID,selectedDistrictID,selectedSalutationId;
    private ArrayList<Country> countryArrayList;
    private ArrayList<State> stateArrayList;
    private ArrayList<District> districtArrayList;
    private ArrayList<Salution> salutionArrayList= new ArrayList<>();;
    private ConfirmInfoModel confirmInfoModel;
    private int Country_check = 2, State_check = 0, District_check = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_donate_to_sbk_depositor_detail, container, false);
        localView=view;
        Country_check = 2; State_check = 0; District_check = 0;
        SelectedStateName=null;SelectedDistrictName=null;
        initializeView();
        CloseKeyBoard();
        return view;
    }
    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
    private void initializeView() {

        selectedCountryID=101;
        countryArrayList=new ArrayList<>();
        selectedStateID=0;
        stateArrayList=new ArrayList<>();
        selectedDistrictID=0;
        districtArrayList= new ArrayList<>();

        initialize_TextViews();
        initialize_EditTextViews();
        initializeSpinnerViews();
        initialize_Spinner_Country();
        initialize_Spinner_State();
        initialize_Spinner_District();
        handle_NextButton();
        //set_validation_on_Marital_Status_Dropdown();
        set_Validation_Name();
        set_Validation_Address();
        set_Validation_Address2();
        //System.out.println("77777777777");
        PreFillDataIfUserLogin();
    }

    private void PreFillDataIfUserLogin()
    {
        if (MainActivity.IsLogin)
        {
            Gson gson = new GsonBuilder().setLenient().create();
            LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);

            Name_EditText.setText(""+ userInfoModel.getFirstName()!=null? userInfoModel.getFirstName() : "");

            Email_EditText.setText(""+ userInfoModel.getEmail()!=null? userInfoModel.getEmail() : "");

            City_EditText.setText(""+ userInfoModel.getCity()!=null? userInfoModel.getCity() : "");

            Address_Line1_EditText.setText("" + userInfoModel.getAddress1() != null ? userInfoModel.getAddress1() : "");

            Address_Line2_EditText.setText("" + userInfoModel.getAddress2() != null ? userInfoModel.getAddress2() : "");

            Pincode_Zipcode_EditText.setText("" + userInfoModel.getPincode() != null ? userInfoModel.getPincode() : "");

        }
    }

    private void initialize_TextViews()
    {
        Name_TextView= (TextView) localView.findViewById(R.id.name_textview);
        Address_Line1_TextView= (TextView) localView.findViewById(R.id.addressline1_textview);
        Country_TextView= (TextView) localView.findViewById(R.id.country_textview);
        State_TextView= (TextView) localView.findViewById(R.id.state_textview);
        District_TextView= (TextView) localView.findViewById(R.id.district_textview);
        City_TextView= (TextView) localView.findViewById(R.id.city_textview);
        Email_TextView= (TextView) localView.findViewById(R.id.email_textview);
        putRedAsterisk(localView, Name_TextView, R.id.name_textview);
        putRedAsterisk(localView, Address_Line1_TextView, R.id.addressline1_textview);
        putRedAsterisk(localView, Country_TextView, R.id.country_textview);
        putRedAsterisk(localView, State_TextView, R.id.state_textview);
        putRedAsterisk(localView, District_TextView, R.id.district_textview);
        putRedAsterisk(localView, City_TextView, R.id.city_textview);
       // putRedAsterisk(localView, Mobile_No_TextView, R.id.mobile_no_textview);
        putRedAsterisk(localView, Email_TextView, R.id.email_textview);
    }
    private void initialize_EditTextViews()
    {
        Name_EditText= (EditText) localView.findViewById(R.id.name_edittext);
        Address_Line1_EditText = (EditText) localView.findViewById(R.id.addressline1_edittext);
        Address_Line2_EditText= (EditText) localView.findViewById(R.id.addressline2_edittext);
        City_EditText= (EditText) localView.findViewById(R.id.city_edittext);
        Pincode_Zipcode_EditText= (EditText) localView.findViewById(R.id.pincode_zipcode_edittext);

        Email_EditText= (EditText) localView.findViewById(R.id.email_edittext);

    }
    private void initializeSpinnerViews()
    {
        Spinner_Marital_Status = (Spinner) localView.findViewById(R.id.spinner_maritalstatus);
       /* String[] items = new String[]{"-Select-", "Mr.", "Ms./Mrs."};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
        Spinner_Marital_Status.setAdapter(adapter);*/

        Spinner_Marital_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                selectedSalutationId = salutionArrayList.get(position).getSalutationId();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }

        });

        Spinner_Country= (Spinner) localView.findViewById(R.id.spinner_country);

        HitSalutationAPI();

        Spinner_State= (Spinner) localView.findViewById(R.id.spinner_state);
        Spinner_District= (Spinner) localView.findViewById(R.id.spinner_district);
        Spinner_MobileNo= (Spinner) localView.findViewById(R.id.spinner_countrypnone_code);

        Spinner_Country.setSelection(selectedCountryID);
        Spinner_State.setSelection(selectedStateID);
        Spinner_District.setSelection(selectedDistrictID);
    }
    private void handle_NextButton()
    {
        nextButton= (Button) localView.findViewById(R.id.next_button);
        setOnClick_NextButton();
    }

    private void changeFragment_ConfirmInfoFragment()
    {
        Bundle bundle=new Bundle();
        bundle.putString("Name",Name_EditText.getText().toString());
        bundle.putString("Address1",Address_Line1_EditText.getText().toString());
        bundle.putString("Address2",Address_Line2_EditText.getText().toString());
        bundle.putString("Country",Spinner_Country.getSelectedItem().toString());
        bundle.putString("State",Spinner_State.getSelectedItem().toString());
        bundle.putString("District",Spinner_District.getSelectedItem().toString());
        bundle.putString("City",City_EditText.getText().toString());
        bundle.putString("Pincode",Pincode_Zipcode_EditText.getText().toString());
        bundle.putString("Mobile", Prefs.getString("LoggedInMobileNumber", ""));
        bundle.putString("Email",Email_EditText.getText().toString());
        System.out.println(bundle.get("District"));
        System.out.println(bundle.get("State"));
        System.out.println(bundle.get("Country"));




        Fragment confirmInfoFragment = new ConfirmInfoFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();


        confirmInfoFragment.setArguments(bundle);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, confirmInfoFragment);
        ft.addToBackStack(null);
        ft.commit();
    }


    private void setOnClick_NextButton()
    {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailText=Email_EditText.getText().toString();
                NameText=Name_EditText.getText().toString();
                AddressLine1Text= Address_Line1_EditText.getText().toString();


                if(NameText.matches("")) {
                    Name_EditText.setError("Please Enter Name!");
                    Toast.makeText(getActivity(),"Please Enter Name!",Toast.LENGTH_SHORT).show();
                    return;
                }
                else if (AddressLine1Text.matches("")) {
                    Address_Line1_EditText.setError("Please Enter Address!");
                    Toast.makeText(getActivity(),"Please Enter Address!",Toast.LENGTH_SHORT).show();
                    return;
                }
                else if (EmailText.matches("")) {
                    Email_EditText.setError("Please Enter Email!");
                    Toast.makeText(getActivity(),"Please Enter Email!",Toast.LENGTH_LONG).show();
                    return;
                }else if (City_EditText.getText().toString().matches("")) {
                    City_EditText.setError("Please Enter City!");
                    Toast.makeText(getActivity(),"Please Enter City!",Toast.LENGTH_LONG).show();
                    return;
                }

                else if (!isValidEmail(EmailText)) {
                    Email_EditText.setError("Invalid Email!");
                    Toast.makeText(getActivity(),"Invalid Email",Toast.LENGTH_LONG).show();
                }
                else if (SelectedStateName == null  ) {
                    ((TextView)Spinner_State.getSelectedView()).setError("Select State");
                    Toast.makeText(getActivity(),"Select State",Toast.LENGTH_LONG).show();
                }
                else if (SelectedDistrictName == null  ) {
                    ((TextView)Spinner_District.getSelectedView()).setError("Select District");
                    Toast.makeText(getActivity(),"Select District",Toast.LENGTH_LONG).show();
                }
                else
                {

                      HitSubmitDepositorDetailsAPI();
                }
            }
        });
    }

    private void HitSubmitDepositorDetailsAPI() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDepositorDeatilsSubmitURL;
        final ProgressDialog pDialog = new ProgressDialog(DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                DepositorDetailsSubmitModel depositorCategoryModel = gson.fromJson(response, DepositorDetailsSubmitModel.class);


                if (depositorCategoryModel.getReceiptEntryId()!=null&&depositorCategoryModel.getAcsessTokenValue()!=null&&depositorCategoryModel.getRefreshTokenValue()!=null)
                {
                    MyToken.setToken(depositorCategoryModel.getRefreshTokenValue());
                    MyToken.setAuthorization(depositorCategoryModel.getAcsessTokenValue());

                     changeFragment_ConfirmInfoFragment();


                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryID", String.valueOf(Prefs.getInt("ReceiptEntryId",0)));
                params.put("DepositorName", Name_EditText.getText().toString());
                params.put("Address1", Address_Line1_EditText.getText().toString());
                params.put("Address2", Address_Line2_EditText.getText().toString());
                params.put("City", City_EditText.getText().toString());
                params.put("Pincode", Pincode_Zipcode_EditText.getText().toString());
                params.put("Email", Email_EditText.getText().toString());
                params.put("MobileNo", Prefs.getString("LoggedInMobileNumber",""));
                params.put("StateId", String.valueOf(selectedStateID));
                params.put("DistrictId", String.valueOf(selectedDistrictID));
                params.put("CountryId", String.valueOf(selectedCountryID));
                params.put("CountryCode", "91");
                params.put("SalutationId", String.valueOf(selectedSalutationId));
                params.put("PaymentMode", "O");
                params.put("Flag", "0");
                params.put("RefreshTokenValue", MyToken.getToken());
                if (MainActivity.IsLogin)
                {
                    params.put("UserId",  MainActivity.UserID );
                    params.put("UserName", MainActivity.UserName);
                }


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
               0,
               DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }

    private void putRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }
    private void set_validation_on_Marital_Status_Dropdown()
    {
        Spinner_Marital_Status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String interesting = Spinner_Marital_Status.getItemAtPosition(position).toString();
                if(interesting.equals("-Select-"))
                {
                    Name_EditText.setEnabled(false);
                }
                else
                {
                    Name_EditText.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void set_Validation_Name()
    {
        InputFilter filter= new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String checkMe = String.valueOf(source.charAt(i));

                    Pattern pattern = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz.0123456789]");
                    Matcher matcher = pattern.matcher(checkMe);
                    boolean valid = matcher.matches();
                    if(!valid){
                        Log.d("", "invalid");
                        Name_EditText.setError("Invalid Entry");
                        return "";
                    }
                }
                return null;
            }
        };

        Name_EditText.setFilters(new InputFilter[]{filter});
    }
    private void set_Validation_Address()
    {
        InputFilter filter= new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String checkMe = String.valueOf(source.charAt(i));

                    Pattern pattern = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz0123456789'.,-/]");
                    Matcher matcher = pattern.matcher(checkMe);
                    boolean valid = matcher.matches();
                    if(!valid){
                        Log.d("", "invalid");
                        Address_Line1_EditText.setError("Invalid Entry");
                        return "";
                    }
                }
                return null;
            }
        };

        Address_Line1_EditText.setFilters(new InputFilter[]{filter});
    }
    private void set_Validation_Address2()
    {
        InputFilter filter= new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    String checkMe = String.valueOf(source.charAt(i));

                    Pattern pattern = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz0123456789'.,-/]");
                    Matcher matcher = pattern.matcher(checkMe);
                    boolean valid = matcher.matches();
                    if(!valid){
                        Log.d("", "invalid");
                        Address_Line2_EditText.setError("Invalid Entry");
                        return "";
                    }
                }
                return null;
            }
        };

        Address_Line2_EditText.setFilters(new InputFilter[]{filter});
    }
    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    /*private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", text)) {
            if(phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
                txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }*/

    private void initialize_Spinner_Country()
    {

        Spinner_Country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                  //  String item = Spinner_Country.getItemAtPosition(position).toString();
                    selectedCountryID = countryArrayList.get(position).getCountryId();
                    Hit_StateList_API();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }

        });

    }


    private void initialize_Spinner_State()
    {

        Spinner_State.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              //  if(++State_check > 1) {

                    SelectedStateName = Spinner_State.getItemAtPosition(position).toString();
                    selectedStateID=stateArrayList.get(position).getStateId();
                  //  selectedStateID = stateArrayList.get(Spinner_State.getSelectedItemPosition()).getStateId();

                    Hit_DistrictList_API();
              //  }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void initialize_Spinner_District()
    {

        Spinner_District.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

               // if(++District_check > 1) {
                    SelectedDistrictName = Spinner_District.getItemAtPosition(position).toString();
                    selectedDistrictID=districtArrayList.get(position).getDistrictId();
                   // selectedDistrictID = districtArrayList.get(Spinner_District.getSelectedItemPosition()).getDistrictId();
               // }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void Hit_CountryList_API() {

        System.out.println("Hit_CountryList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getCountryListURL;
        final ProgressDialog pDialog = new ProgressDialog(DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                CountryModel countryModel = gson.fromJson(response, CountryModel.class);
                countryArrayList.clear();

                countryArrayList= (ArrayList<Country>) countryModel.getCountryList();
                String[] countryItems=new String[countryArrayList.size()];
                for(int i=0;i<countryArrayList.size();i++)
                {
                    countryItems[i]=countryArrayList.get(i).getCountryName().toString();
                }




                ArrayAdapter<String> adapter = new ArrayAdapter<String>(DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, countryItems);
                Spinner_Country.setAdapter(adapter);
                Spinner_Country.setSelection(99);

                if (MainActivity.IsLogin) {

                    LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);

                    for(int i=0;i<countryModel.getCountryList().size();i++)
                    {
                        if (userInfoModel.getCountryId().equals(countryModel.getCountryList().get(i).getCountryId()))
                        {
                            Spinner_Country.setSelection(i);
                        }
                    }
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private void Hit_StateList_API() {

        System.out.println("Hit_StateList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getStateListURL;
        final ProgressDialog pDialog = new ProgressDialog(DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                StateModel stateModel = gson.fromJson(response, StateModel.class);
                stateArrayList.clear();

                stateArrayList= stateModel.getStateList();
                String[] stateItems=new String[stateArrayList.size()];
                for(int i=0;i<stateArrayList.size();i++)
                {
                    stateItems[i]=stateArrayList.get(i).getStateName().toString();
                }
                //stateItems[stateArrayList.size()]="Select";

              //  ArrayAdapter<String> adapter = new ArrayAdapter<String>(DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, stateItems);

                HintAdapter<String> adapter = new HintAdapter<String>(DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, stateItems);



                Spinner_State.setAdapter(adapter);

                if (MainActivity.IsLogin) {

                    LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);


                    for(int i=0;i<stateModel.getStateList().size();i++)
                    {
                        if (userInfoModel.getStateId().equals(stateModel.getStateList().get(i).getStateId()))
                        {
                            Spinner_State.setSelection(i);
                        }
                    }


                }

             //   Spinner_State.setSelection(adapter.getCount());

               // initialize_Spinner_State();

              //  selectedStateID=stateArrayList.get(Spinner_State.getSelectedItemPosition()).getStateId();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ParentControlId", ""+selectedCountryID);

                //System.out.println(LoginScreen.selectedBankID + "==" + MyToken.getToken() + "==" + LoginScreen.SelectedSchemeId+"=="+LoginScreen.selectedSchemeName);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_DistrictList_API() {

        System.out.println("Hit_DistrictList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDistrictListURL;
        final ProgressDialog pDialog = new ProgressDialog(DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                DistrictModel districtModel = gson.fromJson(response, DistrictModel.class);
                districtArrayList.clear();

                districtArrayList= districtModel.getDistrictList();
                String[] districtItems=new String[districtArrayList.size()];
                for(int i=0;i<districtArrayList.size();i++)
                {
                    districtItems[i]=districtArrayList.get(i).getDistrictName().toString();
                }

               // districtItems[districtArrayList.size()]="Select";

                HintAdapter<String> adapter = new HintAdapter<String>(DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, districtItems);
                Spinner_District.setAdapter(adapter);
                Spinner_District.setSelection(0);
                if (MainActivity.IsLogin) {

                    LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);

                    for(int i=0;i<districtModel.getDistrictList().size();i++)
                    {
                        if (userInfoModel.getDistrictId().equals(districtModel.getDistrictList().get(i).getDistrictId()))
                        {
                            Spinner_District.setSelection(i);
                        }
                    }


                }
                SelectedDistrictName = Spinner_District.getItemAtPosition(Spinner_District.getSelectedItemPosition()).toString();


                for(int i=0;i<districtModel.getDistrictList().size();i++)
                {
                    if (SelectedDistrictName.equals(districtModel.getDistrictList().get(i).getDistrictName()))
                    {
                        selectedDistrictID= districtArrayList.get(i).getDistrictId();
                    }
                }
              //  Spinner_District.setSelection(adapter.getCount());

               // selectedDistrictID=districtArrayList.get(Spinner_District.getSelectedItemPosition()).getDistrictId();


             //   initialize_Spinner_District();

                /*System.out.println("***** "+Spinner_District.getSelectedItem().toString());
                System.out.println(Spinner_State.getSelectedItem().toString());
                System.out.println(Spinner_Country.getSelectedItem().toString());*/


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ParentControlId", ""+selectedStateID);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    private void HitSalutationAPI() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getSalutationListURL;
        final ProgressDialog pDialog = new ProgressDialog(DepositorDetailsFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                SalutaionModel salutaionModel = gson.fromJson(response, SalutaionModel.class);

                MyToken.setToken(salutaionModel.getRefreshTokenValue());
                MyToken.setAuthorization(salutaionModel.getAcsessTokenValue());

                if (salutaionModel.getErrorCodes()!=null&&salutaionModel.getErrorCodes().matches("Success")&&salutaionModel.getSalutionList()!=null&&salutaionModel.getSalutionList().size()>0)
                {


                    salutionArrayList.clear();

                    salutionArrayList= (ArrayList<Salution>) salutaionModel.getSalutionList();

                    String[] salutationList=new String[salutaionModel.getSalutionList().size()];
                    for(int i=0;i<salutaionModel.getSalutionList().size();i++)
                    {
                        salutationList[i]=salutaionModel.getSalutionList().get(i).getSalutationName();
                    }




                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DepositorDetailsFragment.this.getActivity(), R.layout.single_row_spinner, salutationList);
                    Spinner_Marital_Status.setAdapter(adapter);
                    Spinner_Marital_Status.setSelection(0);
                    Hit_CountryList_API();
                }
                else
                {
                    showDialog_ServerError("Server Error !");
                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showDialog_ServerError("Server Error !");
                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("DepositorCategoryId", String.valueOf(Prefs.getInt("selectedDepositorId",1)) );
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("parentControlId2","0");


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //  strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }
    public class HintAdapter<String>
            extends ArrayAdapter<String> {


        public HintAdapter(@NonNull Context context, int resource, @NonNull String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public int getCount() {
            // don't display last item. It is used as hint.
            int count = super.getCount();
            return count > 0 ? count - 1 : count;
        }
    }

    public void showDialog_ServerError(String errorText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // getActivity().finish();
                dialog.dismiss();
                //System.exit(0);
            }
        });
    }
}
