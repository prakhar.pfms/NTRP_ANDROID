package testme.myapp.com.ntrp.module_nonregisteredusers.adapters;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsersFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_ConfirmInfoFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.FormDetail;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.PurposeDetaillst;

public class ConfirmInfoAdaptor extends RecyclerView.Adapter<ConfirmInfoAdaptor.MyViewHolder> {

    private List<PurposeDetaillst> purposeLists;
    private Context context;
    private NonRegisteredUsers_ConfirmInfoFragment nonRegisteredUsersFragment;

    public ConfirmInfoAdaptor(Context context, NonRegisteredUsers_ConfirmInfoFragment nonRegisteredUsersFragment, List<PurposeDetaillst> purposeList)
    {
        this.context=context;
        this.nonRegisteredUsersFragment=nonRegisteredUsersFragment;
        this.purposeLists=purposeList;

    }

    @Override
    public ConfirmInfoAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_view_non_registered_user_deposit_detail,parent, false);
        return new ConfirmInfoAdaptor.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ConfirmInfoAdaptor.MyViewHolder holder, int position) {

        PurposeDetaillst purposeList=purposeLists.get(position);


        holder.pao_name_textview_vlue.setText(purposeList.getPAOName());
        holder.ddo_name_textview_vlue.setText(purposeList.getDDOName());
        holder.ministry_textview_vlue.setText(purposeList.getControllerName());
        holder.amount_textview_vlue.setText(""+purposeList.getAmountValue());
        holder.payment_frequency_textview_vlue.setText(""+purposeList.getAccountingPeriod());

        SpannableString str = new SpannableString(purposeList.getPurposeDescription());
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        holder.purpose_textview_vlue.setText(str);
        holder.delete_purpose_layout.setVisibility(View.GONE);

//        holder.PurposeTextView.setSelected(true);
//        holder.PurposeTextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return purposeLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public LinearLayout delete_purpose_layout;
        public TextView purpose_textview_vlue,pao_name_textview_vlue,ddo_name_textview_vlue,ministry_textview_vlue,amount_textview_vlue,payment_frequency_textview_vlue;
        public MyViewHolder(View itemView) {
            super(itemView);
            purpose_textview_vlue= (TextView) itemView.findViewById(R.id.purpose_textview_vlue);
            pao_name_textview_vlue= (TextView) itemView.findViewById(R.id.pao_name_textview_vlue);
            ddo_name_textview_vlue= (TextView) itemView.findViewById(R.id.ddo_name_textview_vlue);
            ministry_textview_vlue= (TextView) itemView.findViewById(R.id.ministry_textview_vlue);
            amount_textview_vlue= (TextView) itemView.findViewById(R.id.amount_textview_vlue);
            payment_frequency_textview_vlue= (TextView) itemView.findViewById(R.id.payment_frequency_textview_vlue);
            delete_purpose_layout= (LinearLayout) itemView.findViewById(R.id.delete_purpose_layout);

        }
    }
}
