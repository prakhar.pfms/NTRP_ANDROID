package testme.myapp.com.ntrp.module_trackyourpayment.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainScreen;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.OpenPdfUrlFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.TrackYourPaymentFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.models.UserInputDetailModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.VerifyOTPModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.utils.AllUrls;

/**
 * Created by deepika.s on 21-06-2017.
 */

public class GetPdfTask extends AsyncTask {

    private Context context;

    private String pdfUrl,FileName,DownloadedPath;

    private ProgressDialog progressDialog;

    private ByteArrayOutputStream output;

    private Fragment fragment;

    public GetPdfTask(Context ctx,String pdfUrl,String FileName,Fragment fragment)
    {
        this.context=ctx;
        this.fragment=fragment;
        this.pdfUrl=pdfUrl;
        this.FileName="ntrp_details";//FileName;
    }
    private int responseCode;
    private String  response;
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog=new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Downloading...");
        progressDialog.show();
    }

    @Override
    protected Object doInBackground(Object[] params) {
       PostData();

        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        progressDialog.hide();
        progressDialog.dismiss();

//                OpenPdfUrlFragment transactionGridViewFragment = new OpenPdfUrlFragment();
//                transactionGridViewFragment.Url=output.toString();
//                FragmentTransaction ft = fragment.getActivity().getSupportFragmentManager().beginTransaction();
//                ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
//                ft.replace(R.id.container_frame, transactionGridViewFragment);
//                ft.commit();
        showDialog("File downloaded successfully to "+DownloadedPath);
    }

    private void showDialog(String warningText) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_warning);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }
    private void getByteArrayImage() throws Exception{
        URL url = new URL(pdfUrl);
        output = new ByteArrayOutputStream();
        URLConnection conn = url.openConnection();
        //conn.setRequestProperty("User-Agent", "Firefox");

        try (InputStream inputStream = conn.getInputStream()) {
            int n = 0;
            byte[] buffer = new byte[4096];
            while (-1 != (n = inputStream.read(buffer))) {
                output.write(buffer, 0, n);
            }
        }
        byte[] img = output.toByteArray();





        int i=img.length;//   System.out.println(img.length);

       // WriteBtn(img);
    }

    public void Hit_API_VerifyOTP_POSBased(final Activity activity, final Fragment trackYourPaymentFragment) {

        System.out.println("Verify Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = pdfUrl;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("IsMobileApi", "1");
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }



    private void PostData() {

        InputStream is = null;


        try
        {


            URL url = new URL(pdfUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestProperty("Content-type","application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("Authorization", "Bearer "+MyToken.getAuthorization());
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setUseCaches(false);
            //urlConnection.setRequestProperty("Accept-Encoding", "identity");

            Map<String,Object> params = new LinkedHashMap<>();

            params.put("RefreshTokenValue", MyToken.getToken());
            params.put("IsMobileApi", "1");


            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String,Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            Log.d("DEBUG", "The post data is: " + postData.toString());
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");
            urlConnection.setRequestProperty("Content-Length", Integer.toString(postDataBytes.length));
            urlConnection.getOutputStream().write(postDataBytes);
            urlConnection.connect();


            responseCode = urlConnection.getResponseCode();
            response = urlConnection.getResponseMessage();
            Log.d("DEBUG", "The response is: " + response);
            Log.d("DEBUG", "The response code is: " + responseCode);
            is = urlConnection.getInputStream();

            if (response.matches("OK")) {


                byte[] pdfByte = readBytes(is);

                WriteBtn(pdfByte);


            }

        }
        catch (UnsupportedEncodingException e) {
         //   e.printStackTrace();
        }
        catch(Exception exception)  {
          //  exception.printStackTrace();
        }

    }



    private void WriteBtn( byte[] pdfBytes) throws Exception {




        File root = android.os.Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        File dir = new File(root.getAbsolutePath());
        dir.mkdirs();
        File file = new File(dir, FileName+"_"+ SystemClock.currentThreadTimeMillis()+".pdf");

        try {
            FileOutputStream f = new FileOutputStream(file);


            byte[] arr=pdfBytes;
            f.write(arr);
            f.flush();
            f.close();
            MediaScannerConnection.scanFile(
                    context,
                    new String[]{file.getAbsolutePath()},
                    null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.v("grokkingandroid",
                                    "file " + path + " was scanned seccessfully: " + uri);
                            DownloadedPath=path+uri;

                        }
                    });

        } catch (FileNotFoundException e) {
          //  e.printStackTrace();
            android.util.Log
                    .i("tag",
                            "******* File not found. Did you add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } catch (IOException e) {
          //  e.printStackTrace();
        }





    }

    private byte[] readBytes(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 2048;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }
    private String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");

       
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    private String readErr(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }
    private String streamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }
}
