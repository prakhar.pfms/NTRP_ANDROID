package testme.myapp.com.ntrp.module_trackyourpayment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.activity.MainScreen;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.TrackYourPaymentFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.models.SendOTPModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TransctionGridModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.UserInputDetailModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.VerifyOTPModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.utils.AllUrls;

/**
 * Created by deepika.s on 27-06-2017.
 */

public class HitAPI {



    public static String MobileNo,Email;

    public static void Hit_API_SENDOTP(final Activity activity, final TrackYourPaymentFragment trackYourPaymentFragment, final String MobileNo, final String Email, final String CountrtyCode) {


    ///    HandleHttpsRequest.setSSL();

        HitAPI.MobileNo=MobileNo;
        HitAPI.Email=Email;

        System.out.println("Send Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.sendOTPURL;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                SendOTPModel sendOTPModel=gson.fromJson(response,SendOTPModel.class);


                MyToken.setToken(sendOTPModel.getRefreshTokenValue());
                MyToken.setAuthorization(sendOTPModel.getAcsessTokenValue());
                
                System.out.println(sendOTPModel.getResponseStatus());




                if (sendOTPModel.getResponseStatus().matches("Success")) {

                    trackYourPaymentFragment.Mobile_No_EditText.setText("");
                    trackYourPaymentFragment.all_Data_Matched();
                    if(TrackYourPaymentFragment.IsCountDownStart==false) {
                        trackYourPaymentFragment.setTimer(trackYourPaymentFragment.OTP_Expires_Textview,trackYourPaymentFragment.millisToGo);
                        TrackYourPaymentFragment.IsCountDownStart=true;
                    }

                    return;
                } else {

                    trackYourPaymentFragment.Security_Code_EditText.setText("");
                    trackYourPaymentFragment.generate_Captcha();
                    trackYourPaymentFragment.showDialog_Invalid_Email_Phone("Neither Mobile No. Nor Email is exists");
                    return;
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                trackYourPaymentFragment.showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                System.out.println( trackYourPaymentFragment.User_MobileNumber+"  "+trackYourPaymentFragment.User_EmailID+"  "+trackYourPaymentFragment.User_CountryCode);
                params.put("MobileNumber", MobileNo);
                params.put("EmailID", Email);
                params.put("countryCode", "91");
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };




      //  strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }



    public static void Hit_API_Token(final Activity activity, final TrackYourPaymentFragment trackYourPaymentFragment,final String MobileNo, final String Email, final String CountrtyCode) {
      //  HandleHttpsRequest.setSSL();
        System.out.println("Token API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(activity, R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                try {
                    TokenModel tokenModel=gson.fromJson(response,TokenModel.class);
                    MyToken.setToken(tokenModel.refresh_token);
                    MyToken.setAuthorization(tokenModel.access_token);
                    if(MyToken.getToken()!=null)
                    {

                        if (CheckNetworkConnection.getInstance(activity).isOnline())
                        {
                            // Hit_API_VerifyOTP(activity,trackYourPaymentFragment);
                            Hit_API_SENDOTP(activity,trackYourPaymentFragment,MobileNo,Email,CountrtyCode);
                        }
                        else {
                            Toast.makeText(activity, "Check Your Internet Connection!",
                                    Toast.LENGTH_LONG).show();

                        }
                    }
                    else
                    {
                        trackYourPaymentFragment.Enter_OTP_Code_EditText.setText("");
                        trackYourPaymentFragment.showDialog_False_OTP_Error("OTP entered by you, does not matches with the one which was sent to you.");
                    }
                }
                catch (Exception e)
                {
                    trackYourPaymentFragment.showDialog_ServerError("Invalid credential/Server Error, Please Try again later");
                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                trackYourPaymentFragment.showDialog_ServerError("Invalid credential/Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String username="";

                if (!Email.matches(""))
                {
                    username=Email;
                }
                else if (!MobileNo.matches(""))
                {
                    username=MobileNo;
                }

                params.put("UserName", username);
                params.put("AppType", "NTRP-APP");
                params.put("DeviceID", "123");
                params.put("AppVersion", "1.0");
                params.put("password", "123456");
                params.put("OS", "android");
                params.put("Operator", "airtel");
                params.put("grant_type", "password");
                return params;
            }


        };

      //  strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public static void Hit_API_Token_POSBased(String user_AtrNumber, final Activity activity, final TrackYourPaymentFragment trackYourPaymentFragment) {
      //  HandleHttpsRequest.setSSL();

        System.out.println("Token API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(activity, R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                try {



                TokenModel tokenModel=gson.fromJson(response,TokenModel.class);

                if (tokenModel!=null&&tokenModel.refresh_token!=null)
                {
                    MyToken.setToken(tokenModel.refresh_token);
                    MyToken.setAuthorization(tokenModel.access_token);
                }
                else
                {
                    trackYourPaymentFragment.showDialog_False_OTP_Error("Some error occured !");
                }


                if(MyToken.getToken()!=null)
                {

                    if (CheckNetworkConnection.getInstance(activity).isOnline())
                    {
                        Hit_API_VerifyOTP_POSBased(activity,trackYourPaymentFragment);
                    }
                    else {
                        Toast.makeText(activity, "Some error occured!",
                                Toast.LENGTH_LONG).show();

                    }
                }
                else
                {
                    trackYourPaymentFragment.Enter_OTP_Code_EditText.setText("");
                    trackYourPaymentFragment.showDialog_False_OTP_Error("Some error occured!");
                }
                }
                catch (Exception e)
                {
                    Toast.makeText(activity, "Some error occured!",
                            Toast.LENGTH_LONG).show();
                }



            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                trackYourPaymentFragment.showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("UserName", user_AtrNumber);
                params.put("AppType", "NTRP-APP");
                params.put("DeviceID", "123");
                params.put("AppVersion", "1.0");
                params.put("password", "123456");
                params.put("OS", "android");
                params.put("Operator", "airtel");
                params.put("grant_type", "password");
                params.put("Flag", "3");
                return params;
            }


        };

      //  strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public static void Hit_API_Token_CustomBased(String user_PassportNumber, final Activity activity, final TrackYourPaymentFragment trackYourPaymentFragment) {
        //HandleHttpsRequest.setSSL();

        System.out.println("Token API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(activity, R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                try {



                TokenModel tokenModel=gson.fromJson(response,TokenModel.class);
                if (tokenModel!=null&&tokenModel.refresh_token!=null)
                {
                    MyToken.setToken(tokenModel.refresh_token);
                    MyToken.setAuthorization(tokenModel.access_token);
                }
                else
                {
                    trackYourPaymentFragment.showDialog_False_OTP_Error("Some error occured !");
                }
                if(MyToken.getToken()!=null)
                {

                    if (CheckNetworkConnection.getInstance(activity).isOnline())
                    {
                        Hit_API_VerifyOTP_CustomBased(activity,trackYourPaymentFragment);
                    }
                    else {
                        Toast.makeText(activity, "Check Your Internet Connection!",
                                Toast.LENGTH_LONG).show();

                    }
                }
                else
                {
                    trackYourPaymentFragment.Enter_OTP_Code_EditText.setText("");
                    trackYourPaymentFragment.showDialog_False_OTP_Error("You have entered wrong details or OTP.");
                }
                }
                catch (Exception e)
                {
                    trackYourPaymentFragment.showDialog_False_OTP_Error("Some error occured !");
                }



            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                trackYourPaymentFragment.showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("UserName", user_PassportNumber);
                params.put("AppType", "NTRP-APP");
                params.put("DeviceID", "123");
                params.put("AppVersion", "1.0");
                params.put("password", "123456");
                params.put("OS", "android");
                params.put("Operator", "airtel");
                params.put("grant_type", "password");
                params.put("Flag", "4");
                return params;
            }


        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }



    public static void Hit_API_VerifyOTP(final Activity activity, final TrackYourPaymentFragment trackYourPaymentFragment, String inputOTP) {

        System.out.println("Verify Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.verifyOTPURL;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
               for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                VerifyOTPModel verifyOTPModel=gson.fromJson(response,VerifyOTPModel.class);
                MyToken.setToken(verifyOTPModel.getRefreshTokenValue());
                MyToken.setAuthorization(verifyOTPModel.getAcsessTokenValue());

                if (verifyOTPModel.getResponseStatus().matches("Successfully Verified"))
                {
                    trackYourPaymentFragment.verified_OTP("2022-2023",null,null,null,"0");
                    return;
                }
                else
                {
                    trackYourPaymentFragment.Enter_OTP_Code_EditText.setText("");
                    trackYourPaymentFragment.showDialog_False_OTP_Error(""+verifyOTPModel.getErrorCodes());
                    return;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                trackYourPaymentFragment.showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("MobileNumber", MobileNo);
                params.put("EmailId", Email);
                params.put("OTP", inputOTP);
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    public static void Hit_API_VerifyOTP_POSBased(final Activity activity, final TrackYourPaymentFragment trackYourPaymentFragment) {

        System.out.println("Verify Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.verifyOTPURL;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                VerifyOTPModel verifyOTPModel=gson.fromJson(response,VerifyOTPModel.class);
                MyToken.setToken(verifyOTPModel.getRefreshTokenValue());
                MyToken.setAuthorization(verifyOTPModel.getAcsessTokenValue());


                if (verifyOTPModel.getResponseStatus().matches("Successfully Verified"))
                {
                    UserInputDetailModel userInputDetailModel = (UserInputDetailModel) MainScreen.UserInputBundle.getParcelable("userInputDetailModel");
                    trackYourPaymentFragment.verified_OTP( "2022-2023",userInputDetailModel.AtrNumber, null, null,null);
                    return;
                }
                else if (verifyOTPModel.getResponseStatus().matches("This POS has no records found ..."))
                {
                    trackYourPaymentFragment.Enter_OTP_Code_EditText.setText("");
                    trackYourPaymentFragment.showDialog_Blue_POSDialog("Either we have not received this transaction info from the POS Issuing bank/ or this is not a valid ATRN");
                    return;

                }
                else if (verifyOTPModel.getResponseStatus().matches("Fail"))
                {
                    trackYourPaymentFragment.Enter_OTP_Code_EditText.setText("");
                    trackYourPaymentFragment.showDialog_False_OTP_Error(""+verifyOTPModel.getErrorCodes());
                    return;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                trackYourPaymentFragment.showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                UserInputDetailModel userInputDetailModel = (UserInputDetailModel) MainScreen.UserInputBundle.getParcelable("userInputDetailModel");
                params.put("IsPOS", "Yes");
                params.put("AtrNo", userInputDetailModel.AtrNumber);
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

      //  strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public static void Hit_API_VerifyOTP_CustomBased(final Activity activity, final TrackYourPaymentFragment trackYourPaymentFragment) {

        System.out.println("Verify Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.verifyOTPURL;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                VerifyOTPModel verifyOTPModel=gson.fromJson(response,VerifyOTPModel.class);
                MyToken.setToken(verifyOTPModel.getRefreshTokenValue());
                MyToken.setAuthorization(verifyOTPModel.getAcsessTokenValue());


                if (verifyOTPModel.getResponseStatus().matches("Successfully Verified"))
                {
                    UserInputDetailModel userInputDetailModel = (UserInputDetailModel) MainScreen.UserInputBundle.getParcelable("userInputDetailModel");
                    trackYourPaymentFragment.verified_OTP("2022-2023",null,userInputDetailModel.BrNumber, userInputDetailModel.PassportNumber,null);
                    return;
                }
                else if (verifyOTPModel.getResponseStatus().matches("BRNo. and passport no. has not found ..."))
                {
                    trackYourPaymentFragment.Enter_OTP_Code_EditText.setText("");
                    trackYourPaymentFragment.showDialog_Blue_POSDialog("Either we have not received this transaction info from the POS Issuing bank/ or this is not a valid ATRN");
                    return;

                }
                else  if (verifyOTPModel.getResponseStatus().matches("Fail"))
                {
                    trackYourPaymentFragment.Enter_OTP_Code_EditText.setText("");
                    trackYourPaymentFragment.showDialog_False_OTP_Error(""+verifyOTPModel.getErrorCodes());
                    return;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                trackYourPaymentFragment.showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                UserInputDetailModel userInputDetailModel = (UserInputDetailModel) MainScreen.UserInputBundle.getParcelable("userInputDetailModel");
                params.put("IsCustom", "Yes");
                params.put("BrNo", userInputDetailModel.BrNumber);
                params.put("PassportNo", userInputDetailModel.PassportNumber);
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }



    public static void Hit_API_GetTrackTransaction(final Activity activity, final TrackYourPaymentFragment trackYourPaymentFragment, final String FinancialYr, final String AtrNo, final String BrNo, final String PassportNo, final String needArchiveTransaction) {

        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTrackTransactionURL;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("tyu....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                TransctionGridModel transactionGridModel = gson.fromJson(response, TransctionGridModel.class);
                if(transactionGridModel.tracktransaction==null)
                {
                    System.out.println("track transactionGridModel is null");
                }
                else
                {
                    MyToken.setToken(transactionGridModel.RefreshTokenValue);
                    MyToken.setAuthorization(transactionGridModel.AcsessTokenValue);
                    System.out.println(transactionGridModel.tracktransaction);
                    MainScreen.TrackBundle.putParcelable("transactionGridModel", transactionGridModel);

                    trackYourPaymentFragment.changeFragment_TransactionGridViewFragment();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                trackYourPaymentFragment.showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                if(trackYourPaymentFragment.countryIndex==100&&MobileNo!=null)
                {

                    params.put("mobileno", MobileNo);
                    params.put("RefreshTokenValue", MyToken.getToken());
                    params.put("NTRPFY", FinancialYr);
                    params.put("ActiveInActive", needArchiveTransaction);
                    return params;
                }
                else if (Email!=null)
                {
                    params.put("EmailId", Email);
                    params.put("RefreshTokenValue", MyToken.getToken());
                    params.put("NTRPFY", FinancialYr);
                    return params;
                }
                else if (AtrNo!=null)
                {
                    params.put("IsPOS", "Yes");
                    params.put("AtrNo", AtrNo);
                    params.put("RefreshTokenValue", MyToken.getToken());
                    params.put("NTRPFY", FinancialYr);

                    return params;
                }
                else if (BrNo!=null)
                {
                    params.put("IsCustom", "Yes");
                    params.put("BrNo", BrNo);
                    params.put("PassportNo", PassportNo);
                    params.put("RefreshTokenValue", MyToken.getToken());
                    params.put("NTRPFY", FinancialYr);
                    return params;
                }
                else if (MainActivity.IsLogin)
                {
                   // params.put("mobileno", MobileNo !=null ? MobileNo : "");
                    params.put("RefreshTokenValue", MyToken.getToken());
                    params.put("NTRPFY", FinancialYr);
                    params.put("ActiveInActive", needArchiveTransaction);
                    params.put("userid", MainActivity.UserID);

                    return params;
                }

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }





}
