package testme.myapp.com.ntrp.adapters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.ButtonCards;

/**
 * Created by prakhar.s on 4/12/2017.
 */

public class MainPageButtonsAdapter extends RecyclerView.Adapter<MainPageButtonsAdapter.MyViewHolder>{
    private List<ButtonCards> buttonCardsList;

    public MainPageButtonsAdapter(List<ButtonCards> buttonCardsList) {
        this.buttonCardsList = buttonCardsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.button_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ButtonCards movie = buttonCardsList.get(position);
        holder.button.setText(movie.getButton_Text());

    }

    @Override
    public int getItemCount() {
        return buttonCardsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Button button;

        public MyViewHolder(View view) {
            super(view);

            button = (Button) view.findViewById(R.id.button);

        }
    }


}
