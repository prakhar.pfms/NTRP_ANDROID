package testme.myapp.com.ntrp.module_publicationdivisonstore.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import java.security.SecureRandom;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.ConvertNumberToWords;


public abstract class BaseFragment extends Fragment {

    private ProgressDialog mProgressDialog;

    public BaseFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_base, container, false);
    }

    public void init() {
        initView();
        bindDataWithUi();
        initListener();
    }


    protected abstract void initView();

    protected abstract void initListener();

    protected abstract void bindDataWithUi();

    public void addFragment(Fragment fragment, int containerId) {
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .add(containerId, fragment)
                    .commitAllowingStateLoss();
        }
    }

    public void replaceFragment(Fragment fragment, int containerId, boolean addToBackStack) {
        if (getActivity() != null) {
            FragmentTransaction replace = getActivity().getSupportFragmentManager().beginTransaction();
            replace.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
            replace.replace(containerId, fragment);
            if (addToBackStack) {
                replace.addToBackStack(fragment.getClass().getName());
            }
            replace.commitAllowingStateLoss();
        }
    }

    public void removeFragment() {
        if (getActivity() != null) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            if (fm.getBackStackEntryCount() > 0)
                fm.popBackStack();
        }
    }

    public void hideSoftInputKeyboard() {
        if (getActivity() == null) return;
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showLoadingDialog() {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.show();
            //mProgressDialog.setContentView(R.layout.dialog_progress);
        }
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    public void hideLoadingDialog() {

        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public void putRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }

    public boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public String set_validation_On_Input_Amount_String(String bigString) {
        // tempAmountValue=bigString;
        String finalString = "RUPEES ";
        int length = bigString.length();
        if (length == 10) {
            String first = bigString.substring(0, 3);
            String last = bigString.substring(3, length);
            int firstvalue = Integer.parseInt(first);
            int lastvalue = Integer.parseInt(last);
            finalString = finalString + ConvertNumberToWords.convertToWords(firstvalue);
            finalString = finalString + " CRORE(S) ";
            finalString = finalString + ConvertNumberToWords.convertToWords(lastvalue);
        } else if (length == 11) {
            String first = bigString.substring(0, 4);
            String last = bigString.substring(4, length);
            int firstvalue = Integer.parseInt(first);
            int lastvalue = Integer.parseInt(last);
            finalString = finalString + ConvertNumberToWords.convertToWords(firstvalue);
            finalString = finalString + " CRORE(S) ";
            finalString = finalString + ConvertNumberToWords.convertToWords(lastvalue);
        } else if (length < 10) {
            finalString = finalString + ConvertNumberToWords.convertToWords(Integer.parseInt(bigString));
        } else {
            finalString = finalString + "";
        }
        finalString = finalString + "ONLY";

        //FinalAmountText=finalString;
        return finalString;

    }

    public String HashingSha256(String Input) {
        final HashFunction hashFunction = Hashing.sha256();
        final HashCode hc = hashFunction
                .newHasher()
                .putString(Input, Charsets.UTF_8)
                .hash();
        final String sha256 = hc.toString();
        return sha256;
    }
    public  String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        SecureRandom rnd = new SecureRandom();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }
    public boolean checkForPermission() {
        if ((ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
            return true;

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE},1000);
            return false;
        }
    }
}