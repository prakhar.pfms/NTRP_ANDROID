package testme.myapp.com.ntrp.module_trackyourpayment.mycaptcha;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;

import java.io.CharArrayWriter;
import java.util.ArrayList;
import java.util.Random;

import testme.myapp.com.ntrp.R;


/**
 * Created by prakhar.s on 4/18/2017.
 */

public class TextCaptcha extends Captcha {

    protected TextOptions options;
    private int wordLength;
    private char mCh;
    private int bgWidth;
    public String input_captcha_string=null;
    Context context;
    public enum TextOptions {
        UPPERCASE_ONLY,
        LOWERCASE_ONLY,
        NUMBERS_ONLY,
        LETTERS_ONLY,
        NUMBERS_AND_LETTERS
    }

    public TextCaptcha(int wordLength, TextOptions opt) {
        new TextCaptcha(0, 0, wordLength, opt);

    }

    public TextCaptcha(int width, int height, int wordLength, TextOptions opt) {
        bgWidth=width;
        setHeight(height);
        setWidth(width);
        this.options = opt;
        usedColors = new ArrayList<>();
        this.wordLength = wordLength;
        this.image = image();
    }

    @Override
    protected Bitmap image() {
        LinearGradient gradient = new LinearGradient(0, 0, getWidth() / this.wordLength, getHeight() / 2, color(), color(), Shader.TileMode.CLAMP);
        Paint p = new Paint();
        p.setDither(true);
        //p.setShader(gradient);
        p.setColor(Color.WHITE);
        Bitmap bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        Paint p2 = new Paint();
        p2.setColor(Color.LTGRAY);
        c.drawRect(0, 0, getWidth(), getHeight(), p2);
        c.drawRect(5, 5, getWidth()-5, getHeight()-5, p);
        Paint tp = new Paint();
        tp.setDither(true);
        tp.setTextSize(getWidth() / getHeight() * 120);
        tp.setAntiAlias(true);
        tp.setTypeface(Typeface.create("Arial", Typeface.BOLD_ITALIC));
        tp.setColor(Color.rgb(3,86,129));

        Random r = new Random(System.currentTimeMillis());
        CharArrayWriter cab = new CharArrayWriter();
        this.answer = "";
        for (int i = 0; i < this.wordLength; i++) {
            char ch = ' ';
            switch (options) {
                case UPPERCASE_ONLY:
                    ch = (char) (r.nextInt(91 - 65) + (65));
                    break;
                case LOWERCASE_ONLY:
                    ch = (char) (r.nextInt(123 - 97) + (97));
                    break;
                case NUMBERS_ONLY:
                    ch = (char) (r.nextInt(58 - 49) + (49));
                    break;
                case LETTERS_ONLY:
                    ch = getLetters(r);
                    break;
                case NUMBERS_AND_LETTERS:
                    ch = getLettersNumbers(r);
                    break;
                default:
                    ch = getLettersNumbers(r);
                    break;
            }
            cab.append(ch);
            this.answer += ch;
        }
        Canvas cc = new Canvas(bitmap);
        char[] data = cab.toCharArray();
        for (int i = 0; i < data.length; i++) {
            //this.x += (30 - (3 * this.wordLength)) + (Math.abs(r.nextInt()) % (65 - (1.2 * this.wordLength)));
            //this.y = 50 + Math.abs(r.nextInt()) % 50;
            this.x +=100;
            this.y = 200 ;

            //tp.setTextSkewX(r.nextFloat() - r.nextFloat());
            //tp.setColor(Color.BLACK);
            //cc.drawText(data, i, 1, this.x, this.y, tp);

            tp.setTextSkewX(0);
        }
        //cc.drawText(answer,getWidth()/2-answer.length()*2,200,tp);
        c.drawText(answer,getWidth()/2-tp.measureText(answer)/2,getHeight()/2-((tp.descent()+tp.ascent())/2),tp);
        input_captcha_string=answer;
        return bitmap;
    }

    private char getLetters(Random r) {
        int rint = (r.nextInt(123 - 65) + (65));
        if (((rint > 90) && (rint < 97)))
            getLetters(r);
        else
            mCh = (char) rint;
        return mCh;
    }

    private char getLettersNumbers(Random r) {
        int rint = (r.nextInt(123 - 49) + (49));

        if (((rint > 90) && (rint < 97)))
            getLettersNumbers(r);
        else if (((rint > 57) && (rint < 65)))
            getLettersNumbers(r);
        else
            mCh = (char) rint;
        return mCh;
    }
}
