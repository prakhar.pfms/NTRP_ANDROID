package testme.myapp.com.ntrp.module_nonregisteredusers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NonRegisterUserGetConfirmInfoModel {


    @SerializedName("DepositerDetailslst")
    @Expose
    private List<DepositerDetailslst> depositerDetailslst = null;
    @SerializedName("SwachhBharatlst")
    @Expose
    private Object swachhBharatlst;
    @SerializedName("PurposeDetaillst")
    @Expose
    private List<PurposeDetaillst> purposeDetaillst = null;
    @SerializedName("AdditionalChargedetailslst")
    @Expose
    private List<Object> additionalChargedetailslst = null;
    @SerializedName("ShoppinngDepositorDetails")
    @Expose
    private Object shoppinngDepositorDetails;
    @SerializedName("ShoppinngDepositorDetailslst")
    @Expose
    private Object shoppinngDepositorDetailslst;
    @SerializedName("LstmodProductDetails")
    @Expose
    private Object lstmodProductDetails;
    @SerializedName("CbecPurposeslst")
    @Expose
    private Object cbecPurposeslst;
    @SerializedName("CbecDetaillst")
    @Expose
    private Object cbecDetaillst;
    @SerializedName("TotalPurposeAmount")
    @Expose
    private Double totalPurposeAmount;
    @SerializedName("TotalAddtionalChangesamount")
    @Expose
    private Double totalAddtionalChangesamount;
    @SerializedName("SwachhBharatCurrencyCodeInWords")
    @Expose
    private Object swachhBharatCurrencyCodeInWords;
    @SerializedName("PurposeCurrencyCodeInWords")
    @Expose
    private String purposeCurrencyCodeInWords;
    @SerializedName("AdditionalchargeCurrencyCodeInWords")
    @Expose
    private Object additionalchargeCurrencyCodeInWords;
    @SerializedName("ConversionPurposeCurrencyCode")
    @Expose
    private String conversionPurposeCurrencyCode;
    @SerializedName("ConversionAddtionalCurrencyCode")
    @Expose
    private Object conversionAddtionalCurrencyCode;
    @SerializedName("ConversionAddtionalXMLAmount")
    @Expose
    private Object conversionAddtionalXMLAmount;
    @SerializedName("XMLAddtionalCurrencycode")
    @Expose
    private Object xMLAddtionalCurrencycode;
    @SerializedName("XMLCurrencycode")
    @Expose
    private String xMLCurrencycode;
    @SerializedName("ConversionPurposeXMLAmount")
    @Expose
    private String conversionPurposeXMLAmount;
    @SerializedName("ErrorCodes")
    @Expose
    private String errorCodes;
    @SerializedName("ResponseStatus")
    @Expose
    private String responseStatus;
    @SerializedName("XmlDocumentResult")
    @Expose
    private Object xmlDocumentResult;
    @SerializedName("AcsessTokenValue")
    @Expose
    private String acsessTokenValue;
    @SerializedName("RefreshTokenValue")
    @Expose
    private String refreshTokenValue;
    @SerializedName("ResponseEmail")
    @Expose
    private Object responseEmail;
    @SerializedName("ResponseMobileNo")
    @Expose
    private Object responseMobileNo;
    @SerializedName("ResponseUserType_CBEC")
    @Expose
    private Object responseUserTypeCBEC;
    @SerializedName("ResponseFirstName")
    @Expose
    private Object responseFirstName;
    @SerializedName("ResponseLastName")
    @Expose
    private Object responseLastName;
    @SerializedName("ResponseCountryCode")
    @Expose
    private Object responseCountryCode;
    @SerializedName("ResponseUserId")
    @Expose
    private Integer responseUserId;
    @SerializedName("PdfFileByte")
    @Expose
    private Object pdfFileByte;
    @SerializedName("XmlDreports")
    @Expose
    private Object xmlDreports;
    @SerializedName("ReceiptEntryId")
    @Expose
    private Integer receiptEntryId;
    @SerializedName("ShippingCharges")
    @Expose
    private Object shippingCharges;
    @SerializedName("PubUserType")
    @Expose
    private Integer pubUserType;
    @SerializedName("DisplayPopup")
    @Expose
    private Object displayPopup;
    @SerializedName("RowNo")
    @Expose
    private Object rowNo;
    @SerializedName("PaymentPurposeID")
    @Expose
    private Integer paymentPurposeID;
    @SerializedName("TotalDiscount")
    @Expose
    private Object totalDiscount;
    @SerializedName("PageCount")
    @Expose
    private Integer pageCount;

    public List<DepositerDetailslst> getDepositerDetailslst() {
        return depositerDetailslst;
    }

    public void setDepositerDetailslst(List<DepositerDetailslst> depositerDetailslst) {
        this.depositerDetailslst = depositerDetailslst;
    }

    public Object getSwachhBharatlst() {
        return swachhBharatlst;
    }

    public void setSwachhBharatlst(Object swachhBharatlst) {
        this.swachhBharatlst = swachhBharatlst;
    }

    public List<PurposeDetaillst> getPurposeDetaillst() {
        return purposeDetaillst;
    }

    public void setPurposeDetaillst(List<PurposeDetaillst> purposeDetaillst) {
        this.purposeDetaillst = purposeDetaillst;
    }

    public List<Object> getAdditionalChargedetailslst() {
        return additionalChargedetailslst;
    }

    public void setAdditionalChargedetailslst(List<Object> additionalChargedetailslst) {
        this.additionalChargedetailslst = additionalChargedetailslst;
    }

    public Object getShoppinngDepositorDetails() {
        return shoppinngDepositorDetails;
    }

    public void setShoppinngDepositorDetails(Object shoppinngDepositorDetails) {
        this.shoppinngDepositorDetails = shoppinngDepositorDetails;
    }

    public Object getShoppinngDepositorDetailslst() {
        return shoppinngDepositorDetailslst;
    }

    public void setShoppinngDepositorDetailslst(Object shoppinngDepositorDetailslst) {
        this.shoppinngDepositorDetailslst = shoppinngDepositorDetailslst;
    }

    public Object getLstmodProductDetails() {
        return lstmodProductDetails;
    }

    public void setLstmodProductDetails(Object lstmodProductDetails) {
        this.lstmodProductDetails = lstmodProductDetails;
    }

    public Object getCbecPurposeslst() {
        return cbecPurposeslst;
    }

    public void setCbecPurposeslst(Object cbecPurposeslst) {
        this.cbecPurposeslst = cbecPurposeslst;
    }

    public Object getCbecDetaillst() {
        return cbecDetaillst;
    }

    public void setCbecDetaillst(Object cbecDetaillst) {
        this.cbecDetaillst = cbecDetaillst;
    }

    public Double getTotalPurposeAmount() {
        return totalPurposeAmount;
    }

    public void setTotalPurposeAmount(Double totalPurposeAmount) {
        this.totalPurposeAmount = totalPurposeAmount;
    }

    public Double getTotalAddtionalChangesamount() {
        return totalAddtionalChangesamount;
    }

    public void setTotalAddtionalChangesamount(Double totalAddtionalChangesamount) {
        this.totalAddtionalChangesamount = totalAddtionalChangesamount;
    }

    public Object getSwachhBharatCurrencyCodeInWords() {
        return swachhBharatCurrencyCodeInWords;
    }

    public void setSwachhBharatCurrencyCodeInWords(Object swachhBharatCurrencyCodeInWords) {
        this.swachhBharatCurrencyCodeInWords = swachhBharatCurrencyCodeInWords;
    }

    public String getPurposeCurrencyCodeInWords() {
        return purposeCurrencyCodeInWords;
    }

    public void setPurposeCurrencyCodeInWords(String purposeCurrencyCodeInWords) {
        this.purposeCurrencyCodeInWords = purposeCurrencyCodeInWords;
    }

    public Object getAdditionalchargeCurrencyCodeInWords() {
        return additionalchargeCurrencyCodeInWords;
    }

    public void setAdditionalchargeCurrencyCodeInWords(Object additionalchargeCurrencyCodeInWords) {
        this.additionalchargeCurrencyCodeInWords = additionalchargeCurrencyCodeInWords;
    }

    public String getConversionPurposeCurrencyCode() {
        return conversionPurposeCurrencyCode;
    }

    public void setConversionPurposeCurrencyCode(String conversionPurposeCurrencyCode) {
        this.conversionPurposeCurrencyCode = conversionPurposeCurrencyCode;
    }

    public Object getConversionAddtionalCurrencyCode() {
        return conversionAddtionalCurrencyCode;
    }

    public void setConversionAddtionalCurrencyCode(Object conversionAddtionalCurrencyCode) {
        this.conversionAddtionalCurrencyCode = conversionAddtionalCurrencyCode;
    }

    public Object getConversionAddtionalXMLAmount() {
        return conversionAddtionalXMLAmount;
    }

    public void setConversionAddtionalXMLAmount(Object conversionAddtionalXMLAmount) {
        this.conversionAddtionalXMLAmount = conversionAddtionalXMLAmount;
    }

    public Object getXMLAddtionalCurrencycode() {
        return xMLAddtionalCurrencycode;
    }

    public void setXMLAddtionalCurrencycode(Object xMLAddtionalCurrencycode) {
        this.xMLAddtionalCurrencycode = xMLAddtionalCurrencycode;
    }

    public String getXMLCurrencycode() {
        return xMLCurrencycode;
    }

    public void setXMLCurrencycode(String xMLCurrencycode) {
        this.xMLCurrencycode = xMLCurrencycode;
    }

    public String getConversionPurposeXMLAmount() {
        return conversionPurposeXMLAmount;
    }

    public void setConversionPurposeXMLAmount(String conversionPurposeXMLAmount) {
        this.conversionPurposeXMLAmount = conversionPurposeXMLAmount;
    }

    public String getErrorCodes() {
        return errorCodes;
    }

    public void setErrorCodes(String errorCodes) {
        this.errorCodes = errorCodes;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getXmlDocumentResult() {
        return xmlDocumentResult;
    }

    public void setXmlDocumentResult(Object xmlDocumentResult) {
        this.xmlDocumentResult = xmlDocumentResult;
    }

    public String getAcsessTokenValue() {
        return acsessTokenValue;
    }

    public void setAcsessTokenValue(String acsessTokenValue) {
        this.acsessTokenValue = acsessTokenValue;
    }

    public String getRefreshTokenValue() {
        return refreshTokenValue;
    }

    public void setRefreshTokenValue(String refreshTokenValue) {
        this.refreshTokenValue = refreshTokenValue;
    }

    public Object getResponseEmail() {
        return responseEmail;
    }

    public void setResponseEmail(Object responseEmail) {
        this.responseEmail = responseEmail;
    }

    public Object getResponseMobileNo() {
        return responseMobileNo;
    }

    public void setResponseMobileNo(Object responseMobileNo) {
        this.responseMobileNo = responseMobileNo;
    }

    public Object getResponseUserTypeCBEC() {
        return responseUserTypeCBEC;
    }

    public void setResponseUserTypeCBEC(Object responseUserTypeCBEC) {
        this.responseUserTypeCBEC = responseUserTypeCBEC;
    }

    public Object getResponseFirstName() {
        return responseFirstName;
    }

    public void setResponseFirstName(Object responseFirstName) {
        this.responseFirstName = responseFirstName;
    }

    public Object getResponseLastName() {
        return responseLastName;
    }

    public void setResponseLastName(Object responseLastName) {
        this.responseLastName = responseLastName;
    }

    public Object getResponseCountryCode() {
        return responseCountryCode;
    }

    public void setResponseCountryCode(Object responseCountryCode) {
        this.responseCountryCode = responseCountryCode;
    }

    public Integer getResponseUserId() {
        return responseUserId;
    }

    public void setResponseUserId(Integer responseUserId) {
        this.responseUserId = responseUserId;
    }

    public Object getPdfFileByte() {
        return pdfFileByte;
    }

    public void setPdfFileByte(Object pdfFileByte) {
        this.pdfFileByte = pdfFileByte;
    }

    public Object getXmlDreports() {
        return xmlDreports;
    }

    public void setXmlDreports(Object xmlDreports) {
        this.xmlDreports = xmlDreports;
    }

    public Integer getReceiptEntryId() {
        return receiptEntryId;
    }

    public void setReceiptEntryId(Integer receiptEntryId) {
        this.receiptEntryId = receiptEntryId;
    }

    public Object getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(Object shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public Integer getPubUserType() {
        return pubUserType;
    }

    public void setPubUserType(Integer pubUserType) {
        this.pubUserType = pubUserType;
    }

    public Object getDisplayPopup() {
        return displayPopup;
    }

    public void setDisplayPopup(Object displayPopup) {
        this.displayPopup = displayPopup;
    }

    public Object getRowNo() {
        return rowNo;
    }

    public void setRowNo(Object rowNo) {
        this.rowNo = rowNo;
    }

    public Integer getPaymentPurposeID() {
        return paymentPurposeID;
    }

    public void setPaymentPurposeID(Integer paymentPurposeID) {
        this.paymentPurposeID = paymentPurposeID;
    }

    public Object getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Object totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

}
