package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.adaptors;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.PaymentFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.AggregatorListModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.BankListCorrespondingToSelectedAggregatorModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.ChannelListModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

public class ChannelListAdaptor extends RecyclerView.Adapter<ChannelListAdaptor.MyViewHolder> {

    private ChannelListModel channelListModel;
    private PaymentFragment fragment;
    private Context context;
    private RadioButton lastCheckedRB = null;

    public ChannelListAdaptor(Context context, PaymentFragment fragment, ChannelListModel channelListModel) {
        this.context = context;
        this.fragment = fragment;
        this.channelListModel = channelListModel;

    }

    @Override
    public ChannelListAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_view_channel, parent, false);
        return new ChannelListAdaptor.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChannelListAdaptor.MyViewHolder holder, int position) {



        holder.aggregator_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton checked_rb = (RadioButton) group.findViewById(checkedId);
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }

                //store the clicked radiobutton
                lastCheckedRB = checked_rb;

            }
        });
        Picasso.with(context)
                .load("https://training.pfms.gov.in/Bharatkosh/App_Themes/Receipt/images/"+channelListModel.getAggregatorCardTypeList().get(position).getAggregatorLogoName())
                .placeholder(R.drawable.loading_error_image)
                .error(R.drawable.loading_error_image)
                .into(holder.payment_gateway_image);


        holder.aggregator_radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PaymentFragment.BankId=String.valueOf(channelListModel.getAggregatorCardTypeList().get(holder.getAdapterPosition()).getAggregatorID());


            }
        });




    }






    @Override
    public int getItemCount() {
        return channelListModel.getAggregatorCardTypeList().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public RadioButton aggregator_radio_button;
        public RadioGroup aggregator_radio_group;

        public ImageView payment_gateway_image;
        public MyViewHolder(View itemView) {
            super(itemView);


            aggregator_radio_button = (RadioButton) itemView.findViewById(R.id.aggregator_radio_button);
            payment_gateway_image = (ImageView) itemView.findViewById(R.id.payment_gateway_image);
            aggregator_radio_group = (RadioGroup) itemView.findViewById(R.id.aggregator_radio_group);


        }
    }


}


