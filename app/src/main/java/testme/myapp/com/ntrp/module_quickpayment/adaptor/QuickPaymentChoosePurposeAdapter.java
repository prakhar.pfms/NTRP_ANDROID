package testme.myapp.com.ntrp.module_quickpayment.adaptor;

import android.app.Dialog;
import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.PurposeFunctionalMinisty;
import testme.myapp.com.ntrp.module_quickpayment.fragments.QuickPaymentFragment;
import testme.myapp.com.ntrp.utils.MessageEvent;

public class QuickPaymentChoosePurposeAdapter extends RecyclerView.Adapter<QuickPaymentChoosePurposeAdapter.MyViewHolder> {

    private List<PurposeFunctionalMinisty> purposeLists;
    private Context context;
    private QuickPaymentFragment nonRegisteredUsersFragment;
    private Dialog ChoosepurposeDialog;
    public QuickPaymentChoosePurposeAdapter(Context context, QuickPaymentFragment nonRegisteredUsersFragment, List<PurposeFunctionalMinisty> purposeList, Dialog ChoosepurposeDialog)
    {
        this.context=context;
        this.nonRegisteredUsersFragment=nonRegisteredUsersFragment;
        this.purposeLists=purposeList;
        this.ChoosepurposeDialog=ChoosepurposeDialog;
    }

    @Override
    public QuickPaymentChoosePurposeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_choose_purpose_row,parent, false);
        return new QuickPaymentChoosePurposeAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(QuickPaymentChoosePurposeAdapter.MyViewHolder holder, int position) {

        PurposeFunctionalMinisty purposeList=purposeLists.get(position);
        holder.PurposeTextView.setText(purposeList.getPurposeDescription());
        holder.PaymentType_TextView.setText(purposeList.getPaymentTypeDescription());
        holder.FunctionHead_TextView.setText(purposeList.getFuncHeadDescription());
        holder.Ministry_TextView.setText(purposeList.getControllerName());

        SpannableString str = new SpannableString(purposeList.getPurposeDescription());
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        holder.PurposeTextView.setText(str);

        holder.PurposeTextView.setSelected(true);
        holder.PaymentType_TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("clicked.......textview..");

                EventBus.getDefault().post(new MessageEvent(
                        purposeList.getPurposeDescription(),
                        purposeList.getPaymentTypeDescription(),
                        purposeList.getFuncHeadDescription(),
                        purposeList.getControllerName(),purposeList
                ));

                ChoosepurposeDialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return purposeLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        public TextView PurposeTextView,PaymentType_TextView,FunctionHead_TextView,Ministry_TextView;
        public MyViewHolder(View itemView) {
            super(itemView);
            PurposeTextView= (TextView) itemView.findViewById(R.id.purpose_textview);
            PaymentType_TextView= (TextView) itemView.findViewById(R.id.payment_type_textview);
            FunctionHead_TextView= (TextView) itemView.findViewById(R.id.function_head_textview);
            Ministry_TextView= (TextView) itemView.findViewById(R.id.ministry_textview);
        }
    }
}
