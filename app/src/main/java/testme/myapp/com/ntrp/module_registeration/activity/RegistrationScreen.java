package testme.myapp.com.ntrp.module_registeration.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_registeration.fragments.RegisterFragment;



/**
 * Created by deepika.s on 05-05-2017.
 */

public class RegistrationScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_registration_screen);
        changeFragment_RegisterFragment();
    }
    private void changeFragment_RegisterFragment() {

        Fragment registerFragment = new RegisterFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_frame, registerFragment);
        ft.commit();
    }

}
