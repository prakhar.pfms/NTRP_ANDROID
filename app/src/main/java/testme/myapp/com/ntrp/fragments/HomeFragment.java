package testme.myapp.com.ntrp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.adapters.HomeAdaptor;
import testme.myapp.com.ntrp.module_login.fragments.LoginFragment;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeFragment extends Fragment {

    @BindView(R.id.home_recycler_view)
    RecyclerView recyclerViewBottom;

    @BindView(R.id.sendotp_button)
    Button sendotp_Button;
    private List<String> menu_top_names = new ArrayList<>();

    private List<String> menu_names = new ArrayList<>();
    private List<Integer> image_names = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.home_fragment, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle(this.getActivity().getResources().getString(R.string.menu_home));
        initializeData();
        return view;
    }

    private void initializeData() {
        menu_top_names.clear();
        menu_names.clear();
        image_names.clear();

        menu_top_names.add("");
        menu_top_names.add("");

        //menu_top_names.add(this.getActivity().getResources().getString(R.string.online_donation_swachh_bharat_kosh));
        //menu_top_names.add("");
        //menu_top_names.add("");


        menu_names.add(this.getActivity().getResources().getString(R.string.track_payment));
        menu_names.add(this.getActivity().getResources().getString(R.string.donate_to_swachh_bharat_kosh));
        //menu_names.add(this.getActivity().getResources().getString(R.string.current_receipt));
        //menu_names.add(this.getActivity().getResources().getString(R.string.non_registered_user));



        image_names.add(R.drawable.trackpayment);
        image_names.add(R.drawable.swachhbharat);
        //image_names.add(R.drawable.currentreceipt);
        //image_names.add(R.drawable.nonregistereduser);


        recyclerViewBottom.setHasFixedSize(true);

        GridLayoutManager mLayoutManager = new GridLayoutManager(this.getContext(), 1);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);

        recyclerViewBottom.setLayoutManager(mLayoutManager);

        HomeAdaptor bottomAdaptor = new HomeAdaptor(menu_names, image_names,HomeFragment.this,menu_top_names);
        recyclerViewBottom.setAdapter(bottomAdaptor);
        bottomAdaptor.notifyDataSetChanged();
    }


    @OnClick(R.id.sendotp_button)
    public void onClick() {

       LoginFragment newFragment = new LoginFragment();
       FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
       transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
       transaction.replace(R.id.container_frame, newFragment);
       transaction.addToBackStack(null);
       transaction.commit();
    }
}
