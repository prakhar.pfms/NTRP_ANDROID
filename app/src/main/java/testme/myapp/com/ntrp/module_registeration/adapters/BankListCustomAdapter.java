package testme.myapp.com.ntrp.module_registeration.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_registeration.models.BankArrayList;


/**
 * Created by deepika.s on 11-07-2017.
 */

public class BankListCustomAdapter extends BaseAdapter {
    private List<BankArrayList> bankArrayList;
    private static LayoutInflater inflater=null;

    BankArrayList bankList=null;
    public BankListCustomAdapter(Activity a, List<BankArrayList> bankArrayList) {

        this.bankArrayList = bankArrayList;
        inflater = ( LayoutInflater )a.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return bankArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public static class MyViewHolder{

        public TextView textView;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        DepositorListCustomAdapter.MyViewHolder holder;
        if(convertView==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            view = inflater.inflate(R.layout.single_row_spinner_big, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new DepositorListCustomAdapter.MyViewHolder();
            holder.textView = (TextView) view.findViewById(R.id.cust_view);


            /************  Set holder with LayoutInflater ************/
            view.setTag( holder );
        }
        else
            holder=(DepositorListCustomAdapter.MyViewHolder)view.getTag();
        if(bankArrayList.size()<=0)
        {
            holder.textView.setText("No Data");

        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            bankList=null;
            bankList = (BankArrayList) bankArrayList.get( position );

            /************  Set Model values in Holder elements ***********/

            holder.textView.setText( bankList.BankName );



            /******** Set Item Click Listner for LayoutInflater for each row *******/


        }
        return view;
    }


}
