package testme.myapp.com.ntrp.module_publicationdivisonstore.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.MyCustomToast;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.PaymentFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.SubmitConfirmInfoModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_ConfirmInfoFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.NonRegisterUserGetConfirmInfoModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.adapter.PurposeDetailsConfirmInfoAdapter;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ConfirmInfoResponseModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.DepositorAddressModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ShoppinngDepositorDetailModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;


public class PublicationConfirmInfoFragment extends BaseFragment {
    private View mView;
    private DepositorAddressModel depositorAddressModel;
    private TextView tvName, tvMobileNo, tvEmail, tvAddress1, tvAddress2, tvDistrict, tvCity, tvState, tvCountry, tvPincode,
            tvShipAddress1, tvShipAddress2, tvShipDistrict, tvShipCity, tvShipState, tvShipCountry, tvShipPincode,
            tvTotalProductPrice, tvAmountInWords, tvDiscountAmt, lblShippingCharges, tvShippingCharges, tvTotalPayableAmount;
    private RecyclerView rvData;
    private PurposeDetailsConfirmInfoAdapter purposeDetailsConfirmInfoAdapter;
    private ArrayList<ShoppinngDepositorDetailModel> purposeDetailsList;
    private Button btnConfirm;
    //private String receiptEntryId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_publication_confirm_info, container, false);
        init();
        return mView;
    }

    @Override
    protected void initView() {
        tvName = mView.findViewById(R.id.tvName);
        tvMobileNo = mView.findViewById(R.id.tvMobileNo);
        tvEmail = mView.findViewById(R.id.tvEmail);
        tvAddress1 = mView.findViewById(R.id.tvAddress1);
        tvAddress2 = mView.findViewById(R.id.tvAddress2);
        tvDistrict = mView.findViewById(R.id.tvDistrict);
        tvCity = mView.findViewById(R.id.tvCity);
        tvState = mView.findViewById(R.id.tvState);
        tvCountry = mView.findViewById(R.id.tvCountry);
        tvPincode = mView.findViewById(R.id.tvPincode);
        tvShipAddress1 = mView.findViewById(R.id.tvShipAddress1);
        tvShipAddress2 = mView.findViewById(R.id.tvShipAddress2);
        tvShipDistrict = mView.findViewById(R.id.tvShipDistrict);
        tvShipCity = mView.findViewById(R.id.tvShipCity);
        tvShipState = mView.findViewById(R.id.tvShipState);
        tvShipCountry = mView.findViewById(R.id.tvShipCountry);
        tvShipPincode = mView.findViewById(R.id.tvShipPincode);
        tvTotalProductPrice = mView.findViewById(R.id.tvTotalProductPrice);
        tvAmountInWords = mView.findViewById(R.id.tvAmountInWords);
        tvDiscountAmt = mView.findViewById(R.id.tvDiscountAmt);
        lblShippingCharges = mView.findViewById(R.id.lblShippingCharges);
        tvShippingCharges = mView.findViewById(R.id.tvShippingCharges);
        tvTotalPayableAmount = mView.findViewById(R.id.tvTotalPayableAmount);
        rvData = mView.findViewById(R.id.rvData);
        btnConfirm = mView.findViewById(R.id.btnConfirm);
    }

    @Override
    protected void initListener() {
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HitSubmitInfoDetailsAPI();
            }
        });
    }

    @Override
    protected void bindDataWithUi() {
        purposeDetailsList = new ArrayList<>();
        Bundle bundle = this.getArguments();
        if (null != bundle) {
            // receiptEntryId = bundle.getString("ReceiptEntryID");
        }
//        if (null != bundle) {
//            depositorAddressModel = bundle.getParcelable("DepoData");
//            if (null != depositorAddressModel) {
//                tvName.setText(depositorAddressModel.getName());
//                tvMobileNo.setText(depositorAddressModel.getMobileNo());
//                tvEmail.setText(depositorAddressModel.getEmail());
//                tvAddress1.setText(depositorAddressModel.getAddress1());
//                tvAddress2.setText(depositorAddressModel.getAddress2());
//                tvDistrict.setText(depositorAddressModel.getDistrict());
//                tvCity.setText(depositorAddressModel.getCity());
//                tvState.setText(depositorAddressModel.getState());
//                tvCountry.setText(depositorAddressModel.getCountry());
//                tvPincode.setText(depositorAddressModel.getPincode());
//
//                tvShipAddress1.setText(depositorAddressModel.getShipAddress1());
//                tvShipAddress2.setText(depositorAddressModel.getShipAddress2());
//                tvShipDistrict.setText(depositorAddressModel.getShipDistrict());
//                tvShipCity.setText(depositorAddressModel.getShipCity());
//                tvShipState.setText(depositorAddressModel.getShipState());
//                tvShipCountry.setText(depositorAddressModel.getShipCountry());
//                tvShipPincode.setText(depositorAddressModel.getShipPincode());
//            }
//        }
        purposeDetailsConfirmInfoAdapter = new PurposeDetailsConfirmInfoAdapter(getContext(), purposeDetailsList);
        rvData.setLayoutManager(new LinearLayoutManager(getContext()));
        rvData.setAdapter(purposeDetailsConfirmInfoAdapter);
        rvData.setNestedScrollingEnabled(false);
        HitGetInfoDetailsAPI();

    }

    private void HitGetInfoDetailsAPI() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDepositorDeatilsConfirmURL;
        final ProgressDialog pDialog = new ProgressDialog(PublicationConfirmInfoFragment.this.getActivity(), R.style.MyTheme);
        showLoadingDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                ConfirmInfoResponseModel confirmInfoModel = gson.fromJson(response, ConfirmInfoResponseModel.class);


                if (confirmInfoModel.getAcsessTokenValue() != null && confirmInfoModel.getRefreshTokenValue() != null) {
                    MyToken.setToken(confirmInfoModel.getRefreshTokenValue());
                    MyToken.setAuthorization(confirmInfoModel.getAcsessTokenValue());
                    tvName.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getDepositorName());
                    tvMobileNo.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getMobileNo());
                    tvEmail.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getEmail());
                    tvAddress1.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getAddress1());
                    tvAddress2.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getAddress2());
                    tvDistrict.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getDistrictName());
                    tvCity.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getCity());
                    tvState.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getStateName());
                    tvCountry.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getCountryName());
                    tvPincode.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getPincode());

                    tvShipAddress1.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getShippingAddress1());
                    tvShipAddress2.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getShippingAddress2());
                    tvShipDistrict.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getShippingDistrict());
                    tvShipCity.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getShippingCity());
                    tvShipState.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getShippingState());
                    tvShipCountry.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getShippingCountry());
                    tvShipPincode.setText(confirmInfoModel.getShoppinngDepositorDetails().get(0).getShippingPincode());
                    if (null != confirmInfoModel.getShoppinngDepositorDetailslst() && confirmInfoModel.getShoppinngDepositorDetailslst().size() > 0) {
                        confirmInfoModel.getShoppinngDepositorDetailslst().get(0).setLstmodProductDetails(confirmInfoModel.getLstmodProductDetails());
                        purposeDetailsConfirmInfoAdapter.setData(confirmInfoModel.getShoppinngDepositorDetailslst());
                        tvTotalProductPrice.setText("₹ " + (confirmInfoModel.getShoppinngDepositorDetailslst().get(0).getTotalAmount() + confirmInfoModel.getShoppinngDepositorDetailslst().get(0).getDiscountAmount()));
                        int amt = (int) confirmInfoModel.getShoppinngDepositorDetailslst().get(0).getTotalAmount();
                        if (amt > 0)
                            tvAmountInWords.setText(set_validation_On_Input_Amount_String(amt + ""));
                        tvDiscountAmt.setText("₹ " + confirmInfoModel.getShoppinngDepositorDetailslst().get(0).getDiscountAmount());
                        lblShippingCharges.setText("Shipping Charges (" + confirmInfoModel.getShoppinngDepositorDetailslst().get(0).getDeliveryChargesType() + ") :");
                        tvShippingCharges.setText("₹ " + confirmInfoModel.getShoppinngDepositorDetailslst().get(0).getDeliveryCharges());
                        tvTotalPayableAmount.setText("₹ " + (confirmInfoModel.getShoppinngDepositorDetailslst().get(0).getTotalAmount()));
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
                System.out.println(error.toString());
                hideLoadingDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryID", CheckoutFragment.ReceiptEntryId + "");//6779654
                params.put("Type", "Publication");
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }

    private void HitSubmitInfoDetailsAPI() {
        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getSubmitConfirmURL;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                SubmitConfirmInfoModel confirmInfoModel = gson.fromJson(response, SubmitConfirmInfoModel.class);
                if (confirmInfoModel.getResponseStatus() != null && confirmInfoModel.getResponseStatus().matches("Y") && confirmInfoModel.getReceiptEntryId() != null && confirmInfoModel.getAcsessTokenValue() != null && confirmInfoModel.getRefreshTokenValue() != null) {
                    MyToken.setToken(confirmInfoModel.getRefreshTokenValue());
                    MyToken.setAuthorization(confirmInfoModel.getAcsessTokenValue());
                    if (!CheckoutFragment.UserType.matches("5")) {
                        Fragment fragment = new PaymentFragment();
                        replaceFragment(fragment, R.id.container_frame, true);
                    } else {
                        btnConfirm.setEnabled(false);
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                        alertDialogBuilder.setMessage("Please Wait for approval from Vidhi sahitya Prakashan");
                        alertDialogBuilder.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
                System.out.println(error.toString());
                hideLoadingDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryID", String.valueOf(Prefs.getInt("ReceiptEntryId", 0)));
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }
}