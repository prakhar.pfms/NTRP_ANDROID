package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import com.google.gson.annotations.SerializedName;

public class AttributeGetListModel {
    @SerializedName("ProductId")
    public int productId;
    @SerializedName("ReceiptAttributeId")
    public int receiptAttributeId;
    @SerializedName("AttributeName")
    public String attributeName;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getReceiptAttributeId() {
        return receiptAttributeId;
    }

    public void setReceiptAttributeId(int receiptAttributeId) {
        this.receiptAttributeId = receiptAttributeId;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
}
