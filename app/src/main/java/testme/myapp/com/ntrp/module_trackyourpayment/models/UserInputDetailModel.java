package testme.myapp.com.ntrp.module_trackyourpayment.models;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by deepika.s on 23-06-2017.
 */

@SuppressLint("ParcelCreator")
public class UserInputDetailModel implements Parcelable {
    public String CountryCode;
    public String MobileNumber;
    public String EmailID;
    public String AtrNumber;
    public String BrNumber;
    public String PassportNumber;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
