package testme.myapp.com.ntrp.module_trackyourpayment.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainScreen;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_trackyourpayment.adapters.AdditionalChargesAdaptor;
import testme.myapp.com.ntrp.module_trackyourpayment.adapters.DepositDetailsAdaptor;
import testme.myapp.com.ntrp.module_trackyourpayment.adapters.PDFAdaptor;
import testme.myapp.com.ntrp.module_trackyourpayment.adapters.ProductCartAdaptor;
import testme.myapp.com.ntrp.module_trackyourpayment.models.GetAllPdfModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.ViewSingleTransactionModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.utils.AllUrls;

/**
 * Created by deepika.s on 28-04-2017.
 */

public class DepositorTransactionDetailsFragment extends Fragment {

    private TextView Transaction_ref_no_value_Textview;
    private TextView Name_Value_Textview,MobileNo_Value_Textview,EmailID_Value_Textview,AadharNo_Value_Textview;
    private TextView PanNo_Value_Textview,Address_Line1_Value_Textview,Address_Line2_Value_Textview,District_Value_Textview;
    private TextView State_Value_Textview,Pincode_Value_Textview,TanNo_Value_Textview,TinNo_Value_Textview;
    private TextView PaymentStatus_Value_Textview,BankName_Value_Textview,AggregatorName_Value_Textview,ChannelName_Value_Textview;
    private TextView ResponseBankName_Value_Textview,ResponseChannelName_Value_Textview;


    private TextView TotalAmount_Value_Textview;

    //==================Prakhar==================================//

    private TextView shipping_nameTextview,ShippingAddress1TextView,ShippingAddress2TextView, ShippingCityTextView,ShippingDistrctTextView,
            ShippingStateTextView,ShippingPinCodeTextView,ShippingCountryTextView,
            ShippingMobileTextView,ShippingEmailTextView,totalproductamount_value_textview,
            shippingcharges_value_textview,grandtotalamount_value_textview
          ;

    private ProgressBar progressBar;

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS =100 ;

    private RecyclerView pdfRecycler,ProductcartRecycler,depositDetail_recyclerview,additionalDetail_recyclerview;

    private Context thisClassCtx;

    private View pdfLayout_CardView, shoppingcart_CardView, shippingaddress_CardView,additionalListCardView,depositDetail_CardView;
    //===========================================================//

    private ViewSingleTransactionModel viewSingleTransactionModel;
    private int DepositerTransactionDetails_Size,PaymentDetails_Size;



    private   float finalAmount;

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_depositor_transaction_details, container, false);
        initializeView(view);
        return view;
    }
    @Override
    public void onActivityCreated( Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }
    private void initializeView(View view) {

        finalAmount=0;
        viewSingleTransactionModel = (ViewSingleTransactionModel) MainScreen.TrackBundle.getParcelable("viewSingleTransactionModel");
        MyToken.setToken(viewSingleTransactionModel.RefreshTokenValue);
        DepositerTransactionDetails_Size=viewSingleTransactionModel.DepositerTransactionDetails.size();
        PaymentDetails_Size=viewSingleTransactionModel.PaymentDetails.size();

        System.out.println(DepositerTransactionDetails_Size);
        System.out.println(PaymentDetails_Size);


        Transaction_ref_no_value_Textview= (TextView) view.findViewById(R.id.transaction_ref_no_value_textview);

        Name_Value_Textview= (TextView) view.findViewById(R.id.name_value_textview);
        MobileNo_Value_Textview= (TextView) view.findViewById(R.id.mobileno_value_textview);
        EmailID_Value_Textview= (TextView) view.findViewById(R.id.emailid_value_textview);
        AadharNo_Value_Textview= (TextView) view.findViewById(R.id.aadharno_value_textview);
        PanNo_Value_Textview= (TextView) view.findViewById(R.id.panno_value_textview);
        Address_Line1_Value_Textview= (TextView) view.findViewById(R.id.address_line1_value_textview);
        Address_Line2_Value_Textview= (TextView) view.findViewById(R.id.address_line2_value_textview);
        District_Value_Textview= (TextView) view.findViewById(R.id.district_value_textview);
        State_Value_Textview= (TextView) view.findViewById(R.id.state_value_textview);
        Pincode_Value_Textview= (TextView) view.findViewById(R.id.pincode_value_textview);
        TanNo_Value_Textview= (TextView) view.findViewById(R.id.tanno_value_textview);
        TinNo_Value_Textview= (TextView) view.findViewById(R.id.tinno_value_textview);


        //==================Prakhar==================================//


        thisClassCtx=this.getContext();

        pdfLayout_CardView =  view.findViewById(R.id.pdfLayout);
        pdfLayout_CardView.setEnabled(false);
        pdfLayout_CardView.setVisibility(View.GONE);

        shoppingcart_CardView =view.findViewById(R.id.shoppingcart_linearlayout);
        shoppingcart_CardView.setEnabled(false);
        shoppingcart_CardView.setVisibility(View.GONE);


        shippingaddress_CardView =view.findViewById(R.id.shippingaddress_linearlayout);
        shippingaddress_CardView.setEnabled(false);
        shippingaddress_CardView.setVisibility(View.GONE);

        additionalListCardView=view.findViewById(R.id.additionalListCardView);
        additionalListCardView.setEnabled(false);
        additionalListCardView.setVisibility(View.GONE);

        depositDetail_CardView=view.findViewById(R.id.depositDetail_CardView);
        depositDetail_CardView.setEnabled(false);
        depositDetail_CardView.setVisibility(View.GONE);


        shippingcharges_value_textview= (TextView) view.findViewById(R.id.shippingcharges_value_textview);
        totalproductamount_value_textview= (TextView) view.findViewById(R.id.totalproductamount_value_textview);
        shipping_nameTextview= (TextView) view.findViewById(R.id.shipping_name_value_textview);
        ShippingAddress1TextView= (TextView) view.findViewById(R.id.shipping_addressline1_value_textview);
        ShippingAddress2TextView= (TextView) view.findViewById(R.id.shipping_addressline2_value_textview);
        ShippingCityTextView= (TextView) view.findViewById(R.id.shipping_city_value_textview);
        ShippingDistrctTextView= (TextView) view.findViewById(R.id.shipping_district_value_textview);
        ShippingStateTextView= (TextView) view.findViewById(R.id.shipping_state_value_textview);
        ShippingPinCodeTextView= (TextView) view.findViewById(R.id.shipping_pincode_value_textview);
        ShippingCountryTextView= (TextView) view.findViewById(R.id.shipping_country_value_textview);
        ShippingMobileTextView= (TextView) view.findViewById(R.id.shipping_mobileno_value_textview);
        ShippingEmailTextView= (TextView) view.findViewById(R.id.shipping_email_value_textview);
        grandtotalamount_value_textview= (TextView) view.findViewById(R.id.grandtotalamount_value_textview);





        progressBar= (ProgressBar) view.findViewById(R.id.pdfProgressBar);
        progressBar.setVisibility(View.VISIBLE);


        pdfRecycler = (RecyclerView) view.findViewById(R.id.pdfdata_recyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        pdfRecycler.setLayoutManager(mLayoutManager);
        pdfRecycler.setItemAnimator(new DefaultItemAnimator());

        ProductcartRecycler = (RecyclerView) view.findViewById(R.id.shoppingcart_recyclerview);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        ProductcartRecycler.setLayoutManager(mLayoutManager1);
        ProductcartRecycler.setItemAnimator(new DefaultItemAnimator());


        depositDetail_recyclerview= (RecyclerView) view.findViewById(R.id.depositDetail_recyclerview);
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getActivity());
        depositDetail_recyclerview.setLayoutManager(mLayoutManager2);
        depositDetail_recyclerview.setItemAnimator(new DefaultItemAnimator());

        additionalDetail_recyclerview= (RecyclerView) view.findViewById(R.id.additionalDetail_recyclerview);
        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getActivity());
        additionalDetail_recyclerview.setLayoutManager(mLayoutManager3);
        additionalDetail_recyclerview.setItemAnimator(new DefaultItemAnimator());

        if (viewSingleTransactionModel.ProductCart!=null&&viewSingleTransactionModel.ProductCart.size()>0) {

            ProductcartRecycler.setAdapter(new ProductCartAdaptor(viewSingleTransactionModel.ProductCart,thisClassCtx));


            totalproductamount_value_textview.setText(viewSingleTransactionModel.ProductCart.get(0).TotalAmount);

            shippingcharges_value_textview.setText(viewSingleTransactionModel.ProductCart.get(0).DeliveryCharges);

            grandtotalamount_value_textview.setText(viewSingleTransactionModel.ProductCart.get(0).TotalGrandAmount);

        }

        if (viewSingleTransactionModel.DepositorDetails!=null&&viewSingleTransactionModel.DepositorDetails.size()>0) {

            depositDetail_CardView.setVisibility(View.VISIBLE);

            for (int i=0;i<viewSingleTransactionModel.DepositorDetails.size();i++)
            {
               finalAmount+=Float.valueOf(viewSingleTransactionModel.DepositorDetails.get(i).AmountValue);
            }

            depositDetail_recyclerview.setAdapter(new DepositDetailsAdaptor(viewSingleTransactionModel.DepositorDetails,thisClassCtx));

        }

        if (viewSingleTransactionModel.AddtionalList!=null&&viewSingleTransactionModel.AddtionalList.size()>0) {

            additionalListCardView.setVisibility(View.VISIBLE);

            for (int i=0;i<viewSingleTransactionModel.AddtionalList.size();i++)
            {
                finalAmount+=Float.valueOf(viewSingleTransactionModel.AddtionalList.get(i).AdditionalAmount);
                //finalAmount+=Float.valueOf(viewSingleTransactionModel.AddtionalList.get(i).AdditionalCharge);
            }

            additionalDetail_recyclerview.setAdapter(new AdditionalChargesAdaptor(viewSingleTransactionModel.AddtionalList,thisClassCtx));


        }



        if (viewSingleTransactionModel.ShippingDetails!=null&&viewSingleTransactionModel.ShippingDetails.size()>0)
        {
            shoppingcart_CardView.setVisibility(View.VISIBLE);

            shippingaddress_CardView.setVisibility(View.VISIBLE);


            shipping_nameTextview.setText(viewSingleTransactionModel.ShippingDetails.get(0).ShippingSalutations);

            ShippingAddress1TextView.setText(viewSingleTransactionModel.ShippingDetails.get(0).ShippingAddress1);

            ShippingAddress2TextView.setText(viewSingleTransactionModel.ShippingDetails.get(0).ShippingAddress2);

            ShippingCityTextView.setText(viewSingleTransactionModel.ShippingDetails.get(0).ShippingCity);

            ShippingDistrctTextView.setText(viewSingleTransactionModel.ShippingDetails.get(0).ShippingDistrictName);

            ShippingStateTextView.setText(viewSingleTransactionModel.ShippingDetails.get(0).ShippingStateName);

            ShippingPinCodeTextView.setText(viewSingleTransactionModel.ShippingDetails.get(0).ShippingPincode);

            ShippingCountryTextView.setText(viewSingleTransactionModel.ShippingDetails.get(0).ShippingCountryName);

            ShippingMobileTextView.setText(viewSingleTransactionModel.ShippingDetails.get(0).ShippingMobileNo);

            ShippingEmailTextView.setText(viewSingleTransactionModel.ShippingDetails.get(0).ShippingEmail);


        }



        Hit_API_GetAllPDFLinks();

        //===========================End Of Prakhar================================//


        if(DepositerTransactionDetails_Size==1)
        {
            Transaction_ref_no_value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).TransactionRefNumber);
            Name_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).DepositorName);
            MobileNo_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).MobileNo);
            EmailID_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).Email);
            AadharNo_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).AadhaarNumber);
            PanNo_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).PanNumber);
            Address_Line1_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).Address1);
            Address_Line2_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).Address2);
            District_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).DistrictName);
            State_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).StateName);
            Pincode_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).Pincode);
            TanNo_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).TanNumber);
            TinNo_Value_Textview.setText(viewSingleTransactionModel.DepositerTransactionDetails.get(DepositerTransactionDetails_Size-1).TinNumber);
        }
        else
        {
            Transaction_ref_no_value_Textview.setText("");
            Name_Value_Textview.setText("");
            MobileNo_Value_Textview.setText("");
            EmailID_Value_Textview.setText("");
            AadharNo_Value_Textview.setText("");
            PanNo_Value_Textview.setText("");
            Address_Line1_Value_Textview.setText("");
            Address_Line2_Value_Textview.setText("");
            District_Value_Textview.setText("");
            State_Value_Textview.setText("");
            Pincode_Value_Textview.setText("");
            TanNo_Value_Textview.setText("");
            TinNo_Value_Textview.setText("");

        }


        PaymentStatus_Value_Textview= (TextView) view.findViewById(R.id.paymentstatus_value_textview);
        BankName_Value_Textview= (TextView) view.findViewById(R.id.bankname_value_textview);
        AggregatorName_Value_Textview= (TextView) view.findViewById(R.id.aggregatorname_value_textview);
        ChannelName_Value_Textview= (TextView) view.findViewById(R.id.channelname_value_textview);
        ResponseBankName_Value_Textview= (TextView) view.findViewById(R.id.responsebankname_value_textview);
        ResponseChannelName_Value_Textview= (TextView) view.findViewById(R.id.responsechannelname_value_textview);

        if(PaymentDetails_Size==1)
        {
            PaymentStatus_Value_Textview.setText(viewSingleTransactionModel.PaymentDetails.get(PaymentDetails_Size-1).PaymentStatus);
            BankName_Value_Textview.setText(viewSingleTransactionModel.PaymentDetails.get(PaymentDetails_Size-1).BankName);
            AggregatorName_Value_Textview.setText(viewSingleTransactionModel.PaymentDetails.get(PaymentDetails_Size-1).AggregatorName);
            ChannelName_Value_Textview.setText(viewSingleTransactionModel.PaymentDetails.get(PaymentDetails_Size-1).ChannelName);
            ResponseBankName_Value_Textview.setText(viewSingleTransactionModel.PaymentDetails.get(PaymentDetails_Size-1).ResponseBankName);
            ResponseChannelName_Value_Textview.setText(viewSingleTransactionModel.PaymentDetails.get(PaymentDetails_Size-1).ResponsePayMode);
        }
        else
        {
            PaymentStatus_Value_Textview.setText("");
            BankName_Value_Textview.setText("");
            AggregatorName_Value_Textview.setText("");
            ChannelName_Value_Textview.setText("");
            ResponseBankName_Value_Textview.setText("");
            ResponseChannelName_Value_Textview.setText("");
        }



        TotalAmount_Value_Textview= (TextView) view.findViewById(R.id.totalamount_value_textview);


        TotalAmount_Value_Textview.setText("("+viewSingleTransactionModel.DepositerTransactionDetails.get(0).CurrencyCode+") "+finalAmount);






    }
    private void askPermission()
    {
        if (ContextCompat.checkSelfPermission(this.getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            System.out.println("permisssionnnn/..............");
            ActivityCompat.requestPermissions(this.getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {



                } else {

                    ActivityCompat.requestPermissions(this.getActivity(),
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                }
                return;
            }


        }
    }

    private void Hit_API_GetAllPDFLinks() {

        System.out.println("getAllPDFURL API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getAllPDFURL;


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                progressBar.setVisibility(View.INVISIBLE);



                Gson gson = new GsonBuilder().setLenient().create();

                GetAllPdfModel allPdfModel=gson.fromJson(response,GetAllPdfModel.class);
                MyToken.setToken(allPdfModel.RefreshTokenValue);
                MyToken.setAuthorization(allPdfModel.AcsessTokenValue);


                if (allPdfModel.XmlDreports!=null&&allPdfModel.XmlDreports.size()>0)
                {
                    pdfLayout_CardView.setVisibility(View.VISIBLE);
                    pdfRecycler.setAdapter(new PDFAdaptor(viewSingleTransactionModel.DepositerTransactionDetails.get(0).ReceiptEntryID,allPdfModel.XmlDreports,thisClassCtx,DepositorTransactionDetailsFragment.this));
                    askPermission();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                progressBar.setVisibility(View.INVISIBLE);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryId", viewSingleTransactionModel.DepositerTransactionDetails.get(0).ReceiptEntryID);
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
}
