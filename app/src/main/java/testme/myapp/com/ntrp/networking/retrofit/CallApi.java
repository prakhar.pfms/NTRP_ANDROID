package testme.myapp.com.ntrp.networking.retrofit;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class CallApi {

    public static ApiInterface getClient() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://training.pfms.gov.in")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Creating object for our interface
        ApiInterface api = retrofit.create(ApiInterface.class);
        return api;
    }
}
