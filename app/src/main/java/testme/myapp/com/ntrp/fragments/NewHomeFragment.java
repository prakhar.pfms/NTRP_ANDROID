package testme.myapp.com.ntrp.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_commonreceipts.fragments.CommonReceiptsFragment;
import testme.myapp.com.ntrp.module_contribution_to_ndrf.fragments.ContributionToNDRFFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.DonateToSwachhBharatKoshFragment;
import testme.myapp.com.ntrp.module_login.fragments.LoginFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsersFragment;
import testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment;
import testme.myapp.com.ntrp.module_quickpayment.fragments.QuickPaymentFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.TrackYourPaymentFragment;
import testme.myapp.com.ntrp.utils.ScrollTextView;

public class NewHomeFragment extends Fragment implements View.OnClickListener {


    @BindView(R.id.textview_move)
    ScrollTextView textview_move;
    @BindView(R.id.imageSlider)
    SliderLayout sliderLayout;


    @BindView(R.id.button_loginregister)
    Button button_loginregister;
    @BindView(R.id.menuname_trackpayment)
    TextView menuname_trackpayment;
    @BindView(R.id.menuname_donatetokosh)
    TextView menuname_donatetokosh;


    @BindView(R.id.bharat_kosh_logo)
    ImageView bharat_kosh_logo;

    @BindView(R.id.cga_logo)
    ImageView cga_logo;

    @BindView(R.id.layout_nonregistereduser)
    RelativeLayout layout_nonregistereduser;

    @BindView(R.id.layout_goiestore)
    RelativeLayout layout_goiestore;

    @BindView(R.id.layout_quickpayment)
    RelativeLayout layout_quickpayment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home_new, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle(this.getActivity().getResources().getString(R.string.menu_home));

        String text = "Make payments to Government of India using Credit card / Debit Card / UPI / 150+ Netbanking options/ NEFT / RTGS";
        textview_move.setText(text);
        textview_move.setTextColor(Color.BLACK);
        textview_move.startScroll();

        sliderLayout.setIndicatorAnimation(SliderLayout.Animations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(1); //set scroll delay in seconds :

        setSliderViews();


        Picasso.with(this.getContext()).load(R.drawable.bharatkoshlogo).into(bharat_kosh_logo);
        Picasso.with(this.getContext()).load(R.drawable.cga_logo).into(cga_logo);

        button_loginregister.setOnClickListener(this);
        menuname_trackpayment.setOnClickListener(this);
        menuname_donatetokosh.setOnClickListener(this);
        layout_goiestore.setOnClickListener(this);
        layout_quickpayment.setOnClickListener(this);
        return view;


    }

    private void setSliderViews() {

        for (int i = 0; i <= 3; i++) {

            SliderView sliderView = new SliderView(NewHomeFragment.this.getActivity());

            switch (i) {
                case 0:
                    sliderView.setImageDrawable(R.drawable.banner_1);
                    break;
                case 1:
                    sliderView.setImageDrawable(R.drawable.banner_2);
                    break;
                case 2:
                    sliderView.setImageDrawable(R.drawable.banner_3);
                    break;
                case 3:
                    sliderView.setImageDrawable(R.drawable.banner_4);
                    break;
            }

            sliderView.setImageScaleType(ImageView.ScaleType.CENTER_CROP);
            sliderView.setDescription("");
            final int finalI = i;
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    // Toast.makeText(NewHomeFragment.this.getActivity(), "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
                }
            });

            //at last add this view in your layout :
            sliderLayout.addSliderView(sliderView);
        }
    }


    public void go_To_LoginScreen() {

        LoginFragment newFragment = new LoginFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void go_To_TrackYourPayment() {

        TrackYourPaymentFragment newFragment = new TrackYourPaymentFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void go_To_SwacchBharatKosh() {

        DonateToSwachhBharatKoshFragment newFragment = new DonateToSwachhBharatKoshFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void go_To_NonRegisteredUserPage() {

        NonRegisteredUsersFragment newFragment = new NonRegisteredUsersFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void go_To_NDRFPage() {

        ContributionToNDRFFragment newFragment = new ContributionToNDRFFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    public void go_To_CommonReceiptPage() {

        CommonReceiptsFragment newFragment = new CommonReceiptsFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    //Todo: Bhaskar Joshi
    private void callEStore(String type) {
        Bundle args = new Bundle();
        args.putString("type",type);
        PublicationFragment newFragment = new PublicationFragment();
        newFragment.setArguments(args);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void callQuickPayment() {
        QuickPaymentFragment newFragment = new QuickPaymentFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @OnClick(R.id.layout_nonregistereduser)
    public void ClickOnNonRegisteredUser() {
        go_To_NonRegisteredUserPage();
    }

    @OnClick(R.id.layout_contributiontondrf)
    public void ClickOnNDRF() {
        go_To_NDRFPage();
    }

    @OnClick(R.id.layout_commonreceiptgoi)
    public void ClickOnCommonReceipt() {
        go_To_CommonReceiptPage();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_loginregister:
                go_To_LoginScreen();
                break;
            case R.id.menuname_trackpayment:
                go_To_TrackYourPayment();
                break;
            case R.id.menuname_donatetokosh:
                go_To_SwacchBharatKosh();
                break;
            case R.id.layout_goiestore:
                showSelectEStoreDialog();
                break;
            case R.id.layout_quickpayment:
                callQuickPayment();
                break;
        }

    }

    private void showSelectEStoreDialog() {
        TextView tvPublication, tvLaw;
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (null != dialog.getWindow())
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations;
        dialog.setContentView(R.layout.dialog_select_estore);
        tvPublication = dialog.findViewById(R.id.tvPublication);
        tvLaw = dialog.findViewById(R.id.tvLaw);
        tvPublication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PublicationFragment.pTitle="Publications Division Ministry of I & B";
                callEStore("3");
                dialog.dismiss();
            }
        });
        tvLaw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PublicationFragment.pTitle="Min. of Law & Justice";
                callEStore("4");
                dialog.dismiss();

            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);


    }
}
