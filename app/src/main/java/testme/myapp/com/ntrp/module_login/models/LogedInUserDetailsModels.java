package testme.myapp.com.ntrp.module_login.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogedInUserDetailsModels {

    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("HdnEmail")
    @Expose
    private String hdnEmail;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("HdnMobileNo")
    @Expose
    private String hdnMobileNo;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("CINNo")
    @Expose
    private String cINNo;
    @SerializedName("BankIntegratedToCPSMS")
    @Expose
    private String bankIntegratedToCPSMS;
    @SerializedName("IntegratedToNPCI4Send")
    @Expose
    private String integratedToNPCI4Send;
    @SerializedName("BankId")
    @Expose
    private Integer bankId;
    @SerializedName("TanNumber")
    @Expose
    private String tanNumber;
    @SerializedName("OrganizationName")
    @Expose
    private String organizationName;
    @SerializedName("CountryId")
    @Expose
    private Integer countryId;
    @SerializedName("StateId")
    @Expose
    private Integer stateId;
    @SerializedName("DistrictId")
    @Expose
    private Integer districtId;
    @SerializedName("ControllerId")
    @Expose
    private Integer controllerId;
    @SerializedName("CBEC_EDICode_EDIDesc")
    @Expose
    private Object cBECEDICodeEDIDesc;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Pincode")
    @Expose
    private String pincode;
    @SerializedName("DepositorCategoryId")
    @Expose
    private Integer depositorCategoryId;
    @SerializedName("IsNEFT")
    @Expose
    private Boolean isNEFT;
    @SerializedName("AccountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("IFSCCode")
    @Expose
    private String iFSCCode;
    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("ErrorCodes")
    @Expose
    private Object errorCodes;
    @SerializedName("ResponseStatus")
    @Expose
    private String responseStatus;
    @SerializedName("XmlDocumentResult")
    @Expose
    private Object xmlDocumentResult;
    @SerializedName("AcsessTokenValue")
    @Expose
    private String acsessTokenValue;
    @SerializedName("RefreshTokenValue")
    @Expose
    private String refreshTokenValue;
    @SerializedName("ResponseEmail")
    @Expose
    private Object responseEmail;
    @SerializedName("ResponseMobileNo")
    @Expose
    private Object responseMobileNo;
    @SerializedName("ResponseUserType_CBEC")
    @Expose
    private Object responseUserTypeCBEC;
    @SerializedName("ResponseFirstName")
    @Expose
    private Object responseFirstName;
    @SerializedName("ResponseLastName")
    @Expose
    private Object responseLastName;
    @SerializedName("ResponseCountryCode")
    @Expose
    private Object responseCountryCode;
    @SerializedName("ResponseUserId")
    @Expose
    private Integer responseUserId;
    @SerializedName("PdfFileByte")
    @Expose
    private Object pdfFileByte;
    @SerializedName("XmlDreports")
    @Expose
    private Object xmlDreports;
    @SerializedName("ReceiptEntryId")
    @Expose
    private Integer receiptEntryId;
    @SerializedName("ShippingCharges")
    @Expose
    private Object shippingCharges;
    @SerializedName("PubUserType")
    @Expose
    private Integer pubUserType;
    @SerializedName("DisplayPopup")
    @Expose
    private Object displayPopup;
    @SerializedName("RowNo")
    @Expose
    private Object rowNo;
    @SerializedName("PaymentPurposeID")
    @Expose
    private Integer paymentPurposeID;
    @SerializedName("TotalDiscount")
    @Expose
    private Object totalDiscount;
    @SerializedName("PageCount")
    @Expose
    private Integer pageCount;
    @SerializedName("ErrorMessage")
    @Expose
    private Object errorMessage;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHdnEmail() {
        return hdnEmail;
    }

    public void setHdnEmail(String hdnEmail) {
        this.hdnEmail = hdnEmail;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getHdnMobileNo() {
        return hdnMobileNo;
    }

    public void setHdnMobileNo(String hdnMobileNo) {
        this.hdnMobileNo = hdnMobileNo;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCINNo() {
        return cINNo;
    }

    public void setCINNo(String cINNo) {
        this.cINNo = cINNo;
    }

    public String getBankIntegratedToCPSMS() {
        return bankIntegratedToCPSMS;
    }

    public void setBankIntegratedToCPSMS(String bankIntegratedToCPSMS) {
        this.bankIntegratedToCPSMS = bankIntegratedToCPSMS;
    }

    public String getIntegratedToNPCI4Send() {
        return integratedToNPCI4Send;
    }

    public void setIntegratedToNPCI4Send(String integratedToNPCI4Send) {
        this.integratedToNPCI4Send = integratedToNPCI4Send;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getTanNumber() {
        return tanNumber;
    }

    public void setTanNumber(String tanNumber) {
        this.tanNumber = tanNumber;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getControllerId() {
        return controllerId;
    }

    public void setControllerId(Integer controllerId) {
        this.controllerId = controllerId;
    }

    public Object getCBECEDICodeEDIDesc() {
        return cBECEDICodeEDIDesc;
    }

    public void setCBECEDICodeEDIDesc(Object cBECEDICodeEDIDesc) {
        this.cBECEDICodeEDIDesc = cBECEDICodeEDIDesc;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public Integer getDepositorCategoryId() {
        return depositorCategoryId;
    }

    public void setDepositorCategoryId(Integer depositorCategoryId) {
        this.depositorCategoryId = depositorCategoryId;
    }

    public Boolean getIsNEFT() {
        return isNEFT;
    }

    public void setIsNEFT(Boolean isNEFT) {
        this.isNEFT = isNEFT;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIFSCCode() {
        return iFSCCode;
    }

    public void setIFSCCode(String iFSCCode) {
        this.iFSCCode = iFSCCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Object getErrorCodes() {
        return errorCodes;
    }

    public void setErrorCodes(Object errorCodes) {
        this.errorCodes = errorCodes;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getXmlDocumentResult() {
        return xmlDocumentResult;
    }

    public void setXmlDocumentResult(Object xmlDocumentResult) {
        this.xmlDocumentResult = xmlDocumentResult;
    }

    public String getAcsessTokenValue() {
        return acsessTokenValue;
    }

    public void setAcsessTokenValue(String acsessTokenValue) {
        this.acsessTokenValue = acsessTokenValue;
    }

    public String getRefreshTokenValue() {
        return refreshTokenValue;
    }

    public void setRefreshTokenValue(String refreshTokenValue) {
        this.refreshTokenValue = refreshTokenValue;
    }

    public Object getResponseEmail() {
        return responseEmail;
    }

    public void setResponseEmail(Object responseEmail) {
        this.responseEmail = responseEmail;
    }

    public Object getResponseMobileNo() {
        return responseMobileNo;
    }

    public void setResponseMobileNo(Object responseMobileNo) {
        this.responseMobileNo = responseMobileNo;
    }

    public Object getResponseUserTypeCBEC() {
        return responseUserTypeCBEC;
    }

    public void setResponseUserTypeCBEC(Object responseUserTypeCBEC) {
        this.responseUserTypeCBEC = responseUserTypeCBEC;
    }

    public Object getResponseFirstName() {
        return responseFirstName;
    }

    public void setResponseFirstName(Object responseFirstName) {
        this.responseFirstName = responseFirstName;
    }

    public Object getResponseLastName() {
        return responseLastName;
    }

    public void setResponseLastName(Object responseLastName) {
        this.responseLastName = responseLastName;
    }

    public Object getResponseCountryCode() {
        return responseCountryCode;
    }

    public void setResponseCountryCode(Object responseCountryCode) {
        this.responseCountryCode = responseCountryCode;
    }

    public Integer getResponseUserId() {
        return responseUserId;
    }

    public void setResponseUserId(Integer responseUserId) {
        this.responseUserId = responseUserId;
    }

    public Object getPdfFileByte() {
        return pdfFileByte;
    }

    public void setPdfFileByte(Object pdfFileByte) {
        this.pdfFileByte = pdfFileByte;
    }

    public Object getXmlDreports() {
        return xmlDreports;
    }

    public void setXmlDreports(Object xmlDreports) {
        this.xmlDreports = xmlDreports;
    }

    public Integer getReceiptEntryId() {
        return receiptEntryId;
    }

    public void setReceiptEntryId(Integer receiptEntryId) {
        this.receiptEntryId = receiptEntryId;
    }

    public Object getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(Object shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public Integer getPubUserType() {
        return pubUserType;
    }

    public void setPubUserType(Integer pubUserType) {
        this.pubUserType = pubUserType;
    }

    public Object getDisplayPopup() {
        return displayPopup;
    }

    public void setDisplayPopup(Object displayPopup) {
        this.displayPopup = displayPopup;
    }

    public Object getRowNo() {
        return rowNo;
    }

    public void setRowNo(Object rowNo) {
        this.rowNo = rowNo;
    }

    public Integer getPaymentPurposeID() {
        return paymentPurposeID;
    }

    public void setPaymentPurposeID(Integer paymentPurposeID) {
        this.paymentPurposeID = paymentPurposeID;
    }

    public Object getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Object totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }
}
