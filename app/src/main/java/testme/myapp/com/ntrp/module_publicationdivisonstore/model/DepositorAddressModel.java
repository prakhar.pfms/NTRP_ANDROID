package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DepositorAddressModel implements Parcelable {

    private String SalutationId, name, address1, address2, country, state, district, city, pincode, mobileNo, email,
            shipSalutationId, shipAddress1, shipAddress2, shipCountry, shipState, shipDistrict, shipCity, shipPincode;

    public String getShipSalutationId() {
        return shipSalutationId;
    }

    public void setShipSalutationId(String shipSalutationId) {
        this.shipSalutationId = shipSalutationId;
    }

    public String getName() {
        return name;
    }

    public String getSalutationId() {
        return SalutationId;
    }

    public void setSalutationId(String salutationId) {
        SalutationId = salutationId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getShipAddress1() {
        return shipAddress1;
    }

    public void setShipAddress1(String shipAddress1) {
        this.shipAddress1 = shipAddress1;
    }

    public String getShipAddress2() {
        return shipAddress2;
    }

    public void setShipAddress2(String shipAddress2) {
        this.shipAddress2 = shipAddress2;
    }

    public String getShipCountry() {
        return shipCountry;
    }

    public void setShipCountry(String shipCountry) {
        this.shipCountry = shipCountry;
    }

    public String getShipState() {
        return shipState;
    }

    public void setShipState(String shipState) {
        this.shipState = shipState;
    }

    public String getShipDistrict() {
        return shipDistrict;
    }

    public void setShipDistrict(String shipDistrict) {
        this.shipDistrict = shipDistrict;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipPincode() {
        return shipPincode;
    }

    public void setShipPincode(String shipPincode) {
        this.shipPincode = shipPincode;
    }


    public DepositorAddressModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SalutationId);
        dest.writeString(this.name);
        dest.writeString(this.address1);
        dest.writeString(this.address2);
        dest.writeString(this.country);
        dest.writeString(this.state);
        dest.writeString(this.district);
        dest.writeString(this.city);
        dest.writeString(this.pincode);
        dest.writeString(this.mobileNo);
        dest.writeString(this.email);
        dest.writeString(this.shipAddress1);
        dest.writeString(this.shipAddress2);
        dest.writeString(this.shipCountry);
        dest.writeString(this.shipState);
        dest.writeString(this.shipDistrict);
        dest.writeString(this.shipCity);
        dest.writeString(this.shipPincode);
    }

    public void readFromParcel(Parcel source) {
        this.SalutationId = source.readString();
        this.name = source.readString();
        this.address1 = source.readString();
        this.address2 = source.readString();
        this.country = source.readString();
        this.state = source.readString();
        this.district = source.readString();
        this.city = source.readString();
        this.pincode = source.readString();
        this.mobileNo = source.readString();
        this.email = source.readString();
        this.shipAddress1 = source.readString();
        this.shipAddress2 = source.readString();
        this.shipCountry = source.readString();
        this.shipState = source.readString();
        this.shipDistrict = source.readString();
        this.shipCity = source.readString();
        this.shipPincode = source.readString();
    }

    protected DepositorAddressModel(Parcel in) {
        this.SalutationId = in.readString();
        this.name = in.readString();
        this.address1 = in.readString();
        this.address2 = in.readString();
        this.country = in.readString();
        this.state = in.readString();
        this.district = in.readString();
        this.city = in.readString();
        this.pincode = in.readString();
        this.mobileNo = in.readString();
        this.email = in.readString();
        this.shipAddress1 = in.readString();
        this.shipAddress2 = in.readString();
        this.shipCountry = in.readString();
        this.shipState = in.readString();
        this.shipDistrict = in.readString();
        this.shipCity = in.readString();
        this.shipPincode = in.readString();
    }

    public static final Creator<DepositorAddressModel> CREATOR = new Creator<DepositorAddressModel>() {
        @Override
        public DepositorAddressModel createFromParcel(Parcel source) {
            return new DepositorAddressModel(source);
        }

        @Override
        public DepositorAddressModel[] newArray(int size) {
            return new DepositorAddressModel[size];
        }
    };
}
