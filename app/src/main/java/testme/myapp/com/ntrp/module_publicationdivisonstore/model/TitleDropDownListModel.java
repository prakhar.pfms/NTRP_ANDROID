package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import com.google.gson.annotations.SerializedName;

public class TitleDropDownListModel {
    @SerializedName("Value")
    private String value;
    @SerializedName("Text")
    private String text;
    private boolean selected;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
