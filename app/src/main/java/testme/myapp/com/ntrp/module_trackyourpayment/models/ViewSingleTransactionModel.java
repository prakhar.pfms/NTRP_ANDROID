package testme.myapp.com.ntrp.module_trackyourpayment.models;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by deepika.s on 27-04-2017.
 */

@SuppressLint("ParcelCreator")
public class ViewSingleTransactionModel implements Parcelable {

    public List<DepositorTransactionDetailsList> DepositerTransactionDetails;
    public List<PaymentDetailsList> PaymentDetails;
    public List<DepositDetailsList> DepositorDetails= null;;
    public String AcsessTokenValue;
    public String RefreshTokenValue;

    public List<ShippingAddressModel> ShippingDetails= null;

    public List<AllProductCarts> ProductCart= null;

    public List<DepositAddtionalList> AddtionalList= null;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
