package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BankListCorrespondingToSelectedAggregatorModel {

    @SerializedName("AggregatorList")
    @Expose
    private Object aggregatorList;
    @SerializedName("AggregatorBankList")
    @Expose
    private List<AggregatorBank> aggregatorBankList = null;
    @SerializedName("AggregatorCardTypeList")
    @Expose
    private Object aggregatorCardTypeList;
    @SerializedName("ErrorCodes")
    @Expose
    private Object errorCodes;
    @SerializedName("ResponseStatus")
    @Expose
    private Object responseStatus;
    @SerializedName("XmlDocumentResult")
    @Expose
    private Object xmlDocumentResult;
    @SerializedName("AcsessTokenValue")
    @Expose
    private String acsessTokenValue;
    @SerializedName("RefreshTokenValue")
    @Expose
    private String refreshTokenValue;
    @SerializedName("ResponseEmail")
    @Expose
    private Object responseEmail;
    @SerializedName("ResponseMobileNo")
    @Expose
    private Object responseMobileNo;
    @SerializedName("ResponseUserType_CBEC")
    @Expose
    private Object responseUserTypeCBEC;
    @SerializedName("ResponseFirstName")
    @Expose
    private Object responseFirstName;
    @SerializedName("ResponseLastName")
    @Expose
    private Object responseLastName;
    @SerializedName("ResponseCountryCode")
    @Expose
    private Object responseCountryCode;
    @SerializedName("ResponseUserId")
    @Expose
    private Integer responseUserId;
    @SerializedName("PdfFileByte")
    @Expose
    private Object pdfFileByte;
    @SerializedName("XmlDreports")
    @Expose
    private Object xmlDreports;
    @SerializedName("ReceiptEntryId")
    @Expose
    private Integer receiptEntryId;

    public Object getAggregatorList() {
        return aggregatorList;
    }

    public void setAggregatorList(Object aggregatorList) {
        this.aggregatorList = aggregatorList;
    }

    public List<AggregatorBank> getAggregatorBankList() {
        return aggregatorBankList;
    }

    public void setAggregatorBankList(List<AggregatorBank> aggregatorBankList) {
        this.aggregatorBankList = aggregatorBankList;
    }

    public Object getAggregatorCardTypeList() {
        return aggregatorCardTypeList;
    }

    public void setAggregatorCardTypeList(Object aggregatorCardTypeList) {
        this.aggregatorCardTypeList = aggregatorCardTypeList;
    }

    public Object getErrorCodes() {
        return errorCodes;
    }

    public void setErrorCodes(Object errorCodes) {
        this.errorCodes = errorCodes;
    }

    public Object getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Object responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getXmlDocumentResult() {
        return xmlDocumentResult;
    }

    public void setXmlDocumentResult(Object xmlDocumentResult) {
        this.xmlDocumentResult = xmlDocumentResult;
    }

    public String getAcsessTokenValue() {
        return acsessTokenValue;
    }

    public void setAcsessTokenValue(String acsessTokenValue) {
        this.acsessTokenValue = acsessTokenValue;
    }

    public String getRefreshTokenValue() {
        return refreshTokenValue;
    }

    public void setRefreshTokenValue(String refreshTokenValue) {
        this.refreshTokenValue = refreshTokenValue;
    }

    public Object getResponseEmail() {
        return responseEmail;
    }

    public void setResponseEmail(Object responseEmail) {
        this.responseEmail = responseEmail;
    }

    public Object getResponseMobileNo() {
        return responseMobileNo;
    }

    public void setResponseMobileNo(Object responseMobileNo) {
        this.responseMobileNo = responseMobileNo;
    }

    public Object getResponseUserTypeCBEC() {
        return responseUserTypeCBEC;
    }

    public void setResponseUserTypeCBEC(Object responseUserTypeCBEC) {
        this.responseUserTypeCBEC = responseUserTypeCBEC;
    }

    public Object getResponseFirstName() {
        return responseFirstName;
    }

    public void setResponseFirstName(Object responseFirstName) {
        this.responseFirstName = responseFirstName;
    }

    public Object getResponseLastName() {
        return responseLastName;
    }

    public void setResponseLastName(Object responseLastName) {
        this.responseLastName = responseLastName;
    }

    public Object getResponseCountryCode() {
        return responseCountryCode;
    }

    public void setResponseCountryCode(Object responseCountryCode) {
        this.responseCountryCode = responseCountryCode;
    }

    public Integer getResponseUserId() {
        return responseUserId;
    }

    public void setResponseUserId(Integer responseUserId) {
        this.responseUserId = responseUserId;
    }

    public Object getPdfFileByte() {
        return pdfFileByte;
    }

    public void setPdfFileByte(Object pdfFileByte) {
        this.pdfFileByte = pdfFileByte;
    }

    public Object getXmlDreports() {
        return xmlDreports;
    }

    public void setXmlDreports(Object xmlDreports) {
        this.xmlDreports = xmlDreports;
    }

    public Integer getReceiptEntryId() {
        return receiptEntryId;
    }

    public void setReceiptEntryId(Integer receiptEntryId) {
        this.receiptEntryId = receiptEntryId;
    }

}
