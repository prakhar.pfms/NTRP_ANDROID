package testme.myapp.com.ntrp.extra;

public class ConvertNumberToWords {
    // strings at index 0 is not used, it is to make array
    // indexing simple
    static String one[] = { "", "ONE ", "TWO ", "THREE ", "FOUR ",
            "FIVE ", "SIX ", "SEVEN ", "EIGHT ",
            "NINE ", "TEN ", "ELEVEN ", "TWELVE ",
            "THIRTEEN ", "FOURTEEN ", "FIFTEEN ",
            "SIXTEEN ", "SEVENTEEN ", "EIGHTEEN ",
            "NINETEEN "
    };

    // strings at index 0 and 1 are not used, they is to
// make array indexing simple
    static String ten[] = { "", "", "TWENTY ", "THIRTY ", "FORTY ",
            "FIFTY ", "SIXTY ", "SEVENTY ", "EIGHTY ",
            "NINETY "
    };


    // Function to print a given number in words
   public static  String convertToWords(long n)
    {
        // stores word representation of given number n
        String out="";

        // handles digits at ten millions and hundred
        // millions places (if any)



        out += numToWords((int)(n / 10000000), "CRORE(S) ");

        // handles digits at hundred thousands and one
        // millions places (if any)
        out += numToWords((int)((n / 100000) % 100), "LAKH(S) ");

        // handles digits at thousands and tens thousands
        // places (if any)
        out += numToWords((int)((n / 1000) % 100), "THOUSAND ");

        // handles digit at hundreds places (if any)
        out += numToWords((int)((n / 100) % 10), "HUNDRED ");

        if (n > 100)
        {
            if( n % 100==0)
            {
                out += "AND ";
            }
        }


        // handles digits at ones and tens places (if any)
        out += numToWords((int)(n % 100), "");

        return out;
    }

    public static String numToWords(int n, String s)
    {
        String str = "";
        // if n is more than 19, divide it
        if (n > 19)
            str += ten[n / 10] + one[n % 10];
        else
            str += one[n];

        // if n is non-zero
        if (n!=0)
            str += s;

        return str;
    }
}
