package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Depositor {

    @SerializedName("DepositorId")
    @Expose
    private Integer depositorId;
    @SerializedName("DepositorName")
    @Expose
    private String depositorName;

    public Integer getDepositorId() {
        return depositorId;
    }

    public void setDepositorId(Integer depositorId) {
        this.depositorId = depositorId;
    }

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

}
