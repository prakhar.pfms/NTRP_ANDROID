package testme.myapp.com.ntrp.module_trackyourpayment.models;

/**
 * Created by deepika.s on 02-05-2017.
 */

public class PaymentDetailsList {
    public String AggregatorName;
    public String ChannelName;
    public String BankName;
    public String PaymentStatus;
    public String ResponseBankName;
    public String ResponsePayMode;
}
