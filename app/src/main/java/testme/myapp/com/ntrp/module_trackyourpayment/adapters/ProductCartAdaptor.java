package testme.myapp.com.ntrp.module_trackyourpayment.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_trackyourpayment.models.AllProductCarts;
import testme.myapp.com.ntrp.module_trackyourpayment.utils.BitmapTransform;

/**
 * Created by prakhar.s on 8/3/2017.
 */

public class ProductCartAdaptor extends RecyclerView.Adapter<ProductCartAdaptor.ProductCartModelViewHolder> {

    private List<AllProductCarts> data;
    private Context ctx;

    private static final int MAX_WIDTH = 1024;
    private static final int MAX_HEIGHT = 768;

    public ProductCartAdaptor(List<AllProductCarts> data, Context ctx) {
        this.data = data;

        this.ctx=ctx;
    }


    @Override
    public ProductCartAdaptor.ProductCartModelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.single_view_transcationcart_summary, parent, false);

        return new ProductCartAdaptor.ProductCartModelViewHolder(itemView);
    }




    @Override
    public void onBindViewHolder(ProductCartAdaptor.ProductCartModelViewHolder holder, int i) {

        final AllProductCarts productDetail = data.get(i);





        holder.ItemName.setText(productDetail.ProductName);

        holder.Issue.setText("Attribute: "+ productDetail.ProductAttribute);

      //  holder.Issue.setText("Issue: "+ productDetail.ProductIssues);

        holder.Launguage.setText("Language: "+ productDetail.ProductLanguage);

        holder.Country.setText("Country: "+ productDetail.ProductCountry);

        holder.UnitPrice.setText(productDetail.ItemPrice);


        holder.Quantity.setText(productDetail.TotalQty);

        holder.SubTotal.setText(productDetail.TotalPrice);

        holder.ShippingCharges.setText(productDetail.ProductShippingAmount);

        holder.TotalValue.setText(productDetail.TotalPriceShippingChargesSum);

        holder.DeliveryType.setText(productDetail.DeliveryType);


        int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));
        Picasso.with(ctx).load("https://training.pfms.gov.in/Bharatkosh/App_Themes/Receipt/images/"+productDetail.ProductImage)
                 .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                                    .placeholder(R.drawable.pdf)
                .resize(size, size)
                .centerInside().into(holder.book_imageview);


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }




    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
    }


    protected class ProductCartModelViewHolder extends RecyclerView.ViewHolder {


        protected TextView ItemName,Issue,Launguage,Country,UnitPrice,Quantity,SubTotal,ShippingCharges,
                TotalValue,DeliveryType;

        protected ImageView book_imageview;

        protected ProductCartModelViewHolder(View v) {
            super(v);

            book_imageview =  (ImageView) v.findViewById(R.id.book_imageview);

            ItemName =  (TextView) v.findViewById(R.id.bookname_textview);

            Issue =  (TextView) v.findViewById(R.id.book_issuedate_value_textview);

            Launguage =  (TextView) v.findViewById(R.id.book_language_value_textview);

            Country =  (TextView) v.findViewById(R.id.book_country_value_textview);


            UnitPrice =  (TextView) v.findViewById(R.id.book_unitprice_value_textview);

            Quantity =  (TextView) v.findViewById(R.id.book_quantity_value_textview);

            SubTotal =  (TextView) v.findViewById(R.id.book_subtotal_value_textview);

            ShippingCharges =  (TextView) v.findViewById(R.id.book_shippingcharges_value_textview);


            TotalValue =  (TextView) v.findViewById(R.id.book_total_value_textview);

            DeliveryType =  (TextView) v.findViewById(R.id.book_deliverytype_value_textview);



        }

    }

}

