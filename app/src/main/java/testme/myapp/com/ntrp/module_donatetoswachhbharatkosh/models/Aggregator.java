
package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Aggregator {

    @SerializedName("AggregatorID")
    @Expose
    private Integer aggregatorID;
    @SerializedName("AggregatorBankName")
    @Expose
    private String aggregatorBankName;
    @SerializedName("AggregatorLogoName")
    @Expose
    private String aggregatorLogoName;
    @SerializedName("IsPosPurpose")
    @Expose
    private Integer isPosPurpose;
    @SerializedName("AggregatorwiseSuccessRatio")
    @Expose
    private Integer aggregatorwiseSuccessRatio;

    public Integer getAggregatorID() {
        return aggregatorID;
    }

    public void setAggregatorID(Integer aggregatorID) {
        this.aggregatorID = aggregatorID;
    }

    public String getAggregatorBankName() {
        return aggregatorBankName;
    }

    public void setAggregatorBankName(String aggregatorBankName) {
        this.aggregatorBankName = aggregatorBankName;
    }

    public String getAggregatorLogoName() {
        return aggregatorLogoName;
    }

    public void setAggregatorLogoName(String aggregatorLogoName) {
        this.aggregatorLogoName = aggregatorLogoName;
    }

    public Integer getIsPosPurpose() {
        return isPosPurpose;
    }

    public void setIsPosPurpose(Integer isPosPurpose) {
        this.isPosPurpose = isPosPurpose;
    }

    public Integer getAggregatorwiseSuccessRatio() {
        return aggregatorwiseSuccessRatio;
    }

    public void setAggregatorwiseSuccessRatio(Integer aggregatorwiseSuccessRatio) {
        this.aggregatorwiseSuccessRatio = aggregatorwiseSuccessRatio;
    }

}
