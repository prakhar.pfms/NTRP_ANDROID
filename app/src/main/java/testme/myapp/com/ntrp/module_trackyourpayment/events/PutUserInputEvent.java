package testme.myapp.com.ntrp.module_trackyourpayment.events;

public class PutUserInputEvent {

    public PutUserInputEvent(String countryCode, String mobileNumber, String emailID) {
        CountryCode = countryCode;
        MobileNumber = mobileNumber;
        EmailID = emailID;
    }

    public String CountryCode;
    public String  MobileNumber;
    public String EmailID;



}
