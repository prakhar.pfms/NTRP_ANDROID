package testme.myapp.com.ntrp.module_publicationdivisonstore.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.MenuModel;

public class MenuAdapter extends ArrayAdapter<MenuModel> {
    private Context mContext;
    private ArrayList<MenuModel> mList;

    public MenuAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public MenuAdapter(@NonNull Context context, int resource, ArrayList<MenuModel> mList) {
        super(context, resource, mList);
        this.mContext = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public MenuModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setTextColor(mContext.getResources().getColor(R.color.black));
        label.setTextSize(18);
        label.setPadding(30, 2, 30, 2);
        label.setText(mList.get(position).getTitle());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setTextColor(mContext.getResources().getColor(R.color.black));
        label.setTextSize(18);
        label.setPadding(40, 10, 40, 10);
        label.setText(mList.get(position).getTitle());
        label.setTag(mList.get(position).getiD());

        return label;
    }
}
