package testme.myapp.com.ntrp.module_publicationdivisonstore.fragments;

import static testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment.pTitle;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyCustomToast;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.DepositorDetailsFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.DonateToSwachhBharatKoshFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.Country;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.CountryModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.DepositorDetailsSubmitModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.District;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.DistrictModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.State;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.StateModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.adapters.ConfirmInfoAdaptor;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_ConfirmInfoFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_DepositorDetailsFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.DisplayPopUpForPaymentModeModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.NonRegisterUserGetConfirmInfoModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.BaseResponse;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.DepositorAddressModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.TitleDropDownListModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;


public class DepositorFragment extends BaseFragment {
    private View mView;
    private TextView tvName, tvAddress1, tvAddress2, tvCountry, tvState, tvDistrict, tvCity, tvPincode, tvMobileNo, tvEmail,
            tvShipName, tvShipAddress1, tvShipAddress2, tvShipCountry, tvShipState, tvShipDistrict, tvShipCity, tvShipPincode, tvShipMobileNo, tvShipEmail;
    private Spinner spTitle, spCountry, spState, spDistrict, spCountryCode,
            spShipTitle, spShipCountry, spShipState, spShipDistrict, spShipCountryCode;
    private EditText etName, etAddress1, etAddress2, etCity, etPincode, etMobileNo, etEmail,
            etShipName, etShipAddress1, etShipAddress2, etShipCity, etShipPincode, etShipMobileNo, etShipEmail;
    private ArrayList<Country> countryArrayList = new ArrayList<>();
    private ArrayList<State> stateArrayList = new ArrayList<>(), shipStateArrayList = new ArrayList<>();
    private ArrayList<District> districtArrayList, shipDistrictArrayList;
    private int selectedCountryID, selectedStateID, selectedDistrictID, selectedShipCountryID, selectedShipStateID, selectedShipDistrictID,
            type = 1;
    private Button btnNext;
    private CheckBox cbSameAddress;
    private boolean isSameAddress;
    private RadioGroup rgPayment;
    private RadioButton rbSwift;
    private String paymentMode = "O", paymentPurposeID, selectedTitle, selectedShipTitle, agentUserName;
    DepositorAddressModel depositorAddressModel = new DepositorAddressModel();
    private ArrayList<TitleDropDownListModel> dropDownList = new ArrayList<>();
    private boolean agentLoginSuccess;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_depositor, container, false);
        init();
        return mView;
    }

    @Override
    public void onStart() {
        getActivity().setTitle(pTitle);
        cbSameAddress.setChecked(false);
        isSameAddress = false;
        super.onStart();
    }

    @Override
    protected void initView() {
//        countryArrayList = new ArrayList<>();
//        stateArrayList = new ArrayList<>();
        districtArrayList = new ArrayList<>();
        shipStateArrayList = new ArrayList<>();
        shipDistrictArrayList = new ArrayList<>();
        tvName = mView.findViewById(R.id.tvName);
        tvAddress1 = mView.findViewById(R.id.tvAddress1);
        tvAddress2 = mView.findViewById(R.id.tvAddress2);
        tvCountry = mView.findViewById(R.id.tvCountry);
        tvState = mView.findViewById(R.id.tvState);
        tvDistrict = mView.findViewById(R.id.tvDistrict);
        tvCity = mView.findViewById(R.id.tvCity);
        tvPincode = mView.findViewById(R.id.tvPincode);
        tvMobileNo = mView.findViewById(R.id.tvMobileNo);
        tvEmail = mView.findViewById(R.id.tvEmail);

        etName = mView.findViewById(R.id.etName);
        etAddress1 = mView.findViewById(R.id.etAddress1);
        etAddress2 = mView.findViewById(R.id.etAddress2);
        etCity = mView.findViewById(R.id.etCity);
        etPincode = mView.findViewById(R.id.etPincode);
        etMobileNo = mView.findViewById(R.id.etMobileNo);
        etEmail = mView.findViewById(R.id.etEmail);

        cbSameAddress = mView.findViewById(R.id.cbSameAddress);

        tvShipName = mView.findViewById(R.id.tvShipName);
        tvShipAddress1 = mView.findViewById(R.id.tvShipAddress1);
        tvShipAddress2 = mView.findViewById(R.id.tvShipAddress2);
        tvShipCountry = mView.findViewById(R.id.tvShipCountry);
        tvShipState = mView.findViewById(R.id.tvShipState);
        tvShipDistrict = mView.findViewById(R.id.tvShipDistrict);
        tvShipCity = mView.findViewById(R.id.tvShipCity);
        tvShipPincode = mView.findViewById(R.id.tvShipPincode);
        tvShipMobileNo = mView.findViewById(R.id.tvShipMobileNo);
        tvShipEmail = mView.findViewById(R.id.tvShipEmail);

        spTitle = mView.findViewById(R.id.spTitle);
        spCountry = mView.findViewById(R.id.spCountry);
        spState = mView.findViewById(R.id.spState);
        spDistrict = mView.findViewById(R.id.spDistrict);
        spCountryCode = mView.findViewById(R.id.spCountryCode);
        spShipTitle = mView.findViewById(R.id.spShipTitle);
        spShipCountry = mView.findViewById(R.id.spShipCountry);
        spShipState = mView.findViewById(R.id.spShipState);
        spShipDistrict = mView.findViewById(R.id.spShipDistrict);
        spShipCountryCode = mView.findViewById(R.id.spShipCountryCode);

        etShipName = mView.findViewById(R.id.etShipName);
        etShipAddress1 = mView.findViewById(R.id.etShipAddress1);
        etShipAddress2 = mView.findViewById(R.id.etShipAddress2);
        etShipCity = mView.findViewById(R.id.etShipCity);
        etShipPincode = mView.findViewById(R.id.etShipPincode);
        etShipMobileNo = mView.findViewById(R.id.etShipMobileNo);
        etShipEmail = mView.findViewById(R.id.etShipEmail);

        rgPayment = mView.findViewById(R.id.rgPayment);
        rbSwift = mView.findViewById(R.id.rbSwift);
        btnNext = mView.findViewById(R.id.btnNext);
    }

    @Override
    protected void initListener() {
        spTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0)
                    selectedTitle = dropDownList.get(position - 1).getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spShipTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0)
                    selectedShipTitle = dropDownList.get(position - 1).getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cbSameAddress.setChecked(false);
                isSameAddress = false;
                String item = spCountry.getItemAtPosition(position).toString();
                selectedCountryID = countryArrayList.get(position).getCountryId();
                // if (selectedStateID == 0)
                Hit_StateList_API(1);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });
        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cbSameAddress.setChecked(false);
                isSameAddress = false;
                if (position > 0) {

                    String item = spState.getItemAtPosition(position).toString();
                    selectedStateID = stateArrayList.get(spState.getSelectedItemPosition()).getStateId();
                    Hit_DistrictList_API(1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cbSameAddress.setChecked(false);
                isSameAddress = false;
                if (position > 0) {
                    String item = spDistrict.getItemAtPosition(position).toString();
                    selectedDistrictID = districtArrayList.get(spDistrict.getSelectedItemPosition()).getDistrictId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spShipCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //cbSameAddress.setChecked(false);
                //isSameAddress = false;
                String item = spShipCountry.getItemAtPosition(position).toString();
                selectedShipCountryID = countryArrayList.get(position).getCountryId();
                if (!isSameAddress)
                    Hit_StateList_API(2);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

        });
        spShipState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // cbSameAddress.setChecked(false);
                // isSameAddress = false;
                if (position > 0) {

                    String item = spShipState.getItemAtPosition(position).toString();
                    selectedShipStateID = shipStateArrayList.get(spShipState.getSelectedItemPosition()).getStateId();
                    if (!isSameAddress)
                        Hit_DistrictList_API(2);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spShipDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // cbSameAddress.setChecked(false);
                if (position > 0) {
                    String item = spShipDistrict.getItemAtPosition(position).toString();
                    if (!isSameAddress)
                        selectedShipDistrictID = shipDistrictArrayList.get(spShipDistrict.getSelectedItemPosition()).getDistrictId();
                    else
                        selectedShipDistrictID = districtArrayList.get(spShipDistrict.getSelectedItemPosition()).getDistrictId();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        rgPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbOnline) {
                    paymentMode = "O";
                } else if (checkedId == R.id.rbSwift) {
                    //Hit_CheckIfNEFTAvailable_API();
                    btnNext.setEnabled(false);
                }
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    depositorAddressModel.setSalutationId(selectedTitle);
                    depositorAddressModel.setName(etName.getText().toString());
                    depositorAddressModel.setAddress1(etAddress1.getText().toString());
                    depositorAddressModel.setAddress2(etAddress2.getText().toString());
                    depositorAddressModel.setCountry(spCountry.getSelectedItem().toString());
                    depositorAddressModel.setState(spState.getSelectedItem().toString());
                    depositorAddressModel.setDistrict(spDistrict.getSelectedItem().toString());
                    depositorAddressModel.setCity(etCity.getText().toString());
                    depositorAddressModel.setPincode(etPincode.getText().toString());
                    depositorAddressModel.setMobileNo(etMobileNo.getText().toString());
                    depositorAddressModel.setEmail(etEmail.getText().toString());

                    depositorAddressModel.setShipSalutationId(selectedShipTitle);
                    depositorAddressModel.setShipAddress1(etShipAddress1.getText().toString());
                    depositorAddressModel.setShipAddress2(etShipAddress2.getText().toString());
                    depositorAddressModel.setShipCountry(spShipCountry.getSelectedItem().toString());
                    depositorAddressModel.setShipState(spShipState.getSelectedItem().toString());
                    depositorAddressModel.setShipDistrict(spShipDistrict.getSelectedItem().toString());
                    depositorAddressModel.setShipCity(etShipCity.getText().toString());
                    depositorAddressModel.setShipPincode(etShipPincode.getText().toString());
                    HitSubmitDepositorDetailsAPI();
//                    Bundle args = new Bundle();
//                    args.putParcelable("DepoData", depositorAddressModel);
//                    Fragment fragment = new PublicationConfirmInfoFragment();
//                    fragment.setArguments(args);
//                    replaceFragment(fragment, R.id.container_frame, true);
                }
            }
        });

        cbSameAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSameAddress = !isSameAddress;
                if (isValid()) {
                    if (isSameAddress) {
                        String[] districtItems = new String[districtArrayList.size() + 1];
                        districtItems[0] = "Select";
                        for (int i = 1; i < districtArrayList.size(); i++) {
                            districtItems[i] = districtArrayList.get(i).getDistrictName().toString();
                        }
                        HintAdapter<String> adapter = new HintAdapter<String>(getContext(), R.layout.single_row_spinner, districtItems);
                        spShipDistrict.setAdapter(adapter);
                        spShipTitle.setSelection(spTitle.getSelectedItemPosition());
                        etShipName.setText(etName.getText().toString());
                        etShipAddress1.setText(etAddress1.getText().toString());
                        etShipAddress2.setText(etAddress2.getText().toString());
                        spShipCountry.setSelection(spCountry.getSelectedItemPosition());
                        spShipState.setSelection(spState.getSelectedItemPosition());
                        spShipDistrict.setSelection(spDistrict.getSelectedItemPosition());
                        etShipCity.setText(etCity.getText().toString());
                        etShipPincode.setText(etPincode.getText().toString());
                        etShipMobileNo.setText(etMobileNo.getText().toString());
                        etShipEmail.setText(etEmail.getText().toString());

                    } else {

                    }
                } else {
                    cbSameAddress.setChecked(false);
                    isSameAddress = false;
                }

            }
        });
    }

    @Override
    protected void bindDataWithUi() {
        Bundle bundle = this.getArguments();
        if (null != bundle) {
            // receiptEntryId = bundle.getString("ReceiptEntryId");
            paymentPurposeID = bundle.getString("PaymentPurposeID");
            agentLoginSuccess = bundle.getBoolean("agentLoginSuccess");
            if (agentLoginSuccess) {
                agentUserName = bundle.getString("agentUserName");
            }
        }
        putRedAsterisk(mView, tvName, R.id.tvName);
        putRedAsterisk(mView, tvAddress1, R.id.tvAddress1);
        putRedAsterisk(mView, tvCountry, R.id.tvCountry);
        putRedAsterisk(mView, tvState, R.id.tvState);
        putRedAsterisk(mView, tvDistrict, R.id.tvDistrict);
        putRedAsterisk(mView, tvCity, R.id.tvCity);
        putRedAsterisk(mView, tvPincode, R.id.tvPincode);
        putRedAsterisk(mView, tvMobileNo, R.id.tvMobileNo);
        putRedAsterisk(mView, tvEmail, R.id.tvEmail);

        putRedAsterisk(mView, tvShipName, R.id.tvShipName);
        putRedAsterisk(mView, tvShipAddress1, R.id.tvShipAddress1);
        putRedAsterisk(mView, tvShipCountry, R.id.tvShipCountry);
        putRedAsterisk(mView, tvShipState, R.id.tvShipState);
        putRedAsterisk(mView, tvShipDistrict, R.id.tvShipDistrict);
        putRedAsterisk(mView, tvShipCity, R.id.tvShipCity);
        putRedAsterisk(mView, tvShipPincode, R.id.tvShipPincode);
        putRedAsterisk(mView, tvShipMobileNo, R.id.tvShipMobileNo);
        putRedAsterisk(mView, tvShipEmail, R.id.tvShipEmail);

        String[] country_mobile_number_codes = new String[1];
        country_mobile_number_codes[0] = "91";
        ArrayAdapter<String> adapterCountry = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, country_mobile_number_codes);
        spCountryCode.setAdapter(adapterCountry);
        spShipCountryCode.setAdapter(adapterCountry);
        if (agentLoginSuccess) {
            Hit_UserProfile_API();
        } else {
            Hit_TitleDropdown_API();
        }

    }

    private void Hit_UserProfile_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getUserProfile;
        showLoadingDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println(response);
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    if (responseData.getResponseStatus().matches("Success")) {
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        Hit_TitleDropdown_API();
                        etName.setText(responseData.getFirstName());
                        etMobileNo.setText(responseData.getMobileNo());
                        etEmail.setText(responseData.getEmail());
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
                hideLoadingDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("LoginUserName", agentUserName);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }

    private void Hit_TitleDropdown_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTitleDropDown;
        showLoadingDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println(response);
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    if (responseData.getErrorCodes().matches("Success")) {
                        Hit_CountryList_API();
                        dropDownList.clear();
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        dropDownList.addAll(responseData.getDropDownList());
                        String[] titleItems = new String[dropDownList.size() + 1];
                        titleItems[0] = "Select";
                        for (int i = 0; i < dropDownList.size(); i++) {
                            titleItems[i + 1] = dropDownList.get(i).getText();
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, titleItems);
                        spTitle.setAdapter(adapter);
                        for (int i = 0; i < dropDownList.size(); i++) {
                            if (dropDownList.get(i).getValue().equalsIgnoreCase(selectedTitle)) {
                                spTitle.setSelection(i + 1);
                                break;
                            }
                        }
                        spShipTitle.setAdapter(adapter);
                        for (int i = 0; i < dropDownList.size(); i++) {
                            if (dropDownList.get(i).getValue().equalsIgnoreCase(selectedShipTitle)) {
                                spShipTitle.setSelection(i + 1);
                                break;
                            }
                        }
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
                hideLoadingDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("flag", "ShippingSalutation");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }

    private void Hit_CountryList_API() {

        System.out.println("Hit_CountryList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getCountryListURL;
        showLoadingDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                CountryModel countryModel = gson.fromJson(response, CountryModel.class);
                countryArrayList.clear();

                countryArrayList = (ArrayList<Country>) countryModel.getCountryList();
                String[] countryItems = new String[countryArrayList.size()];
                for (int i = 0; i < countryArrayList.size(); i++) {
                    countryItems[i] = countryArrayList.get(i).getCountryName().toString();
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, countryItems);
                spCountry.setAdapter(adapter);
                spCountry.setSelection(99);
                spShipCountry.setAdapter(adapter);
                spShipCountry.setSelection(99);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                hideLoadingDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_StateList_API(int type) {

        System.out.println("Hit_StateList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getStateListURL;
        showLoadingDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                StateModel stateModel = gson.fromJson(response, StateModel.class);
                if (type == 1) {
                    stateArrayList.clear();
                    State model = new State();
                    model.setStateId(0);
                    model.setStateName("Select");
                    stateArrayList.add(model);
                    stateArrayList.addAll(stateModel.getStateList());
                    String[] stateItems = new String[stateArrayList.size()];
                    //stateItems[0] = "Select";
                    for (int i = 0; i < stateArrayList.size(); i++) {
                        stateItems[i] = stateArrayList.get(i).getStateName();
                    }
                    //if (selectedStateID == 0) {
                    HintAdapter<String> adapter = new HintAdapter<String>(getContext(), R.layout.single_row_spinner, stateItems);
                    spState.setAdapter(adapter);
                    for (int i = 0; i < stateArrayList.size(); i++) {
                        if (stateArrayList.get(i).getStateId() == selectedStateID) {
                            spState.setSelection(i);
                            break;
                        }
                    }
                    //spState.set
                    // }


                } else {
                    shipStateArrayList.clear();
                    State model = new State();
                    model.setStateId(0);
                    model.setStateName("Select");
                    shipStateArrayList.add(model);
                    shipStateArrayList.addAll(stateModel.getStateList());
                    String[] stateItems = new String[shipStateArrayList.size()];
                    //stateItems[0] = "Select";
                    for (int i = 0; i < shipStateArrayList.size(); i++) {
                        stateItems[i] = shipStateArrayList.get(i).getStateName();
                    }
                    HintAdapter<String> adapter = new HintAdapter<String>(getContext(), R.layout.single_row_spinner, stateItems);
                    spShipState.setAdapter(adapter);
                    for (int i = 0; i < shipStateArrayList.size(); i++) {
                        if (shipStateArrayList.get(i).getStateId() == selectedShipStateID) {
                            spShipState.setSelection(i);
                            break;
                        }
                    }
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                hideLoadingDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                if (type == 1)
                    params.put("ParentControlId", "" + selectedCountryID);
                else
                    params.put("ParentControlId", "" + selectedShipCountryID);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                return headers;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public class HintAdapter<String> extends ArrayAdapter<String> {
        public HintAdapter(@NonNull Context context, int resource, @NonNull String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public int getCount() {
            // don't display last item. It is used as hint.
            int count = super.getCount();
            return count > 0 ? count - 1 : count;
        }
    }

    private void Hit_DistrictList_API(int type) {

        System.out.println("Hit_DistrictList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDistrictListURL;
        showLoadingDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                DistrictModel districtModel = gson.fromJson(response, DistrictModel.class);
                if (type == 1) {
                    districtArrayList.clear();
                    District model = new District();
                    model.setDistrictId(0);
                    model.setDistrictName("Select");
                    districtArrayList.add(model);
                    districtArrayList.addAll(districtModel.getDistrictList());
                    String[] districtItems = new String[districtArrayList.size()];
                    //districtItems[0] = "Select";
                    for (int i = 0; i < districtArrayList.size(); i++) {
                        districtItems[i] = districtArrayList.get(i).getDistrictName();
                    }
                    HintAdapter<String> adapter = new HintAdapter<String>(getContext(), R.layout.single_row_spinner, districtItems);
                    spDistrict.setAdapter(adapter);
                    for (int i = 0; i < districtArrayList.size(); i++) {
                        if (districtArrayList.get(i).getDistrictId() == selectedDistrictID) {
                            spDistrict.setSelection(i);
                            break;
                        }
                    }
                } else {
                    shipDistrictArrayList.clear();
                    District model = new District();
                    model.setDistrictId(0);
                    model.setDistrictName("Select");
                    shipDistrictArrayList.add(model);
                    shipDistrictArrayList.addAll(districtModel.getDistrictList());
                    String[] districtItems = new String[shipDistrictArrayList.size()];
                    //districtItems[0] = "Select";
                    for (int i = 0; i < shipDistrictArrayList.size(); i++) {
                        districtItems[i] = shipDistrictArrayList.get(i).getDistrictName();
                    }
                    HintAdapter<String> adapter = new HintAdapter<String>(getContext(), R.layout.single_row_spinner, districtItems);
                    spShipDistrict.setAdapter(adapter);
                    for (int i = 0; i < shipDistrictArrayList.size(); i++) {
                        if (shipDistrictArrayList.get(i).getDistrictId() == selectedShipDistrictID) {
                            spShipDistrict.setSelection(i);
                            break;
                        }
                    }
                }


                //spDistrict.setSelection(adapter.getCount());


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                hideLoadingDialog();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                if (type == 1)
                    params.put("ParentControlId", "" + selectedStateID);
                else
                    params.put("ParentControlId", "" + selectedShipStateID);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private boolean isValid() {
        if (spTitle.getSelectedItemPosition() == 0) {
            MyCustomToast.createErrorToast(getActivity(), "Please select title");
            return false;
        }
        if (TextUtils.isEmpty(etName.getText().toString())) {
            MyCustomToast.createErrorToast(getActivity(), "Please enter depositor's name");
            return false;
        }
        if (TextUtils.isEmpty(etAddress1.getText().toString())) {
            MyCustomToast.createErrorToast(getActivity(), "Please enter depositor's address");
            return false;
        }
        if (spCountry.getSelectedItemPosition() == 0) {
            MyCustomToast.createErrorToast(getActivity(), "Please select country");
            return false;
        }
        if (spState.getSelectedItemPosition() == 0) {
            MyCustomToast.createErrorToast(getActivity(), "Please select state");
            return false;
        }
        if (spDistrict.getSelectedItemPosition() == 0) {
            MyCustomToast.createErrorToast(getActivity(), "Please select district");
            return false;
        }
        if (TextUtils.isEmpty(etCity.getText().toString())) {
            MyCustomToast.createErrorToast(getActivity(), "Please enter depositor's city");
            return false;
        }
        if (TextUtils.isEmpty(etPincode.getText().toString())) {
            MyCustomToast.createErrorToast(getActivity(), "Please enter depositor's pincode/zipcode");
            return false;
        }
        if (TextUtils.isEmpty(etMobileNo.getText().toString())) {
            MyCustomToast.createErrorToast(getActivity(), "Please enter depositor's mobile no.");
            return false;
        }
        if (etMobileNo.getText().toString().trim().length() < 10) {
            MyCustomToast.createErrorToast(getActivity(), "Please enter depositor's 10 digits mobile no.");
            return false;
        }
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            MyCustomToast.createErrorToast(getActivity(), "Please enter depositor's email");
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            MyCustomToast.createErrorToast(getActivity(), "Please enter valid email");
            return false;
        }
        if (!isSameAddress) {
            if (spShipTitle.getSelectedItemPosition() == 0) {
                MyCustomToast.createErrorToast(getActivity(), "Please select title");
                return false;
            }
            if (TextUtils.isEmpty(etShipName.getText().toString())) {
                MyCustomToast.createErrorToast(getActivity(), "Please enter name in shipping details");
                return false;
            }
            if (TextUtils.isEmpty(etShipAddress1.getText().toString())) {
                MyCustomToast.createErrorToast(getActivity(), "Please enter address in shipping details");
                return false;
            }
            if (spShipCountry.getSelectedItemPosition() == 0) {
                MyCustomToast.createErrorToast(getActivity(), "Please select country in shipping details");
                return false;
            }
            if (spShipState.getSelectedItemPosition() == 0) {
                MyCustomToast.createErrorToast(getActivity(), "Please select state in shipping details");
                return false;
            }
            if (spShipDistrict.getSelectedItemPosition() == 0) {
                MyCustomToast.createErrorToast(getActivity(), "Please select district in shipping details");
                return false;
            }
            if (TextUtils.isEmpty(etShipCity.getText().toString())) {
                MyCustomToast.createErrorToast(getActivity(), "Please enter city in shipping details");
                return false;
            }
            if (TextUtils.isEmpty(etShipPincode.getText().toString())) {
                MyCustomToast.createErrorToast(getActivity(), "Please enter pincode/zipcode in shipping details");
                return false;
            }
            if (TextUtils.isEmpty(etShipMobileNo.getText().toString())) {
                MyCustomToast.createErrorToast(getActivity(), "Please enter mobile no. in shipping details");
                return false;
            }
            if (etShipMobileNo.getText().toString().trim().length() < 10) {
                MyCustomToast.createErrorToast(getActivity(), "Please enter 10 digits mobile no. in shipping details");
                return false;
            }
            if (TextUtils.isEmpty(etShipEmail.getText().toString())) {
                MyCustomToast.createErrorToast(getActivity(), "Please enter email in shipping details");
                return false;
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(etShipEmail.getText().toString()).matches()) {
                MyCustomToast.createErrorToast(getActivity(), "Please enter valid email in shipping details");
                return false;
            }
        }
        return true;
    }

    private void HitSubmitDepositorDetailsAPI() {
        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDepositorDeatilsSubmitURL;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println(response);
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                DepositorDetailsSubmitModel depositorCategoryModel = gson.fromJson(response, DepositorDetailsSubmitModel.class);
                if (depositorCategoryModel.getReceiptEntryId() != null && depositorCategoryModel.getAcsessTokenValue() != null && depositorCategoryModel.getRefreshTokenValue() != null) {
                    MyToken.setToken(depositorCategoryModel.getRefreshTokenValue());
                    MyToken.setAuthorization(depositorCategoryModel.getAcsessTokenValue());

                    Bundle args = new Bundle();
                    // args.putParcelable("DepoData", depositorAddressModel);
                    // args.putString("ReceiptEntryID", ReceiptEntryId);
                    Fragment fragment = new PublicationConfirmInfoFragment();
                    //fragment.setArguments(args);
                    replaceFragment(fragment, R.id.container_frame, true);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
                System.out.println(error.toString());
                hideLoadingDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("ReceiptEntryID", CheckoutFragment.ReceiptEntryId + "");//6779654
                //String.valueOf(Prefs.getInt("ReceiptEntryId",0)));
                params.put("DepositorName", depositorAddressModel.getName());
                params.put("Address1", depositorAddressModel.getAddress1());
                params.put("Address2", depositorAddressModel.getAddress2());
                params.put("City", depositorAddressModel.getCity());
                params.put("Pincode", depositorAddressModel.getPincode());
                params.put("Email", depositorAddressModel.getEmail());
                params.put("MobileNo", depositorAddressModel.getMobileNo());
                params.put("StateId", String.valueOf(selectedStateID));
                params.put("DistrictId", String.valueOf(selectedDistrictID));
                params.put("CountryId", String.valueOf(selectedCountryID));
                params.put("CountryCode", "91");
                params.put("SalutationId", depositorAddressModel.getSalutationId());
                params.put("PaymentMode", paymentMode);
                params.put("PaymentPurposeID", paymentPurposeID); //15
                params.put("ShippingAddress1", depositorAddressModel.getShipAddress1());
                params.put("ShippingAddress2", depositorAddressModel.getShipAddress2());
                params.put("ShippingCity", depositorAddressModel.getShipCity());
                params.put("ShippingPincode", depositorAddressModel.getShipPincode());
                params.put("ShippingEmail", depositorAddressModel.getEmail());
                params.put("ShippingMobileNo", depositorAddressModel.getMobileNo());
                params.put("ShippingStateId", String.valueOf(selectedStateID));
                params.put("ShippingDistrictId", String.valueOf(selectedShipDistrictID));
                params.put("ShippingCountryId", String.valueOf(selectedShipCountryID));
                params.put("ShippingCountryCode", "91");
                params.put("ShippingSalutations", depositorAddressModel.getShipSalutationId());
                params.put("ShippingName", depositorAddressModel.getName());
                params.put("Flag", "2");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_CheckIfNEFTAvailable_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_CheckPaymentMode_URL;
        showLoadingDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println(response);
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                DisplayPopUpForPaymentModeModel depositorCategoryModel = gson.fromJson(response, DisplayPopUpForPaymentModeModel.class);
                if (depositorCategoryModel.getAcsessTokenValue() != null && depositorCategoryModel.getRefreshTokenValue() != null) {
                    Hit_CountryList_API();
                    MyToken.setToken(depositorCategoryModel.getRefreshTokenValue());
                    MyToken.setAuthorization(depositorCategoryModel.getAcsessTokenValue());

                    if (depositorCategoryModel.getDisplayPopup().matches("true")) {
                        rbSwift.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(getActivity(), "Error: " + depositorCategoryModel.getErrorMessage(), Toast.LENGTH_LONG);
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
                hideLoadingDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("LoggedInUserName", "");
                params.put("ReceiptEntryId", CheckoutFragment.ReceiptEntryId + "");
                params.put("UniqueUserDeviceId", PublicationFragment.UNIQUE_ID);
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }


}
