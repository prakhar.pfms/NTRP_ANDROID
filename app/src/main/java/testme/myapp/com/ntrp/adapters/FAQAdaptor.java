package testme.myapp.com.ntrp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.widget.TextView;

import androidx.fragment.app.Fragment;


import androidx.recyclerview.widget.RecyclerView;




import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.model.Objfaq;


public class FAQAdaptor extends RecyclerView.Adapter<FAQAdaptor.FAQViewHolder> {

    private List<Objfaq> data;



    public FAQAdaptor(List<Objfaq> data) {
        this.data = data;


    }

    @Override
    public FAQAdaptor.FAQViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.single_view_faq, parent, false);
        return new FAQAdaptor.FAQViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FAQAdaptor.FAQViewHolder holder, final int position) {
        holder.bind(position);
    }
    @Override
    public int getItemCount() {
        return data.size();
    }

    public class FAQViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.question)
        TextView question;
        @BindView(R.id.answer)
        TextView answer;

        public FAQViewHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }

        public void bind(final int position)
        {
            question.setText("Question "+(position+1)+". "+data.get(position).getQuestion());
            answer.setText("Answer: -"+data.get(position).getAnswer());
        }
    }
}

