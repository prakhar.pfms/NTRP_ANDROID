package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductListModel extends BaseResponse implements Parcelable {
    @SerializedName("ProductName")
    private String productName;
    @SerializedName("CategoryId")
    private int categoryId;
    @SerializedName("ProductId")
    private int productId;
    @SerializedName("ProductImage")
    private String productImage;
    @SerializedName("DisplayProductPrice")
    private double displayProductPrice;
    @SerializedName("OutOfStock")
    private String outOfStock;
    @SerializedName("ParentCategory")
    public String parentCategory;
    @SerializedName("CategoryName")
    public String categoryName;
    @SerializedName("ProductItemQuantity")
    public int productItemQuantity;
    @SerializedName("ProductItemPrice")
    public double productItemPrice;
    @SerializedName("ProductItemCode")
    public String productItemCode;
    @SerializedName("WriterName")
    public String writerName;
    @SerializedName("PublisherName")
    public String publisherName;
    @SerializedName("NoOfPages")
    public int noOfPages;
    @SerializedName("ISBN")
    public String iSBN;
    @SerializedName("PublishDate")
    public String publishDate;
    @SerializedName("Edition")
    public String edition;
    @SerializedName("DisplayItemPrice")
    public String displayItemPrice;
    @SerializedName("ProductAttributeMappingId")
    public int productAttributeMappingId;
    @SerializedName("ProductDescription")
    public String productDescription;
    @SerializedName("AttributeName")
    public String attributeName;
    @SerializedName("AttributeValue")
    public String attributeValue;
    @SerializedName("ReceiptAttributeId")
    public String receiptAttributeId;
    @SerializedName("ReceiptProductItemId")
    public String receiptProductItemId;
    @SerializedName("CategoryReceiptAccountId")
    public String categoryReceiptAccountId;
    @SerializedName("ProductCode")
    public String productCode;
    @SerializedName("AttributeGetList")
    public ArrayList<AttributeGetListModel> attributeGetList;
    @SerializedName("AttributeGetDetailList")
    public ArrayList<AttributeGetDetailListModel> attributeGetDetailList;

    public String getReceiptProductItemId() {
        return receiptProductItemId;
    }

    public void setReceiptProductItemId(String receiptProductItemId) {
        this.receiptProductItemId = receiptProductItemId;
    }

    public String getCategoryReceiptAccountId() {
        return categoryReceiptAccountId;
    }

    public void setCategoryReceiptAccountId(String categoryReceiptAccountId) {
        this.categoryReceiptAccountId = categoryReceiptAccountId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getProductAttributeMappingId() {
        return productAttributeMappingId;
    }

    public void setProductAttributeMappingId(int productAttributeMappingId) {
        this.productAttributeMappingId = productAttributeMappingId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public double getDisplayProductPrice() {
        return displayProductPrice;
    }

    public void setDisplayProductPrice(double displayProductPrice) {
        this.displayProductPrice = displayProductPrice;
    }

    public String getOutOfStock() {
        return outOfStock;
    }

    public void setOutOfStock(String outOfStock) {
        this.outOfStock = outOfStock;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(String parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getProductItemQuantity() {
        return productItemQuantity;
    }

    public void setProductItemQuantity(int productItemQuantity) {
        this.productItemQuantity = productItemQuantity;
    }

    public double getProductItemPrice() {
        return productItemPrice;
    }

    public void setProductItemPrice(double productItemPrice) {
        this.productItemPrice = productItemPrice;
    }

    public String getProductItemCode() {
        return productItemCode;
    }

    public void setProductItemCode(String productItemCode) {
        this.productItemCode = productItemCode;
    }

    public String getWriterName() {
        return writerName;
    }

    public void setWriterName(String writerName) {
        this.writerName = writerName;
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public int getNoOfPages() {
        return noOfPages;
    }

    public void setNoOfPages(int noOfPages) {
        this.noOfPages = noOfPages;
    }

    public String getiSBN() {
        return iSBN;
    }

    public void setiSBN(String iSBN) {
        this.iSBN = iSBN;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getDisplayItemPrice() {
        return displayItemPrice;
    }

    public void setDisplayItemPrice(String displayItemPrice) {
        this.displayItemPrice = displayItemPrice;
    }

    public ArrayList<AttributeGetListModel> getAttributeGetList() {
        return attributeGetList;
    }

    public void setAttributeGetList(ArrayList<AttributeGetListModel> attributeGetList) {
        this.attributeGetList = attributeGetList;
    }

    public ArrayList<AttributeGetDetailListModel> getAttributeGetDetailList() {
        return attributeGetDetailList;
    }

    public void setAttributeGetDetailList(ArrayList<AttributeGetDetailListModel> attributeGetDetailList) {
        this.attributeGetDetailList = attributeGetDetailList;
    }

    public static Creator<ProductListModel> getCREATOR() {
        return CREATOR;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getReceiptAttributeId() {
        return receiptAttributeId;
    }

    public void setReceiptAttributeId(String receiptAttributeId) {
        this.receiptAttributeId = receiptAttributeId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.productName);
        dest.writeInt(this.categoryId);
        dest.writeInt(this.productId);
        dest.writeString(this.productImage);
        dest.writeDouble(this.displayProductPrice);
        dest.writeString(this.outOfStock);
    }

    public void readFromParcel(Parcel source) {
        this.productName = source.readString();
        this.categoryId = source.readInt();
        this.productId = source.readInt();
        this.productImage = source.readString();
        this.displayProductPrice = source.readDouble();
        this.outOfStock = source.readString();
    }

    public ProductListModel() {
    }

    protected ProductListModel(Parcel in) {
        this.productName = in.readString();
        this.categoryId = in.readInt();
        this.productId = in.readInt();
        this.productImage = in.readString();
        this.displayProductPrice = in.readDouble();
        this.outOfStock = in.readString();
    }

    public static final Parcelable.Creator<ProductListModel> CREATOR = new Parcelable.Creator<ProductListModel>() {
        @Override
        public ProductListModel createFromParcel(Parcel source) {
            return new ProductListModel(source);
        }

        @Override
        public ProductListModel[] newArray(int size) {
            return new ProductListModel[size];
        }
    };
}
