package testme.myapp.com.ntrp.module_trackyourpayment.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.fragment.app.Fragment;

import testme.myapp.com.ntrp.R;

public class OpenPdfUrlFragment extends Fragment {

    private WebView webView;

    public static String Url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_webview_download_pdf, container, false);
        initializeView(view);
        webView = (WebView) view.findViewById(R.id.pdf_web_view);

        webView.requestFocus();
        webView.getSettings().setLightTouchEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.setSoundEffectsEnabled(true);
        webView.loadData(Url,
                "text/html", "UTF-8");
        webView.loadUrl(Url);
        return view;
    }

    private void initializeView(View view) {

    }

}
