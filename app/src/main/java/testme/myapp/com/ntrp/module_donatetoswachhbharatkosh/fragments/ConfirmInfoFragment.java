package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.ConfirmInfoModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.DepositorDetailsSubmitModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.GetConfirmInfoModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.SubmitConfirmInfoModel;
import testme.myapp.com.ntrp.module_trackyourpayment.utils.EnglishNumberToWords;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

public class ConfirmInfoFragment extends Fragment {

    private View localView;
    private TextView name_textview_value,address1_textview_value,address2_textview_value,city_textview_value;
    private TextView district_textview_value,state_textview_value,country_textview_value,pin_zip_textview_value;
    private TextView email_textview_value,mobile_textview_value;
    private TextView serial_no_textview_value,agency_name_textview_value,purpose_textview_value;
    private TextView amount_textview_value,final_amount_textview_value,amount_in_text_textview;
    private Button confirm_button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_confirm_info, container, false);
        localView=view;
        initializeView();
        CloseKeyBoard();

        Bundle bundle=getArguments();
        if(bundle!=null)
        {


            /*name_textview_value.setText(bundle.getString("Name"));
            address1_textview_value.setText(bundle.getString("Address1"));
            address2_textview_value.setText(bundle.getString("Address2"));
            city_textview_value.setText(bundle.getString("Country"));
            district_textview_value.setText(bundle.getString("State"));
            state_textview_value.setText(bundle.getString("District"));
            country_textview_value.setText(bundle.getString("City"));
            pin_zip_textview_value.setText(bundle.getString("Pincode"));
            email_textview_value.setText(bundle.getString("Mobile"));
            mobile_textview_value.setText(bundle.getString("Email"));

            serial_no_textview_value.setText(bundle.getString("1"));
            agency_name_textview_value.setText(bundle.getString("Swachh Bharat Kosh"));
            purpose_textview_value.setText(bundle.getString("Donation To Swachh Bharat Kosh"));
            amount_textview_value.setText(""+DonateToSwachhBharatKoshFragment.FinalAmount);
            final_amount_textview_value.setText(""+DonateToSwachhBharatKoshFragment.FinalAmount);
            amount_in_text_textview.setText(""+DonateToSwachhBharatKoshFragment.FinalAmountText);*/


        }



        return view;
    }

    private void initializeView() {

        name_textview_value=localView.findViewById(R.id.name_textview_value);
        address1_textview_value=localView.findViewById(R.id.address1_textview_value);
        address2_textview_value=localView.findViewById(R.id.address2_textview_value);
        city_textview_value=localView.findViewById(R.id.city_textview_value);
        district_textview_value=localView.findViewById(R.id.district_textview_value);
        state_textview_value=localView.findViewById(R.id.state_textview_value);
        country_textview_value=localView.findViewById(R.id.country_textview_value);
        pin_zip_textview_value=localView.findViewById(R.id.pin_zip_textview_value);
        email_textview_value=localView.findViewById(R.id.email_textview_value);
        mobile_textview_value=localView.findViewById(R.id.mobile_textview_value);

        serial_no_textview_value=localView.findViewById(R.id.serial_no_textview_value);
        agency_name_textview_value=localView.findViewById(R.id.agency_name_textview_value);
        purpose_textview_value=localView.findViewById(R.id.purpose_textview_value);
        amount_textview_value=localView.findViewById(R.id.amount_textview_value);
        final_amount_textview_value=localView.findViewById(R.id.final_amount_textview_value);
        amount_in_text_textview=localView.findViewById(R.id.amount_in_text_textview);

        confirm_button=localView.findViewById(R.id.confirm_button);
        set_OnClick_ConfirmButton();
        HitGetInfoDetailsAPI();
    }
    private void HitGetInfoDetailsAPI() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDepositorDeatilsConfirmURL;
        final ProgressDialog pDialog = new ProgressDialog(ConfirmInfoFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                GetConfirmInfoModel confirmInfoModel = gson.fromJson(response, GetConfirmInfoModel.class);


                if (confirmInfoModel.getReceiptEntryId()!=null&&confirmInfoModel.getAcsessTokenValue()!=null&&confirmInfoModel.getRefreshTokenValue()!=null)
                {
                    MyToken.setToken(confirmInfoModel.getRefreshTokenValue());
                    MyToken.setAuthorization(confirmInfoModel.getAcsessTokenValue());


                    name_textview_value.setText(confirmInfoModel.getDepositerDetailslst().get(0).getDepositorName());
                    address1_textview_value.setText(confirmInfoModel.getDepositerDetailslst().get(0).getAddress1());
                    address2_textview_value.setText(confirmInfoModel.getDepositerDetailslst().get(0).getAddress2());
                    city_textview_value.setText(confirmInfoModel.getDepositerDetailslst().get(0).getCity());
                    district_textview_value.setText(confirmInfoModel.getDepositerDetailslst().get(0).getDistrict());
                    state_textview_value.setText(confirmInfoModel.getDepositerDetailslst().get(0).getState());
                    country_textview_value.setText(confirmInfoModel.getDepositerDetailslst().get(0).getCountry());
                    pin_zip_textview_value.setText(confirmInfoModel.getDepositerDetailslst().get(0).getPincode());
                    email_textview_value.setText(confirmInfoModel.getDepositerDetailslst().get(0).getEmail());
                    mobile_textview_value.setText(confirmInfoModel.getDepositerDetailslst().get(0).getMobileNo());

                    serial_no_textview_value.setText("1");
                    agency_name_textview_value.setText(confirmInfoModel.getSwachhBharatlst().get(0).getAgencyName());
                    purpose_textview_value.setText(confirmInfoModel.getSwachhBharatlst().get(0).getPurposeDescription());
                    amount_textview_value.setText(""+confirmInfoModel.getSwachhBharatlst().get(0).getAmountValue());
                    final_amount_textview_value.setText(""+confirmInfoModel.getSwachhBharatlst().get(0).getAmountValue());
                    amount_in_text_textview.setText("Amount (in words): INR "+EnglishNumberToWords.convert(confirmInfoModel.getSwachhBharatlst().get(0).getAmountValue().longValue()));







                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryID", String.valueOf(Prefs.getInt("ReceiptEntryId",0)));
                params.put("Type", "SBK");
                params.put("RefreshTokenValue", MyToken.getToken());
                if (MainActivity.IsLogin)
                {
                    params.put("UserId",  MainActivity.UserID );
                    params.put("UserName", MainActivity.UserName);
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }
    private void set_OnClick_ConfirmButton()
    {
        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HitSubmitInfoDetailsAPI();

            }
        });
    }

    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
    private void HitSubmitInfoDetailsAPI() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getSubmitConfirmURL;
        final ProgressDialog pDialog = new ProgressDialog(ConfirmInfoFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                SubmitConfirmInfoModel confirmInfoModel = gson.fromJson(response, SubmitConfirmInfoModel.class);


                if (confirmInfoModel.getResponseStatus()!=null&&confirmInfoModel.getResponseStatus().matches("Y")&& confirmInfoModel.getReceiptEntryId()!=null&&confirmInfoModel.getAcsessTokenValue()!=null&&confirmInfoModel.getRefreshTokenValue()!=null)
                {
                    MyToken.setToken(confirmInfoModel.getRefreshTokenValue());
                    MyToken.setAuthorization(confirmInfoModel.getAcsessTokenValue());

                    changeFragment_PaymentFragment();

                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryID", String.valueOf(Prefs.getInt("ReceiptEntryId",0)));

                params.put("RefreshTokenValue", MyToken.getToken());

                if (MainActivity.IsLogin)
                {
                    params.put("UserId",  MainActivity.UserID );
                    params.put("UserName", MainActivity.UserName);
                }
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }
    private void changeFragment_PaymentFragment()
    {
        Fragment paymentFragment = new PaymentFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, paymentFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

}
