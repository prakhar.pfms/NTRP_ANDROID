package testme.myapp.com.ntrp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.DonateToSwachhBharatKoshFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.TrackYourPaymentFragment;


import testme.myapp.com.ntrp.module_trackyourpayment.utils.BitmapTransform;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.utils.RepeatSafeToast;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeAdaptor extends RecyclerView.Adapter<testme.myapp.com.ntrp.adapters.HomeAdaptor.HomePageViewHolder> {

    private List<String> data;
    private List<String> top_data;

    private List<Integer> image;
    private Fragment fragment;

    public HomeAdaptor(List<String> data, List<Integer> image, Fragment fragment,List<String> top_data) {
        this.data = data;
        this.image = image;
        this.fragment = fragment;
        this.top_data=top_data;
    }

    @Override
    public HomePageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.single_view_home, parent, false);
        return new HomePageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final HomePageViewHolder holder, final int position) {
        holder.bind(position);
    }
    @Override
    public int getItemCount() {
        return data.size();
    }

    public class HomePageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.menu_name_top)
        TextView menu_name_top;
        @BindView(R.id.menu_name)
        TextView menu_name;
        @BindView(R.id.image_icon)
        ImageView image_icon;
        @BindView(R.id.full_layout)
        RelativeLayout full_layout;

        public HomePageViewHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }

        public void bind(final int position)
        {
            menu_name_top.setText(top_data.get(position));
            menu_name.setText(data.get(position));


            int size = (int) Math.ceil(Math.sqrt(MainActivity.MAX_WIDTH * MainActivity.MAX_HEIGHT));


            Picasso.with(fragment.getContext())
                    .load(image.get(position))
                    .transform(new BitmapTransform(MainActivity.MAX_WIDTH, MainActivity.MAX_HEIGHT))
                    .resize(size, size).centerInside()

                    .into(image_icon);


            if (position>1)
            {
                image_icon.setImageAlpha(100);
            }


            full_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position==0)
                    {
                        if (CheckNetworkConnection.getInstance(fragment.getActivity()).isOnline()) {
                            TrackYourPaymentFragment newFragment = new TrackYourPaymentFragment();

                            /*Bundle bundle=new Bundle();
                            bundle.putString("Heading",data.get(position));
                            newFragment.setArguments(bundle);*/

                            FragmentTransaction transaction = fragment.getActivity().getSupportFragmentManager().beginTransaction();
                            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right,R.anim.slide_enter, R.anim.slide_exit);
                            transaction.replace(R.id.container_frame, newFragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                        else
                        {
                            RepeatSafeToast.show(fragment.getActivity(), fragment.getActivity().getResources().getString(R.string.check_your_internet_connection));
                        }
                    }
                    else if (position==1)
                    {
                        if (CheckNetworkConnection.getInstance(fragment.getActivity()).isOnline()) {
                            DonateToSwachhBharatKoshFragment newFragment = new DonateToSwachhBharatKoshFragment();

                            /*Bundle bundle=new Bundle();
                            bundle.putString("Heading",data.get(position));
                            newFragment.setArguments(bundle);*/

                            FragmentTransaction transaction = fragment.getActivity().getSupportFragmentManager().beginTransaction();
                            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right,R.anim.slide_enter, R.anim.slide_exit);
                            transaction.replace(R.id.container_frame, newFragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                        else
                        {
                            RepeatSafeToast.show(fragment.getActivity(), fragment.getActivity().getResources().getString(R.string.check_your_internet_connection));
                        }
                    }




                }
            });
        }
    }
}
