package testme.myapp.com.ntrp.module_trackyourpayment.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by deepika.s on 25-04-2017.
 */

public class TransactionList {

    @SerializedName("TransactionRefNumber")
    @Expose
    private String transactionRefNumber;
    @SerializedName("ChallanNo")
    @Expose
    private String challanNo;
    @SerializedName("OrderCode")
    @Expose
    private Object orderCode;
    @SerializedName("TransactionNumber")
    @Expose
    private String transactionNumber;
    @SerializedName("ReceiptEntryID")
    @Expose
    private Integer receiptEntryID;
    @SerializedName("ReceiptEntryDetailID")
    @Expose
    private Integer receiptEntryDetailID;
    @SerializedName("TransactionDate")
    @Expose
    private String transactionDate;
    @SerializedName("PayeeName")
    @Expose
    private String payeeName;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("UtrnNo")
    @Expose
    private String utrnNo;
    @SerializedName("UTRNoStatus")
    @Expose
    private Object uTRNoStatus;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("PaymentMode")
    @Expose
    private Object paymentMode;
    @SerializedName("Email")
    @Expose
    private Object email;
    @SerializedName("CurrencyCode")
    @Expose
    private String currencyCode;
    @SerializedName("ReceiptNumber")
    @Expose
    private Object receiptNumber;
    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("Totalamount")
    @Expose
    private Double totalamount;
    @SerializedName("ReceiptFile")
    @Expose
    private Object receiptFile;
    @SerializedName("ChallanFile")
    @Expose
    private Object challanFile;
    @SerializedName("NEFTDate")
    @Expose
    private String nEFTDate;
    @SerializedName("NTRPFinYear")
    @Expose
    private String nTRPFinYear;
    @SerializedName("CBEC_PassportNO")
    @Expose
    private Object cBECPassportNO;
    @SerializedName("ActiveArchive")
    @Expose
    private Boolean activeArchive;
    @SerializedName("TotalRecCount")
    @Expose
    private Integer totalRecCount;

    public String getTransactionRefNumber() {
        return transactionRefNumber;
    }

    public void setTransactionRefNumber(String transactionRefNumber) {
        this.transactionRefNumber = transactionRefNumber;
    }

    public String getChallanNo() {
        return challanNo;
    }

    public void setChallanNo(String challanNo) {
        this.challanNo = challanNo;
    }

    public Object getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(Object orderCode) {
        this.orderCode = orderCode;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public Integer getReceiptEntryID() {
        return receiptEntryID;
    }

    public void setReceiptEntryID(Integer receiptEntryID) {
        this.receiptEntryID = receiptEntryID;
    }

    public Integer getReceiptEntryDetailID() {
        return receiptEntryDetailID;
    }

    public void setReceiptEntryDetailID(Integer receiptEntryDetailID) {
        this.receiptEntryDetailID = receiptEntryDetailID;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUtrnNo() {
        return utrnNo;
    }

    public void setUtrnNo(String utrnNo) {
        this.utrnNo = utrnNo;
    }

    public Object getUTRNoStatus() {
        return uTRNoStatus;
    }

    public void setUTRNoStatus(Object uTRNoStatus) {
        this.uTRNoStatus = uTRNoStatus;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Object getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(Object paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Object getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(Object receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(Double totalamount) {
        this.totalamount = totalamount;
    }

    public Object getReceiptFile() {
        return receiptFile;
    }

    public void setReceiptFile(Object receiptFile) {
        this.receiptFile = receiptFile;
    }

    public Object getChallanFile() {
        return challanFile;
    }

    public void setChallanFile(Object challanFile) {
        this.challanFile = challanFile;
    }

    public String getNEFTDate() {
        return nEFTDate;
    }

    public void setNEFTDate(String nEFTDate) {
        this.nEFTDate = nEFTDate;
    }

    public String getNTRPFinYear() {
        return nTRPFinYear;
    }

    public void setNTRPFinYear(String nTRPFinYear) {
        this.nTRPFinYear = nTRPFinYear;
    }

    public Object getCBECPassportNO() {
        return cBECPassportNO;
    }

    public void setCBECPassportNO(Object cBECPassportNO) {
        this.cBECPassportNO = cBECPassportNO;
    }

    public Boolean getActiveArchive() {
        return activeArchive;
    }

    public void setActiveArchive(Boolean activeArchive) {
        this.activeArchive = activeArchive;
    }

    public Integer getTotalRecCount() {
        return totalRecCount;
    }

    public void setTotalRecCount(Integer totalRecCount) {
        this.totalRecCount = totalRecCount;
    }

}
