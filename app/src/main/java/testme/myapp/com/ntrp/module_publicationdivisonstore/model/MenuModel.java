package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import com.google.gson.annotations.SerializedName;

public class MenuModel {
    @SerializedName("@url")
    private String url;
    @SerializedName("@ID")
    private String iD;
    @SerializedName("@ParentID")
    private String parentID;
    @SerializedName("@Code")
    private String code;
    @SerializedName("@Title")
    private String title;
    @SerializedName("@Text")
    private String text;
    @SerializedName("@DisplayOrder")
    private String displayOrder;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getiD() {
        return iD;
    }

    public void setiD(String iD) {
        this.iD = iD;
    }

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(String displayOrder) {
        this.displayOrder = displayOrder;
    }
}
