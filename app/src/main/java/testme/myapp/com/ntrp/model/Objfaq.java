
package testme.myapp.com.ntrp.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class Objfaq {

    @SerializedName("Question")
    @Expose
    private String question;
    @SerializedName("Answer")
    @Expose
    private String answer;
    @SerializedName("Type")
    @Expose
    private String type;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
