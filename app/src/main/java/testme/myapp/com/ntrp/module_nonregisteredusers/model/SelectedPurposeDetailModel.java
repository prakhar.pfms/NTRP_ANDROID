package testme.myapp.com.ntrp.module_nonregisteredusers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SelectedPurposeDetailModel {

    @SerializedName("ddlYearLst")
    @Expose
    private Object ddlYearLst;
    @SerializedName("ddlCurrencylst")
    @Expose
    private List<DdlCurrencylst> ddlCurrencylst = null;
    @SerializedName("ddlReceiptPaymentTypelst")
    @Expose
    private List<DdlReceiptPaymentTypelst> ddlReceiptPaymentTypelst = null;
    @SerializedName("ddlPeriodLst")
    @Expose
    private List<DdlPeriodLst> ddlPeriodLst = null;
    @SerializedName("ddlPAOLst")
    @Expose
    private List<DdlPAOLst> ddlPAOLst = null;
    @SerializedName("ddlDDOLst")
    @Expose
    private List<DdlDDOLst> ddlDDOLst = null;
    @SerializedName("lblPaymenttypetext")
    @Expose
    private String lblPaymenttypetext;
    @SerializedName("lblMinistryInfo")
    @Expose
    private String lblMinistryInfo;
    @SerializedName("txtBxPurpose")
    @Expose
    private String txtBxPurpose;
    @SerializedName("ddlCurrency")
    @Expose
    private String ddlCurrency;
    @SerializedName("ministryId")
    @Expose
    private String ministryId;
    @SerializedName("ddlPAO")
    @Expose
    private String ddlPAO;
    @SerializedName("ddlPeriod")
    @Expose
    private Object ddlPeriod;
    @SerializedName("ddlPeriodTypeText")
    @Expose
    private String ddlPeriodTypeText;
    @SerializedName("ddlReceiptPaymentType")
    @Expose
    private String ddlReceiptPaymentType;
    @SerializedName("PaymentPeriodFreq")
    @Expose
    private String paymentPeriodFreq;
    @SerializedName("ddlServiceYearLst")
    @Expose
    private List<Object> ddlServiceYearLst = null;
    @SerializedName("lstPurposeMinAndMaxAmountDetails")
    @Expose
    private List<LstPurposeMinAndMaxAmountDetail> lstPurposeMinAndMaxAmountDetails = null;
    @SerializedName("functionalHeadId")
    @Expose
    private String functionalHeadId;
    @SerializedName("validationType")
    @Expose
    private String validationType;
    @SerializedName("accountId")
    @Expose
    private String accountId;
    @SerializedName("accGroupId")
    @Expose
    private String accGroupId;
    @SerializedName("agencyId")
    @Expose
    private String agencyId;
    @SerializedName("agencyInfo")
    @Expose
    private String agencyInfo;
    @SerializedName("ddlDepsitorCategorySelectedValue")
    @Expose
    private Object ddlDepsitorCategorySelectedValue;
    @SerializedName("lblFunctionHeadInfo")
    @Expose
    private String lblFunctionHeadInfo;
    @SerializedName("hdnAccShortName")
    @Expose
    private Object hdnAccShortName;
    @SerializedName("hdnPaymentModeId")
    @Expose
    private Object hdnPaymentModeId;
    @SerializedName("hdnObjectHead")
    @Expose
    private Object hdnObjectHead;
    @SerializedName("ErrorCodes")
    @Expose
    private Object errorCodes;
    @SerializedName("ResponseStatus")
    @Expose
    private Object responseStatus;
    @SerializedName("XmlDocumentResult")
    @Expose
    private Object xmlDocumentResult;
    @SerializedName("AcsessTokenValue")
    @Expose
    private String acsessTokenValue;
    @SerializedName("RefreshTokenValue")
    @Expose
    private String refreshTokenValue;
    @SerializedName("ResponseEmail")
    @Expose
    private Object responseEmail;
    @SerializedName("ResponseMobileNo")
    @Expose
    private Object responseMobileNo;
    @SerializedName("ResponseUserType_CBEC")
    @Expose
    private Object responseUserTypeCBEC;
    @SerializedName("ResponseFirstName")
    @Expose
    private Object responseFirstName;
    @SerializedName("ResponseLastName")
    @Expose
    private Object responseLastName;
    @SerializedName("ResponseCountryCode")
    @Expose
    private Object responseCountryCode;
    @SerializedName("ResponseUserId")
    @Expose
    private Integer responseUserId;
    @SerializedName("PdfFileByte")
    @Expose
    private Object pdfFileByte;
    @SerializedName("XmlDreports")
    @Expose
    private Object xmlDreports;
    @SerializedName("ReceiptEntryId")
    @Expose
    private Integer receiptEntryId;
    @SerializedName("ShippingCharges")
    @Expose
    private Object shippingCharges;
    @SerializedName("PubUserType")
    @Expose
    private Integer pubUserType;
    @SerializedName("DisplayPopup")
    @Expose
    private Object displayPopup;
    @SerializedName("RowNo")
    @Expose
    private Object rowNo;
    @SerializedName("PaymentPurposeID")
    @Expose
    private Integer paymentPurposeID;
    @SerializedName("TotalDiscount")
    @Expose
    private Object totalDiscount;
    @SerializedName("PageCount")
    @Expose
    private Integer pageCount;

    public Object getDdlYearLst() {
        return ddlYearLst;
    }

    public void setDdlYearLst(Object ddlYearLst) {
        this.ddlYearLst = ddlYearLst;
    }

    public List<DdlCurrencylst> getDdlCurrencylst() {
        return ddlCurrencylst;
    }

    public void setDdlCurrencylst(List<DdlCurrencylst> ddlCurrencylst) {
        this.ddlCurrencylst = ddlCurrencylst;
    }

    public List<DdlReceiptPaymentTypelst> getDdlReceiptPaymentTypelst() {
        return ddlReceiptPaymentTypelst;
    }

    public void setDdlReceiptPaymentTypelst(List<DdlReceiptPaymentTypelst> ddlReceiptPaymentTypelst) {
        this.ddlReceiptPaymentTypelst = ddlReceiptPaymentTypelst;
    }

    public List<DdlPeriodLst> getDdlPeriodLst() {
        return ddlPeriodLst;
    }

    public void setDdlPeriodLst(List<DdlPeriodLst> ddlPeriodLst) {
        this.ddlPeriodLst = ddlPeriodLst;
    }

    public List<DdlPAOLst> getDdlPAOLst() {
        return ddlPAOLst;
    }

    public void setDdlPAOLst(List<DdlPAOLst> ddlPAOLst) {
        this.ddlPAOLst = ddlPAOLst;
    }

    public List<DdlDDOLst> getDdlDDOLst() {
        return ddlDDOLst;
    }

    public void setDdlDDOLst(List<DdlDDOLst> ddlDDOLst) {
        this.ddlDDOLst = ddlDDOLst;
    }

    public String getLblPaymenttypetext() {
        return lblPaymenttypetext;
    }

    public void setLblPaymenttypetext(String lblPaymenttypetext) {
        this.lblPaymenttypetext = lblPaymenttypetext;
    }

    public String getLblMinistryInfo() {
        return lblMinistryInfo;
    }

    public void setLblMinistryInfo(String lblMinistryInfo) {
        this.lblMinistryInfo = lblMinistryInfo;
    }

    public String getTxtBxPurpose() {
        return txtBxPurpose;
    }

    public void setTxtBxPurpose(String txtBxPurpose) {
        this.txtBxPurpose = txtBxPurpose;
    }

    public String getDdlCurrency() {
        return ddlCurrency;
    }

    public void setDdlCurrency(String ddlCurrency) {
        this.ddlCurrency = ddlCurrency;
    }

    public String getMinistryId() {
        return ministryId;
    }

    public void setMinistryId(String ministryId) {
        this.ministryId = ministryId;
    }

    public String getDdlPAO() {
        return ddlPAO;
    }

    public void setDdlPAO(String ddlPAO) {
        this.ddlPAO = ddlPAO;
    }

    public Object getDdlPeriod() {
        return ddlPeriod;
    }

    public void setDdlPeriod(Object ddlPeriod) {
        this.ddlPeriod = ddlPeriod;
    }

    public String getDdlPeriodTypeText() {
        return ddlPeriodTypeText;
    }

    public void setDdlPeriodTypeText(String ddlPeriodTypeText) {
        this.ddlPeriodTypeText = ddlPeriodTypeText;
    }

    public String getDdlReceiptPaymentType() {
        return ddlReceiptPaymentType;
    }

    public void setDdlReceiptPaymentType(String ddlReceiptPaymentType) {
        this.ddlReceiptPaymentType = ddlReceiptPaymentType;
    }

    public String getPaymentPeriodFreq() {
        return paymentPeriodFreq;
    }

    public void setPaymentPeriodFreq(String paymentPeriodFreq) {
        this.paymentPeriodFreq = paymentPeriodFreq;
    }

    public List<Object> getDdlServiceYearLst() {
        return ddlServiceYearLst;
    }

    public void setDdlServiceYearLst(List<Object> ddlServiceYearLst) {
        this.ddlServiceYearLst = ddlServiceYearLst;
    }

    public List<LstPurposeMinAndMaxAmountDetail> getLstPurposeMinAndMaxAmountDetails() {
        return lstPurposeMinAndMaxAmountDetails;
    }

    public void setLstPurposeMinAndMaxAmountDetails(List<LstPurposeMinAndMaxAmountDetail> lstPurposeMinAndMaxAmountDetails) {
        this.lstPurposeMinAndMaxAmountDetails = lstPurposeMinAndMaxAmountDetails;
    }

    public String getFunctionalHeadId() {
        return functionalHeadId;
    }

    public void setFunctionalHeadId(String functionalHeadId) {
        this.functionalHeadId = functionalHeadId;
    }

    public String getValidationType() {
        return validationType;
    }

    public void setValidationType(String validationType) {
        this.validationType = validationType;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccGroupId() {
        return accGroupId;
    }

    public void setAccGroupId(String accGroupId) {
        this.accGroupId = accGroupId;
    }

    public String getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId;
    }

    public String getAgencyInfo() {
        return agencyInfo;
    }

    public void setAgencyInfo(String agencyInfo) {
        this.agencyInfo = agencyInfo;
    }

    public Object getDdlDepsitorCategorySelectedValue() {
        return ddlDepsitorCategorySelectedValue;
    }

    public void setDdlDepsitorCategorySelectedValue(Object ddlDepsitorCategorySelectedValue) {
        this.ddlDepsitorCategorySelectedValue = ddlDepsitorCategorySelectedValue;
    }

    public String getLblFunctionHeadInfo() {
        return lblFunctionHeadInfo;
    }

    public void setLblFunctionHeadInfo(String lblFunctionHeadInfo) {
        this.lblFunctionHeadInfo = lblFunctionHeadInfo;
    }

    public Object getHdnAccShortName() {
        return hdnAccShortName;
    }

    public void setHdnAccShortName(Object hdnAccShortName) {
        this.hdnAccShortName = hdnAccShortName;
    }

    public Object getHdnPaymentModeId() {
        return hdnPaymentModeId;
    }

    public void setHdnPaymentModeId(Object hdnPaymentModeId) {
        this.hdnPaymentModeId = hdnPaymentModeId;
    }

    public Object getHdnObjectHead() {
        return hdnObjectHead;
    }

    public void setHdnObjectHead(Object hdnObjectHead) {
        this.hdnObjectHead = hdnObjectHead;
    }

    public Object getErrorCodes() {
        return errorCodes;
    }

    public void setErrorCodes(Object errorCodes) {
        this.errorCodes = errorCodes;
    }

    public Object getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Object responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getXmlDocumentResult() {
        return xmlDocumentResult;
    }

    public void setXmlDocumentResult(Object xmlDocumentResult) {
        this.xmlDocumentResult = xmlDocumentResult;
    }

    public String getAcsessTokenValue() {
        return acsessTokenValue;
    }

    public void setAcsessTokenValue(String acsessTokenValue) {
        this.acsessTokenValue = acsessTokenValue;
    }

    public String getRefreshTokenValue() {
        return refreshTokenValue;
    }

    public void setRefreshTokenValue(String refreshTokenValue) {
        this.refreshTokenValue = refreshTokenValue;
    }

    public Object getResponseEmail() {
        return responseEmail;
    }

    public void setResponseEmail(Object responseEmail) {
        this.responseEmail = responseEmail;
    }

    public Object getResponseMobileNo() {
        return responseMobileNo;
    }

    public void setResponseMobileNo(Object responseMobileNo) {
        this.responseMobileNo = responseMobileNo;
    }

    public Object getResponseUserTypeCBEC() {
        return responseUserTypeCBEC;
    }

    public void setResponseUserTypeCBEC(Object responseUserTypeCBEC) {
        this.responseUserTypeCBEC = responseUserTypeCBEC;
    }

    public Object getResponseFirstName() {
        return responseFirstName;
    }

    public void setResponseFirstName(Object responseFirstName) {
        this.responseFirstName = responseFirstName;
    }

    public Object getResponseLastName() {
        return responseLastName;
    }

    public void setResponseLastName(Object responseLastName) {
        this.responseLastName = responseLastName;
    }

    public Object getResponseCountryCode() {
        return responseCountryCode;
    }

    public void setResponseCountryCode(Object responseCountryCode) {
        this.responseCountryCode = responseCountryCode;
    }

    public Integer getResponseUserId() {
        return responseUserId;
    }

    public void setResponseUserId(Integer responseUserId) {
        this.responseUserId = responseUserId;
    }

    public Object getPdfFileByte() {
        return pdfFileByte;
    }

    public void setPdfFileByte(Object pdfFileByte) {
        this.pdfFileByte = pdfFileByte;
    }

    public Object getXmlDreports() {
        return xmlDreports;
    }

    public void setXmlDreports(Object xmlDreports) {
        this.xmlDreports = xmlDreports;
    }

    public Integer getReceiptEntryId() {
        return receiptEntryId;
    }

    public void setReceiptEntryId(Integer receiptEntryId) {
        this.receiptEntryId = receiptEntryId;
    }

    public Object getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(Object shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public Integer getPubUserType() {
        return pubUserType;
    }

    public void setPubUserType(Integer pubUserType) {
        this.pubUserType = pubUserType;
    }

    public Object getDisplayPopup() {
        return displayPopup;
    }

    public void setDisplayPopup(Object displayPopup) {
        this.displayPopup = displayPopup;
    }

    public Object getRowNo() {
        return rowNo;
    }

    public void setRowNo(Object rowNo) {
        this.rowNo = rowNo;
    }

    public Integer getPaymentPurposeID() {
        return paymentPurposeID;
    }

    public void setPaymentPurposeID(Integer paymentPurposeID) {
        this.paymentPurposeID = paymentPurposeID;
    }

    public Object getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Object totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}
