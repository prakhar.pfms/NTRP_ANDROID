package testme.myapp.com.ntrp.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import testme.myapp.com.ntrp.adapters.ExpandableListAdapter;
import testme.myapp.com.ntrp.events.MakeUserLogout;
import testme.myapp.com.ntrp.extra.MyCustomToast;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.extra.RepeatSafeToast;
import testme.myapp.com.ntrp.extra.UserModel;
import testme.myapp.com.ntrp.fragments.DisclaimerFragment;
import testme.myapp.com.ntrp.fragments.NewHomeFragmentTwo;
import testme.myapp.com.ntrp.fragments.UserGuideFragment;
import testme.myapp.com.ntrp.module_commonreceipts.fragments.CommonReceiptsFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.DonateToSwachhBharatKoshFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.PaymentWebViewFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.RegisterUserModel;
import testme.myapp.com.ntrp.module_login.events.AskMobileNumberDialogEvent;
import testme.myapp.com.ntrp.module_login.events.UserLoginEvent;
import testme.myapp.com.ntrp.module_login.fragments.LoginFragment;
import testme.myapp.com.ntrp.module_login.fragments.UserLogedInHomeFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.HitAPI;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.TrackYourPaymentFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.UserInputDetailModel;

import android.view.Window;

import testme.myapp.com.ntrp.R;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import testme.myapp.com.ntrp.fragments.AboutUsFragment;
import testme.myapp.com.ntrp.fragments.FAQFragment;
import testme.myapp.com.ntrp.fragments.ContactUsFragment;
import testme.myapp.com.ntrp.fragments.TermsAndConditionsFragment;
import testme.myapp.com.ntrp.fragments.RefundPolicyFragment;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.payments.CheckoutActivity;
import testme.myapp.com.ntrp.utils.AllUrls;

public class MainActivity extends AppCompatActivity  {

    public static String contactName, organizationName;
    public static String deviceID = "", deviceType = "";
    public static boolean IsLogin = false,IsDOTMReceiptMenuVisible=false;
    public static String mobileNumber = "";
    public static String UserID,UserName;
    public TextView name_textview, address_textView,user_name_in_left_menu;


    public static final int MAX_WIDTH = 400;
    public static final int MAX_HEIGHT = 400;

    public static int totalCartValue = 0;
    public static int totalCartPrice = 0;
    public static int totalCartUSDPrice = 0;
    public static Bundle TrackBundle = new Bundle();
    public static Bundle UserInputBundle = new Bundle();
    public static String rawHTML = "<HTML>" +
            "<head>" + "<style  type=\"text/css\">" +
            "body,h1{color: #ffffff;" +
            "</style></head>" +
            "<body style=\"text-align:justify\"> %s </body>" +
            "</HTML>";
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    private Menu nav_Menu;

    ExpandableListAdapter mMenuAdapter;
    ExpandableListView expandableList;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;


    private ImageView left_menu_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (TextUtils.isEmpty(Prefs.getString("userData"))) {
            showMobileNumberDialog();
        } else {
            UserModel model = new UserModel();
            model = new Gson().fromJson(Prefs.getString("userData"), UserModel.class);
            MyToken.setToken(model.getRefreshToken());
            MyToken.setAuthorization(model.getAccessToken());
            MyToken.setMobileNumber(model.getMobileNumber());
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        deviceType = Build.MODEL;

        put_UserInputData("null", "null", "null");



        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //navigationView.setNavigationItemSelectedListener(this);



        expandableList = (ExpandableListView) findViewById(R.id.navigationmenu);


        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }




        setMenus();
        changeScreen();

        //Intent intent=new Intent(MainActivity.this,CheckoutActivity.class);
        //startActivity(intent);


        View header = navigationView.getHeaderView(0);
        user_name_in_left_menu = (TextView) header.findViewById(R.id.user_name_in_left_menu);
        left_menu_image=(ImageView) header.findViewById(R.id.left_menu_image);


    }

    private void setMenus()
    {


        prepareListData();

        mMenuAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expandableList.setAdapter(mMenuAdapter);





        expandableList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView,
                                        View view,
                                        int groupPosition,
                                        int childPosition, long id) {
                //Log.d("DEBUG", "submenu item clicked");
               /* Toast.makeText(MainActivity.this,
                                "Header: "+String.valueOf(groupPosition) +
                                        "\nItem: "+ String.valueOf(childPosition), Toast.LENGTH_SHORT)
                        .show();*/

                OpenPagesOnLeftMenuClick(groupPosition,childPosition);

                /*view.setSelected(true);
                if (view_Group != null) {
                    view_Group.setBackgroundColor(Color.parseColor("#ffffff"));
                }
                view_Group = view;
                view_Group.setBackgroundColor(Color.parseColor("#DDDDDD"));*/
                drawer.closeDrawers();




                return false;
            }
        });
        expandableList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long l) {

                OpenPagesOnLeftMenuClick(groupPosition,5666);

                return false;
            }
        });
    }



    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            expandableList.setIndicatorBounds(expandableList.getRight()- 80, expandableList.getWidth());
        } else {
            expandableList.setIndicatorBoundsRelative(expandableList.getRight()- 80, expandableList.getWidth());
        }
    }
    private void changeScreen() {

        NewHomeFragmentTwo newFragment = new NewHomeFragmentTwo();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_frame, newFragment);
        transaction.commit();
    }
    private void change_LoginScreen() {

        LoginFragment newFragment = new LoginFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void change_HomeScreen() {

        NewHomeFragmentTwo newFragment = new NewHomeFragmentTwo();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void change_AboutUsFragment() {

        AboutUsFragment newFragment = new AboutUsFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void change_UserGuideFragment() {

        UserGuideFragment newFragment = new UserGuideFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void change_DisclaimerFragment() {

        DisclaimerFragment newFragment = new DisclaimerFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void change_ContactUsFragment() {
        ContactUsFragment newFragment = new ContactUsFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void change_FAQFragment() {
        FAQFragment newFragment = new FAQFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void change_RefundPolicyFragment() {
        RefundPolicyFragment newFragment = new RefundPolicyFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void change_TermsAndConditionsFragment() {
        TermsAndConditionsFragment newFragment = new TermsAndConditionsFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private void change_MyAccount(int childPosition) {

        if (childPosition==0)
        {
            drawer.closeDrawers();

            UserLogedInHomeFragment test = (UserLogedInHomeFragment) getSupportFragmentManager().findFragmentByTag("UserLogedInHomeFragment");
            if (test != null && test.isVisible()&& test.isResumed()) {
                //DO STUFF
            }
            else {

                UserLogedInHomeFragment newFragment = new UserLogedInHomeFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
                transaction.replace(R.id.container_frame, newFragment,"UserLogedInHomeFragment");
                transaction.addToBackStack(null);
                transaction.commit();
            }


        }

    }
    private void change_MakePayment(int childPosition) {

        if (childPosition==0)
        {
            drawer.closeDrawers();
            DonateToSwachhBharatKoshFragment newFragment = new DonateToSwachhBharatKoshFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
            transaction.replace(R.id.container_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        else if (childPosition==1)
        {
            drawer.closeDrawers();
            CommonReceiptsFragment newFragment = new CommonReceiptsFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
            transaction.replace(R.id.container_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
        else if (childPosition==2)
        {
            drawer.closeDrawers();
            Bundle b = new Bundle();
            b.putBoolean("makeDOTPurposeVisible",true);

            CommonReceiptsFragment newFragment = new CommonReceiptsFragment();
            newFragment.setArguments(b);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
            transaction.replace(R.id.container_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }
    private void change_TrackPayment() {
        TrackYourPaymentFragment newFragment = new TrackYourPaymentFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
        HitAPI.Hit_API_GetTrackTransaction(this,newFragment,"2022-2023",null,null,null,"0");
    }
    private void change_Logout() {
        user_name_in_left_menu.setVisibility(View.GONE);

        left_menu_image.setImageResource(R.drawable.bharatkoshlogo);
        left_menu_image.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        IsLogin=false;
        UserID=null;
        UserName=null;
        drawer.closeDrawers();
        setUsersMenus();
        changeScreen();
        showDialog("You have been logout !");

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

       // listDataHeader.add("Login/Register");
        listDataHeader.add("Home");
        listDataHeader.add("About Us");
        listDataHeader.add("Contact Us");
        listDataHeader.add("FAQ");
        listDataHeader.add("Terms & Condition");
        listDataHeader.add("ChargeBack/Refund Policy");
        listDataHeader.add("Disclaimer");
        listDataHeader.add("User Guide");



    }

    private void setUsersMenus()
    {
        if (IsLogin)
        {
            listDataHeader.add("My Account");
            listDataHeader.add("Make Your payment");
            listDataHeader.add("Track Payment");

            listDataHeader.add("Logout");


            List<String> heading1 = new ArrayList<String>();
            heading1.add("My Profile");


            List<String> heading2 = new ArrayList<String>();
            heading2.add("Donate to SwachhBharatKosh");
            heading2.add("Payment To GOI");
            if(IsDOTMReceiptMenuVisible)
            {
                heading2.add("Receipt for DOT");
            }


            listDataChild.put(listDataHeader.get(8), heading1);
            listDataChild.put(listDataHeader.get(9), heading2);

            mMenuAdapter.notifyDataSetChanged();
        }
        else
        {
            listDataChild.clear();
            listDataHeader.subList(8,12).clear();
            mMenuAdapter.notifyDataSetChanged();
        }


    }

    private void OpenPagesOnLeftMenuClick(int Clicked_position,int childPosition)
    {
        switch (Clicked_position) {

            /*case 0:
                drawer.closeDrawers();
                change_LoginScreen();
                break;*/
            case 0:
                drawer.closeDrawers();
               change_HomeScreen();
                break;
            case 1:
                drawer.closeDrawers();
                change_AboutUsFragment();
                break;
            case 2:
                drawer.closeDrawers();
                change_ContactUsFragment();
                break;
            case 3:
                drawer.closeDrawers();
                change_FAQFragment();
                break;
            case 4:
                drawer.closeDrawers();
                change_TermsAndConditionsFragment();
                break;
            case 5:
                drawer.closeDrawers();
                change_RefundPolicyFragment();
                break;
            case 6:
                drawer.closeDrawers();
                change_DisclaimerFragment();
                break;
            case 7:
                drawer.closeDrawers();
                change_UserGuideFragment();
                break;
            case 8:
                drawer.closeDrawers();
                change_MyAccount(childPosition);
                break;
            case 9:
                drawer.closeDrawers();
                change_MakePayment(childPosition);
                break;
            case 10:
                drawer.closeDrawers();
                change_TrackPayment();
                break;
            case 11:
                drawer.closeDrawers();
                change_Logout();
                break;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        //revision: this don't works, use setOnChildClickListener() and setOnGroupClickListener() above instead
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        drawer.closeDrawers();
                        return true;
                    }
                });
    }
    /*@Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();


        if (id == R.id.nav_home) {

            setTitle("Home");
            NewHomeFragment newFragment = new NewHomeFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
            transaction.replace(R.id.container_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (id == R.id.nav_about) {
            AboutUsFragment newFragment = new AboutUsFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
            transaction.replace(R.id.container_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (id == R.id.nav_contact) {
            ContactUsFragment newFragment = new ContactUsFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
            transaction.replace(R.id.container_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (id == R.id.nav_faq) {
            FAQFragment newFragment = new FAQFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
            transaction.replace(R.id.container_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (id == R.id.nav_refundpolicy) {
            RefundPolicyFragment newFragment = new RefundPolicyFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
            transaction.replace(R.id.container_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (id == R.id.nav_tandc) {
            TermsAndConditionsFragment newFragment = new TermsAndConditionsFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
            transaction.replace(R.id.container_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/

    public void setMenuItemsUserWise() {
        nav_Menu = navigationView.getMenu();
        if (MainActivity.IsLogin == false) {
            nav_Menu.findItem(R.id.nav_home).setVisible(true);
            nav_Menu.findItem(R.id.nav_about).setVisible(true);
            nav_Menu.findItem(R.id.nav_contact).setVisible(true);
            nav_Menu.findItem(R.id.nav_faq).setVisible(true);
            nav_Menu.findItem(R.id.nav_tandc).setVisible(true);
            nav_Menu.findItem(R.id.nav_refundpolicy).setVisible(true);
            nav_Menu.findItem(R.id.my_account).setVisible(false);
            nav_Menu.findItem(R.id.make_your_payment).setVisible(false);
            nav_Menu.findItem(R.id.nav_track_payment).setVisible(false);
            nav_Menu.findItem(R.id.nav_logout).setVisible(false);

        } else if (MainActivity.IsLogin){

            nav_Menu.findItem(R.id.nav_home).setVisible(true);
            nav_Menu.findItem(R.id.nav_about).setVisible(true);
            nav_Menu.findItem(R.id.nav_contact).setVisible(true);
            nav_Menu.findItem(R.id.nav_faq).setVisible(true);
            nav_Menu.findItem(R.id.nav_tandc).setVisible(true);
            nav_Menu.findItem(R.id.nav_refundpolicy).setVisible(true);
            nav_Menu.findItem(R.id.my_account).setVisible(true);
            nav_Menu.findItem(R.id.make_your_payment).setVisible(true);
            nav_Menu.findItem(R.id.nav_track_payment).setVisible(true);
            nav_Menu.findItem(R.id.nav_logout).setVisible(true);
        }
    }

    /*@Override
    public void onBackPressed() {

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.exit_dialog);
        dialog.setCancelable(false);
        dialog.show();

        Button dialog_yes_button = (Button) dialog.findViewById(R.id.dialog_yes_button);
        dialog_yes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                dialog.dismiss();
                System.exit(0);
            }
        });
        Button dialog_no_button = (Button) dialog.findViewById(R.id.dialog_no_button);
        dialog_no_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }*/
    private void put_UserInputData(String countryCode, String mobileno, String emailID) {
        UserInputDetailModel userInputDetailModel = new UserInputDetailModel();
        userInputDetailModel.CountryCode = null;
        userInputDetailModel.MobileNumber = null;
        userInputDetailModel.EmailID = null;
        UserInputBundle.putParcelable("userInputDetailModel", userInputDetailModel);
    }

    private void showMobileNumberDialog() {
        TextView tvProceed;
        EditText etMobile;
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (null != dialog.getWindow())
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations;
        dialog.setContentView(R.layout.dialog_mobile_number);
        tvProceed = dialog.findViewById(R.id.tvProceed);
        etMobile = dialog.findViewById(R.id.etMobile);
        dialog.setCancelable(false);
        tvProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(etMobile.getText().toString())) {
                    if(etMobile.getText().toString().length()==10)
                    {
                        if (CheckNetworkConnection.getInstance(MainActivity.this).isOnline())
                            Hit_RegisterUser_API(etMobile.getText().toString(), dialog);
                        else
                            MyCustomToast.createErrorToast(MainActivity.this, "Check Your Internet Connection!");
                    }
                    else
                    {
                        RepeatSafeToast.show(MainActivity.this, "Please enter 10 digit mobile number");
                    }
                    // Hit_API_Token();
                } else {
                    RepeatSafeToast.show(MainActivity.this, "Please enter mobile number");
                    // Toast.makeText(MainActivity.this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    private void Hit_RegisterUser_API(String mobileNumber, Dialog dialog) {
        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.registerAPIUserURL;
        final ProgressDialog pDialog = new ProgressDialog(MainActivity.this, R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                RegisterUserModel registerUserModel = gson.fromJson(response, RegisterUserModel.class);

                if(registerUserModel.getErrorCodes().toString().matches("User Already Exists")||
                        registerUserModel.getErrorCodes().toString().matches("User Registered Successfully"))
                {
                    Hit_API_Token(mobileNumber,dialog);
                }
                else
                {

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("MobileNo", ""+mobileNumber);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public void Hit_API_Token(String mobileNumber, Dialog dialog) {
        //System.out.println("Token API for registration..............");
        String tag_json_obj = "json_obj_req";
         String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                TokenModel tokenModel = gson.fromJson(response, TokenModel.class);
                MyToken.setToken(tokenModel.refresh_token);
                MyToken.setAuthorization(tokenModel.access_token);
                if (MyToken.getToken() != null) {
                    UserModel model = new UserModel();
                    model.setMobileNumber(mobileNumber);
                    model.setAccessToken(tokenModel.access_token);
                    model.setRefreshToken(tokenModel.refresh_token);
                    Prefs.putString("userData", new Gson().toJson(model));
                    dialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError("Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserName", mobileNumber);
                params.put("grant_type", "password");
                params.put("Operator", "airtel");
                params.put("DeviceID", "123");
                params.put("AppType", "NTRP-APP");
                params.put("AppVersion", "1.0");
                params.put("OS", "android");
                // params.put("UserName",mobileNumber );
                //params.put("password", "123456");
                params.put("Flag", "5");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }


        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    public void setTitle(String str)
    {
        //mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        ((TextView) toolbar.getChildAt(0)).setTextSize(20);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(str);
    }

    @Override
    public void onBackPressed() {
       // setTitle("Home");
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container_frame);
        if (fragment instanceof PaymentWebViewFragment) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            super.onBackPressed();
        }

    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserLoginEvent(UserLoginEvent event) {

        user_name_in_left_menu.setVisibility(View.VISIBLE);
        user_name_in_left_menu.setText("Logged in as "+UserName);
        left_menu_image.setImageResource(R.drawable.account);
        left_menu_image.setMaxHeight(40);
        left_menu_image.setMaxWidth(40);
        setUsersMenus();


    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserLogoutEvent(MakeUserLogout event) {
        user_name_in_left_menu.setVisibility(View.GONE);

        left_menu_image.setImageResource(R.drawable.bharatkoshlogo);
        left_menu_image.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        IsLogin=false;
        IsDOTMReceiptMenuVisible=false;
        UserID=null;
        UserName=null;
        drawer.closeDrawers();
        setUsersMenus();
        showDialog("You have been logout !");
    }



    private void showDialog(String warningText) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_success);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAskMobileNumberEvent(AskMobileNumberDialogEvent event) {

        showMobileNumberDialog();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}