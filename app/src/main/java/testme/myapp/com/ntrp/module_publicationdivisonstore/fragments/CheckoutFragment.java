package testme.myapp.com.ntrp.module_publicationdivisonstore.fragments;

import static android.app.Activity.RESULT_OK;
import static testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment.UNIQUE_ID;
import static testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment.checkoutString;
import static testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment.pTitle;
import static testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment.productCartList;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pixplicity.easyprefs.library.Prefs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyCustomToast;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.RegisterUserModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_DepositorDetailsFragment;
import testme.myapp.com.ntrp.module_publicationdivisonstore.IItemListener;
import testme.myapp.com.ntrp.module_publicationdivisonstore.adapter.AttributeSpinnerAdapter;
import testme.myapp.com.ntrp.module_publicationdivisonstore.adapter.CheckoutAdapter;
import testme.myapp.com.ntrp.module_publicationdivisonstore.adapter.ShippingTypeAdapter;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.BaseResponse;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.CartData;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ProductCartListModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ProductListModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ShippingUserTypeListModel;
import testme.myapp.com.ntrp.module_trackyourpayment.mycaptcha.TextCaptcha;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.utils.AllUrls;


public class CheckoutFragment extends BaseFragment {
    private View mView;
    private CheckoutAdapter checkoutAdapter;
    private RecyclerView rvData;
    private TextView tvTotal, tvShippingCharges, tvDiscountAmt, tvGrandTotal, tvAddAttachment;
    private Button btnPlaceOrder, reloadCaptcha, btnShopNow;
    private TextCaptcha textCaptcha;
    private ImageView captcha_ImageView, ivFileRemove;
    private EditText etCaptcha;
    private LinearLayout llDetails, llEmptyCart, llFile;
    private Spinner spPubUserType, spShippingType;
    private ArrayList<ShippingUserTypeListModel> shippingTypeList = new ArrayList<>(), publicationUserTypeList = new ArrayList<>();
    private ShippingTypeAdapter userTypeAdapter, shippingTypeAdapter;
    private RelativeLayout rlAddPdf;
    private String ShippingTypeId = "", PubUserType = "";
    public static int ReceiptEntryId;
    private boolean agentLoginSuccess;
    private boolean hasAttachments = false;
    //  private File file = null;
    private byte[] BYTE_ARRAY;
    private Uri uri;
    public static final int DOC = 1008;
    private boolean isShippingSelectionFirstTym = true;
    private double grandTotal, discountAmt, totalAmt;
    public static String UserType;
    private String agentUserName;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_checkout, container, false);
        init();
        return mView;

    }

    @Override
    public void onStart() {
        getActivity().setTitle(pTitle);
        etCaptcha.setText("");
        super.onStart();
    }

    @Override
    protected void initView() {
        //shippingTypeList = new ArrayList<>();
        //publicationUserTypeList = new ArrayList<>();
        rvData = mView.findViewById(R.id.rvData);
        tvTotal = mView.findViewById(R.id.tvTotal);
        tvShippingCharges = mView.findViewById(R.id.tvShippingCharges);
        tvDiscountAmt = mView.findViewById(R.id.tvDiscountAmt);
        tvGrandTotal = mView.findViewById(R.id.tvGrandTotal);
        btnPlaceOrder = mView.findViewById(R.id.btnPlaceOrder);
        captcha_ImageView = mView.findViewById(R.id.captcha_imageview);
        reloadCaptcha = mView.findViewById(R.id.reload_captcha_button);
        etCaptcha = mView.findViewById(R.id.etCaptcha);
        llDetails = mView.findViewById(R.id.llDetails);
        llEmptyCart = mView.findViewById(R.id.llEmptyCart);
        btnShopNow = mView.findViewById(R.id.btnShopNow);
        spPubUserType = mView.findViewById(R.id.spPubUserType);
        spShippingType = mView.findViewById(R.id.spShippingType);
        ivFileRemove = mView.findViewById(R.id.ivFileRemove);
        tvAddAttachment = mView.findViewById(R.id.tvAddAttachment);
        llFile = mView.findViewById(R.id.llFile);
        rlAddPdf = mView.findViewById(R.id.rlAddPdf);
    }

    @Override
    protected void initListener() {
        reloadCaptcha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generate_Captcha();
            }
        });
        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PubUserType.matches("4")) {
                    if (!agentLoginSuccess)
                        showLoginDialog();
                    else {
                        HitPublicationCheckOutApi(ShippingTypeId, PubUserType);
                    }
                } else if (PubUserType.matches("5")) {
                    if (hasAttachments) {
                        HitPublicationCheckOutApi(ShippingTypeId, PubUserType);
                    } else {
                        MyCustomToast.createErrorToast(getActivity(), "Please add attachment");
                    }
                } else {
                    if (TextUtils.isEmpty(etCaptcha.getText().toString())) {
                        MyCustomToast.createErrorToast(getActivity(), "Please enter security code");
                        return;
                    }
                    if (!etCaptcha.getText().toString().equals(textCaptcha.input_captcha_string)) {
                        etCaptcha.setText("");
                        generate_Captcha();
                        MyCustomToast.createErrorToast(getActivity(), "Incorrect security code!");
                        return;
                    }
                    HitPublicationCheckOutApi(ShippingTypeId, PubUserType);
                }

                // removeFragment();

            }
        });
        btnShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFragment();
            }
        });
        spPubUserType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PubUserType = publicationUserTypeList.get(position).getValue();
                if (!TextUtils.isEmpty(ShippingTypeId))
                    HitGetTotalDiscountApi(shippingTypeList.get(spShippingType.getSelectedItemPosition()).getValue(), publicationUserTypeList.get(spPubUserType.getSelectedItemPosition()).getValue());
                if (PubUserType.matches("4")) {
                    rlAddPdf.setVisibility(View.GONE);
                    if (!agentLoginSuccess)
                        showLoginDialog();
                } else if (PubUserType.matches("5")) {
                    rlAddPdf.setVisibility(View.VISIBLE);
                } else {
                    rlAddPdf.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spShippingType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ShippingTypeId = shippingTypeList.get(position).getValue();
                if (!isShippingSelectionFirstTym)
                    HitGetTotalDiscountApi(shippingTypeList.get(spShippingType.getSelectedItemPosition()).getValue(), publicationUserTypeList.get(spPubUserType.getSelectedItemPosition()).getValue());
                else
                    isShippingSelectionFirstTym = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tvAddAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkForPermission()) {
                    showFileChooser();
                } else
                    checkForPermission();
            }
        });
        ivFileRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hasAttachments = false;
                llFile.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void bindDataWithUi() {
        rvData.setLayoutManager(new LinearLayoutManager(getContext()));
        int totalAmt = 0;
        for (CartData model : CartData.cartDataModelList) {
            totalAmt += model.getTotalPrice();
        }
        tvTotal.setText("₹ " + totalAmt);
        tvGrandTotal.setText("₹ " + totalAmt);
        checkoutAdapter =
                new CheckoutAdapter(getContext(), productCartList, new IItemListener<ProductCartListModel>() {
                    @Override
                    public void onItemClick(ProductCartListModel productCartListModel, int position) {
                        if (position == 3) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                            alertDialogBuilder.setMessage("Are you sure to remove product from basket?");
                            alertDialogBuilder.setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            HitRemoveItemApi(productCartListModel.getReceiptProductItemId() + "");
                                        }
                                    });

                            alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //finish();
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();

                        }
                    }
                });
        rvData.setAdapter(checkoutAdapter);
        rvData.setNestedScrollingEnabled(false);
        generate_Captcha();
        isShippingSelectionFirstTym = true;
        shippingTypeAdapter = new ShippingTypeAdapter(getContext(), R.layout.single_row_spinner, shippingTypeList);
        spShippingType.setAdapter(shippingTypeAdapter);
        userTypeAdapter = new ShippingTypeAdapter(getContext(), R.layout.single_row_spinner, publicationUserTypeList);
        spPubUserType.setAdapter(userTypeAdapter);
        HitGetBasketApi();
    }

    private void generate_Captcha() {
        textCaptcha = new TextCaptcha(1100, 400, 6, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
        captcha_ImageView.setImageBitmap(textCaptcha.getImage());
    }

    private void HitGetShippingTypeApi(String flag) {
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDropDownList;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    MyToken.setToken(responseData.getRefreshTokenValue());
                    MyToken.setAuthorization(responseData.getAcsessTokenValue());
                    if (flag.equalsIgnoreCase("PublicationUserType")) {
                        publicationUserTypeList.clear();
                        publicationUserTypeList.addAll(responseData.getShippingUserTypeList());
                        userTypeAdapter = new ShippingTypeAdapter(getContext(), R.layout.single_row_spinner, publicationUserTypeList);
                        spPubUserType.setAdapter(userTypeAdapter);
                        for (int i = 0; i < publicationUserTypeList.size(); i++) {
                            if (publicationUserTypeList.get(i).getValue().equalsIgnoreCase(PubUserType)) {
                                spPubUserType.setSelection(i);
                                break;
                            }
                        }
                        HitGetShippingTypeApi("ShippingType");

                    }
                    if (flag.equalsIgnoreCase("ShippingType")) {
                        shippingTypeList.clear();
                        shippingTypeList.addAll(responseData.getShippingUserTypeList());
                        shippingTypeAdapter = new ShippingTypeAdapter(getContext(), R.layout.single_row_spinner, shippingTypeList);
                        spShippingType.setAdapter(shippingTypeAdapter);
                        for (int i = 0; i < shippingTypeList.size(); i++) {
                            if (shippingTypeList.get(i).getValue().equalsIgnoreCase(ShippingTypeId)) {
                                spShippingType.setSelection(i);
                                break;
                            }
                        }
                        HitGetTotalDiscountApi(shippingTypeList.get(spShippingType.getSelectedItemPosition()).getValue(), publicationUserTypeList.get(spPubUserType.getSelectedItemPosition()).getValue());

                    }


                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID); //wnqvxha30noqitfzq1201446631
                //);
                params.put("flag", flag); //ShippingType,PublicationUserType
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void HitGetBasketApi() {
        productCartList.clear();
        ShippingTypeId = "";
        PubUserType = "";
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getBasket;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    if (responseData.getErrorCodes().matches("Success") || responseData.getErrorCodes().matches("Record Not Found !")) {
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        if (null != responseData.getProductCartList() && responseData.getProductCartList().size() > 0) {
                            if (publicationUserTypeList.size() == 0) {
                                HitGetShippingTypeApi("PublicationUserType");
                            } else {
                                isShippingSelectionFirstTym = true;
                            }
                            productCartList.addAll(responseData.getProductCartList());
                            checkoutAdapter.setData(productCartList);
                            int totalQty = 0;
                            grandTotal = 0;
                            for (ProductCartListModel item : productCartList) {
                                totalQty += item.getTotalQty();
                                grandTotal += item.getTotalPrice();
                            }
                            totalAmt = grandTotal;
                            tvTotal.setText("₹ " + grandTotal);
                            tvGrandTotal.setText("₹ " + grandTotal);
                            //llCheckout.setVisibility(View.VISIBLE);
                            // tvQtyAmt.setText(totalQty + " Item(s) | ₹ " + totalPrice);

                        } else {
                            checkoutAdapter.setData(new ArrayList<>());
                            productCartList.clear();
                            llDetails.setVisibility(View.GONE);
                            llEmptyCart.setVisibility(View.VISIBLE);
                            checkoutString = "";
                        }
                    } else {
                        MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
                        llDetails.setVisibility(View.GONE);
                    }
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                llDetails.setVisibility(View.GONE);
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void HitPublicationCheckOutApi(String ShippingTypeId, String PubUserType) {
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getPublicationCheckOut;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    if (responseData.getResponseStatus().matches("Success")) {
                        UserType = PubUserType;
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        Prefs.putInt("ReceiptEntryId", responseData.getReceiptEntryId());
                        ReceiptEntryId = responseData.getReceiptEntryId();
                        Bundle args = new Bundle();
                        //args.putString("ReceiptEntryId", responseData.getReceiptEntryId() + "");
                        args.putString("PaymentPurposeID", responseData.getPaymentPurposeID() + "");
                        args.putString("agentUserName", agentUserName);
                        args.putBoolean("agentLoginSuccess", agentLoginSuccess);
                        Fragment fragment = new DepositorFragment();
                        fragment.setArguments(args);
                        replaceFragment(fragment, R.id.container_frame, true);
                    } else {
                        MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID); //wnqvxha30noqitfzq1201446631
                params.put("ShippingTypeId", ShippingTypeId); //2
                params.put("PubUserType", PubUserType); //3
                if (hasAttachments)
                    params.put("PdfFileByte", Base64.encodeToString(BYTE_ARRAY, Base64.DEFAULT));
                //Base64.encodeToString(BYTE_ARRAY, Base64.DEFAULT);
                //params.put("PdfFileByte", Arrays.toString(BYTE_ARRAY));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void HitGetTotalDiscountApi(String ShippingTypeId, String PubUserType) {
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTotalDiscount;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    if (responseData.getResponseStatus().matches("Success")) {
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        if (null != responseData.getTotalDiscount())
                            if (TextUtils.isDigitsOnly(responseData.getTotalDiscount())) {
                                int dis = Integer.parseInt(responseData.getTotalDiscount());
                                grandTotal = totalAmt;
                                grandTotal = grandTotal - dis;
                                tvGrandTotal.setText("₹ " + grandTotal);
                                tvDiscountAmt.setText("₹ " + responseData.getTotalDiscount());
                            } else {
                                tvDiscountAmt.setText("₹ 0");
                            }
                        else {
                            tvDiscountAmt.setText("₹ 0");
                        }

                        HitGetShippingChargesApi(ShippingTypeId, PubUserType);
                    }


                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                // MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID); //wnqvxha30noqitfzq1201446631
                params.put("ShippingTypeId", ShippingTypeId); //2
                params.put("UserTypeId", PubUserType); //3
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void HitGetShippingChargesApi(String ShippingTypeId, String PubUserType) {
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getShippingCharges;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    if (responseData.getResponseStatus().matches("Success")) {
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        if (null != responseData.getShippingCharges())
                            if (TextUtils.isDigitsOnly(responseData.getShippingCharges())) {
                                int ship = Integer.parseInt(responseData.getShippingCharges());
                                grandTotal = grandTotal + ship;
                                tvGrandTotal.setText("₹ " + grandTotal);
                                tvShippingCharges.setText("₹ " + responseData.getShippingCharges());
                            } else {
                                tvShippingCharges.setText("₹ 0");
                            }
                        else {
                            tvShippingCharges.setText("₹ 0");
                        }
//                        if (null != responseData.getShippingCharges())
//                            tvShippingCharges.setText("₹ " + responseData.getShippingCharges());
//                        else
//                            tvShippingCharges.setText("₹ 0");
                    }


                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID); //wnqvxha30noqitfzq1201446631
                params.put("ShippingTypeId", ShippingTypeId); //2
                params.put("UserTypeId", PubUserType); //3
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void showLoginDialog() {
        final TextCaptcha[] textCaptchaForLogin = new TextCaptcha[1];
        TextView tvOk, tvCancel;
        ImageView captchaImageview;
        EditText etUserName, etPass, etCaptcha;
        Button reloadCaptcha;
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (null != dialog.getWindow())
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations;
        dialog.setContentView(R.layout.dialog_publication_agent_login);
        captchaImageview = dialog.findViewById(R.id.captcha_imageview);
        etCaptcha = dialog.findViewById(R.id.etCaptcha);
        etUserName = dialog.findViewById(R.id.etUserName);
        etPass = dialog.findViewById(R.id.etPass);
        reloadCaptcha = dialog.findViewById(R.id.reloadCaptcha);
        tvOk = dialog.findViewById(R.id.tvOk);
        tvCancel = dialog.findViewById(R.id.tvCancel);
        dialog.setCancelable(false);
        textCaptchaForLogin[0] = new TextCaptcha(1100, 400, 6, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
        captchaImageview.setImageBitmap(textCaptchaForLogin[0].getImage());
        reloadCaptcha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textCaptchaForLogin[0] = new TextCaptcha(1100, 400, 6, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
                captchaImageview.setImageBitmap(textCaptchaForLogin[0].getImage());
            }
        });
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etUserName.getText()) || TextUtils.isEmpty(etPass.getText())) {
                    MyCustomToast.createErrorToast(getActivity(), "Please enter username & password");
                    return;
                }
                if (TextUtils.isEmpty(etCaptcha.getText().toString())) {
                    MyCustomToast.createErrorToast(getActivity(), "Please enter security code");
                    return;
                }
                if (!etCaptcha.getText().toString().equals(textCaptchaForLogin[0].input_captcha_string)) {
                    etCaptcha.setText("");
                    textCaptchaForLogin[0] = new TextCaptcha(1100, 400, 6, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
                    captchaImageview.setImageBitmap(textCaptchaForLogin[0].getImage());
                    MyCustomToast.createErrorToast(getActivity(), "Incorrect security code!");
                    return;
                }
                String passwordHas = HashingSha256(etPass.getText().toString());
                String randomString = getRandomNumberString();
                String finalPassword = HashingSha256(randomString + passwordHas.toUpperCase());
                etPass.setText(finalPassword);

                HitAgentLoginApi(etUserName.getText().toString(), finalPassword, randomString, dialog);
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    private void HitRemoveItemApi(String receiptProductItemId) {
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getRemoveItem;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    if (responseData.getResponseStatus().matches("Success")) {
                        MyCustomToast.show(getContext(), responseData.getErrorCodes());
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        HitGetBasketApi();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID);
                params.put("ReceiptProductItemId", receiptProductItemId);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void HitAgentLoginApi(String userName, String password, String rowNo, Dialog dialog) {
        //hideSoftInputKeyboard();
        EditText et = dialog.findViewById(R.id.etPass);
        TextCaptcha[] textCaptchaForLogin = new TextCaptcha[1];
        ImageView captchaImageview = dialog.findViewById(R.id.captcha_imageview);
        EditText etCaptcha = dialog.findViewById(R.id.etCaptcha);
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getLogin;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    if (responseData.getResponseStatus().matches("Success")) {
                        dialog.dismiss();
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        agentUserName = userName;
                        agentLoginSuccess = true;
                    } else if (responseData.getResponseStatus().matches("Fail")) {
                        et.setText("");
                        etCaptcha.setText("");
                        textCaptchaForLogin[0] = new TextCaptcha(1100, 400, 6, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
                        captchaImageview.setImageBitmap(textCaptchaForLogin[0].getImage());
                        MyCustomToast.show(getContext(), responseData.getErrorCodes());
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                    } else {
                        et.setText("");
                        etCaptcha.setText("");
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                et.setText("");
                etCaptcha.setText("");
                textCaptchaForLogin[0] = new TextCaptcha(1100, 400, 6, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
                captchaImageview.setImageBitmap(textCaptchaForLogin[0].getImage());
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UserName", userName);
                params.put("Password", password);
                params.put("RowNo", rowNo);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void showFileChooser() {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT); //ACTION_GET_CONTENT  //ACTION_OPEN_DOCUMENT
        i.setType("application/pdf");
        startActivityForResult(i, DOC);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DOC) {
            if (resultCode == RESULT_OK) {
                Uri data1 = data.getData();
                if (data1 != null && !data1.equals(Uri.EMPTY)) {
                    File file = new File(data1.getPath());
                    try {
                        InputStream is = getActivity().getContentResolver().openInputStream(data1);
                        byte[] bytesArray = new byte[is.available()];
                        is.read(bytesArray);
                        BYTE_ARRAY = bytesArray;
//                        filename="Assignment";
//                        extension=".pdf";
                        hasAttachments = true;
                        llFile.setVisibility(View.VISIBLE);
                    } catch (FileNotFoundException e) {
                        //  e.printStackTrace();
                        hasAttachments = false;
                        //filename = extension = "";
                        this.uri = Uri.EMPTY;
                        MyCustomToast.createErrorToast(getActivity(), "Error, Please Try again later");
                    } catch (IOException e) {
                        //   e.printStackTrace();
                        hasAttachments = false;
                        //filename = extension = "";
                        this.uri = Uri.EMPTY;
                        MyCustomToast.createErrorToast(getActivity(), "Error, Please Try again later");
                    }
                } else {
                    MyCustomToast.createErrorToast(getActivity(), "Unable to load file");
                }
            }
        }
    }
}