package testme.myapp.com.ntrp.module_login.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.common.base.Charsets;
import com.google.common.base.Converter;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.Country;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.CountryModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.District;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.DistrictModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.State;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.StateModel;
import testme.myapp.com.ntrp.module_login.events.AskMobileNumberDialogEvent;
import testme.myapp.com.ntrp.module_login.events.UserLoginEvent;
import testme.myapp.com.ntrp.module_login.models.ChangePasswordModel;
import testme.myapp.com.ntrp.module_login.models.LogedInUserDetailsModels;
import testme.myapp.com.ntrp.module_login.models.LoginUserModel;
import testme.myapp.com.ntrp.module_login.models.UpdateProfileModel;
import testme.myapp.com.ntrp.module_login.models.UserMenuModel;
import testme.myapp.com.ntrp.module_login.models.UserMenuResponse;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsersFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_DepositorDetailsFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.MinistryListModel;
import testme.myapp.com.ntrp.module_registeration.models.BankListModel;
import testme.myapp.com.ntrp.module_trackyourpayment.events.PutUserInputEvent;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

public class UserLogedInHomeFragment extends Fragment {

    private View localView;

    private String UserName;

    @BindView(R.id.spinner_ministry)
    public Spinner spinner_ministry;

    @BindView(R.id.spinner_bank)
    public Spinner spinner_bank;

    @BindView(R.id.spinner_country)
    public Spinner spinner_country;

    @BindView(R.id.spinner_state)
    public Spinner spinner_state;

    @BindView(R.id.spinner_district)
    public Spinner spinner_district;

    @BindView(R.id.name_edittext)
    public EditText name_edittext;

    @BindView(R.id.last_name_edittext)
    public EditText last_name_edittext;


    @BindView(R.id.bank_acc_number_edittext)
    public EditText bank_acc_number_edittext;

    @BindView(R.id.ifsc_number_edittext)
    public EditText ifsc_number_edittext;

    @BindView(R.id.address_1_edittext)
    public EditText address_1_edittext;

    @BindView(R.id.address_2_edittext)
    public EditText address_2_edittext;


    @BindView(R.id.city_edittext)
    public EditText city_edittext;

    @BindView(R.id.pincode_edittext)
    public EditText pincode_edittext;


    @BindView(R.id.email_edittext)
    public EditText email_edittext;

    @BindView(R.id.mobile_number_edittext)
    public EditText mobile_number_edittext;



    @BindView(R.id.current_password_edittext)
    public EditText current_password_edittext;

    @BindView(R.id.new_password_edittext)
    public EditText new_password_edittext;

    @BindView(R.id.confirm_new_password_edittext)
    public EditText confirm_new_password_edittext;



    @BindView(R.id.welcome_text_view)
    public TextView welcome_text_view;

    @BindView(R.id.change_password_layout)
    public RelativeLayout change_password_layout;


    private ArrayList<Country> countryArrayList;

    private ArrayList<State> stateArrayList;

    private ArrayList<District> districtArrayList;

    private LogedInUserDetailsModels userInfoModel;

    private BankListModel bankListModel;

    private  MinistryListModel ministryListModel;

    private int  State_check = 0, District_check = 0;

    private String selectedBankId,selectedMinistryId,selectedCountryID,selectedStateID,selectedDistrictID;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_logedin_user_home_page, container, false);
        localView=view;
        State_check = 0; District_check = 0;
        ButterKnife.bind(this,view);
        getActivity().setTitle("Home");
        Bundle bundle = getArguments();
        if (bundle != null) {
            UserName = bundle.getString("UserName");

        }

        UserName=MainActivity.UserName;
        initializeView();

        return view;
    }

    private void initializeView() {

        /*spinner_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedBankId=bankListModel.BankList.get(position).BankId;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_ministry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedMinistryId= String.valueOf(ministryListModel.getMinistryList().get(position).getMinistryId());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                selectedCountryID = String.valueOf(countryArrayList.get(position).getCountryId());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }

        });

        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(++State_check > 1) {
                    selectedStateID = String.valueOf(stateArrayList.get(position).getStateId());

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }

        });

        spinner_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(++District_check > 1) {
                    selectedDistrictID = String.valueOf(districtArrayList.get(position).getDistrictId());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }

        });*/




        Hit_User_Details_API(UserName);

    }



    private void Hit_User_Details_API(String UserName) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getUserDetailsURL;
        final ProgressDialog pDialog = new ProgressDialog(UserLogedInHomeFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                 userInfoModel = gson.fromJson(response, LogedInUserDetailsModels.class);



                MyToken.setToken(userInfoModel.getRefreshTokenValue());
                MyToken.setAuthorization(userInfoModel.getAcsessTokenValue());


                if (userInfoModel.getEmail()!=null&&userInfoModel.getUserName()!=null&&userInfoModel.getAcsessTokenValue()!=null&&userInfoModel.getRefreshTokenValue()!=null)
                {
                    Prefs.putString("userLogedInData", new Gson().toJson(userInfoModel));

                    name_edittext.setText("" + userInfoModel.getFirstName() != null ? userInfoModel.getFirstName() : "");

                    last_name_edittext.setText("" + userInfoModel.getLastName() != null ? userInfoModel.getLastName() : "");


                    email_edittext.setText("" + userInfoModel.getEmail() != null ? userInfoModel.getEmail() : "");

                    mobile_number_edittext.setText("" + userInfoModel.getMobileNo() != null ? userInfoModel.getMobileNo() : "");

                    String FirstName=userInfoModel.getFirstName() != null ? userInfoModel.getFirstName() : "User";

                    String LastName=userInfoModel.getLastName() != null ? userInfoModel.getLastName() : "";

                    welcome_text_view.setText("Welcome "+FirstName+" "+LastName+" !");


                    bank_acc_number_edittext.setText("" + userInfoModel.getAccountNumber() != null ? userInfoModel.getAccountNumber() : "");

                    ifsc_number_edittext.setText("" + userInfoModel.getIFSCCode() != null ? userInfoModel.getIFSCCode() : "");

                    address_1_edittext.setText("" + userInfoModel.getAddress1() != null ? userInfoModel.getAddress1() : "");

                    address_2_edittext.setText("" + userInfoModel.getAddress2() != null ? userInfoModel.getAddress2() : "");

                    city_edittext.setText("" + userInfoModel.getCity() != null ? userInfoModel.getCity() : "");

                    pincode_edittext.setText("" + userInfoModel.getPincode() != null ? userInfoModel.getPincode() : "");


                    Hit_Get_Bank_List_API(userInfoModel.getBankId(),userInfoModel.getControllerId(),userInfoModel.getCountryId(),userInfoModel.getStateId(),userInfoModel.getDistrictId());


                }

                else
                {
                    // wrong_credntial_textview.setVisibility(View.VISIBLE);
                    //  wrong_credntial_textview.setText("Wrong Credentials !");
                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("LoginUserName", UserName);


                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }

    private void Hit_Get_Ministry_List_API(Integer controllerId, Integer CountryId, Integer stateId, Integer districtId) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Ministry_List;
        final ProgressDialog pDialog = new ProgressDialog(UserLogedInHomeFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                ministryListModel = gson.fromJson(response, MinistryListModel.class);
               // MyToken.setToken(ministryListModel.getRefreshTokenValue());
               // MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());


                if(ministryListModel.getResponseStatus().matches("Success"))
                {


                    ArrayList<String> items = new ArrayList<String>();

                    int userMinistryindex=0;

                    for(int i=0;i<ministryListModel.getMinistryList().size();i++)
                    {
                        items.add(ministryListModel.getMinistryList().get(i).getMinistryName());

                        if (controllerId.equals(ministryListModel.getMinistryList().get(i).getMinistryId()))
                        {
                            userMinistryindex=i;
                        }

                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
                    spinner_ministry.setAdapter(adapter);
                    spinner_ministry.setSelection(userMinistryindex);
                    Hit_CountryList_API(CountryId,stateId,districtId);

                }
                else
                {
                    Toast.makeText(UserLogedInHomeFragment.this.getActivity(), "Failed to get Depositor Category List!",
                            Toast.LENGTH_LONG).show();
                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private void Hit_Get_Bank_List_API(Integer BankId, Integer controllerId, Integer countryId, Integer stateId, Integer districtId) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getBankListURL;
        final ProgressDialog pDialog = new ProgressDialog(UserLogedInHomeFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                    int userMinistryindex=0;



                    bankListModel=gson.fromJson(response, BankListModel.class);

                    MyToken.setToken(bankListModel.RefreshTokenValue);
                    MyToken.setAuthorization(bankListModel.AcsessTokenValue);



                    ArrayList<String> bank_names=new ArrayList<>();

                    for(int i=0;i<bankListModel.BankList.size();i++)
                    {
                        bank_names.add(bankListModel.BankList.get(i).BankName);

                        if (BankId.equals(Integer.valueOf(bankListModel.BankList.get(i).BankId)))
                        {
                            userMinistryindex=i;
                        }

                    }



                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, bank_names);
                    spinner_bank.setAdapter(adapter);
                    spinner_bank.setSelection(userMinistryindex);



                Hit_Get_Ministry_List_API(controllerId, countryId,  stateId,  districtId);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_CountryList_API(Integer countryId, Integer stateId, Integer districtId) {

        System.out.println("Hit_CountryList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getCountryListURL;
        final ProgressDialog pDialog = new ProgressDialog(UserLogedInHomeFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                CountryModel countryModel = gson.fromJson(response, CountryModel.class);
                int userCountryindex=0;

                 countryArrayList= (ArrayList<Country>) countryModel.getCountryList();
                String[] countryItems=new String[countryArrayList.size()];
                for(int i=0;i<countryArrayList.size();i++)
                {
                    countryItems[i]=countryArrayList.get(i).getCountryName();

                    if (countryId.equals(countryArrayList.get(i).getCountryId()))
                    {
                        userCountryindex=i;
                    }

                }




                ArrayAdapter<String> adapter = new ArrayAdapter<String>(UserLogedInHomeFragment.this.getActivity(), R.layout.single_row_spinner, countryItems);
                spinner_country.setAdapter(adapter);
                spinner_country.setSelection(userCountryindex);

                Hit_StateList_API(countryId,stateId,districtId);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }



    private void Hit_StateList_API(Integer countryId, Integer stateId, Integer districtId) {

        System.out.println("Hit_StateList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getStateListURL;
        final ProgressDialog pDialog = new ProgressDialog(UserLogedInHomeFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                StateModel stateModel = gson.fromJson(response, StateModel.class);

                int userStateindex=0;
                stateArrayList= stateModel.getStateList();
                String[] stateItems=new String[stateArrayList.size()+1];
                for(int i=0;i<stateArrayList.size();i++)
                {
                    stateItems[i]=stateArrayList.get(i).getStateName();

                    if (stateId.equals(stateArrayList.get(i).getStateId()))
                    {
                        userStateindex=i;
                    }
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(UserLogedInHomeFragment.this.getActivity(), R.layout.single_row_spinner, stateItems);

                spinner_state.setAdapter(adapter);

                spinner_state.setSelection(userStateindex);

                Hit_DistrictList_API(stateId,districtId);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ParentControlId", ""+countryId);

                //System.out.println(LoginScreen.selectedBankID + "==" + MyToken.getToken() + "==" + LoginScreen.SelectedSchemeId+"=="+LoginScreen.selectedSchemeName);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private void Hit_DistrictList_API(Integer stateId, Integer districtId) {

        System.out.println("Hit_DistrictList_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDistrictListURL;
        final ProgressDialog pDialog = new ProgressDialog(UserLogedInHomeFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                DistrictModel districtModel = gson.fromJson(response, DistrictModel.class);
                int userDistrictindex=0;

                ArrayList<District> districtArrayList= districtModel.getDistrictList();
                String[] districtItems=new String[districtArrayList.size()+1];
                for(int i=0;i<districtArrayList.size();i++)
                {
                    districtItems[i]=districtArrayList.get(i).getDistrictName();

                    Integer id=districtArrayList.get(i).getDistrictId();

                    System.out.println(id);

                    if (districtId.equals(districtArrayList.get(i).getDistrictId()))
                    {
                        userDistrictindex=i;
                    }

                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(UserLogedInHomeFragment.this.getActivity(), R.layout.single_row_spinner, districtItems);

                spinner_district.setAdapter(adapter);

                spinner_district.setSelection(userDistrictindex);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ParentControlId", ""+stateId);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    @OnClick(R.id.change_password_button)
    public void OnChangePasswordButtonClick()
    {
        change_password_layout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.update_password_button)
    public void OnUpdatePasswordButtonClick()
    {
        if (checkPasswordValidation())
        {
            Hit_Update_Password_API();
        }
    }

    @OnClick(R.id.update_profile_button)
    public void OnUpdateProfileButtonClick()
    {

            Hit_Update_Profile_API();

    }

    private void Hit_Update_Profile_API() {
        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.updateUserProfileURL;
        final ProgressDialog pDialog = new ProgressDialog(UserLogedInHomeFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                UpdateProfileModel userInfoModel = gson.fromJson(response, UpdateProfileModel.class);



                MyToken.setToken(userInfoModel.getRefreshTokenValue());
                MyToken.setAuthorization(userInfoModel.getAcsessTokenValue());


                if (userInfoModel.getErrorCodes()!=null&userInfoModel.getErrorCodes().matches("Successfully Updated")&&userInfoModel.getResponseStatus()!=null&&userInfoModel.getResponseStatus().matches("Success")&&userInfoModel.getAcsessTokenValue()!=null&&userInfoModel.getRefreshTokenValue()!=null)
                {
                    /*Toast.makeText(UserLogedInHomeFragment.this.getActivity(), "Details updated !",
                            Toast.LENGTH_LONG).show();*/
                    showDialog("Details updated successfully !");
                    Hit_User_Details_API(UserName);
                }

                else
                {
                    Toast.makeText(UserLogedInHomeFragment.this.getActivity(), "Details not updated !",
                            Toast.LENGTH_LONG).show();
                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("UserName", userInfoModel.getUserName());

                params.put("FName", name_edittext.getText().toString());

                params.put("LName", last_name_edittext.getText().toString());

                params.put("Email", email_edittext.getText().toString());

                params.put("HdnEmail", email_edittext.getText().toString());

                params.put("MobileNumber", mobile_number_edittext.getText().toString());

                params.put("HdnMobileNumber", mobile_number_edittext.getText().toString());

                params.put("Address1", address_1_edittext.getText().toString());

                params.put("Address2", address_2_edittext.getText().toString());

                params.put("CINNo", userInfoModel.getCINNo());

                params.put("BankIntegratedToCPSMS", userInfoModel.getBankIntegratedToCPSMS());

                params.put("BankId", userInfoModel.getBankId().toString());

                params.put("TanNumber", userInfoModel.getTanNumber());

                params.put("OrganizationName",  userInfoModel.getOrganizationName());

                params.put("CountryId",  userInfoModel.getCountryId().toString());

                params.put("StateId",  userInfoModel.getStateId().toString());

                params.put("DistrictId",  userInfoModel.getDistrictId().toString());

                params.put("ControllerId",  userInfoModel.getControllerId().toString());

                params.put("City",  city_edittext.getText().toString());

                params.put("Pincode",  pincode_edittext.getText().toString());

                params.put("IsNEFT",  userInfoModel.getIsNEFT()?  "True" : "False");

                params.put("AccountNumber",  userInfoModel.getAccountNumber());

                params.put("IFSCCode",  userInfoModel.getIFSCCode());

                params.put("UserId",  userInfoModel.getUserId().toString());

                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    private boolean checkPasswordValidation()
    {
        if (current_password_edittext.getText().toString().matches(""))
        {
            current_password_edittext.setError("Can not be blank");
            return false;
        }
        else if (new_password_edittext.getText().toString().matches(""))
        {
            new_password_edittext.setError("Can not be blank");
            return false;
        }
        else if (confirm_new_password_edittext.getText().toString().matches(""))
        {
            confirm_new_password_edittext.setError("Can not be blank");
            return false;
        }
        else if (isValidPassword(new_password_edittext.getText().toString())==false)
        {
            /*Toast.makeText(UserLogedInHomeFragment.this.getActivity(), "Password must contain minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character",
                    Toast.LENGTH_LONG).show();*/
            showDialog_Error("Password must contain minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character");
            return false;
        }
        else if (IsHavingSpecialCharector(new_password_edittext.getText().toString())==false)
        {
            /*Toast.makeText(UserLogedInHomeFragment.this.getActivity(), "Password must contain minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character",
                    Toast.LENGTH_LONG).show();*/
            showDialog_Error("Password must contain minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character");
            return false;
        }
        else if (isValidPassword(confirm_new_password_edittext.getText().toString())==false)
        {
            /*Toast.makeText(UserLogedInHomeFragment.this.getActivity(), "Password must contain minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character",
                    Toast.LENGTH_LONG).show();*/
            showDialog_Error("Password must contain minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character");
            return false;
        }
        else if (IsHavingSpecialCharector(confirm_new_password_edittext.getText().toString())==false)
        {
            /*Toast.makeText(UserLogedInHomeFragment.this.getActivity(), "Password must contain minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character",
                    Toast.LENGTH_LONG).show();*/
            showDialog_Error("Password must contain minimum 8 characters at least 1 Alphabet, 1 Number and 1 Special Character");
            return false;
        }
        else if (confirm_new_password_edittext.getText().toString().matches(new_password_edittext.getText().toString())==false)
        {
            /*Toast.makeText(UserLogedInHomeFragment.this.getActivity(), "Confirm Password must match with password",
                    Toast.LENGTH_LONG).show();*/
            showDialog_Error("Confirm Password must match with password");
            return false;
        }
        return true;
    }
    private void showDialog_Error(String errorText) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.error_textview);
        error_textview.setText(errorText);

        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }



    private boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    private boolean IsHavingSpecialCharector(String s){


        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(s);

        boolean b = m.find();
        if (b)
            System.out.println("There is a special character in my string ");
        else
            System.out.println("There is no special char.");
        return b;

    }
    private void Hit_Update_Password_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getChangePasswordURL;
        final ProgressDialog pDialog = new ProgressDialog(UserLogedInHomeFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                ChangePasswordModel userInfoModel = gson.fromJson(response, ChangePasswordModel.class);



                MyToken.setToken(userInfoModel.getRefreshTokenValue());
                MyToken.setAuthorization(userInfoModel.getAcsessTokenValue());


                if (userInfoModel.getResponseStatus()!=null&&userInfoModel.getResponseStatus().matches("Success")&&userInfoModel.getAcsessTokenValue()!=null&&userInfoModel.getRefreshTokenValue()!=null)
                {
                    Toast.makeText(UserLogedInHomeFragment.this.getActivity(), "Password updated !",
                            Toast.LENGTH_LONG).show();
                    change_password_layout.setVisibility(View.GONE);
                }

                else
                {
                    Toast.makeText(UserLogedInHomeFragment.this.getActivity(), "Password not updated !",
                            Toast.LENGTH_LONG).show();
                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("LoginUserName", UserName);

                String password_hashed=HashingSha256(new_password_edittext.getText().toString());

                params.put("ConfirmPasswrd", password_hashed);

                params.put("newPassword", password_hashed);

                params.put("RefreshTokenValue", MyToken.getToken());



                String password_hashed_=HashingSha256(current_password_edittext.getText().toString());

                String RandomString=getRandomNumberString();

                String password_hashed_hexa=HashingSha256(RandomString+password_hashed_.toUpperCase());


                params.put("Password", HashingSha256(password_hashed_hexa));

                params.put("Flag",  "ChangePassword");

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }
    private  String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        SecureRandom rnd = new SecureRandom ();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }
    private String HashingSha256(String Input) {
        final HashFunction hashFunction = Hashing.sha256();
        final HashCode hc = hashFunction
                .newHasher()
                .putString(Input, Charsets.UTF_8)
                .hash();
        final String sha256 = hc.toString();
        return sha256;
    }


    public class HintAdapter<String>
            extends ArrayAdapter<String> {


        public HintAdapter(@NonNull Context context, int resource, @NonNull String[] objects) {
            super(context, resource, objects);
        }

        @Override
        public int getCount() {
            // don't display last item. It is used as hint.
            int count = super.getCount();
            return count > 0 ? count - 1 : count;
        }
    }

    private void showDialog(String warningText) {

        final Dialog dialog = new Dialog(this.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_success);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }
}
