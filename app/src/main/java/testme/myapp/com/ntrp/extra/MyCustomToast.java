package testme.myapp.com.ntrp.extra;
import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;

/**
 * Created by prakhar.s on 4/17/2017.
 */
public class MyCustomToast {

    private static final int DURATION = 4000;
    private static final Map<Object, Long> lastShown = new HashMap<Object, Long>();
    private static boolean isRecent(Object obj) {
        Long last = lastShown.get(obj);
        if (last == null) {
            return false;
        }
        long now = System.currentTimeMillis();
        if (last + DURATION < now) {
            return false;
        }
        return true;
    }

    public static synchronized void show(Context context, String msg) {
        if (isRecent(msg)) {
            return;
        }
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        lastShown.put(msg, System.currentTimeMillis());
    }

    public static void createCustomToast(Activity activity) {
        Context context = activity.getApplicationContext();
        LayoutInflater inflater = activity.getLayoutInflater();
        View customToastroot = inflater.inflate(R.layout.mytoast, null);
        Toast customtoast = new Toast(context);
        customtoast.setView(customToastroot);
        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_SHORT);
        customtoast.show();
    }
    public static synchronized void createErrorToast(Activity activity,String textString) {

        if (isRecent(textString)) {
            return;
        }
        //Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();


        Context context = activity.getApplicationContext();
        LayoutInflater inflater = activity.getLayoutInflater();
        View customToastroot = inflater.inflate(R.layout.red_toast, null);
        TextView textView= (TextView) customToastroot.findViewById(R.id.textView1);
        textView.setText(textString);
        Toast customtoast = new Toast(context);
        customtoast.setView(customToastroot);
        //customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_SHORT);
        customtoast.show();

        lastShown.put(textString, System.currentTimeMillis());

    }
    public static void createWarningToast(Activity activity) {
        Context context = activity.getApplicationContext();
        LayoutInflater inflater = activity.getLayoutInflater();
        View customToastroot = inflater.inflate(R.layout.yellow_toast, null);
        Toast customtoast = new Toast(context);
        customtoast.setView(customToastroot);
        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_SHORT);
        customtoast.show();
    }
    public static void createMessageToast(Activity activity) {
        Context context = activity.getApplicationContext();
        LayoutInflater inflater = activity.getLayoutInflater();
        View customToastroot = inflater.inflate(R.layout.blue_toast, null);
        Toast customtoast = new Toast(context);
        customtoast.setView(customToastroot);
        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        customtoast.setDuration(Toast.LENGTH_SHORT);
        customtoast.show();
    }
}
