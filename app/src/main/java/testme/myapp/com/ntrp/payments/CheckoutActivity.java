package testme.myapp.com.ntrp.payments;

import static android.content.ContentValues.TAG;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paynimo.android.payment.PaymentActivity;
import com.paynimo.android.payment.PaymentModesActivity;
import com.paynimo.android.payment.model.Checkout;
import com.paynimo.android.payment.util.Constant;
import com.pixplicity.easyprefs.library.Prefs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.PaymentDetailsModel;
import testme.myapp.com.ntrp.module_login.models.LogedInUserDetailsModels;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

public class CheckoutActivity  extends AppCompatActivity {

    private String ReceiptGetTransferToBankId,TransactionAmount,Currency,Country,DetailsChallanNumber,EPFBAccountNumber,AmountInOtherDetails;

    private final String  MerchantIdentifier="T59419", ProductID="013455";


    @BindView(R.id.StatusCode_textview_value)
    public TextView StatusCode_textview_value;

    @BindView(R.id.StatusMessage_value)
    public TextView StatusMessage_value;

    @BindView(R.id.ErrorMessage_textview_value)
    public TextView ErrorMessage_textview_value;

    @BindView(R.id.Amount_textview_value)
    public TextView Amount_textview_value;

    @BindView(R.id.DateTime_textview_value)
    public TextView DateTime_textview_value;

    @BindView(R.id.MerchantTransactionIdentifier_textview_value)
    public TextView MerchantTransactionIdentifier_textview_value;

    @BindView(R.id.Identifier_textview_value)
    public TextView Identifier_textview_value;

    @BindView(R.id.BankSelectionCode_textview_value)
    public TextView BankSelectionCode_textview_value;

    @BindView(R.id.BankReferenceIdentifier_textview_value)
    public TextView BankReferenceIdentifier_textview_value;

    @BindView(R.id.Additional_Details_textview_value)
    public TextView Additional_Details_textview_value;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_payment);
        ButterKnife.bind(this);
        ReceiptGetTransferToBankId = getIntent().getExtras().getString("ReceiptGetTransferToBankId");
        TransactionAmount = getIntent().getExtras().getString("TransactionAmount");
        Currency = getIntent().getExtras().getString("Currency");
        Country = getIntent().getExtras().getString("Country");
        DetailsChallanNumber = getIntent().getExtras().getString("DetailsChallanNumber");
        EPFBAccountNumber = getIntent().getExtras().getString("EPFBAccountNumber");
        AmountInOtherDetails = getIntent().getExtras().getString("AmountInOtherDetails");

        initPaymentGateway();
    }

    private void initPaymentGateway()
    {
        Checkout checkout = new Checkout();

        checkout.setMerchantIdentifier(MerchantIdentifier); //where T1234 is the MERCHANT CODE, update it with Merchant Code provided by Ingenico
        checkout.setTransactionIdentifier(DetailsChallanNumber); //where TXN001 is the Merchant Transaction Identifier, it should be different for each transaction (alphanumeric value, no special character allowed)
        checkout.setTransactionReference (ReceiptGetTransferToBankId); //where ORD0001 is the Merchant Transaction Reference number
        checkout.setTransactionType
                (PaymentActivity.TRANSACTION_TYPE_SALE);//Transaction Type
        checkout.setTransactionSubType
                (PaymentActivity.TRANSACTION_SUBTYPE_DEBIT);//Transaction Subtype
        checkout.setTransactionCurrency (Currency); //Currency Type
        checkout.setTransactionAmount (TransactionAmount); //Transaction Amount


        checkout.setTransactionDateTime (GetDateInString()); //Transaction Date
// setting Consumer fields values
        checkout.setConsumerIdentifier (""); //Consumer Identifier, default value "", set this value as application user name if you want Instrument Vaulting, SI on Cards. Consumer ID should be alpha-numeric value with no space
        checkout.setConsumerAccountNo (""); //Account Number, default value "". For eMandate, you can set this value here or can be set later in SDK.



// setting Consumer Cart Item

        checkout.addCartItem(ProductID,TransactionAmount,"0", "0", "ProductSKU", "ProductReference", "Challan Number: "+DetailsChallanNumber,"ProductProviderID");


        Intent authIntent = new Intent(this, PaymentModesActivity.class);

// Checkout Object
        Log.d("Checkout Request Object",
                checkout.getMerchantRequestPayload().toString());

        authIntent.putExtra(Constant.ARGUMENT_DATA_CHECKOUT, checkout);

// Public Key
        authIntent.putExtra(PaymentActivity.EXTRA_PUBLIC_KEY, "1234-6666-6789-56");

// Requested Payment Mode
        authIntent.putExtra(PaymentActivity.EXTRA_REQUESTED_PAYMENT_MODE,
                PaymentActivity.PAYMENT_METHOD_DEFAULT);

        startActivityForResult(authIntent, PaymentActivity.REQUEST_CODE);



    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PaymentActivity.REQUEST_CODE) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG, "Result Code :" + RESULT_OK);
                if (data != null) {
                    try {
                        Checkout checkout_res = (Checkout) data
                                .getSerializableExtra(Constant
                                        .ARGUMENT_DATA_CHECKOUT);
                        Log.d("Checkout Response Obj", checkout_res
                                .getMerchantResponsePayload().toString());
                        String transactionSubType = checkout_res.getMerchantRequestPayload().getTransaction().getSubType();
                        System.out.println("Payment type => " + transactionSubType);
                        // Transaction Completed and Got SUCCESS
                        if (checkout_res.getMerchantResponsePayload().getPaymentMethod().getPaymentTransaction().getStatusCode().equalsIgnoreCase(
                                PaymentActivity.TRANSACTION_STATUS_SALES_DEBIT_SUCCESS)) {
                            Toast.makeText(this, "Transaction Status - Success", Toast.LENGTH_SHORT).show();
                            Log.v("TRANSACTION STATUS=>", "SUCCESS");
                            System.out.println("TRANSACTION_STATUS_SALES_DEBIT_SUCCESS");
                            /**
                             * TRANSACTION STATUS - SUCCESS (status code
                             * 0300 means success), NOW MERCHANT CAN PERFORM
                             * ANY OPERATION OVER SUCCESS RESULT
                             */
                            if (checkout_res.getMerchantResponsePayload().
                                    getPaymentMethod().getPaymentTransaction().
                                    getInstruction().getId() != null && checkout_res.getMerchantResponsePayload().
                                    getPaymentMethod().getPaymentTransaction().
                                    getInstruction().getId().isEmpty()) {
                                Log.v("TRANSACTION SI STATUS=>",
                                        "SI Transaction Not Initiated");
                                System.out.println("TRANSACTION SI  SI Transaction Not Initiated");
                            } else if (checkout_res.getMerchantResponsePayload().
                                    getPaymentMethod().getPaymentTransaction().
                                    getInstruction().getId() != null && !checkout_res.getMerchantResponsePayload().
                                    getPaymentMethod().getPaymentTransaction().
                                    getInstruction().getId().isEmpty()) {
                            /*
                              SI TRANSACTION STATUS - SUCCESS (Mandate  Registration ID received means success)
                             */
                                System.out.println("TRANSACTION SI SUCCESS (Mandate  Registration ID received means success)");
                                Log.v("TRANSACTION SI STATUS=>", "SUCCESS");
                            }
                        } else if (checkout_res
                                .getMerchantResponsePayload().getPaymentMethod().getPaymentTransaction().getStatusCode().equalsIgnoreCase(
                                        PaymentActivity.TRANSACTION_STATUS_DIGITAL_MANDATE_SUCCESS
                                )) {
                            Toast.makeText(this, "Transaction Status - Success", Toast.LENGTH_SHORT).show();
                            Log.v("TRANSACTION STATUS=>", "SUCCESS");
                            System.out.println("TRANSACTION_STATUS_DIGITAL_MANDATE_SUCCESS");
                            /**
                             * TRANSACTION STATUS - SUCCESS (status code
                             * 0398 means success), NOW MERCHANT CAN PERFORM
                             * ANY OPERATION OVER SUCCESS RESULT
                             */
                            if (checkout_res.getMerchantResponsePayload().
                                    getPaymentMethod().getPaymentTransaction().
                                    getInstruction().getId() != null
                                    && !checkout_res
                                    .getMerchantResponsePayload().getPaymentMethod().getPaymentTransaction().getInstruction().getId().isEmpty()) {
                                /**
                                 * SI TRANSACTION STATUS - SUCCESS (status
                                 * code 0300 means success)
                                 */
                                Log.v("TRANSACTION SI STATUS=>",
                                        "INITIATED");
                                System.out.println("Transaction Digital Mandate Success");
                            } else {
                                System.out.println("Transaction Digital Mandate Failure");
                                /**
                                 * SI TRANSACTION STATUS - Failure (status
                                 * code OTHER THAN 0300 means failure)
                                 */
                                Log.v("TRANSACTION SI STATUS=>", "FAILURE");
                            }
                        } else {
                            System.out.println("Bank Error Failure");
                            // some error from bank side
                            Log.v("TRANSACTION STATUS=>", "FAILURE");
                            Toast.makeText(this,
                                    "Transaction Status - Failure",
                                    Toast.LENGTH_SHORT).show();
                        }
                        String umrnNo = "";
                        String addDetails = checkout_res
                                .getMerchantResponsePayload().getMerchantAdditionalDetails();
                        if (addDetails != null && addDetails.contains("UMRNNumber")) {
                            String[] arrMandateData = addDetails.split("mandateData\\{");
                            if (arrMandateData != null && arrMandateData.length > 1) {
                                String[] arrMandateParams = arrMandateData[1].split("~");
                                if (arrMandateParams != null
                                        && arrMandateParams.length > 1) {
                                    String[] arrUMRN = arrMandateParams[0].split(":");
                                    if (arrUMRN != null && arrUMRN.length > 1) {
                                        umrnNo = arrUMRN[1];
                                    }
                                }
                            }
                        }

                        String result = "StatusCode : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getStatusCode()
                                + "\nStatusMessage : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getStatusMessage()
                                + "\nErrorMessage : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getErrorMessage()
                                + "\nAmount : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod().getPaymentTransaction().getAmount()
                                + "\nDateTime : " + checkout_res.
                                getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getDateTime()
                                + "\nMerchantTransactionIdentifier : "
                                + checkout_res.getMerchantResponsePayload()
                                .getMerchantTransactionIdentifier()
                                + "\nIdentifier : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                 .getPaymentTransaction().getIdentifier()
                                + "\nBankSelectionCode : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getBankSelectionCode()
                                + "\nBankReferenceIdentifier : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getBankReferenceIdentifier()
                                + "\nRefundIdentifier : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getRefundIdentifier()
                                + "\nBalanceAmount : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getBalanceAmount()
                                + "\nInstrumentAliasName : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getInstrumentAliasName()
                                + "\nSI Mandate Id : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getInstruction().getId()
                                + "\nUMRN No : "
                                + umrnNo

                                + "\nSI Mandate Status : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getInstruction().getStatusCode()



                                + "\nMerchant Transaction Request Type : " + checkout_res
                                .getMerchantResponsePayload().getMerchantTransactionRequestType()

                                + "\nMerchant Additional Details : " + checkout_res
                                .getMerchantResponsePayload().getMerchantAdditionalDetails()



                                + "\nSI Mandate Error Code : " + checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getInstruction().getErrorcode();

                        System.out.println("result: " +result);



                        StatusCode_textview_value.setText(""+checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getStatusCode());

                        StatusMessage_value.setText(""+checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getStatusMessage());

                        ErrorMessage_textview_value.setText(""+checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getErrorMessage());

                        Amount_textview_value.setText(""+checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getAmount());

                        DateTime_textview_value.setText(""+checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getDateTime());

                        MerchantTransactionIdentifier_textview_value.setText(""+checkout_res
                                .getMerchantResponsePayload().getMerchantTransactionIdentifier());

                        Identifier_textview_value.setText(""+checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getIdentifier());

                        BankSelectionCode_textview_value.setText(""+checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getBankSelectionCode());

                        BankReferenceIdentifier_textview_value.setText(""+checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getBankReferenceIdentifier());

                        Additional_Details_textview_value.setText(""+checkout_res
                                .getMerchantResponsePayload().getMerchantAdditionalDetails());


                        HitSubmitPaymentDetailsAPI(checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getStatusCode(),checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getStatusMessage(),checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getErrorMessage(),checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getAmount(),checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getDateTime(),checkout_res
                                .getMerchantResponsePayload().getMerchantTransactionIdentifier(),checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getPaymentTransaction().getIdentifier(),checkout_res
                                .getMerchantResponsePayload().getPaymentMethod()
                                .getBankSelectionCode());
                    } catch (Exception e) {
                      //  e.printStackTrace();
                    }
                }
            } else if (resultCode == PaymentActivity.RESULT_ERROR) {
                Log.d(TAG, "got an error");
                if (data.hasExtra(PaymentActivity.RETURN_ERROR_CODE) &&
                        data.hasExtra(PaymentActivity.RETURN_ERROR_DESCRIPTION)) {
                    String error_code = (String) data
                            .getStringExtra(PaymentActivity.RETURN_ERROR_CODE);
                    String error_desc = (String) data
                            .getStringExtra(PaymentActivity.RETURN_ERROR_DESCRIPTION);
                    Toast.makeText(this, " Got error :"
                                    + error_code + "--- " + error_desc, Toast.LENGTH_SHORT)
                            .show();
                    Log.d(TAG + " Code=>", error_code);
                    Log.d(TAG + " Desc=>", error_desc);
                }
            } else if (resultCode == PaymentActivity.RESULT_CANCELED) {
                Toast.makeText(this, "Transaction Aborted by User",
                        Toast.LENGTH_SHORT).show();
                Log.d(TAG, "User pressed back button");
            } else if (resultCode == PaymentActivity.RESULT_PENDING) {
                Toast.makeText(this, "Transaction is in Pending state",
                        Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Transaction is in Pending state");
            }
        }
    }


    private void HitSubmitPaymentDetailsAPI(String StatusCode,String StatusMessage,String ErrorMessage,String Amount,
                                            String DateTime,String DetailsChallanNo,
                                            String Identifier,String BankSelectionCode) {

        HandleHttpsRequest.setSSL();


        System.out.println("Send Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.SEND_PAYMENT_DETAILS_URL;
        final ProgressDialog pDialog = new ProgressDialog(this,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                PaymentDetailsModel sendOTPModel=gson.fromJson(response,PaymentDetailsModel.class);



                MyToken.setToken(sendOTPModel.getRefreshTokenValue());
                MyToken.setAuthorization(sendOTPModel.getAcsessTokenValue());


              /*  if (sendOTPModel.getResponseStatus().matches("Success")
                        &&sendOTPModel.getEncryptedRequestparameter()!=null



                ) {
                    MyToken.setToken(sendOTPModel.getRefreshTokenValue());
                    MyToken.setAuthorization(sendOTPModel.getAcsessTokenValue());

                 //   changeFragment_PaymentWebViewFragment(sendOTPModel);

                    //  return;
                } else {


                    showDialog_ServerError("Some Error occured !");
                    //  return;
                }*/
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());


                params.put("encData",StatusCode+"|"+StatusMessage+"|"+ErrorMessage+"|"+Amount+"|"+DateTime+"|"+DetailsChallanNo+"|"+Identifier+"|"+BankSelectionCode);

                if (MainActivity.IsLogin) {
                    Gson gson = new GsonBuilder().setLenient().create();
                    LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);

                    params.put("loggedInUserName",userInfoModel.getUserName());
                }
                else
                {
                    params.put("loggedInUserName","");
                }

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }

        };




        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }
    public void showDialog_ServerError(String errorText)
    {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // getActivity().finish();
                dialog.dismiss();
                //System.exit(0);
            }
        });
    }
    private String GetDateInString()
    {
        Date cDate = Calendar.getInstance().getTime();
        String fDate = new SimpleDateFormat("dd-MM-yyyy").format(cDate);

        return fDate;
    }

}
