package testme.myapp.com.ntrp.module_nonregisteredusers.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

import butterknife.OnClick;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.events.MakeUserLogout;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.RegisterUserModel;
import testme.myapp.com.ntrp.module_login.events.AskMobileNumberDialogEvent;
import testme.myapp.com.ntrp.module_login.models.LogedInUserDetailsModels;
import testme.myapp.com.ntrp.module_nonregisteredusers.adapters.AddFormAdaptor;
import testme.myapp.com.ntrp.module_nonregisteredusers.adapters.ChoosePurposeAdapter;
import testme.myapp.com.ntrp.module_nonregisteredusers.events.BackPressEvent;
import testme.myapp.com.ntrp.module_nonregisteredusers.events.DeletePurposeEvent;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.AddFormModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.AutoCompleteTextModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.DeletePurposeModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.DepositorCategoryModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.MinistryListModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.NextPageModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.PurposeFunctionalMinisty;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.PurposeMinistryFunctionHeadListModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.SelectedPurposeDetailModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;
import testme.myapp.com.ntrp.utils.MessageEvent;

/**
 * Created by deepika.s on 29-05-2017.
 */

public class NonRegisteredUsersFragment extends Fragment {

    private View localView;
    private TextView DepositorCategory_TextView,Purpose_TextView,DDO_TextView,Amount_TextView,PaymentFrequency_TextView;
    private Button CrossDialogButton,AddButton;
    private RecyclerView  choosepurpose_recyclerview;
    private ImageView SearchPurposeButton;

    @BindView(R.id.spinner_depositorcategory)
    public Spinner spinner_depositorcategory;

    @BindView(R.id.added_item_list_recycler_view)
    public RecyclerView added_item_list_recycler_view;

    @BindView(R.id.scrollView)
    public ScrollView scrollView;

    @BindView(R.id.next_button)
    public Button next_button;


    @BindView(R.id.spinner_pao)
    public Spinner spinner_pao;

    @BindView(R.id.spinner_ddo)
    public Spinner spinner_ddo;

    @BindView(R.id.spinner_currency)
    public Spinner spinner_currency;

    @BindView(R.id.spinner_payment_frequency)
    public Spinner spinner_payment_frequency;

    @BindView(R.id.spinner_year_for_payment)
    public Spinner spinner_year_for_payment;

    @BindView(R.id.spinner_month_for_payment)
    public Spinner spinner_month_for_payment;

    @BindView(R.id.spinner_six_month_for_payment)
    public Spinner spinner_six_month_for_payment;




    @BindView(R.id.remarks_edittext)
    public EditText remarks_edittext;

    @BindView(R.id.amountvalue_edittext)
    public EditText amountvalue_edittext;

    @BindView(R.id.mobile_number_edittext)
    public EditText mobile_number_edittext;

    @BindView(R.id.selected_purpose_textview)
    public TextView selected_purpose_textview;

    @BindView(R.id.selected_paymenttype_textview)
    public TextView selected_paymenttype_textview;

    @BindView(R.id.selected_functionhead_textview)
    public TextView selected_functionhead_textview;

    @BindView(R.id.mobile_number_textview)
    public TextView mobile_number_textview;

    @BindView(R.id.remarks_textview)
    public TextView remarks_textview;



    @BindView(R.id.selected_ministry_textview)
    public TextView selected_ministry_textview;

    @BindView(R.id.from_date_edittext)
    public EditText from_date_edittext;

    @BindView(R.id.to_date_edittext)
    public EditText to_date_edittext;


    @BindView(R.id.from_date_range_linear_layout)
    public LinearLayout from_date_range_linear_layout;

    @BindView(R.id.to_date_range_linear_layout)
    public LinearLayout to_date_range_linear_layout;

    @BindView(R.id.monthly_payment_linear_layout)
    public LinearLayout monthly_payment_linear_layout;

    private Calendar fromCalendar= Calendar.getInstance();

    private Calendar toCalendar= Calendar.getInstance();

    private DatePickerDialog.OnDateSetListener fromDatePickerDialogListner,toDatePickerDialogListner;


    //private String[] MonthsArray;

    private DepositorCategoryModel depositorCategorymodel;


    private Spinner spinner_ministry;

    private AutoCompleteTextView typed_purose_textview;

    private MinistryListModel ministryListModel;

    private String selectedDDOID;

    private String selectedMinistryName,SelectedPurpose="";

    private Dialog ChoosepurposeDialog;

    public static int choosenPuposePosition;

    private SelectedPurposeDetailModel selectedPurposeDetailModel;

    private PurposeFunctionalMinisty purposeFunctionalMinisty;

    private int DepositorCategoryId,selectedMinistryId;

    private String UNIQUE_ID;

    private LinearLayout page_number_layout;

    private String ReceiptEntryId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_non_registered_users_payment_purpose, container, false);
        localView=view;
        ButterKnife.bind(this,view);
        getActivity().setTitle("Non Register User");
        CloseKeyBoard();
        initializeView();

        return view;
    }

    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
    private void GenerateDeviceID()
    {
        UNIQUE_ID= String.valueOf(UUID.randomUUID());
    }
    private void initializeView() {

        GenerateDeviceID();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, getResources().getStringArray(R.array.months_for_payment_frequency));
        spinner_month_for_payment.setAdapter(adapter);
        spinner_month_for_payment.setSelection(0);


        ArrayAdapter<String> adapterforyear = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, getResources().getStringArray(R.array.financialyear_for_payment_frequency));
        spinner_year_for_payment.setAdapter(adapterforyear);
        spinner_year_for_payment.setSelection(0);

        ArrayAdapter<String> adapterforsixmonth = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, getResources().getStringArray(R.array.six_months_for_payment_frequency));
        spinner_six_month_for_payment.setAdapter(adapterforsixmonth);
        spinner_six_month_for_payment.setSelection(0);

        next_button.setVisibility(View.GONE);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        added_item_list_recycler_view.setLayoutManager(mLayoutManager);
        added_item_list_recycler_view.setItemAnimator(new DefaultItemAnimator());

        //MonthsArray=getResources().getStringArray(R.array.months_for_payment_frequency);

        amountvalue_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(amountvalue_edittext.getText().length() == 1 && amountvalue_edittext.getText().toString().trim().equals("0")){
                    amountvalue_edittext.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        remarks_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int charleft=60-s.length();

                remarks_textview.setText(charleft+ "character left");

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        spinner_ddo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                selectedDDOID=selectedPurposeDetailModel.getDdlDDOLst().get(position).getValue();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_depositorcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                DepositorCategoryId=depositorCategorymodel.getDepositorCategoryList().get(position).getDepositorCategoryId();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        fromDatePickerDialogListner =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                fromCalendar.set(Calendar.YEAR, year);
                fromCalendar.set(Calendar.MONTH,month);
                fromCalendar.set(Calendar.DAY_OF_MONTH,day);
                updateFromEditText();

            }
        };



        toDatePickerDialogListner =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                toCalendar.set(Calendar.YEAR, year);
                toCalendar.set(Calendar.MONTH,month);
                toCalendar.set(Calendar.DAY_OF_MONTH,day);
                updateToEditText();

            }
        };


        spinner_payment_frequency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (spinner_payment_frequency.getSelectedItem().toString().matches("Specified Period"))
                {
                   from_date_range_linear_layout.setVisibility(View.VISIBLE);
                   to_date_range_linear_layout.setVisibility(View.VISIBLE);

                    monthly_payment_linear_layout.setVisibility(View.GONE);

                }
                 else if (spinner_payment_frequency.getSelectedItem().toString().matches("Monthly"))
                {
                    from_date_range_linear_layout.setVisibility(View.GONE);
                    to_date_range_linear_layout.setVisibility(View.GONE);

                    monthly_payment_linear_layout.setVisibility(View.VISIBLE);
                    spinner_six_month_for_payment.setVisibility(View.GONE);

                }
                else if (spinner_payment_frequency.getSelectedItem().toString().matches("One Time"))
                {
                    from_date_range_linear_layout.setVisibility(View.GONE);
                    to_date_range_linear_layout.setVisibility(View.GONE);

                    monthly_payment_linear_layout.setVisibility(View.GONE);

                }
                else if (spinner_payment_frequency.getSelectedItem().toString().matches("Annual"))
                {
                    from_date_range_linear_layout.setVisibility(View.GONE);
                    to_date_range_linear_layout.setVisibility(View.GONE);

                    monthly_payment_linear_layout.setVisibility(View.VISIBLE);
                    spinner_month_for_payment.setVisibility(View.GONE);
                    spinner_six_month_for_payment.setVisibility(View.GONE);

                }
                else if (spinner_payment_frequency.getSelectedItem().toString().matches("Half Yearly"))
                {
                    from_date_range_linear_layout.setVisibility(View.GONE);
                    to_date_range_linear_layout.setVisibility(View.GONE);

                    monthly_payment_linear_layout.setVisibility(View.VISIBLE);
                    spinner_month_for_payment.setVisibility(View.GONE);
                    spinner_six_month_for_payment.setVisibility(View.VISIBLE);


                }
                else
                {
                    monthly_payment_linear_layout.setVisibility(View.GONE);
                    from_date_range_linear_layout.setVisibility(View.GONE);
                    to_date_range_linear_layout.setVisibility(View.GONE);

                }


            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




       /* from_date_edittext.setText(getFormatedDate(from_date_calender.getDate()));
        to_date_edittext.setText(getFormatedDate(to_date_calender.getDate()));

        from_date_calender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView,int year, int month, int dayOfMonth) {
                to_date_calender.setMinDate(calendarView.getDate());
                from_date_edittext.setText(getFormatedDate(calendarView.getDate()));
            }
        });

        to_date_calender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView,int year, int month, int dayOfMonth) {
                from_date_calender.setMinDate(calendarView.getDate());
                to_date_edittext.setText(getFormatedDate(calendarView.getDate()));
            }
        });*/




        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    Hit_Next_Button_API();

            }
        });

        initialize_TextViews();
        initialize_SearchPurposeButton();
        initialize_AddButton();

        mobile_number_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               if (s.length()==10)
               {
                   Hit_RegisterUser_API();
                 //Hit_API_Token();
               }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        MakeuserLogout();
    }

    private void MakeuserLogout() {



        if (MainActivity.IsLogin)
        {
            EventBus.getDefault().post(new MakeUserLogout());
            showDialog("You have been logout !");
        }

    }
    private void showDialog(String warningText) {

        final Dialog dialog = new Dialog(this.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_success);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
            }
        });

    }

    private void initialize_TextViews() {
        DepositorCategory_TextView= (TextView) localView.findViewById(R.id.depositorcategory_textview);
        Purpose_TextView= (TextView) localView.findViewById(R.id.purpose_textview);
        DDO_TextView= (TextView) localView.findViewById(R.id.ddo_textview);
        Amount_TextView= (TextView) localView.findViewById(R.id.amount_textview);
        PaymentFrequency_TextView= (TextView) localView.findViewById(R.id.paymentfrequency_textview);
        putRedAsterisk(localView,DepositorCategory_TextView,R.id.depositorcategory_textview);
        putRedAsterisk(localView,Purpose_TextView,R.id.purpose_textview);
        putRedAsterisk(localView,DDO_TextView,R.id.ddo_textview);
        putRedAsterisk(localView,Amount_TextView,R.id.amount_textview);
        putRedAsterisk(localView,mobile_number_textview,R.id.mobile_number_textview);
        putRedAsterisk(localView,PaymentFrequency_TextView,R.id.paymentfrequency_textview);
    }
    private void initialize_SearchPurposeButton()
    {
        SearchPurposeButton= (ImageView) localView.findViewById(R.id.searchpurpose_button);
        SearchPurposeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mobile_number_edittext.getText().toString().matches(""))
                {
                    mobile_number_edittext.setError("This field can not be blank");
                }
                else if (mobile_number_edittext.getText().toString().matches("")==false)
                {
                    showDialog_Choosepurpose();
                }

                return;
            }
        });
    }
    private void showDialog_Choosepurpose() {
        ChoosepurposeDialog = new Dialog(getActivity());
        ChoosepurposeDialog.setContentView(R.layout.dialog_nonregisteredusers_choosepurpose);
        ChoosepurposeDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ChoosepurposeDialog.show();

        typed_purose_textview=(AutoCompleteTextView) ChoosepurposeDialog.findViewById(R.id.typed_purose_textview);
        EditText page_number_textview=(EditText) ChoosepurposeDialog.findViewById(R.id.page_number_textview);
        Button search_button=(Button) ChoosepurposeDialog.findViewById(R.id.search_button);


        Button go_page_number_button=(Button) ChoosepurposeDialog.findViewById(R.id.go_page_number_button);
        page_number_layout=(LinearLayout) ChoosepurposeDialog.findViewById(R.id.page_number_layout);
        page_number_layout.setVisibility(View.GONE);




        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    Hit_Get_Purpose_List_API("1");



            }
        });

        go_page_number_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Hit_Get_Purpose_List_API(page_number_textview.getText().toString());



            }
        });


        spinner_ministry=(Spinner) ChoosepurposeDialog.findViewById(R.id.spinner_ministry);

        spinner_ministry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                selectedMinistryName=ministryListModel.getMinistryList().get(position).getMinistryName();
                selectedMinistryId=ministryListModel.getMinistryList().get(position).getMinistryId();
                typed_purose_textview.setAdapter(null);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        choosepurpose_recyclerview=(RecyclerView) ChoosepurposeDialog.findViewById(R.id.choosepurpose_recyclerview);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        choosepurpose_recyclerview.setLayoutManager(mLayoutManager);
        choosepurpose_recyclerview.setItemAnimator(new DefaultItemAnimator());
        choosepurpose_recyclerview.setVisibility(View.GONE);


        CrossDialogButton= (Button) ChoosepurposeDialog.findViewById(R.id.cross_button);
        CrossDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChoosepurposeDialog.dismiss();
                mobile_number_edittext.setText("");
                typed_purose_textview.setAdapter(null);
                choosepurpose_recyclerview.setAdapter(null);
                SelectedPurpose="";
            }
        });


        typed_purose_textview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()==3)
                {
                    Hit_Auto_Complete_API(typed_purose_textview.getText().toString(), String.valueOf(selectedMinistryId));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        typed_purose_textview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SelectedPurpose= typed_purose_textview.getText().toString();



            }
        });


        Hit_Get_Ministry_List_API();
    }
    private void initialize_AddButton()
    {
        AddButton= (Button) localView.findViewById(R.id.add_button);
        AddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amountvalue_edittext.getText().toString().matches(""))
                {
                    amountvalue_edittext.setError("This field can not be blank");
                }
                if (mobile_number_edittext.getText().toString().matches(""))
                {
                    mobile_number_edittext.setError("This field can not be blank");
                }
                if (selected_purpose_textview.getText().toString().matches("")||spinner_ministry.getAdapter()==null)
                {
                    selected_purpose_textview.setError("Choose purpose again");
                }
                if (from_date_range_linear_layout.getVisibility()==View.VISIBLE&&toCalendar.compareTo(fromCalendar)<0)
                {
                    to_date_edittext.setError("To date can not be less than from date");
                   Toast.makeText(getContext(),"To date can not be less than from date",Toast.LENGTH_LONG).show();

                }
                else if (amountvalue_edittext.getText().toString().matches("")==false&&mobile_number_edittext.getText().toString().matches("")==false&&selected_purpose_textview.getText().toString().matches("")==false)
                {
                    Hit_Add_Form_API(purposeFunctionalMinisty,selectedPurposeDetailModel );
                }

                return;
            }
        });
    }
//    private void showDialog_PaymentPurposePreview() {
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.setContentView(R.layout.dialog_nonregisteredusers_paymentpurposepreview);
//        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        dialog.show();
//
//
//
//        CrossDialogButton= (Button) dialog.findViewById(R.id.cross_button);
//        CrossDialogButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        Button BackDialogButton= (Button) dialog.findViewById(R.id.back_button);
//        BackDialogButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        Button NextDialogButton= (Button) dialog.findViewById(R.id.next_button);
//        NextDialogButton .setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                changeFragment_DepositorDetails();
//                return;
//            }
//        });
//
//
//
//    }
    private void changeFragment_DepositorDetails()
    {
        Bundle b = new Bundle();
        b.putString("ReceiptEntryId",ReceiptEntryId);
        b.putString("MobileNumber",mobile_number_edittext.getText().toString());
        b.putString("UniqueUserDeviceId",UNIQUE_ID);
        b.putInt("DepositorCategoryId",DepositorCategoryId);
        Fragment nonRegisteredUsers_depositorDetailsFragment = new NonRegisteredUsers_DepositorDetailsFragment();
        nonRegisteredUsers_depositorDetailsFragment.setArguments(b);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, nonRegisteredUsers_depositorDetailsFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void putRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }

    private void Hit_Get_Depositor_Category_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Depositor_Category;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                DepositorCategoryModel depositorCategoryModel = gson.fromJson(response, DepositorCategoryModel.class);

                if(depositorCategoryModel.getResponseStatus().matches("Success"))
                {
                    depositorCategorymodel=depositorCategoryModel;

                    ArrayList<String> items = new ArrayList<String>();

                    for(int i=0;i<depositorCategoryModel.getDepositorCategoryList().size();i++)
                    {
                        items.add(depositorCategoryModel.getDepositorCategoryList().get(i).getDepositorCategoryName());
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
                    spinner_depositorcategory.setAdapter(adapter);
                    spinner_depositorcategory.setSelection(0);


                }
                else
                {
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Failed to get Depositor Category List!",
                            Toast.LENGTH_LONG).show();
                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

       // strReq.setRetryPolicy(new DefaultRetryPolicy(20000, 3, 1.0f));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_Get_Ministry_List_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Ministry_List;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                 ministryListModel = gson.fromJson(response, MinistryListModel.class);
                //MyToken.setToken(ministryListModel.getRefreshTokenValue());
                //MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if(ministryListModel.getResponseStatus().matches("Success"))
                {
                    ArrayList<String> items = new ArrayList<String>();

                    for(int i=0;i<ministryListModel.getMinistryList().size();i++)
                    {
                        items.add(ministryListModel.getMinistryList().get(i).getMinistryName());
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
                    spinner_ministry.setAdapter(adapter);
                    spinner_ministry.setSelection(0);
                }
                else
                {
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Failed to get Depositor Category List!",
                            Toast.LENGTH_LONG).show();
                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_Get_Purpose_List_API(String pageIndex) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.GetPurpose_List;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                PurposeMinistryFunctionHeadListModel ministryListModel = gson.fromJson(response, PurposeMinistryFunctionHeadListModel.class);

                MyToken.setToken(ministryListModel.getRefreshTokenValue());
                MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if(ministryListModel.getErrorCodes().matches("Success"))
                {


                    ChoosePurposeAdapter choosePurposeAdapter=new ChoosePurposeAdapter(getContext(),NonRegisteredUsersFragment.this,ministryListModel.getPurposeFunctionalMinistyList(),ChoosepurposeDialog);

                    choosepurpose_recyclerview.setAdapter(choosePurposeAdapter);
                    page_number_layout.setVisibility(View.VISIBLE);
                    choosepurpose_recyclerview.setVisibility(View.VISIBLE);
                }
                else
                {
                    ChoosepurposeDialog.dismiss();
                    mobile_number_edittext.setText("");
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Data not available!",
                            Toast.LENGTH_LONG).show();

                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Some error occured !"+error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("ControllerName", selectedMinistryName);
                params.put("PurposeName", SelectedPurpose);
                params.put("FunctionHeadNo", "");
                params.put("pageIndex", pageIndex);
                params.put("userId", "0");
                params.put("flag", "0");

                /*if (MainActivity.IsLogin)
                {
                    params.put("UserId",  MainActivity.UserID );
                    params.put("UserName", MainActivity.UserName);
                }*/

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
             //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }





    private void Hit_API_Token() {
        HandleHttpsRequest.setSSL();
        System.out.println("Token API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                TokenModel tokenModel=gson.fromJson(response,TokenModel.class);
                MyToken.setToken(tokenModel.refresh_token);
                MyToken.setAuthorization(tokenModel.access_token);
                if(MyToken.getToken()!=null)
                {

                    if (CheckNetworkConnection.getInstance(NonRegisteredUsersFragment.this.getActivity()).isOnline())
                    {

                        Prefs.putString("LoggedInMobileNumber", mobile_number_edittext.getText().toString());
                        Hit_Get_Depositor_Category_API();

                    }
                    else {
                        Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Some error occured!",
                                Toast.LENGTH_LONG).show();

                    }
                }
                else
                {
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Some error occured!",
                            Toast.LENGTH_LONG).show();
                }




            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("UserName", mobile_number_edittext.getText().toString());
                params.put("AppType", "NTRP-APP");
                params.put("DeviceID", "123");
                params.put("AppVersion", "1.0");
                params.put("password", "123456");
                params.put("OS", "android");
                params.put("Operator", "airtel");
                params.put("grant_type", "password");
                params.put("Flag", "5");
                return params;
            }


        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }

    private void Hit_After_Purpose_Selected_API(PurposeFunctionalMinisty purposeFunctionalMinisty) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Get_After_Selected_List;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                SelectedPurposeDetailModel ministryListModel = gson.fromJson(response, SelectedPurposeDetailModel.class);

                MyToken.setToken(ministryListModel.getRefreshTokenValue());
                MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if(ministryListModel.getRefreshTokenValue()!=null)
                {

                     selectedPurposeDetailModel=ministryListModel;

                    ArrayList<String> items = new ArrayList<String>();

                    for(int i=0;i<ministryListModel.getDdlPAOLst().size();i++)
                    {
                        items.add(ministryListModel.getDdlPAOLst().get(i).getText());
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
                    spinner_pao.setAdapter(adapter);
                    spinner_pao.setSelection(0);


                    ArrayList<String> items1 = new ArrayList<String>();

                    for(int i=0;i<ministryListModel.getDdlDDOLst().size();i++)
                    {
                        items1.add(ministryListModel.getDdlDDOLst().get(i).getText());
                    }


                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items1);
                    spinner_ddo.setAdapter(adapter1);
                    spinner_ddo.setSelection(0);


//                    ArrayAdapter adapter1 = new ArrayAdapter(getContext(),
//                            R.layout.spinner_textview, items1);
//                    adapter1.setDropDownViewResource(R.layout.spinner_textview);
//                    spinner_ddo.setAdapter(adapter1);

                    ArrayList<String> items2 = new ArrayList<String>();

                    for(int i=0;i<ministryListModel.getDdlCurrencylst().size();i++)
                    {
                        items2.add(ministryListModel.getDdlCurrencylst().get(i).getText());
                    }


                    ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items2);
                    spinner_currency.setAdapter(adapter2);
                    spinner_currency.setSelection(0);


                    ArrayList<String> items3 = new ArrayList<String>();

                    for(int i=0;i<ministryListModel.getDdlPeriodLst().size();i++)
                    {
                        items3.add(ministryListModel.getDdlPeriodLst().get(i).getText());
                    }


                    ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items3);
                    spinner_payment_frequency.setAdapter(adapter3);
                    spinner_payment_frequency.setSelection(0);


                    selected_purpose_textview.setText(ministryListModel.getTxtBxPurpose());

                    selected_paymenttype_textview.setText(ministryListModel.getLblPaymenttypetext());

                    selected_functionhead_textview.setText(ministryListModel.getLblFunctionHeadInfo());

                    selected_ministry_textview.setText(ministryListModel.getLblMinistryInfo());



                }
                else
                {
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Failed to get PAO & DDO list!",
                            Toast.LENGTH_LONG).show();
                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Some error occured !"+error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("DepsitorCategory", spinner_depositorcategory.getSelectedItem().toString());
                params.put("PurposeName", "");

                params.put("FunctionHeadId", String.valueOf(purposeFunctionalMinisty.getFunctionHead()));

                params.put("Paymenttypeid", String.valueOf(purposeFunctionalMinisty.getPaymentTypeid()));
                params.put("userId", "0");
                params.put("PaymentPurposeId", String.valueOf(purposeFunctionalMinisty.getNTRPPurposeId()));
                params.put("ValidationType", String.valueOf(purposeFunctionalMinisty.getValidationType()));
                params.put("PaymentModeId", String.valueOf(purposeFunctionalMinisty.getPaymentModeId()));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }



    private void Hit_Add_Form_API(PurposeFunctionalMinisty purposeFunctionalMinisty, SelectedPurposeDetailModel selectedPurposeDetailModel) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Get_App_Form_URL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                AddFormModel addFormModel = gson.fromJson(response, AddFormModel.class);

                MyToken.setToken(addFormModel.getRefreshTokenValue());
                MyToken.setAuthorization(addFormModel.getAcsessTokenValue());

                if(addFormModel.getResponseStatus().matches("Success"))
                {

                    AddFormAdaptor choosePurposeAdapter=new AddFormAdaptor(getContext(),NonRegisteredUsersFragment.this,addFormModel.getFormDetailList());

                    added_item_list_recycler_view.setAdapter(choosePurposeAdapter);
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    });
                    if (addFormModel.getFormDetailList()!=null&&addFormModel.getFormDetailList().size()>0)
                    {
                        next_button.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        next_button.setVisibility(View.GONE);
                    }
                    //Hit_GetAll_Purpose_API();
                }

                else if (addFormModel.getErrorCodes()!=null)
                {
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), addFormModel.getErrorCodes(),
                            Toast.LENGTH_LONG).show();
                    mobile_number_edittext.setText("");
                    ResetData();
                }

                else
                {
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Some error occured, Please resubmit all details!",
                            Toast.LENGTH_LONG).show();
                    mobile_number_edittext.setText("");
                    ResetData();
                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Some error occured !"+error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());



                params.put("ddlDepsitorCategorySelectedValue",   String.valueOf(DepositorCategoryId) );

                params.put("hdnMinistryInfo",  selectedPurposeDetailModel.getLblMinistryInfo()!=null?  selectedPurposeDetailModel.getLblMinistryInfo() : "");

                params.put("hdnMinistryId", String.valueOf(selectedPurposeDetailModel.getMinistryId()));

                params.put("HdnObjectHead", String.valueOf(purposeFunctionalMinisty.getObjectHead()));

                params.put("hdnServiceYear", String.valueOf(purposeFunctionalMinisty.getServiceYear()));

                params.put("ddlPAO", String.valueOf(selectedPurposeDetailModel.getDdlPAO()));

                params.put("hdnPaymenttypeid", String.valueOf(purposeFunctionalMinisty.getPaymentTypeid()));

                params.put("ddlPAOText", String.valueOf(spinner_pao.getSelectedItem().toString()));

                params.put("ddlDDO", selectedDDOID);

                params.put("ddlDDOSelectedText", String.valueOf(spinner_ddo.getSelectedItem().toString()));

                params.put("hdnAgencyId", String.valueOf(selectedPurposeDetailModel.getAgencyId()));

                params.put("lblAgencyInfo", String.valueOf(selectedPurposeDetailModel.getAgencyInfo()));

                params.put("txtAmount", String.valueOf(amountvalue_edittext.getText().toString()));

                params.put("hdPaymentPurposeId", String.valueOf(selectedPurposeDetailModel.getPaymentPurposeID()));

                params.put("ddlCurrencyText", String.valueOf(spinner_currency.getSelectedItem().toString()));

                params.put("hdnAccountId", String.valueOf(selectedPurposeDetailModel.getAccountId()));

                params.put("lblFunctionHeadInfo", String.valueOf(selectedPurposeDetailModel.getLblFunctionHeadInfo()));

                params.put("txtBxPurpose", String.valueOf(selectedPurposeDetailModel.getTxtBxPurpose()));

                params.put("hdnFunctionalHeadId", String.valueOf(selectedPurposeDetailModel.getFunctionalHeadId()));

                params.put("hdnPaymentModeId", String.valueOf(purposeFunctionalMinisty.getPaymentModeId()));

                params.put("hdnAccShortName", selectedPurposeDetailModel.getHdnAccShortName()==null? "": String.valueOf(selectedPurposeDetailModel.getHdnAccShortName()));



                params.put("txtPurposeRemarks", remarks_edittext.getText().toString());

                params.put("hdnAccGroupId", String.valueOf(selectedPurposeDetailModel.getAccGroupId()));

                params.put("ddlPeriod",  selectedPurposeDetailModel.getDdlPeriodLst()==null ? "" :   String.valueOf(selectedPurposeDetailModel.getDdlPeriodLst().get(0).getValue())); //

                params.put("ddlPeriodText", String.valueOf(spinner_payment_frequency.getSelectedItem().toString()));

                params.put("ddlYearSelectedText", spinner_payment_frequency.getSelectedItem().toString().matches("Annual")||spinner_payment_frequency.getSelectedItem().toString().matches("Monthly")? spinner_year_for_payment.getSelectedItem().toString() : "");

                params.put("txtFromDate",   from_date_range_linear_layout.getVisibility()==View.VISIBLE && spinner_payment_frequency.getSelectedItem().toString().matches("Specified Period") ? from_date_edittext.getText().toString() : "");

                params.put("txtToDate", to_date_range_linear_layout.getVisibility()==View.VISIBLE && spinner_payment_frequency.getSelectedItem().toString().matches("Specified Period") ?  to_date_edittext.getText().toString() : "");



                params.put("ddlMothlySelectedText", spinner_payment_frequency.getSelectedItem().toString().matches("Monthly") ? spinner_month_for_payment.getSelectedItem().toString() : "");

                params.put("ddlQUARTERLYSelectedText", "");

                params.put("ddlhalfyearlyText", spinner_payment_frequency.getSelectedItem().toString().matches("Half Yearly") ?  spinner_six_month_for_payment.getSelectedItem().toString() : "");

                params.put("lblPaymenttypetext", selectedPurposeDetailModel.getLblPaymenttypetext()!=null ? selectedPurposeDetailModel.getLblPaymenttypetext() :"");

                params.put("UniqueUserDeviceId", UNIQUE_ID);

                    if (spinner_payment_frequency.getSelectedItem().toString().matches("Half Yearly")
                    ||spinner_payment_frequency.getSelectedItem().toString().matches("Monthly")
                            ||spinner_payment_frequency.getSelectedItem().toString().matches("Annual")

                            ||spinner_payment_frequency.getSelectedItem().toString().matches("Specified Period")
                    )
                    {
                        params.put("ddlYear", "2023");
                    }



                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_Next_Button_API() {


        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Get_Next_URL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                NextPageModel ministryListModel = gson.fromJson(response, NextPageModel.class);

                MyToken.setToken(ministryListModel.getRefreshTokenValue());
                MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if(ministryListModel.getRefreshTokenValue()!=null)
                {

                    ReceiptEntryId=ministryListModel.getReceiptEntryId()!=null? ministryListModel.getReceiptEntryId().toString() : null;

                    if (ReceiptEntryId!=null)
                    {
                        changeFragment_DepositorDetails();
                    }
                    else
                    {
                        Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Some error occured while getting data",
                                Toast.LENGTH_LONG).show();
                    }

                }
                else
                {
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Failed to get data!",
                            Toast.LENGTH_LONG).show();
                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Some error occured !"+error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    private void Hit_Auto_Complete_API(String TextToBeSearch,String MinistryId) {


        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_Auto_Complete_Text_URL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                AutoCompleteTextModel ministryListModel = gson.fromJson(response, AutoCompleteTextModel.class);

                MyToken.setToken(ministryListModel.getRefreshTokenValue());
                MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if(ministryListModel.getRefreshTokenValue()!=null&&ministryListModel.getErrorCodes().matches("Success")&&ministryListModel.getPurposeList()!=null&&ministryListModel.getPurposeList().size()>0)
                {

                     ArrayList<String> purposetextList=new ArrayList<>();

                     for (int i=0;ministryListModel.getPurposeList()!=null&&i<ministryListModel.getPurposeList().size();i++)
                     {
                         purposetextList.add(ministryListModel.getPurposeList().get(i).getText());
                     }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, purposetextList);
                   // typed_purose_textview.setThreshold(1);
                    typed_purose_textview.setAdapter(adapter);
                    typed_purose_textview.showDropDown();



                }
                else
                {
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "No record found !",
                            Toast.LENGTH_LONG).show();

                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "No record found !",
                        Toast.LENGTH_LONG).show();
                mobile_number_edittext.setText("");

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("prefix", TextToBeSearch);
                params.put("ministryid", MinistryId);
                params.put("userId", "0");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    private void Hit_Delete_Purpose_API(String PurposeListId) {


        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_Delete_Purpose_URL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                DeletePurposeModel ministryListModel = gson.fromJson(response, DeletePurposeModel.class);

                MyToken.setToken(ministryListModel.getRefreshTokenValue());
                MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if(ministryListModel.getRefreshTokenValue()!=null&&ministryListModel.getResponseStatus().matches("Record Deleted Successfully"))
                {

                    Hit_GetAll_Purpose_API();

                }
                else
                {
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Failed to get data!",
                            Toast.LENGTH_LONG).show();
                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Some error occured !"+error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("PurposeListId", PurposeListId);
                params.put("UniqueUserDeviceId", UNIQUE_ID);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    private void Hit_GetAll_Purpose_API() {


        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_Get_List_Of_Purpose_URL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                AddFormModel addFormModel = gson.fromJson(response, AddFormModel.class);

                MyToken.setToken(addFormModel.getRefreshTokenValue());
                MyToken.setAuthorization(addFormModel.getAcsessTokenValue());

                if(addFormModel.getResponseStatus().matches("Success"))
                {

                    AddFormAdaptor choosePurposeAdapter=new AddFormAdaptor(getContext(),NonRegisteredUsersFragment.this,addFormModel.getFormDetailList());

                    added_item_list_recycler_view.setAdapter(choosePurposeAdapter);
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    });


                    if (addFormModel.getFormDetailList()!=null&&addFormModel.getFormDetailList().size()>0)
                    {
                        next_button.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        next_button.setVisibility(View.GONE);
                    }



                }
                else
                {
                    Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Failed to get data!",
                            Toast.LENGTH_LONG).show();
                }






            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(NonRegisteredUsersFragment.this.getActivity(), "Some error occured !"+error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());


                params.put("UniqueUserDeviceId", UNIQUE_ID);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {


        purposeFunctionalMinisty=event.purposeFunctionalMinisty;
         Hit_After_Purpose_Selected_API(event.purposeFunctionalMinisty);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDeletePurposeEvent(DeletePurposeEvent event) {

        Hit_Delete_Purpose_API(event.PurposeListId);

    }



    private void Hit_RegisterUser_API() {
        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.registerAPIUserURL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsersFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                RegisterUserModel registerUserModel = gson.fromJson(response, RegisterUserModel.class);

                if(registerUserModel.getErrorCodes().toString().matches("User Already Exists")||
                        registerUserModel.getErrorCodes().toString().matches("User Registered Successfully"))
                {
                    Hit_API_Token();
                }
                else
                {

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("MobileNo", mobile_number_edittext.getText().toString());
                params.put("Flag", "5");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    @Override
    public void onStart() {
        super.onStart();
           EventBus.getDefault().register(this);

            getActivity().setTitle("Non Register User");

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @OnClick(R.id.from_date_edittext)
    public void clickOnFromEditText()
    {
        new DatePickerDialog(getActivity(),fromDatePickerDialogListner,fromCalendar.get(Calendar.YEAR),fromCalendar.get(Calendar.MONTH),fromCalendar.get(Calendar.DAY_OF_MONTH)).show();

    }
    @OnClick(R.id.to_date_edittext)
    public void clickOnToEditText()
    {
        new DatePickerDialog(getActivity(),toDatePickerDialogListner,fromCalendar.get(Calendar.YEAR),fromCalendar.get(Calendar.MONTH),fromCalendar.get(Calendar.DAY_OF_MONTH)).show();

    }

    private String getFormatedDate(long inputDateValue)
    {

        Date date=new Date(inputDateValue);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
        return df2.format(date);
    }

    private String getFormatedYear(long inputDateValue)
    {

        Date date = new Date(inputDateValue);
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return String.valueOf(calendar.get(Calendar.YEAR));
    }

    private void updateFromEditText(){
        String myFormat="dd/MM/yyyy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.US);
        from_date_edittext.setText(dateFormat.format(fromCalendar.getTime()));
    }

    private void updateToEditText(){
        String myFormat="dd/MM/yyyy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.US);
        to_date_edittext.setText(dateFormat.format(toCalendar.getTime()));
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBackPressEvent(BackPressEvent event) {

        ResetData();

    }

    private void ResetData() {
        selected_ministry_textview.setText("");
        selected_paymenttype_textview.setText("");
        selected_functionhead_textview.setText("");
        selected_purpose_textview.setText("");
        spinner_ministry.setAdapter(null);
        spinner_pao.setAdapter(null);
        spinner_ddo.setAdapter(null);
       // mobile_number_edittext.setText("");
    }

}
