package testme.myapp.com.ntrp.module_nonregisteredusers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LstPurposeMinAndMaxAmountDetail {

    @SerializedName("NTRPPurposeId")
    @Expose
    private Integer nTRPPurposeId;
    @SerializedName("MinAmount")
    @Expose
    private Double minAmount;
    @SerializedName("MaxAmount")
    @Expose
    private Double maxAmount;
    @SerializedName("CurrencyCode")
    @Expose
    private Object currencyCode;

    public Integer getNTRPPurposeId() {
        return nTRPPurposeId;
    }

    public void setNTRPPurposeId(Integer nTRPPurposeId) {
        this.nTRPPurposeId = nTRPPurposeId;
    }

    public Double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Double minAmount) {
        this.minAmount = minAmount;
    }

    public Double getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Double maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Object getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(Object currencyCode) {
        this.currencyCode = currencyCode;
    }

}
