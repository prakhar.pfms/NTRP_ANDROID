package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class Salution {

    @SerializedName("SalutationId")
    @Expose
    private Integer salutationId;
    @SerializedName("SalutationName")
    @Expose
    private String salutationName;

    public Integer getSalutationId() {
        return salutationId;
    }

    public void setSalutationId(Integer salutationId) {
        this.salutationId = salutationId;
    }

    public String getSalutationName() {
        return salutationName;
    }

    public void setSalutationName(String salutationName) {
        this.salutationName = salutationName;
    }
}
