package testme.myapp.com.ntrp.module_contribution_to_ndrf.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_commonreceipts.fragments.CommonReceiptsFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.RegisterUserModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.events.DeletePurposeEvent;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_DepositorDetailsFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.AddFormModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.AutoCompleteTextModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.DeletePurposeModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.DepositorCategoryModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.MinistryListModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.NextPageModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.PurposeFunctionalMinisty;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.PurposeMinistryFunctionHeadListModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.SelectedPurposeDetailModel;
import testme.myapp.com.ntrp.module_quickpayment.adaptor.QuickPaymentAddFormAdaptor;
import testme.myapp.com.ntrp.module_quickpayment.fragments.QuickPaymentFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;
import testme.myapp.com.ntrp.utils.MessageEvent;

public class ContributionToNDRFFragment extends Fragment {
    private View localView;
    private TextView DepositorCategory_TextView, Purpose_TextView, DDO_TextView, Amount_TextView, PaymentFrequency_TextView;
    private Button CrossDialogButton, AddButton;
    private RecyclerView choosepurpose_recyclerview;
    private ImageView SearchPurposeButton;

    @BindView(R.id.spinner_depositorcategory)
    public Spinner spinner_depositorcategory;

    @BindView(R.id.added_item_list_recycler_view)
    public RecyclerView added_item_list_recycler_view;

    @BindView(R.id.scrollView)
    public ScrollView scrollView;

    @BindView(R.id.next_button)
    public Button next_button;


    @BindView(R.id.spinner_payment_type)
    public Spinner spinner_payment_type;


    @BindView(R.id.spinner_purose)
    public Spinner spinner_purose;

    @BindView(R.id.spinner_pao)
    public Spinner spinner_pao;

    @BindView(R.id.spinner_ddo)
    public Spinner spinner_ddo;

    @BindView(R.id.spinner_currency)
    public Spinner spinner_currency;

    @BindView(R.id.spinner_payment_frequency)
    public Spinner spinner_payment_frequency;

    @BindView(R.id.remarks_edittext)
    public EditText remarks_edittext;

    @BindView(R.id.amountvalue_edittext)
    public EditText amountvalue_edittext;

    @BindView(R.id.mobile_number_edittext)
    public EditText mobile_number_edittext;

    @BindView(R.id.selected_purpose_textview)
    public TextView selected_purpose_textview;

//    @BindView(R.id.selected_paymenttype_textview)
//    public TextView selected_paymenttype_textview;

    @BindView(R.id.selected_functionhead_textview)
    public TextView selected_functionhead_textview;

    @BindView(R.id.mobile_number_textview)
    public TextView mobile_number_textview;

    @BindView(R.id.paymenttype_textview_value)
    public TextView selected_paymenttype_textview;

    @BindView(R.id.remarks_textview_)
    public TextView remarks_textview;

    @BindView(R.id.spinner_ministry)
    public Spinner spinner_ministry;


    @BindView(R.id.selected_ministry_textview)
    public TextView selected_ministry_textview;

    private DepositorCategoryModel depositorCategorymodel;


    private PurposeMinistryFunctionHeadListModel purposeMinistryFunctionHeadListModel;

    private AutoCompleteTextView typed_purose_textview;

    private MinistryListModel ministryListModel;

    private String selectedDDOID;

    private String selectedMinistryName, SelectedPurpose = "";

    private Dialog ChoosepurposeDialog;


    private SelectedPurposeDetailModel selectedPurposeDetailModel;

    private PurposeFunctionalMinisty purposeFunctionalMinisty;

    private int DepositorCategoryId, selectedMinistryId;

    private final String UNIQUE_ID = String.valueOf(UUID.randomUUID());

    private LinearLayout page_number_layout;

    private String ReceiptEntryId;

    private int Ministry_dropdown_check = 0, purose_dropdown_check = 0, District_check = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contribution_to_ndrf, container, false);
        localView = view;
        ButterKnife.bind(this, view);
        getActivity().setTitle("Contribution to NDRF");
        CloseKeyBoard();
        Ministry_dropdown_check = 0;
        purose_dropdown_check = 0;
        District_check = 0;
        initializeView();

        return view;
    }

    private void CloseKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    private void initializeView() {

        next_button.setVisibility(View.GONE);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        added_item_list_recycler_view.setLayoutManager(mLayoutManager);
        added_item_list_recycler_view.setItemAnimator(new DefaultItemAnimator());


        amountvalue_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (amountvalue_edittext.getText().length() == 1 && amountvalue_edittext.getText().toString().trim().equals("0")) {
                    amountvalue_edittext.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        remarks_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int charleft=60-s.length();

                remarks_textview.setText(charleft+ "character left");

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        spinner_ddo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                selectedDDOID = selectedPurposeDetailModel.getDdlDDOLst().get(position).getValue();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_depositorcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                DepositorCategoryId = depositorCategorymodel.getDepositorCategoryList().get(position).getDepositorCategoryId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Hit_Next_Button_API();

            }
        });

        initialize_TextViews();
        // initialize_SearchPurposeButton();
        initialize_AddButton();

        mobile_number_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 10) {
                    Hit_RegisterUser_API();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        /*spinner_ministry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (++Ministry_dropdown_check > 1) {
                    selectedMinistryName = ministryListModel.getMinistryList().get(position).getMinistryName();
                    selectedMinistryId = ministryListModel.getMinistryList().get(position).getMinistryId();
                    Hit_Get_Purpose_List_API("1");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        /*spinner_purose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (++purose_dropdown_check > 1) {
                    EventBus.getDefault().post(new MessageEvent(
                            purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(position).getPurposeDescription(),
                            purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(position).getPaymentTypeDescription(),
                            purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(position).getFuncHeadDescription(),
                            purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(position).getControllerName(), purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(position)
                    ));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

       // Hit_Get_Ministry_List_API();
    }


    private void initialize_TextViews() {
        DepositorCategory_TextView = (TextView) localView.findViewById(R.id.depositorcategory_textview);
        Purpose_TextView = (TextView) localView.findViewById(R.id.purpose_textview);
        DDO_TextView = (TextView) localView.findViewById(R.id.ddo_textview);
        Amount_TextView = (TextView) localView.findViewById(R.id.amount_textview);
        PaymentFrequency_TextView = (TextView) localView.findViewById(R.id.paymentfrequency_textview);
        putRedAsterisk(localView, DepositorCategory_TextView, R.id.depositorcategory_textview);
        putRedAsterisk(localView, Purpose_TextView, R.id.purpose_textview);
        putRedAsterisk(localView, mobile_number_textview, R.id.mobile_number_textview);
        putRedAsterisk(localView, DDO_TextView, R.id.ddo_textview);
        putRedAsterisk(localView, Amount_TextView, R.id.amount_textview);
        putRedAsterisk(localView, PaymentFrequency_TextView, R.id.paymentfrequency_textview);
    }

    private void initialize_SearchPurposeButton() {
        SearchPurposeButton = (ImageView) localView.findViewById(R.id.searchpurpose_button);
        SearchPurposeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mobile_number_edittext.getText().toString().matches("")) {
                    mobile_number_edittext.setError("This field can not be blank");
                } else if (mobile_number_edittext.getText().toString().matches("") == false) {
                    showDialog_Choosepurpose();
                }

                return;
            }
        });
    }

    private void showDialog_Choosepurpose() {
        ChoosepurposeDialog = new Dialog(getActivity());
        ChoosepurposeDialog.setContentView(R.layout.dialog_nonregisteredusers_choosepurpose);
        ChoosepurposeDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ChoosepurposeDialog.show();

        typed_purose_textview = (AutoCompleteTextView) ChoosepurposeDialog.findViewById(R.id.typed_purose_textview);
        EditText page_number_textview = (EditText) ChoosepurposeDialog.findViewById(R.id.page_number_textview);
        Button search_button = (Button) ChoosepurposeDialog.findViewById(R.id.search_button);


        Button go_page_number_button = (Button) ChoosepurposeDialog.findViewById(R.id.go_page_number_button);
        page_number_layout = (LinearLayout) ChoosepurposeDialog.findViewById(R.id.page_number_layout);
        page_number_layout.setVisibility(View.GONE);




        /*search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Hit_Get_Purpose_List_API("1");



            }
        });*/

        go_page_number_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Hit_Get_Purpose_List_API(page_number_textview.getText().toString());


            }
        });


        choosepurpose_recyclerview = (RecyclerView) ChoosepurposeDialog.findViewById(R.id.choosepurpose_recyclerview);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        choosepurpose_recyclerview.setLayoutManager(mLayoutManager);
        choosepurpose_recyclerview.setItemAnimator(new DefaultItemAnimator());
        choosepurpose_recyclerview.setVisibility(View.GONE);


        CrossDialogButton = (Button) ChoosepurposeDialog.findViewById(R.id.cross_button);
        CrossDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChoosepurposeDialog.dismiss();
                mobile_number_edittext.setText("");
                typed_purose_textview.setAdapter(null);
                choosepurpose_recyclerview.setAdapter(null);
                SelectedPurpose="";
            }
        });


        typed_purose_textview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 3) {
                    Hit_Auto_Complete_API(typed_purose_textview.getText().toString(), String.valueOf(selectedMinistryId));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        typed_purose_textview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SelectedPurpose = typed_purose_textview.getText().toString();


            }
        });


    }

    private void initialize_AddButton() {
        AddButton = (Button) localView.findViewById(R.id.add_button);
        AddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (amountvalue_edittext.getText().toString().matches("")) {
                    amountvalue_edittext.setError("This field can not be blank");
                }
                if (mobile_number_edittext.getText().toString().matches("")) {
                    mobile_number_edittext.setError("This field can not be blank");
                }
                if (selected_purpose_textview.getText().toString().matches("")) {
                    selected_purpose_textview.setError("This field can not be blank");
                } else if (amountvalue_edittext.getText().toString().matches("") == false && mobile_number_edittext.getText().toString().matches("") == false && selected_purpose_textview.getText().toString().matches("") == false) {
                    Hit_Add_Form_API(purposeFunctionalMinisty, selectedPurposeDetailModel);
                }

                return;
            }
        });
    }

    private void changeFragment_DepositorDetails() {
        Bundle b = new Bundle();
        b.putString("ReceiptEntryId", ReceiptEntryId);
        b.putString("MobileNumber", mobile_number_edittext.getText().toString());
        b.putString("UniqueUserDeviceId", UNIQUE_ID);
        b.putInt("DepositorCategoryId",DepositorCategoryId);
        Fragment nonRegisteredUsers_depositorDetailsFragment = new NonRegisteredUsers_DepositorDetailsFragment();
        nonRegisteredUsers_depositorDetailsFragment.setArguments(b);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, nonRegisteredUsers_depositorDetailsFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void putRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = "*";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }

    private void Hit_Get_Depositor_Category_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Depositor_Category;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                DepositorCategoryModel depositorCategoryModel = gson.fromJson(response, DepositorCategoryModel.class);

                if (depositorCategoryModel.getResponseStatus().matches("Success")) {
                    depositorCategorymodel = depositorCategoryModel;

                    ArrayList<String> items = new ArrayList<String>();

                    for (int i = 0; i < depositorCategoryModel.getDepositorCategoryList().size(); i++) {
                        items.add(depositorCategoryModel.getDepositorCategoryList().get(i).getDepositorCategoryName());
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
                    spinner_depositorcategory.setAdapter(adapter);
                    spinner_depositorcategory.setSelection(0);

                    Hit_HomePage_API();


                } else {
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Failed to get Depositor Category List!",
                            Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // strReq.setRetryPolicy(new DefaultRetryPolicy(20000, 3, 1.0f));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_Get_Ministry_List_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Ministry_List;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                ministryListModel = gson.fromJson(response, MinistryListModel.class);
                //MyToken.setToken(ministryListModel.getRefreshTokenValue());
                //MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if (ministryListModel.getResponseStatus().matches("Success")) {
                    ArrayList<String> items = new ArrayList<String>();

                    for (int i = 0; i < ministryListModel.getMinistryList().size(); i++) {
                        items.add(ministryListModel.getMinistryList().get(i).getMinistryName());
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
                    spinner_ministry.setAdapter(adapter);
                    spinner_ministry.setSelection(0);
                } else {
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Failed to get Depositor Category List!",
                            Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_Get_Purpose_List_API(String pageIndex) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.GetPurpose_List;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                purposeMinistryFunctionHeadListModel = gson.fromJson(response, PurposeMinistryFunctionHeadListModel.class);

                MyToken.setToken(purposeMinistryFunctionHeadListModel.getRefreshTokenValue());
                MyToken.setAuthorization(purposeMinistryFunctionHeadListModel.getAcsessTokenValue());

                if (purposeMinistryFunctionHeadListModel.getErrorCodes().matches("Success")) {


                    /*QuickPaymentChoosePurposeAdapter choosePurposeAdapter=new QuickPaymentChoosePurposeAdapter(getContext(),QuickPaymentFragment.this,ministryListModel.getPurposeFunctionalMinistyList(),ChoosepurposeDialog);

                    choosepurpose_recyclerview.setAdapter(choosePurposeAdapter);
                    page_number_layout.setVisibility(View.VISIBLE);
                    choosepurpose_recyclerview.setVisibility(View.VISIBLE);*/


                    ArrayList<String> items = new ArrayList<String>();

                    for (int i = 0; i < purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().size(); i++) {
                        items.add(purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(i).getPurposeDescription());
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
                    spinner_purose.setAdapter(adapter);
                    spinner_purose.setSelection(0);


                } else {

                    mobile_number_edittext.setText("");
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Data not available!",
                            Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Some error occured !" + error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("ControllerName", selectedMinistryName);
                params.put("PurposeName", SelectedPurpose);
                params.put("FunctionHeadNo", "");
                params.put("pageIndex", pageIndex);
                params.put("userId", "0");
                params.put("flag", "0");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer " + MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private void Hit_API_Token() {
        HandleHttpsRequest.setSSL();
        System.out.println("Token API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                TokenModel tokenModel = gson.fromJson(response, TokenModel.class);
                MyToken.setToken(tokenModel.refresh_token);
                MyToken.setAuthorization(tokenModel.access_token);
                if (MyToken.getToken() != null) {

                    if (CheckNetworkConnection.getInstance(ContributionToNDRFFragment.this.getActivity()).isOnline()) {

                        Prefs.putString("LoggedInMobileNumber", mobile_number_edittext.getText().toString());
                        Hit_Get_Depositor_Category_API();

                    } else {
                        Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Some error occured!",
                                Toast.LENGTH_LONG).show();

                    }
                } else {
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Some error occured!",
                            Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= " + error.getMessage());
                pDialog.hide();
                pDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("UserName", mobile_number_edittext.getText().toString());
                params.put("AppType", "NTRP-APP");
                params.put("DeviceID", "123");
                params.put("AppVersion", "1.0");
                params.put("password", "123456");
                params.put("OS", "android");
                params.put("Operator", "airtel");
                params.put("grant_type", "password");
                params.put("Flag", "5");
                return params;
            }


        };

        // strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }

    private void Hit_After_Purpose_Selected_API(PurposeFunctionalMinisty purposeFunctionalMinisty) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Get_After_Selected_List;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                SelectedPurposeDetailModel ministryListModel = gson.fromJson(response, SelectedPurposeDetailModel.class);

                MyToken.setToken(ministryListModel.getRefreshTokenValue());
                MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if (ministryListModel.getRefreshTokenValue() != null) {

                    selectedPurposeDetailModel = ministryListModel;

                    ArrayList<String> items = new ArrayList<String>();

                    for (int i = 0; i < ministryListModel.getDdlPAOLst().size(); i++) {
                        items.add(ministryListModel.getDdlPAOLst().get(i).getText());
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
                    spinner_pao.setAdapter(adapter);
                    spinner_pao.setSelection(0);


                    ArrayList<String> items1 = new ArrayList<String>();

                    for (int i = 0; i < ministryListModel.getDdlDDOLst().size(); i++) {
                        items1.add(ministryListModel.getDdlDDOLst().get(i).getText());
                    }


                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items1);
                    spinner_ddo.setAdapter(adapter1);
                    spinner_ddo.setSelection(0);


//                    ArrayAdapter adapter1 = new ArrayAdapter(getContext(),
//                            R.layout.spinner_textview, items1);
//                    adapter1.setDropDownViewResource(R.layout.spinner_textview);
//                    spinner_ddo.setAdapter(adapter1);

                    ArrayList<String> items2 = new ArrayList<String>();

                    for (int i = 0; i < ministryListModel.getDdlCurrencylst().size(); i++) {
                        items2.add(ministryListModel.getDdlCurrencylst().get(i).getText());
                    }


                    ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items2);
                    spinner_currency.setAdapter(adapter2);
                    spinner_currency.setSelection(0);


                    ArrayList<String> items3 = new ArrayList<String>();

                    for (int i = 0; i < ministryListModel.getDdlPeriodLst().size(); i++) {
                        items3.add(ministryListModel.getDdlPeriodLst().get(i).getText());
                    }


                    ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items3);
                    spinner_payment_frequency.setAdapter(adapter3);
                    spinner_payment_frequency.setSelection(0);


                    selected_purpose_textview.setText(ministryListModel.getTxtBxPurpose());

                    selected_paymenttype_textview.setText(ministryListModel.getLblPaymenttypetext());

                    selected_functionhead_textview.setText(ministryListModel.getLblFunctionHeadInfo());

                    selected_ministry_textview.setText(ministryListModel.getLblMinistryInfo());


                } else {
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Failed to get PAO & DDO list!",
                            Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Some error occured !" + error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("DepsitorCategory", spinner_depositorcategory.getSelectedItem().toString());
                params.put("PurposeName", "");

                params.put("FunctionHeadId", String.valueOf(purposeFunctionalMinisty.getFunctionHead()));

                params.put("Paymenttypeid", String.valueOf(purposeFunctionalMinisty.getPaymentTypeid()));
                params.put("userId", "0");
                params.put("PaymentPurposeId", String.valueOf(purposeFunctionalMinisty.getNTRPPurposeId()));
                params.put("ValidationType", String.valueOf(purposeFunctionalMinisty.getValidationType()));
                params.put("PaymentModeId", String.valueOf(purposeFunctionalMinisty.getPaymentModeId()));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer " + MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private void Hit_Add_Form_API(PurposeFunctionalMinisty purposeFunctionalMinisty, SelectedPurposeDetailModel selectedPurposeDetailModel) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Get_App_Form_URL;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                AddFormModel addFormModel = gson.fromJson(response, AddFormModel.class);

                MyToken.setToken(addFormModel.getRefreshTokenValue());
                MyToken.setAuthorization(addFormModel.getAcsessTokenValue());

                if (addFormModel.getResponseStatus().matches("Success")) {

                    QuickPaymentAddFormAdaptor choosePurposeAdapter = new QuickPaymentAddFormAdaptor(getContext(), ContributionToNDRFFragment.this, addFormModel.getFormDetailList());

                    added_item_list_recycler_view.setAdapter(choosePurposeAdapter);
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    });
                    if (addFormModel.getFormDetailList() != null && addFormModel.getFormDetailList().size() > 0) {
                        next_button.setVisibility(View.VISIBLE);
                    } else {
                        next_button.setVisibility(View.GONE);
                    }
                    //Hit_GetAll_Purpose_API();
                } else {
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Failed to get PAO & DDO list!",
                            Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Some error occured !" + error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());

                params.put("ddlDepsitorCategorySelectedValue", String.valueOf(DepositorCategoryId));// String.valueOf(selectedPurposeDetailModel.getDdlDepsitorCategorySelectedValue()));

                params.put("hdnMinistryInfo", selectedPurposeDetailModel.getLblMinistryInfo());

                params.put("hdnMinistryId", String.valueOf(selectedPurposeDetailModel.getMinistryId()));

                params.put("HdnObjectHead", String.valueOf(purposeFunctionalMinisty.getObjectHead()));

                params.put("hdnServiceYear", String.valueOf(purposeFunctionalMinisty.getServiceYear()));

                params.put("ddlPAO", String.valueOf(selectedPurposeDetailModel.getDdlPAO()));

                params.put("hdnPaymenttypeid", String.valueOf(purposeFunctionalMinisty.getPaymentTypeid()));

                params.put("ddlPAOText", String.valueOf(spinner_pao.getSelectedItem().toString()));

                params.put("ddlDDO", selectedDDOID);

                params.put("ddlDDOSelectedText", String.valueOf(spinner_ddo.getSelectedItem().toString()));

                params.put("hdnAgencyId", String.valueOf(selectedPurposeDetailModel.getAgencyId()));

                params.put("lblAgencyInfo", String.valueOf(selectedPurposeDetailModel.getAgencyInfo()));

                params.put("txtAmount", String.valueOf(amountvalue_edittext.getText().toString()));

                params.put("hdPaymentPurposeId", String.valueOf(selectedPurposeDetailModel.getPaymentPurposeID()));

                params.put("ddlCurrencyText", String.valueOf(spinner_currency.getSelectedItem().toString()));

                params.put("hdnAccountId", String.valueOf(selectedPurposeDetailModel.getAccountId()));

                params.put("lblFunctionHeadInfo", String.valueOf(selectedPurposeDetailModel.getLblFunctionHeadInfo()));

                params.put("txtBxPurpose", String.valueOf(selectedPurposeDetailModel.getTxtBxPurpose()));

                params.put("hdnFunctionalHeadId", String.valueOf(selectedPurposeDetailModel.getFunctionalHeadId()));

                params.put("hdnPaymentModeId", String.valueOf(purposeFunctionalMinisty.getPaymentModeId()));

                params.put("hdnAccShortName", selectedPurposeDetailModel.getHdnAccShortName() == null ? "" : String.valueOf(selectedPurposeDetailModel.getHdnAccShortName()));

                // params.put("ddlYear", "");

                params.put("txtPurposeRemarks", remarks_edittext.getText().toString());

                params.put("hdnAccGroupId", String.valueOf(selectedPurposeDetailModel.getAccGroupId()));

                params.put("ddlPeriod", selectedPurposeDetailModel.getDdlPeriodLst() == null ? "" : String.valueOf(selectedPurposeDetailModel.getDdlPeriodLst().get(0).getValue())); //

                params.put("ddlPeriodText", String.valueOf(spinner_payment_frequency.getSelectedItem().toString()));

                params.put("ddlYearSelectedText", "");

                params.put("txtFromDate", "");

                params.put("txtToDate", "");

                params.put("ddlMothlySelectedText", "");

                params.put("ddlQUARTERLYSelectedText", "");

                params.put("ddlhalfyearlyText", "");

                params.put("lblPaymenttypetext", selectedPurposeDetailModel.getLblPaymenttypetext());

                params.put("UniqueUserDeviceId", UNIQUE_ID);


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer " + MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void Hit_Next_Button_API() {


        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Get_Next_URL;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                NextPageModel ministryListModel = gson.fromJson(response, NextPageModel.class);

                MyToken.setToken(ministryListModel.getRefreshTokenValue());
                MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if (ministryListModel.getRefreshTokenValue() != null) {

                    ReceiptEntryId = ministryListModel.getReceiptEntryId() != null ? ministryListModel.getReceiptEntryId().toString() : null;

                    if (ReceiptEntryId != null) {
                        changeFragment_DepositorDetails();
                    } else {
                        Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Some error occured while getting data",
                                Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Failed to get data!",
                            Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Some error occured !" + error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer " + MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    private void Hit_Auto_Complete_API(String TextToBeSearch, String MinistryId) {


        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_Auto_Complete_Text_URL;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                AutoCompleteTextModel ministryListModel = gson.fromJson(response, AutoCompleteTextModel.class);

                MyToken.setToken(ministryListModel.getRefreshTokenValue());
                MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if (ministryListModel.getRefreshTokenValue() != null && ministryListModel.getErrorCodes().matches("Success")&&ministryListModel.getPurposeList()!=null&&ministryListModel.getPurposeList().size()>0) {

                    ArrayList<String> purposetextList = new ArrayList<>();

                    for (int i = 0; ministryListModel.getPurposeList() != null && i < ministryListModel.getPurposeList().size(); i++) {
                        purposetextList.add(ministryListModel.getPurposeList().get(i).getText());
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, purposetextList);
                    // typed_purose_textview.setThreshold(1);
                    typed_purose_textview.setAdapter(adapter);
                    typed_purose_textview.showDropDown();


                } else {
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Data not available!",
                            Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Data not available!",
                        Toast.LENGTH_LONG).show();
                mobile_number_edittext.setText("");

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("prefix", TextToBeSearch);
                params.put("ministryid", MinistryId);
                params.put("userId", "0");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer " + MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    private void Hit_Delete_Purpose_API(String PurposeListId) {


        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_Delete_Purpose_URL;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                DeletePurposeModel ministryListModel = gson.fromJson(response, DeletePurposeModel.class);

                MyToken.setToken(ministryListModel.getRefreshTokenValue());
                MyToken.setAuthorization(ministryListModel.getAcsessTokenValue());

                if (ministryListModel.getRefreshTokenValue() != null && ministryListModel.getResponseStatus().matches("Record Deleted Successfully")) {

                    Hit_GetAll_Purpose_API();

                } else {
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Failed to get data!",
                            Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Some error occured !" + error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("PurposeListId", PurposeListId);
                params.put("UniqueUserDeviceId", UNIQUE_ID);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer " + MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    private void Hit_GetAll_Purpose_API() {


        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_Get_List_Of_Purpose_URL;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                AddFormModel addFormModel = gson.fromJson(response, AddFormModel.class);

                MyToken.setToken(addFormModel.getRefreshTokenValue());
                MyToken.setAuthorization(addFormModel.getAcsessTokenValue());

                if (addFormModel.getResponseStatus().matches("Success")) {

                    QuickPaymentAddFormAdaptor choosePurposeAdapter = new QuickPaymentAddFormAdaptor(getContext(), ContributionToNDRFFragment.this, addFormModel.getFormDetailList());

                    added_item_list_recycler_view.setAdapter(choosePurposeAdapter);
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    });


                    if (addFormModel.getFormDetailList() != null && addFormModel.getFormDetailList().size() > 0) {
                        next_button.setVisibility(View.VISIBLE);
                    } else {
                        next_button.setVisibility(View.GONE);
                    }


                } else {
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Failed to get data!",
                            Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Some error occured !" + error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());


                params.put("UniqueUserDeviceId", UNIQUE_ID);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer " + MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {


        purposeFunctionalMinisty = event.purposeFunctionalMinisty;
        Hit_After_Purpose_Selected_API(event.purposeFunctionalMinisty);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDeletePurposeEvent(DeletePurposeEvent event) {

        Hit_Delete_Purpose_API(event.PurposeListId);

    }


    private void Hit_RegisterUser_API() {
        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.registerAPIUserURL;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                RegisterUserModel registerUserModel = gson.fromJson(response, RegisterUserModel.class);

                if (registerUserModel.getErrorCodes().toString().matches("User Already Exists") ||
                        registerUserModel.getErrorCodes().toString().matches("User Registered Successfully")) {
                    Hit_API_Token();
                } else {

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("MobileNo", mobile_number_edittext.getText().toString());
                params.put("Flag", "5");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    private void Hit_HomePage_API() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.NDRF_HOME_URL;
        final ProgressDialog pDialog = new ProgressDialog(ContributionToNDRFFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                purposeMinistryFunctionHeadListModel = gson.fromJson(response, PurposeMinistryFunctionHeadListModel.class);

                MyToken.setToken(purposeMinistryFunctionHeadListModel.getRefreshTokenValue());
                MyToken.setAuthorization(purposeMinistryFunctionHeadListModel.getAcsessTokenValue());

                if (purposeMinistryFunctionHeadListModel.getResponseStatus().matches("Success")&&purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList()!=null&&purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().size()>0) {



                    ArrayList<String> items = new ArrayList<String>();

                    for (int i = 0; i < purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().size(); i++) {
                        items.add(purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(i).getPurposeDescription());
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
                    spinner_purose.setAdapter(adapter);
                    spinner_purose.setSelection(0);


                    EventBus.getDefault().post(new MessageEvent(
                            purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(0).getPurposeDescription(),
                            purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(0).getPaymentTypeDescription(),
                            purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(0).getFuncHeadDescription(),
                            purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(0).getControllerName(), purposeMinistryFunctionHeadListModel.getPurposeFunctionalMinistyList().get(0)
                    ));




                } else {

                    mobile_number_edittext.setText("");
                    Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Data not available!",
                            Toast.LENGTH_LONG).show();

                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(ContributionToNDRFFragment.this.getActivity(), "Some error occured !" + error.getMessage(),
                        Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer " + MyToken.getAuthorization());
                //   headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);

        getActivity().setTitle("Contribution to NDRF");

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}