package testme.myapp.com.ntrp.networking;

import android.annotation.SuppressLint;
import android.content.Context;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 * Created by deepika.s on 21-12-2017.
 */

public class HandleHttpsRequest {

    private static Context context;

    public HandleHttpsRequest(Context context) {
        this.context = context;
    }

/*    public static void setSSL() {
        try {

            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                    // Not implemented
                }

                @Override
                public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                    // Not implemented
                }
            }};

            try {
                // HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());

                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {

                        if (s.equalsIgnoreCase("training.pfms.nic.in")||s.equalsIgnoreCase("training.pfms.gov.in")||s.equalsIgnoreCase("pfms.gov.in")||s.equalsIgnoreCase("firebase.com"))
                        {
                            return true;
                        }
                        return false;
                    }
                });

//                SSLContext sc = SSLContext.getInstance("TLS");
//
//                sc.init(null, trustAllCerts, new SecureRandom());
//
//                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//
//                SSLContext context = SSLContext.getInstance("TLS");
//                context.init(null, new X509TrustManager[]{new testme.myapp.com.ntrp.networking.NullX509TrustManager()}, new SecureRandom());
//                HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

    @SuppressLint("TrulyRandom")
    public static void setSSL() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception ignored) {
        }

    }
}
