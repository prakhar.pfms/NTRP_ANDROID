package testme.myapp.com.ntrp.module_trackyourpayment.models;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by deepika.s on 25-04-2017.
 */

@SuppressLint("ParcelCreator")
public class TransctionGridModel implements Parcelable {
    public List<TransactionList> tracktransaction;
    public String AcsessTokenValue;
    public String RefreshTokenValue;
    public String ResponseStatus;
    public String ErrorCodes;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
