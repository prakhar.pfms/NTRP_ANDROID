package testme.myapp.com.ntrp.module_nonregisteredusers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankAccountValidationModel {

    @SerializedName("BankId")
    @Expose
    private Integer bankId;
    @SerializedName("BankAccount")
    @Expose
    private String bankAccount;
    @SerializedName("BankIfscCode")
    @Expose
    private String bankIfscCode;
    @SerializedName("IsValidNeft")
    @Expose
    private String isValidNeft;
    @SerializedName("Ismail")
    @Expose
    private Boolean ismail;
    @SerializedName("OffLineUserId")
    @Expose
    private Integer offLineUserId;
    @SerializedName("IsPasswordExpire")
    @Expose
    private Boolean isPasswordExpire;
    @SerializedName("Email")
    @Expose
    private Object email;
    @SerializedName("MobileNo")
    @Expose
    private Object mobileNo;
    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("UserRole")
    @Expose
    private Integer userRole;
    @SerializedName("UserType_CBEC")
    @Expose
    private Object userTypeCBEC;
    @SerializedName("ErrorCodes")
    @Expose
    private String errorCodes;
    @SerializedName("ResponseStatus")
    @Expose
    private String responseStatus;
    @SerializedName("XmlDocumentResult")
    @Expose
    private Object xmlDocumentResult;
    @SerializedName("AcsessTokenValue")
    @Expose
    private String acsessTokenValue;
    @SerializedName("RefreshTokenValue")
    @Expose
    private String refreshTokenValue;
    @SerializedName("ResponseEmail")
    @Expose
    private Object responseEmail;
    @SerializedName("ResponseMobileNo")
    @Expose
    private Object responseMobileNo;
    @SerializedName("ResponseUserType_CBEC")
    @Expose
    private Object responseUserTypeCBEC;
    @SerializedName("ResponseFirstName")
    @Expose
    private Object responseFirstName;
    @SerializedName("ResponseLastName")
    @Expose
    private Object responseLastName;
    @SerializedName("ResponseCountryCode")
    @Expose
    private Object responseCountryCode;
    @SerializedName("ResponseUserId")
    @Expose
    private Integer responseUserId;
    @SerializedName("PdfFileByte")
    @Expose
    private Object pdfFileByte;
    @SerializedName("XmlDreports")
    @Expose
    private Object xmlDreports;
    @SerializedName("ReceiptEntryId")
    @Expose
    private Integer receiptEntryId;
    @SerializedName("ShippingCharges")
    @Expose
    private Object shippingCharges;
    @SerializedName("PubUserType")
    @Expose
    private Integer pubUserType;
    @SerializedName("DisplayPopup")
    @Expose
    private Object displayPopup;
    @SerializedName("RowNo")
    @Expose
    private Object rowNo;
    @SerializedName("PaymentPurposeID")
    @Expose
    private Integer paymentPurposeID;
    @SerializedName("TotalDiscount")
    @Expose
    private Object totalDiscount;
    @SerializedName("PageCount")
    @Expose
    private Integer pageCount;
    @SerializedName("ErrorMessage")
    @Expose
    private Object errorMessage;

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankIfscCode() {
        return bankIfscCode;
    }

    public void setBankIfscCode(String bankIfscCode) {
        this.bankIfscCode = bankIfscCode;
    }

    public String getIsValidNeft() {
        return isValidNeft;
    }

    public void setIsValidNeft(String isValidNeft) {
        this.isValidNeft = isValidNeft;
    }

    public Boolean getIsmail() {
        return ismail;
    }

    public void setIsmail(Boolean ismail) {
        this.ismail = ismail;
    }

    public Integer getOffLineUserId() {
        return offLineUserId;
    }

    public void setOffLineUserId(Integer offLineUserId) {
        this.offLineUserId = offLineUserId;
    }

    public Boolean getIsPasswordExpire() {
        return isPasswordExpire;
    }

    public void setIsPasswordExpire(Boolean isPasswordExpire) {
        this.isPasswordExpire = isPasswordExpire;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Object getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(Object mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserRole() {
        return userRole;
    }

    public void setUserRole(Integer userRole) {
        this.userRole = userRole;
    }

    public Object getUserTypeCBEC() {
        return userTypeCBEC;
    }

    public void setUserTypeCBEC(Object userTypeCBEC) {
        this.userTypeCBEC = userTypeCBEC;
    }

    public String getErrorCodes() {
        return errorCodes;
    }

    public void setErrorCodes(String errorCodes) {
        this.errorCodes = errorCodes;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getXmlDocumentResult() {
        return xmlDocumentResult;
    }

    public void setXmlDocumentResult(Object xmlDocumentResult) {
        this.xmlDocumentResult = xmlDocumentResult;
    }

    public String getAcsessTokenValue() {
        return acsessTokenValue;
    }

    public void setAcsessTokenValue(String acsessTokenValue) {
        this.acsessTokenValue = acsessTokenValue;
    }

    public String getRefreshTokenValue() {
        return refreshTokenValue;
    }

    public void setRefreshTokenValue(String refreshTokenValue) {
        this.refreshTokenValue = refreshTokenValue;
    }

    public Object getResponseEmail() {
        return responseEmail;
    }

    public void setResponseEmail(Object responseEmail) {
        this.responseEmail = responseEmail;
    }

    public Object getResponseMobileNo() {
        return responseMobileNo;
    }

    public void setResponseMobileNo(Object responseMobileNo) {
        this.responseMobileNo = responseMobileNo;
    }

    public Object getResponseUserTypeCBEC() {
        return responseUserTypeCBEC;
    }

    public void setResponseUserTypeCBEC(Object responseUserTypeCBEC) {
        this.responseUserTypeCBEC = responseUserTypeCBEC;
    }

    public Object getResponseFirstName() {
        return responseFirstName;
    }

    public void setResponseFirstName(Object responseFirstName) {
        this.responseFirstName = responseFirstName;
    }

    public Object getResponseLastName() {
        return responseLastName;
    }

    public void setResponseLastName(Object responseLastName) {
        this.responseLastName = responseLastName;
    }

    public Object getResponseCountryCode() {
        return responseCountryCode;
    }

    public void setResponseCountryCode(Object responseCountryCode) {
        this.responseCountryCode = responseCountryCode;
    }

    public Integer getResponseUserId() {
        return responseUserId;
    }

    public void setResponseUserId(Integer responseUserId) {
        this.responseUserId = responseUserId;
    }

    public Object getPdfFileByte() {
        return pdfFileByte;
    }

    public void setPdfFileByte(Object pdfFileByte) {
        this.pdfFileByte = pdfFileByte;
    }

    public Object getXmlDreports() {
        return xmlDreports;
    }

    public void setXmlDreports(Object xmlDreports) {
        this.xmlDreports = xmlDreports;
    }

    public Integer getReceiptEntryId() {
        return receiptEntryId;
    }

    public void setReceiptEntryId(Integer receiptEntryId) {
        this.receiptEntryId = receiptEntryId;
    }

    public Object getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(Object shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public Integer getPubUserType() {
        return pubUserType;
    }

    public void setPubUserType(Integer pubUserType) {
        this.pubUserType = pubUserType;
    }

    public Object getDisplayPopup() {
        return displayPopup;
    }

    public void setDisplayPopup(Object displayPopup) {
        this.displayPopup = displayPopup;
    }

    public Object getRowNo() {
        return rowNo;
    }

    public void setRowNo(Object rowNo) {
        this.rowNo = rowNo;
    }

    public Integer getPaymentPurposeID() {
        return paymentPurposeID;
    }

    public void setPaymentPurposeID(Integer paymentPurposeID) {
        this.paymentPurposeID = paymentPurposeID;
    }

    public Object getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Object totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }
}
