package testme.myapp.com.ntrp.module_login.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_login.events.UserLoginEvent;
import testme.myapp.com.ntrp.module_login.models.LogedInUserDetailsModels;
import testme.myapp.com.ntrp.module_login.models.UserMenuModel;
import testme.myapp.com.ntrp.module_login.models.UserMenuResponse;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;




public class WelcomeFragment extends Fragment {

    @BindView(R.id.welcome_message)
    TextView welcome_text;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome_page, container, false);


        ButterKnife.bind(this,view);
        getActivity().setTitle("Home");
        Hit_User_Menu_API(MainActivity.UserName);
        Hit_User_Details_API(MainActivity.UserName);

        return view;
    }
    private void Hit_User_Menu_API(String UserName) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getUserMenuURL;
        final ProgressDialog pDialog = new ProgressDialog(WelcomeFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();



                UserMenuModel userMenuModel = gson.fromJson(response, UserMenuModel.class);



                MyToken.setToken(userMenuModel.getRefreshTokenValue());
                MyToken.setAuthorization(userMenuModel.getAcsessTokenValue());


                if (userMenuModel.getErrorCodes()!=null&&userMenuModel.getErrorCodes().matches("Success")&&userMenuModel.getResponseStatus()!=null&&userMenuModel.getAcsessTokenValue()!=null&&userMenuModel.getRefreshTokenValue()!=null)
                {

                    String currentUserMenuResponse=userMenuModel.getResponseStatus().replace("\\", "");

                    UserMenuResponse[] userMenuResponses=gson.fromJson(currentUserMenuResponse, UserMenuResponse[].class);

                    for (int i=0;i<userMenuResponses.length;i++)
                    {
                        UserMenuResponse userMenuResponse=userMenuResponses[i];

                        if (userMenuResponse.getText().toLowerCase().contains("dot"))
                        {
                            MainActivity.IsDOTMReceiptMenuVisible=true;
                            EventBus.getDefault().post(new UserLoginEvent(
                                    true

                            ));
                            break;
                        }



                    }

                }

                else
                {
                    //wrong_credntial_textview.setVisibility(View.VISIBLE);
                    //  wrong_credntial_textview.setText("Wrong Credentials !");
                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("UserName", UserName);


                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }
    private void Hit_User_Details_API(String UserName) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getUserDetailsURL;
        final ProgressDialog pDialog = new ProgressDialog(WelcomeFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                LogedInUserDetailsModels userInfoModel = gson.fromJson(response, LogedInUserDetailsModels.class);



                MyToken.setToken(userInfoModel.getRefreshTokenValue());
                MyToken.setAuthorization(userInfoModel.getAcsessTokenValue());


                if (userInfoModel.getEmail()!=null&&userInfoModel.getUserName()!=null&&userInfoModel.getAcsessTokenValue()!=null&&userInfoModel.getRefreshTokenValue()!=null)
                {
                    Prefs.putString("userLogedInData", new Gson().toJson(userInfoModel));

                    String name=userInfoModel.getFirstName() != null ? userInfoModel.getFirstName() : "User";


                    welcome_text.setText("Welcome "+name+" !");




                }

                else
                {
                    // wrong_credntial_textview.setVisibility(View.VISIBLE);
                    //  wrong_credntial_textview.setText("Wrong Credentials !");
                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("LoginUserName", UserName);


                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }
}
