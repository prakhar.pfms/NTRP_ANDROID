package testme.myapp.com.ntrp.module_nonregisteredusers.fragments;

import static testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_DepositorDetailsFragment.OfflineWithAccountValidation;
import static testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_DepositorDetailsFragment.OfflineWithLogin;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.ConfirmInfoFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.PaymentFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.GetConfirmInfoModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.SubmitConfirmInfoModel;
import testme.myapp.com.ntrp.module_login.models.LogedInUserDetailsModels;
import testme.myapp.com.ntrp.module_nonregisteredusers.adapters.AddFormAdaptor;
import testme.myapp.com.ntrp.module_nonregisteredusers.adapters.ConfirmInfoAdaptor;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.BankAccountValidationModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.NonRegisterUserGetConfirmInfoModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.OffLinePaymentModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.UserInfoModel;
import testme.myapp.com.ntrp.module_trackyourpayment.utils.EnglishNumberToWords;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

/**
 * Created by deepika.s on 02-06-2017.
 */

public class NonRegisteredUsers_ConfirmInfoFragment extends Fragment {

    private TextView name_textview_value,address1_textview_value,address2_textview_value,city_textview_value;
    private TextView district_textview_value,state_textview_value,country_textview_value,pin_zip_textview_value;
    private TextView email_textview_value,mobile_textview_value;
    private TextView serial_no_textview_value,agency_name_textview_value,purpose_textview_value;
    private TextView amount_textview_value,final_amount_textview_value,amount_in_text_textview,payment_mode;
    private Button confirm_button;
    private String ReceiptEntryId,UserNameText,BankAccount,BankIfscCode,PaymentPurposeID,BankId;
    private View localView;

    @BindView(R.id.added_item_list_recycler_view)
    public RecyclerView added_item_list_recycler_view;

    @BindView(R.id.outer_scroll_view)
    public ScrollView outer_scroll_view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nonregisteredusers_confirminfo, container, false);
        localView=view;
        ButterKnife.bind(this,view);
        CloseKeyBoard();
        initializeView();
        payment_mode.setText(OfflineWithLogin||
                OfflineWithAccountValidation ? "Payment Mode Offline" : "Payment Mode Online");
        return view;
    }
    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
    private void initializeView()
    {
        name_textview_value=localView.findViewById(R.id.name_textview_value);
        address1_textview_value=localView.findViewById(R.id.address1_textview_value);
        address2_textview_value=localView.findViewById(R.id.address2_textview_value);
        city_textview_value=localView.findViewById(R.id.city_textview_value);
        district_textview_value=localView.findViewById(R.id.district_textview_value);
        state_textview_value=localView.findViewById(R.id.state_textview_value);
        country_textview_value=localView.findViewById(R.id.country_textview_value);
        pin_zip_textview_value=localView.findViewById(R.id.pin_zip_textview_value);
        email_textview_value=localView.findViewById(R.id.email_textview_value);
        mobile_textview_value=localView.findViewById(R.id.mobile_textview_value);

        serial_no_textview_value=localView.findViewById(R.id.serial_no_textview_value);
        agency_name_textview_value=localView.findViewById(R.id.agency_name_textview_value);
        purpose_textview_value=localView.findViewById(R.id.purpose_textview_value);
        amount_textview_value=localView.findViewById(R.id.amount_textview_value);
        final_amount_textview_value=localView.findViewById(R.id.final_amount_textview_value);
        amount_in_text_textview=localView.findViewById(R.id.amount_in_text_textview);

        payment_mode=localView.findViewById(R.id.payment_mode);

        confirm_button=localView.findViewById(R.id.confirm_button);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        added_item_list_recycler_view.setLayoutManager(mLayoutManager);
        added_item_list_recycler_view.setItemAnimator(new DefaultItemAnimator());

        Bundle bundle = getArguments();
        if (bundle != null) {
            ReceiptEntryId = bundle.getString("ReceiptEntryId");
            UserNameText= bundle.getString("UserNameText")!=null ? bundle.getString("UserNameText") : null;

            if (OfflineWithAccountValidation)
            {
                BankAccount=  bundle.getString("BankAccount");
                BankIfscCode= bundle.getString("BankIfscCode");
                PaymentPurposeID= bundle.getString("PaymentPurposeID");
                BankId= bundle.getString("BankId");
            }




        }


        set_OnClick_ConfirmButton();
        HitGetInfoDetailsAPI();
    }
    private void HitGetInfoDetailsAPI() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDepositorDeatilsConfirmURL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_ConfirmInfoFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                NonRegisterUserGetConfirmInfoModel confirmInfoModel = gson.fromJson(response, NonRegisterUserGetConfirmInfoModel.class);


                if (confirmInfoModel.getReceiptEntryId()!=null&&confirmInfoModel.getAcsessTokenValue()!=null&&confirmInfoModel.getRefreshTokenValue()!=null)
                {
                    MyToken.setToken(confirmInfoModel.getRefreshTokenValue());
                    MyToken.setAuthorization(confirmInfoModel.getAcsessTokenValue());


                    name_textview_value.setText(""+confirmInfoModel.getDepositerDetailslst().get(0).getDepositorName());
                    address1_textview_value.setText(""+confirmInfoModel.getDepositerDetailslst().get(0).getAddress1());
                    address2_textview_value.setText(""+confirmInfoModel.getDepositerDetailslst().get(0).getAddress2());
                    city_textview_value.setText(""+confirmInfoModel.getDepositerDetailslst().get(0).getCity());
                    district_textview_value.setText(""+confirmInfoModel.getDepositerDetailslst().get(0).getDistrict());
                    state_textview_value.setText(""+confirmInfoModel.getDepositerDetailslst().get(0).getState());
                    country_textview_value.setText(""+confirmInfoModel.getDepositerDetailslst().get(0).getCountry());
                    pin_zip_textview_value.setText(""+confirmInfoModel.getDepositerDetailslst().get(0).getPincode());
                    email_textview_value.setText(""+confirmInfoModel.getDepositerDetailslst().get(0).getEmail());
                    mobile_textview_value.setText(""+confirmInfoModel.getDepositerDetailslst().get(0).getMobileNo());

                    serial_no_textview_value.setText("1");

                  //  amount_in_text_textview.setText(EnglishNumberToWords.convert(confirmInfoModel.getDepositerDetailslst().get(0).getAmountValue().longValue()));




                    ConfirmInfoAdaptor choosePurposeAdapter=new ConfirmInfoAdaptor(getContext(),NonRegisteredUsers_ConfirmInfoFragment.this,confirmInfoModel.getPurposeDetaillst());

                    added_item_list_recycler_view.setAdapter(choosePurposeAdapter);
                    outer_scroll_view.post(new Runnable() {
                        @Override
                        public void run() {
                            outer_scroll_view.fullScroll(View.FOCUS_DOWN);
                        }
                    });


                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryID", ReceiptEntryId);
                params.put("Type", "NonRegisterUser");
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }
    private void HitSubmitInfoDetailsAPI() {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getSubmitConfirmURL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_ConfirmInfoFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                SubmitConfirmInfoModel confirmInfoModel = gson.fromJson(response, SubmitConfirmInfoModel.class);


                if (confirmInfoModel.getResponseStatus()!=null&&confirmInfoModel.getResponseStatus().matches("Y")&& confirmInfoModel.getReceiptEntryId()!=null&&confirmInfoModel.getAcsessTokenValue()!=null&&confirmInfoModel.getRefreshTokenValue()!=null)
                {
                    MyToken.setToken(confirmInfoModel.getRefreshTokenValue());
                    MyToken.setAuthorization(confirmInfoModel.getAcsessTokenValue());


                    if (OfflineWithLogin)
                    {
                        if (MainActivity.IsLogin)
                        {

                            LogedInUserDetailsModels userInfoModel = gson.fromJson(Prefs.getString("userLogedInData"), LogedInUserDetailsModels.class);

                            Hit_Offline_Details_API(UserNameText,userInfoModel.getPaymentPurposeID().toString(),  userInfoModel.getBankId().toString(),userInfoModel.getAccountNumber(),userInfoModel.getIFSCCode(),name_textview_value.getText().toString());

                        }
                        else
                        {
                            UserInfoModel userInfoModel = gson.fromJson(Prefs.getString("userBankData"), UserInfoModel.class);

                            Hit_Offline_Details_API(UserNameText,userInfoModel.getPaymentPurposeID().toString(),  userInfoModel.getBankId().toString(),userInfoModel.getBankAccount(),userInfoModel.getBankIfscCode(),name_textview_value.getText().toString());


                        }



                    }
                    else if (OfflineWithAccountValidation)
                    {
                        BankAccountValidationModel userInfoModel = gson.fromJson(Prefs.getString("userBankValidationData"), BankAccountValidationModel.class);

                        Hit_Offline_Details_API(UserNameText,userInfoModel.getPaymentPurposeID().toString(),  userInfoModel.getBankId().toString(),userInfoModel.getBankAccount(),userInfoModel.getBankIfscCode(),name_textview_value.getText().toString());

                    }
                    else if (OfflineWithLogin==false&&OfflineWithAccountValidation==false)
                    {
                        changeFragment_PaymentFragment();
                    }


                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryID", ReceiptEntryId);

                params.put("RefreshTokenValue", MyToken.getToken());

                if (MainActivity.IsLogin)
                {
                    params.put("UserId",  MainActivity.UserID );
                    params.put("UserName", MainActivity.UserName);
                }
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }
    private void set_OnClick_ConfirmButton()
    {
        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               HitSubmitInfoDetailsAPI();

            }
        });
    }
    private void Hit_Offline_Details_API(String UserName, String paymentPurposeID, String BankId, String BankACC, String BankIFC, String DepositorName) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_OfflinePaymentMode_URL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_ConfirmInfoFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                OffLinePaymentModel depositorCategoryModel = gson.fromJson(response, OffLinePaymentModel.class);

                // MyToken.setToken(depositorCategoryModel.getRefreshTokenValue());
                // MyToken.setAuthorization(depositorCategoryModel.getAcsessTokenValue());


                if (depositorCategoryModel.getChallanNumber()!=null&&depositorCategoryModel.getAmount()!=null)
                {

                    changeFragment_OfflineConfirmInfoFragment(ReceiptEntryId,depositorCategoryModel.getChallanNumber(),depositorCategoryModel.getAmount());

                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("LoginUserName", MainActivity.IsLogin? MainActivity.UserName : "");


                params.put("paymentPurposeID", paymentPurposeID !=null ? paymentPurposeID :"0");

                params.put("LoggedInUserName",   MainActivity.IsLogin? MainActivity.UserName : "");

                params.put("ReceiptEntryId", ReceiptEntryId);

                params.put("BankId", BankId!=null ? BankId :"");

                params.put("BankACC", BankACC !=null ? BankACC :"" );

                params.put("BankIFC", BankIFC !=null ? BankIFC :"");

                params.put("Salutation", "Mr");

                params.put("DepositorName", DepositorName);

                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }


    private void changeFragment_PaymentFragment()
    {
        Fragment paymentFragment = new NonRegisteredUsers_PaymentFragment();
        Bundle bundle=new Bundle();
        bundle.putString("ReceiptEntryId",ReceiptEntryId);
        paymentFragment.setArguments(bundle);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, paymentFragment);
        ft.addToBackStack(null);
        ft.commit();
    }
    private void changeFragment_OfflineConfirmInfoFragment(String receiptEntryId, String challanNumber, String amount)
    {
        Bundle bundle=new Bundle();

        bundle.putString("amount", amount);
        bundle.putString("challanNumber",challanNumber);
        bundle.putString("ReceiptEntryId",receiptEntryId);



        Fragment confirmInfoFragment = new NonRegisteredUsers_OfflinePaymentFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();


        confirmInfoFragment.setArguments(bundle);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, confirmInfoFragment);
        ft.addToBackStack(null);
        ft.commit();
    }
}
