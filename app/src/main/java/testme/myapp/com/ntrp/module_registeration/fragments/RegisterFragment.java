package testme.myapp.com.ntrp.module_registeration.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.activity.MainScreen;
import testme.myapp.com.ntrp.extra.MyCustomToast;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.extra.RepeatSafeToast;
import testme.myapp.com.ntrp.extra.UserModel;
import testme.myapp.com.ntrp.fragments.NewHomeFragment;
import testme.myapp.com.ntrp.module_registeration.adapters.BankListCustomAdapter;
import testme.myapp.com.ntrp.module_registeration.adapters.ControllerListCustomAdapter;
import testme.myapp.com.ntrp.module_registeration.adapters.DepositorListCustomAdapter;
import testme.myapp.com.ntrp.module_registeration.models.BankArrayList;
import testme.myapp.com.ntrp.module_registeration.models.BankListModel;
import testme.myapp.com.ntrp.module_registeration.models.CheckUserAvaliabilityModel;
import testme.myapp.com.ntrp.module_registeration.models.ControllerArrayList;
import testme.myapp.com.ntrp.module_registeration.models.ControllerListModel;
import testme.myapp.com.ntrp.module_registeration.models.DepositorArrayList;
import testme.myapp.com.ntrp.module_registeration.models.DepositorListModel;
import testme.myapp.com.ntrp.module_registeration.models.InsertUserModel;
import testme.myapp.com.ntrp.module_registeration.models.SendOTPModel_Registration;
import testme.myapp.com.ntrp.module_registeration.models.VerifyModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.module_trackyourpayment.mycaptcha.TextCaptcha;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.utils.AllUrls;

/**
 * Created by deepika.s on 15-05-2017.
 */

public class RegisterFragment extends Fragment {

    private View localView;
    private TextView EmailId_TextView, SecurityCode_TextView, OTPCode_TextView;
    private TextView DepositorCategory_TextView, FirstName_TextView, UserName_TextView, PassportNo_TextView, ConfirmPassword_TextView;
    public TextView OTPExpires_TextView, CheckUsernameAwaliablity_TextView, SelectBankName_TextView, BankAccountNo_TextView;
    public TextView IFSCCode_TextView, SelectController_TextView;
    private TextView OrganizationName_TextView, CompanyIdentificationNo_TextView;
    private View MobileNo_LinearLayout, EmailId_LinearLayout, Captcha_LinearLayout, SecurityCode_LinearLayout, SendOTP_Layout;
    private View OTPExpires_LinearLayout, EnterOTPCode_LinearLayout,Verify_Layout,Resend_Layout, RegisterationEntry_LinearLayout;
    private View ResetAndSubmitButtons_Layout, DepositorCategory_LinearLayout;
    private View SelectController_LinearLayout, FirstName_LinearLayout, LastName_LinearLayout, UserName_LinearLayout;
    private View Password_LinearLayout, ConfirmPassword_LinearLayout;
    private View SelectBankName_LinearLayout, BankAccountNo_LinearLayout, IFSCCode_LinearLayout;
    private View OrganizationName_LinearLayout, CompanyIdentificationNo_LinearLayout, Tan_LinearLayout;
    private Button SendOTPButton, VerifyButton, ResendButton, CaptchaRetryButton, SubmitButton, ResetButton;
    private TextCaptcha textCaptcha;
    private ImageView captcha_ImageView;
    private CheckBox NEFT_Based_Transaction_Checkbox;
    private Spinner Spinner_CountryCode,Spinner_DepositorCategory,Spinner_SelectController,Spinner_SelectBankName;
    private EditText MobileNo_EditText, EmailId_EditText, SecurityCode_EditText, EnterOTPCode_EditText, UserName_EditText;
    private EditText Password_EditText, ConfirmPassword_EditText,Tan_EditText,CompanyIdentificationNo_EditText;
    private EditText OrganizationName_EditText,BankAccountNo_EditText,IFSCCode_EditText, FirstName_EditText, LastName_EditText;
    private CountDownTimer countDownTimer;
    private int hoursToGo,minutesToGo,secondsToGo,millisToGo ;
    private int countryCode,ControllerNameID,BankNameID,DepositorNameID;
    private long milliLeft;
    public boolean IsNeftChecked,IsCountDownStart;
    private String User_EmailId_Text,User_Captcha,User_CountryName,User_CountryCode,User_MobileNo, User_OTP_Text, User_Name_Text;
    private String User_Password_Text,User_ConfirmPassword_Text,User_TanNumber_Text,User_CompanyIdentificationNo_Text;
    private String User_Organization_Text,User_BankAccountNo_Text,User_IFSC_Text,User_FirstName_Text, User_LastName_Text;
    private String validationMessage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_page, container, false);
        localView=view;
        getActivity().setTitle("Registration");
        initializeView();
        show_View_First();
        hide_View_First();
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle("Registration");
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CloseKeyBoard();
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("On Paused");
        System.out.println("IsCountDownStart in on pause== "+IsCountDownStart);
        if(IsCountDownStart) {

            countDownTimer.cancel();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("On Resume");
        System.out.println("IsCountDownStart in on Resume== "+IsCountDownStart);
        if(IsCountDownStart) {

            setTimerOnResume(OTPExpires_TextView,milliLeft);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        System.out.println("Detached");
        System.out.println("IsCountDownStart in on Detached== "+IsCountDownStart);
        if(IsCountDownStart) {
            IsCountDownStart=false;
            countDownTimer.cancel();
        }
    }

    private void initializeView()
    {
        initializeData();
        initialize_TextViews();
        initialize_LayoutViews();
        initialize_EditTextViews();
        initialize_Spinner_DepositorCategory();
        initialize_Spinner_SelectController();
        initialize_Spinner_SelectBankName();
        initialize_NEFTBasedTransaction_Checkbox();
        initialize_Spinner_CountryCode();
        generate_Captcha();
        reload_Captcha_Button();
        set_ResetButton();
        set_SendOTPButton();
        set_SubmitButton();
        set_VerifyButton();
        set_ResendButton();
    }
    private void initializeData()
    {
        hoursToGo=0;
        minutesToGo = 10;
        secondsToGo = 0;
        millisToGo = secondsToGo*1000+minutesToGo*1000*60+hoursToGo*1000*60*60;
        milliLeft=0;
        countryCode=0;
        ControllerNameID=0;
        BankNameID=0;
        DepositorNameID=0;
        IsCountDownStart=false;
        IsNeftChecked=false;
        validationMessage="";
        User_Name_Text="null";
        User_CountryName="null";
        User_CountryCode="null";
        User_MobileNo="null";
        User_EmailId_Text ="null";
        User_Captcha="null";

    }
    private void initialize_TextViews()
    {
        EmailId_TextView = (TextView) localView.findViewById(R.id.email_id_textview);
        putRedAsterisk(localView, EmailId_TextView,R.id.email_id_textview);
        SecurityCode_TextView = (TextView) localView.findViewById(R.id.security_code_textview);
        putRedAsterisk(localView, SecurityCode_TextView,R.id.security_code_textview);
        OTPCode_TextView = (TextView) localView.findViewById(R.id.OTP_Code_textview);
        putRedAsterisk(localView, OTPCode_TextView,R.id.OTP_Code_textview);
        DepositorCategory_TextView = (TextView) localView.findViewById(R.id.depositor_category_textview);
        putRedAsterisk(localView, DepositorCategory_TextView,R.id.depositor_category_textview);
        FirstName_TextView = (TextView) localView.findViewById(R.id.first_name_textview);
        putRedAsterisk(localView, FirstName_TextView,R.id.first_name_textview);
        UserName_TextView = (TextView) localView.findViewById(R.id.user_name_textview);
        putRedAsterisk(localView, UserName_TextView,R.id.user_name_textview);
        PassportNo_TextView = (TextView) localView.findViewById(R.id.passport_no_textview);
        putRedAsterisk(localView, PassportNo_TextView,R.id.passport_no_textview);
        ConfirmPassword_TextView = (TextView) localView.findViewById(R.id.confirm_password_textview);
        putRedAsterisk(localView, ConfirmPassword_TextView,R.id.confirm_password_textview);
        SelectBankName_TextView = (TextView) localView.findViewById(R.id.select_bank_name_textview);
        putRedAsterisk(localView, SelectBankName_TextView,R.id.select_bank_name_textview);
        BankAccountNo_TextView = (TextView) localView.findViewById(R.id.bank_account_no_textview);
        putRedAsterisk(localView, BankAccountNo_TextView,R.id.bank_account_no_textview);
        IFSCCode_TextView = (TextView) localView.findViewById(R.id. ifsc_code_textview);
        putRedAsterisk(localView, IFSCCode_TextView,R.id. ifsc_code_textview);
        SelectController_TextView = (TextView) localView.findViewById(R.id.  select_controller_textview);
        putRedAsterisk(localView, SelectController_TextView,R.id.  select_controller_textview);
        OTPExpires_TextView = (TextView) localView.findViewById(R.id.otp_expiry_textview);
        CheckUsernameAwaliablity_TextView = (TextView) localView.findViewById(R.id.check_username_awaliablity_textview);
        setOnClick_CheckUsernameAwaliablity_TextView();
        OrganizationName_TextView = (TextView) localView.findViewById(R.id.organization_name_textview);
        CompanyIdentificationNo_TextView = (TextView) localView.findViewById(R.id.company_identification_no_textview);
    }
    private void initialize_LayoutViews()
    {
        MobileNo_LinearLayout =localView.findViewById(R.id.mobile_no_linear_layout);
        EmailId_LinearLayout =localView.findViewById(R.id.email_id_linear_layout);
        Captcha_LinearLayout =localView.findViewById(R.id.captcha_linear_layout);
        SecurityCode_LinearLayout =localView.findViewById(R.id.security_code_linear_layout);
        SendOTP_Layout =localView.findViewById(R.id.send_OTP_layout);
        OTPExpires_LinearLayout =localView.findViewById(R.id.otp_expires_linear_layout);
        EnterOTPCode_LinearLayout =localView.findViewById(R.id.enter_OTP_Code_linear_layout);
        Verify_Layout=localView.findViewById(R.id.verify_layout);
        Resend_Layout=localView.findViewById(R.id.resend_layout);
        RegisterationEntry_LinearLayout =localView.findViewById(R.id.registeration_entry_linear_layout);
        ResetAndSubmitButtons_Layout =localView.findViewById(R.id.reset_and_submit_buttons_layout);
        DepositorCategory_LinearLayout =localView.findViewById(R.id.depositor_category_linear_layout);
        SelectController_LinearLayout =localView.findViewById(R.id.select_controller_linear_layout);
        FirstName_LinearLayout =localView.findViewById(R.id.first_name_linear_layout);
        LastName_LinearLayout =localView.findViewById(R.id.last_name_linear_layout);
        UserName_LinearLayout =localView.findViewById(R.id.user_name_linear_layout);
        Password_LinearLayout =localView.findViewById(R.id.password_linear_layout);
        ConfirmPassword_LinearLayout =localView.findViewById(R.id.confirm_password_linear_layout);
        SelectBankName_LinearLayout =localView.findViewById(R.id.select_bank_name_linear_layout);
        BankAccountNo_LinearLayout =localView.findViewById(R.id.bank_account_no_linear_layout);
        IFSCCode_LinearLayout =localView.findViewById(R.id.ifsc_code_linear_layout);
        OrganizationName_LinearLayout =localView.findViewById(R.id.organization_name_linear_layout);
        CompanyIdentificationNo_LinearLayout =localView.findViewById(R.id.company_identification_no_linear_layout);
        Tan_LinearLayout =localView.findViewById(R.id.tan_linear_layout);
    }
    private void initialize_EditTextViews()
    {
        MobileNo_EditText = (EditText) localView.findViewById(R.id.mobile_no_edittext);
        MobileNo_EditText.setEnabled(false);
        MobileNo_EditText.setBackgroundResource(R.drawable.unfocusededittext);
        setTextChangeListener_Mobile_No_EditText();
        EmailId_EditText = (EditText) localView.findViewById(R.id.email_id_edittext);
        SecurityCode_EditText = (EditText) localView.findViewById(R.id.security_code_edittext);
        EnterOTPCode_EditText = (EditText) localView.findViewById(R.id.enter_OTP_Code_edittext);
        UserName_EditText = (EditText) localView.findViewById(R.id.user_name_edittext);
        Password_EditText = (EditText) localView.findViewById(R.id.password_edittext);
        ConfirmPassword_EditText = (EditText) localView.findViewById(R.id.confirm_password_edittext);
        Tan_EditText = (EditText) localView.findViewById(R.id.tan_edittext);
        CompanyIdentificationNo_EditText = (EditText) localView.findViewById(R.id.company_identification_no_edittext);
        OrganizationName_EditText = (EditText) localView.findViewById(R.id.organization_name_edittext);
        BankAccountNo_EditText = (EditText) localView.findViewById(R.id.bank_account_no_edittext);
        IFSCCode_EditText = (EditText) localView.findViewById(R.id.ifsc_code_edittext);
        FirstName_EditText = (EditText) localView.findViewById(R.id.first_name_edittext);
        LastName_EditText = (EditText) localView.findViewById(R.id.last_name_edittext);
    }


    private void set_ResetButton()
    {
        ResetButton = (Button) localView.findViewById(R.id.reset_button);
        ResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NEFT_Based_Transaction_Checkbox.setChecked(false);
                IsNeftChecked=false;
                Spinner_DepositorCategory.setSelection(0);
                Spinner_SelectController.setSelection(0);
                FirstName_EditText.setText("");
                LastName_EditText.setText("");
                UserName_EditText.setText("");
                Password_EditText.setText("");
                ConfirmPassword_EditText.setText("");
                OrganizationName_EditText.setText("");
                CompanyIdentificationNo_EditText.setText("");
                OrganizationName_TextView = (TextView) localView.findViewById(R.id.organization_name_textview);
                CompanyIdentificationNo_TextView = (TextView) localView.findViewById(R.id.company_identification_no_textview);
                OrganizationName_TextView.setText("Organization Name:");
                CompanyIdentificationNo_TextView.setText("Company Identification No.");
                Tan_EditText.setText("");
                Spinner_SelectBankName.setSelection(0);
                BankAccountNo_EditText.setText("");
                IFSCCode_EditText.setText("");

            }
        });
    }
    private void setOnClick_CheckUsernameAwaliablity_TextView()
    {
        CheckUsernameAwaliablity_TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User_Name_Text = UserName_EditText.getText().toString();
                if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {
                    CloseKeyBoard();
                    Hit_API_CheckUserAvalable();
                } else {
                    MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                    return;

                }

            }
        });
    }
    private void set_VerifyButton()
    {
        VerifyButton = (Button) localView.findViewById(R.id.verify_button);
        VerifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CloseKeyBoard();
                User_OTP_Text = EnterOTPCode_EditText.getText().toString();
                if(User_OTP_Text.matches(""))
                {
                    validationMessage="Please Enter OTP";
                    MyCustomToast.createErrorToast(getActivity(), validationMessage);
                    return;
                }
                else
                {
                    if(CheckNetworkConnection.getInstance(getActivity()).isOnline())
                    {
                        CloseKeyBoard();
                        IsCountDownStart=false;
                        countDownTimer.cancel();
                        System.out.println("cancelled timer....");
                       // Hit_API_Token();
                        Hit_API_VeriFyRefisterUserOTP();
                    }
                    else {
                        MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                        return;

                    }
                }


            }
        });
    }
    private void set_ResendButton()
    {
        ResendButton = (Button) localView.findViewById(R.id.resend_button);
        ResendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CheckNetworkConnection.getInstance(getActivity()).isOnline())
                {
                    CloseKeyBoard();
                    Hit_API_SENDOTP();
                }
                else {
                    MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                    return;
                }

            }
        });
    }
    private void initialize_Spinner_DepositorCategory()
    {
        Spinner_DepositorCategory= (Spinner) localView.findViewById(R.id.spinner_depositor_category);
        Spinner_DepositorCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                DepositorNameID=position;
                //System.out.println("your position= "+position);
                if(position==0)
                {
                    SelectController_LinearLayout.setVisibility(View.GONE);
                    OrganizationName_LinearLayout.setVisibility(View.GONE);
                    CompanyIdentificationNo_LinearLayout.setVisibility(View.GONE);
                    Tan_LinearLayout.setVisibility(View.GONE);
                }
                else
                {
                    if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {
                        CloseKeyBoard();
                        Hit_API_ControllerList();
                    } else {
                        MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                        return;

                    }

                }
                if(position==0)
                {
                    OrganizationName_TextView = (TextView) localView.findViewById(R.id.organization_name_textview);
                    CompanyIdentificationNo_TextView = (TextView) localView.findViewById(R.id.company_identification_no_textview);
                    OrganizationName_TextView.setText("Organization Name:");
                    CompanyIdentificationNo_TextView.setText("Company Identification No.");
                }
                if(position==1||position==5)
                {
                    OrganizationName_TextView = (TextView) localView.findViewById(R.id.organization_name_textview);
                    CompanyIdentificationNo_TextView = (TextView) localView.findViewById(R.id.company_identification_no_textview);
                    OrganizationName_TextView.setText("Organization Name:");
                    CompanyIdentificationNo_TextView.setText("Company Identification No.");


                }
                else
                {
                    //System.out.println(OrganizationName_TextView.getText());
                    //System.out.println(CompanyIdentificationNo_TextView.getText());
                    OrganizationName_TextView = (TextView) localView.findViewById(R.id.organization_name_textview);
                    CompanyIdentificationNo_TextView = (TextView) localView.findViewById(R.id.company_identification_no_textview);
                    if(OrganizationName_TextView.getText().toString().matches("Organization Name:")) {
                        putRedAsterisk(localView, OrganizationName_TextView, R.id.organization_name_textview);
                    }
                    if(CompanyIdentificationNo_TextView.getText().toString().matches("Company Identification No.")) {
                        putRedAsterisk(localView, CompanyIdentificationNo_TextView,R.id.  company_identification_no_textview);
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void initialize_Spinner_SelectController()
    {
        Spinner_SelectController= (Spinner) localView.findViewById(R.id.spinner_select_controller);
        Spinner_SelectController.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ControllerNameID=position;
//                if(position==0)
//                {
//
//                }
//                else
//                {
//
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void initialize_Spinner_SelectBankName()
    {
        Spinner_SelectBankName= (Spinner) localView.findViewById(R.id.spinner_select_bank_name);
        Spinner_SelectBankName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                BankNameID=position;
               /* if(position==0)
                {

                }
                else
                {

                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void set_SubmitButton()
    {
        SubmitButton = (Button) localView.findViewById(R.id.submit_button);
        SubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CloseKeyBoard();
                User_Name_Text = UserName_EditText.getText().toString();
                User_Password_Text=Password_EditText.getText().toString();
                User_ConfirmPassword_Text= ConfirmPassword_EditText.getText().toString();
                User_EmailId_Text= EmailId_EditText.getText().toString().trim();
                User_TanNumber_Text=Tan_EditText.getText().toString().trim();
                User_CompanyIdentificationNo_Text=CompanyIdentificationNo_EditText.getText().toString();
                User_Organization_Text=OrganizationName_EditText.getText().toString();
                User_BankAccountNo_Text=BankAccountNo_EditText.getText().toString();
                User_IFSC_Text=IFSCCode_EditText.getText().toString();
                User_FirstName_Text= FirstName_EditText.getText().toString();
                User_LastName_Text = LastName_EditText.getText().toString();

                if(IsNeftChecked==false)
                {
                    if(checkValidation_NonNeft()==false)
                    {
                        MyCustomToast.createErrorToast(getActivity(), validationMessage);

                    }
                    else if (User_Password_Text.matches("")==false&&CheckPasswordVaildation(User_Password_Text)==false)
                    {
                        showDialog_PasswordError("Password must contain minimum 6 characters and maximum 25 characters. Password should contain atleast one alphabet, one numeral and one special character like [@#$%^\\&*])\"\n");
                        return;

                    }
                    else if (User_ConfirmPassword_Text.matches("")==false&&CheckPasswordVaildation(User_ConfirmPassword_Text)==false)
                    {
                        showDialog_ConfirmPasswordError("Password must contain minimum 6 characters and maximum 25 characters. Password should contain atleast one alphabet, one numeral and one special character like [@#$%^\\&*])\"\n");
                        return;

                    }
                    else
                    {
                        if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {
                            CloseKeyBoard();
                            Hit_API_InsertUser();
                        } else {

                            MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                            return;

                        }

                    }
                }
                else
                {
                    if(checkValidation_Neft()==false)
                    {
                        MyCustomToast.createErrorToast(getActivity(), validationMessage);
                        return;
                    }
                    else if (User_Password_Text.matches("")==false&&CheckPasswordVaildation(User_Password_Text)==false)
                    {
                        showDialog_PasswordError("Password must contain minimum 6 characters and maximum 25 characters. Password should contain atleast one alphabet, one numeral and one special character like [@#$%^\\&*])\"\n");
                        return;

                    }
                    else if (User_ConfirmPassword_Text.matches("")==false&&CheckPasswordVaildation(User_ConfirmPassword_Text)==false)
                    {
                        showDialog_ConfirmPasswordError("Password must contain minimum 6 characters and maximum 25 characters. Password should contain atleast one alphabet, one numeral and one special character like [@#$%^\\&*])\"\n");
                        return;

                    }
                    else
                    {
                        if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {
                            CloseKeyBoard();
                            Hit_API_InsertUser();
                        } else {
                            MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                            return;

                        }
                    }
                }
            }
        });
    }
    private boolean CheckPasswordVaildation(String password)
    {
        if(password.length()<6)
        {
            return false;
        }
        else  if(password.length()>25)
        {
            return false;
        }
        else
        {
            if(!isValidPassword(password)){
                return false;

            }else{

                return true;
            }
        }
    }
    public  boolean isValidPassword(String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    private boolean checkValidation_NonNeft()
    {
        if(DepositorNameID==0)
        {
            validationMessage="Please select Depositor Cateogry";
            return false;
        }
        if (DepositorNameID!=0)
        {
            if(ControllerNameID==0) {
                validationMessage="Please select controller name";
                return false;
            }
        }
        if (User_FirstName_Text.matches(""))
        {
            validationMessage="Please enter first name";
            return false;
        }
        if (User_Name_Text.matches(""))
        {
            validationMessage="Please enter user name";
           return false;
        }
        if (User_Password_Text.matches(""))
        {
            validationMessage="Please enter password";
            return false;
        }

        if (User_ConfirmPassword_Text.matches(""))
        {
            validationMessage="Please confirm password";
            return false;
        }
        if(DepositorNameID==2||DepositorNameID==3||DepositorNameID==4||DepositorNameID==6)
        {
            if(User_Organization_Text.matches(""))
            {
                validationMessage="Please enter Organization name";
                return false;
            }
            if(User_CompanyIdentificationNo_Text.matches(""))
            {
                validationMessage="Please enter company identification no. ";
                return false;
            }
        }

        return true;
    }




    private boolean checkValidation_Neft()
    {
        if(DepositorNameID==0)
        {
            validationMessage="Please select Depositor Cateogry";
            return false;
        }
        if (DepositorNameID!=0)
        {
            if(ControllerNameID==0) {
                validationMessage="Please select controller name";
                return false;
            }
        }
        if (User_FirstName_Text.matches(""))
        {
            validationMessage="Please enter first name";
            return false;
        }
        if (User_Name_Text.matches(""))
        {
            validationMessage="Please enter user name";
            return false;
        }
        if (User_Password_Text.matches(""))
        {
            validationMessage="Please enter password ";
            return false;
        }
        if (User_ConfirmPassword_Text.matches(""))
        {
            validationMessage="Please re-enter password";
            return false;
        }
        if(DepositorNameID==2||DepositorNameID==3||DepositorNameID==4||DepositorNameID==6)
        {
            if(User_Organization_Text.matches(""))
            {
                validationMessage="Please enter Organization name";
                return false;
            }
            if(User_CompanyIdentificationNo_Text.matches(""))
            {
                validationMessage="Please enter company identification no. ";
                return false;
            }
        }
        if (BankNameID==0)
        {
            validationMessage="Please select bank name";
            return false;
        }
        if (User_BankAccountNo_Text.matches(""))
        {
            validationMessage="Please enter bank account number";
            return false;
        }
        if (User_IFSC_Text.matches(""))
        {
            validationMessage="Please enter IFSC code";
            return false;
        }

        return true;
    }

    private void set_SendOTPButton()
    {

        SendOTPButton = (Button) localView.findViewById(R.id.send_otp_button);
        SendOTPButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CloseKeyBoard();
                User_MobileNo= MobileNo_EditText.getText().toString();
                if(User_MobileNo.matches(""))
                {
                    User_MobileNo="null";
                }
                User_EmailId_Text = EmailId_EditText.getText().toString();
                User_Captcha= SecurityCode_EditText.getText().toString();

                if (User_EmailId_Text.matches("")) {
                    MyCustomToast.createErrorToast(getActivity(), "Email is required");
                    return;
                }
                else if (!isValidEmail(User_EmailId_Text)) {
                    MyCustomToast.createErrorToast(getActivity(), "Please Enter Valid Email Id");
                    return;
                }
                else if (User_Captcha.matches("")) {
                    MyCustomToast.createErrorToast(getActivity(), "Please Enter Security Code");
                    return;
                }
                else if (!User_Captcha.matches(textCaptcha.input_captcha_string)) {
                    SecurityCode_EditText.setText("");
                    generate_Captcha();
                    showDialog_Captcha_Error("Security Code does not match, Try again!");
                    return;
                }
                else if (User_Captcha.matches(textCaptcha.input_captcha_string)) {

                    if (CheckNetworkConnection.getInstance(getActivity()).isOnline())
                    {
                        CloseKeyBoard();
                        Hit_API_SENDOTP();

                    }
                    else {
                        MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                        return;

                    }
                }

            }
        });
    }
    private void initialize_Spinner_CountryCode() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, getResources().getStringArray(R.array.country_list_with_code));
        final String[] tempArray=getResources().getStringArray(R.array.country_list_with_code);

        Spinner_CountryCode = (Spinner) localView.findViewById(R.id.spinner_country_code);
        Spinner_CountryCode.setAdapter(adapter);
        Spinner_CountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryCode=position;
                //System.out.println("your position= "+position);
                if(countryCode!=0)
                {
                    MobileNo_EditText.setBackgroundResource(R.drawable.back);
                    MobileNo_EditText.setEnabled(true);

                    //System.out.println("Country name= "+tempArray[countryCode]);
                    int beginIndex=0;
                    int endIndex=tempArray[countryCode].indexOf("+")-1;
                    User_CountryName=tempArray[countryCode].substring(beginIndex,endIndex);

                    //System.out.println("User_CountryName= "+User_CountryName);


                    beginIndex=tempArray[countryCode].indexOf("+")+1;
                    endIndex=tempArray[countryCode].lastIndexOf(")");
                    User_CountryCode=tempArray[countryCode].substring(beginIndex,endIndex);

                    //System.out.println("User_CountryCode= "+User_CountryCode);
                }
                else if(countryCode==0)
                {
                    MobileNo_EditText.setText("");
                    MobileNo_EditText.setBackgroundResource(R.drawable.unfocusededittext);
                    MobileNo_EditText.setEnabled(false);
                    User_CountryName="null";
                    User_CountryCode="null";
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //System.out.println("nothing selected");

            }
        });

    }
    private void putRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = " *";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }
    private void show_View_First()
    {
        MobileNo_LinearLayout.setVisibility(View.VISIBLE);
        MobileNo_LinearLayout.setEnabled(true);
        EmailId_LinearLayout.setVisibility(View.VISIBLE);
        EmailId_LinearLayout.setEnabled(true);
        Captcha_LinearLayout.setVisibility(View.VISIBLE);
        Captcha_LinearLayout.setEnabled(true);
        SecurityCode_LinearLayout.setVisibility(View.VISIBLE);
        SecurityCode_LinearLayout.setEnabled(true);
        SendOTP_Layout.setVisibility(View.VISIBLE);
        SendOTP_Layout.setEnabled(true);
    }
    private void hide_View_First()
    {
        OTPExpires_LinearLayout.setVisibility(View.GONE);
        EnterOTPCode_LinearLayout.setVisibility(View.GONE);
        Verify_Layout.setVisibility(View.GONE);
        Resend_Layout.setVisibility(View.GONE);
        RegisterationEntry_LinearLayout.setVisibility(View.GONE);
        ResetAndSubmitButtons_Layout.setVisibility(View.GONE);
    }
    private void show_View_SendOTPOnClick()
    {
        MobileNo_LinearLayout.setVisibility(View.VISIBLE);
        MobileNo_LinearLayout.setEnabled(true);
        EmailId_LinearLayout.setVisibility(View.VISIBLE);
        EmailId_LinearLayout.setEnabled(true);
        OTPExpires_LinearLayout.setVisibility(View.VISIBLE);
        OTPExpires_LinearLayout.setEnabled(true);
        EnterOTPCode_LinearLayout.setVisibility(View.VISIBLE);
        EnterOTPCode_LinearLayout.setEnabled(true);
        Verify_Layout.setVisibility(View.VISIBLE);
        Verify_Layout.setEnabled(true);
    }
    private void hide_View_SendOTPOnClick()
    {
        Captcha_LinearLayout.setVisibility(View.GONE);
        SecurityCode_LinearLayout.setVisibility(View.GONE);
        SendOTP_Layout.setVisibility(View.GONE);
        Resend_Layout.setVisibility(View.GONE);
        RegisterationEntry_LinearLayout.setVisibility(View.GONE);
        ResetAndSubmitButtons_Layout.setVisibility(View.GONE);
    }
    private void show_View_VerifyBtnOnClick()
    {
        MobileNo_LinearLayout.setVisibility(View.VISIBLE);
        MobileNo_LinearLayout.setEnabled(true);
        EmailId_LinearLayout.setVisibility(View.VISIBLE);
        EmailId_LinearLayout.setEnabled(true);
        MobileNo_LinearLayout.setVisibility(View.VISIBLE);
        MobileNo_LinearLayout.setEnabled(true);
        RegisterationEntry_LinearLayout.setVisibility(View.VISIBLE);
        RegisterationEntry_LinearLayout.setEnabled(true);
        ResetAndSubmitButtons_Layout.setVisibility(View.VISIBLE);
        ResetAndSubmitButtons_Layout.setEnabled(true);
    }
    private void hide_View_VerifyBtnOnClick()
    {
        Captcha_LinearLayout.setVisibility(View.GONE);
        SecurityCode_LinearLayout.setVisibility(View.GONE);
        SendOTP_Layout.setVisibility(View.GONE);
        OTPExpires_LinearLayout.setVisibility(View.GONE);
        EnterOTPCode_LinearLayout.setVisibility(View.GONE);
        Verify_Layout.setVisibility(View.GONE);
    }
    private void show_View_OnTimerExpired()
    {
        MobileNo_LinearLayout.setVisibility(View.VISIBLE);
        MobileNo_LinearLayout.setEnabled(true);
        EmailId_LinearLayout.setVisibility(View.VISIBLE);
        EmailId_LinearLayout.setEnabled(true);
        Resend_Layout.setVisibility(View.VISIBLE);
        Resend_Layout.setEnabled(true);
    }
    private void hide_View_OnTimerExpired()
    {
        OTPExpires_LinearLayout.setVisibility(View.GONE);
        SecurityCode_LinearLayout.setVisibility(View.GONE);
        EnterOTPCode_LinearLayout.setVisibility(View.GONE);
        SendOTP_Layout.setVisibility(View.GONE);
        Verify_Layout.setVisibility(View.GONE);
        RegisterationEntry_LinearLayout.setVisibility(View.GONE);
        ResetAndSubmitButtons_Layout.setVisibility(View.GONE);
    }
    private void show_UnChecked()
    {
        DepositorCategory_LinearLayout.setVisibility(View.VISIBLE);
        DepositorCategory_LinearLayout.setEnabled(true);
        FirstName_LinearLayout.setVisibility(View.VISIBLE);
        FirstName_LinearLayout.setEnabled(true);
        LastName_LinearLayout.setVisibility(View.VISIBLE);
        LastName_LinearLayout.setEnabled(true);
        UserName_LinearLayout.setVisibility(View.VISIBLE);
        UserName_LinearLayout.setEnabled(true);
        Password_LinearLayout.setVisibility(View.VISIBLE);
        Password_LinearLayout.setEnabled(true);
        ConfirmPassword_LinearLayout.setVisibility(View.VISIBLE);
        ConfirmPassword_LinearLayout.setEnabled(true);
    }

    private void hide_UnChecked()
    {
        SelectController_LinearLayout.setVisibility(View.GONE);
        SelectBankName_LinearLayout.setVisibility(View.GONE);
        BankAccountNo_LinearLayout.setVisibility(View.GONE);
        IFSCCode_LinearLayout.setVisibility(View.GONE);
        OrganizationName_LinearLayout.setVisibility(View.GONE);
        CompanyIdentificationNo_LinearLayout.setVisibility(View.GONE);
        Tan_LinearLayout.setVisibility(View.GONE);
    }
    private void show_Checked()
    {
        DepositorCategory_LinearLayout.setVisibility(View.VISIBLE);
        DepositorCategory_LinearLayout.setEnabled(true);
        FirstName_LinearLayout.setVisibility(View.VISIBLE);
        FirstName_LinearLayout.setEnabled(true);
        LastName_LinearLayout.setVisibility(View.VISIBLE);
        LastName_LinearLayout.setEnabled(true);
        UserName_LinearLayout.setVisibility(View.VISIBLE);
        UserName_LinearLayout.setEnabled(true);
        Password_LinearLayout.setVisibility(View.VISIBLE);
        Password_LinearLayout.setEnabled(true);
        ConfirmPassword_LinearLayout.setVisibility(View.VISIBLE);
        ConfirmPassword_LinearLayout.setEnabled(true);
        SelectBankName_LinearLayout.setVisibility(View.VISIBLE);
        SelectBankName_LinearLayout.setEnabled(true);
        BankAccountNo_LinearLayout.setVisibility(View.VISIBLE);
        BankAccountNo_LinearLayout.setEnabled(true);
        IFSCCode_LinearLayout.setVisibility(View.VISIBLE);
        IFSCCode_LinearLayout.setEnabled(true);
    }
    private void hide_Checked()
    {

    }
    private void generate_Captcha() {
        textCaptcha = new TextCaptcha(1300, 600, 6, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
        captcha_ImageView = (ImageView) localView.findViewById(R.id.captcha_imageview);
        captcha_ImageView.setImageBitmap(textCaptcha.getImage());
    }

    private void reload_Captcha_Button()
    {
        CaptchaRetryButton = (Button) localView.findViewById(R.id.captcha_retry_button);
        CaptchaRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                generate_Captcha();
                return;
            }
        });
    }
    private void initialize_NEFTBasedTransaction_Checkbox()
    {
        NEFT_Based_Transaction_Checkbox = (CheckBox) localView.findViewById(R.id.is_neft_based_transaction_checkbox);
        NEFT_Based_Transaction_Checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                IsNeftChecked=isChecked;
                if(isChecked)
                {
                    if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {
                        CloseKeyBoard();
                        Hit_API_BankList();
                    } else {
                        MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                        return;
                    }
                }
                else
                {

                    show_UnChecked();
                    hide_UnChecked();
                }
            }
        });
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    private void showDialog_Captcha_Error(String errorText) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.error_textview);
        error_textview.setText(errorText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
    private void setTextChangeListener_Mobile_No_EditText()
    {
        MobileNo_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String enteredString = s.toString();
                if (enteredString.startsWith("0") || enteredString.startsWith("1") || enteredString.startsWith("2") ||
                        enteredString.startsWith("3") || enteredString.startsWith("4") || enteredString.startsWith("5") ||
                        enteredString.startsWith("6")) {

                    MobileNo_EditText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                            String enteredString = s.toString();
                            if (enteredString.startsWith("0") || enteredString.startsWith("1") || enteredString.startsWith("2") ||
                                    enteredString.startsWith("3") || enteredString.startsWith("4") || enteredString.startsWith("5") ||
                                    enteredString.startsWith("6")) {

                                RepeatSafeToast.show(getActivity(),"Mobile No. should start with 7,8 or 9");


                                    if (enteredString.length() > 0) {
                                        MobileNo_EditText.setText(enteredString.substring(1));
                                    } else {
                                        MobileNo_EditText.setText("");
                                    }

                                return;
                            }
                        }
                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                        if (enteredString.length() > 0) {
                            MobileNo_EditText.setText(enteredString.substring(1));
                        } else {
                            MobileNo_EditText.setText("");
                        }


                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    public void Hit_API_SENDOTP() {

        //System.out.println("Send Otp API Registration..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.sendOTPRegisterUserURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                SendOTPModel_Registration sendOTPModelRegistration =gson.fromJson(response,SendOTPModel_Registration.class);




                if(sendOTPModelRegistration !=null)
                {
                    MyToken.setToken(sendOTPModelRegistration.getRefreshTokenValue());
                    MyToken.setAuthorization(sendOTPModelRegistration.getAcsessTokenValue());

                    if(sendOTPModelRegistration.getResponseStatus()!=null&&sendOTPModelRegistration.getResponseStatus().matches("Success"))
                    {

                        Spinner_CountryCode.setEnabled(false);
                        MobileNo_EditText.setEnabled(false);
                        MobileNo_EditText.setBackgroundResource(R.drawable.unfocusededittext);
                        EmailId_EditText.setEnabled(false);
                        EmailId_EditText.setBackgroundResource(R.drawable.unfocusededittext);
                        show_View_SendOTPOnClick();
                        hide_View_SendOTPOnClick();

                        setTimer(OTPExpires_TextView,millisToGo);
                        IsCountDownStart=true;

                    }
                   else if(sendOTPModelRegistration.getErrorCodes()!=null&&sendOTPModelRegistration.getErrorCodes().matches("EmailId already exist"))
                    {
                        showDialog_ShowError("Email Id already exists.");
                        return;
                    }
                    else  if(sendOTPModelRegistration.getResponseStatus()!=null&&sendOTPModelRegistration.getResponseStatus().matches("OTP failed"))
                    {
                        showDialog_ShowError("OTP failed! Try again later.");
                        return;
                    }
                    else
                    {
                        showDialog_ShowError("OTP failed! Try again later.");
                        return;
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                //System.out.println(User_MobileNo+" "+ User_EmailId_Text +" "+User_CountryCode);
                params.put("MobileNumber",User_MobileNo);
                params.put("EmailID", User_EmailId_Text);
                params.put("countryCode", User_CountryCode);
                params.put("Flag", "Registration");

                Gson gson = new GsonBuilder().setLenient().create();
                UserModel userModel = gson.fromJson(Prefs.getString("userData"), UserModel.class);

                params.put("RefreshTokenValue", userModel.getRefreshToken());
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                Gson gson = new GsonBuilder().setLenient().create();
                UserModel userModel = gson.fromJson(Prefs.getString("userData"), UserModel.class);

                headers.put("Authorization", "Bearer "+userModel.getAccessToken());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };
     //   strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    public void showDialog_ShowError(String errorText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailId_EditText.setText("");
                SecurityCode_EditText.setText("");
                MobileNo_EditText.setText("");
                Spinner_CountryCode.setSelection(0);
                generate_Captcha();
                dialog.dismiss();
                return;

            }
        });
    }
    public void setTimer(final TextView textView,long millisToGo)
    {

        System.out.println("set timer.............");
        countDownTimer=new CountDownTimer(millisToGo,1000) {

            @Override
            public void onTick(long millis) {
                milliLeft=millis;
                int seconds = (int) (millis / 1000) % 60 ;
                int minutes = (int) ((millis / (1000*60)) % 60);
                int hours   = (int) ((millis / (1000*60*60)) % 24);
                String text = String.format("%02d hours, %02d minutes, %02d seconds",hours,minutes,seconds);
                String temp="OTP expires in: "+minutes+" minutes ,"+seconds+" seconds";
                textView.setText(temp);
                System.out.println(text);
                if(minutes==00&&seconds==01)
                {
                    show_View_OnTimerExpired();
                    hide_View_OnTimerExpired();
                }
            }
            @Override
            public void onFinish() {

            }
        }.start();
    }
    public void setTimerOnResume(final TextView textView,long milliLeftTemp)
    {
        System.out.println("setTimerOnResume");
        countDownTimer=new CountDownTimer(milliLeftTemp,1000) {

            @Override
            public void onTick(long millis) {
                milliLeft=millis;
                int seconds = (int) (millis / 1000) % 60 ;
                int minutes = (int) ((millis / (1000*60)) % 60);
                int hours   = (int) ((millis / (1000*60*60)) % 24);
                String text = String.format("%02d hours, %02d minutes, %02d seconds",hours,minutes,seconds);
                String temp="OTP expires in: "+minutes+" minutes ,"+seconds+" seconds";
                textView.setText(temp);
                System.out.println("resume= "+text);
                if(minutes==00&&seconds==01)
                {
                    show_View_OnTimerExpired();
                    hide_View_OnTimerExpired();
                }
            }
            @Override
            public void onFinish() {

            }
        }.start();
    }
    public void showDialog_ServerError(String errorText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }
    public void showDialog_PasswordError(String errorText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Password_EditText.setText("");
                User_Password_Text="";
                ConfirmPassword_EditText.setText("");
                User_ConfirmPassword_Text="";
                dialog.dismiss();
            }
        });
    }
    public void showDialog_ConfirmPasswordError(String errorText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmPassword_EditText.setText("");
                User_ConfirmPassword_Text="";
                dialog.dismiss();
            }
        });
    }
    public void Hit_API_Token() {

        //System.out.println("Token API for registration..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                TokenModel tokenModel=gson.fromJson(response,TokenModel.class);
                MyToken.setToken(tokenModel.refresh_token);
                MyToken.setAuthorization(tokenModel.access_token);
                if(MyToken.getToken()!=null)
                {
                    if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {
                            CloseKeyBoard();
                            Hit_API_VeriFyRefisterUserOTP();
                    } else {
                            MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                            return;
                        }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserName", User_EmailId_Text);
                params.put("AppType", "NTRP-APP");
                params.put("DeviceID", "123");
                params.put("AppVersion", "1.0");
                params.put("password", User_OTP_Text);
                params.put("OS", "android");
                params.put("Operator", "airtel");
                params.put("grant_type", "password");
                return params;
            }


        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public  void Hit_API_VeriFyRefisterUserOTP() {

        //System.out.println("VeriFyRefisterUserOTP  API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.VeriFyRefisterUserOTPURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                VerifyModel verifyModel=gson.fromJson(response,VerifyModel.class);
                //System.out.println(verifyModel.RefreshTokenValue);
                //System.out.println(verifyModel.ResponseStatus);

                MyToken.setToken(verifyModel.RefreshTokenValue);
                MyToken.setAuthorization(verifyModel.AcsessTokenValue);

                if (verifyModel.ResponseStatus.matches("Verified Success"))
                {
                    if (CheckNetworkConnection.getInstance(getActivity()).isOnline())
                    {
                        Hit_API_DepositorList();
                    }
                    else {
                        MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
                        return;
                    }
                }
                else if (verifyModel.ResponseStatus.matches("Verification Fail"))
                {
                    RepeatSafeToast.show(getActivity(),verifyModel.ErrorCodes);
                    return;
                }
                else if (verifyModel.ResponseStatus.matches("EmailId  Should not be blank"))
                {
                    RepeatSafeToast.show(getActivity(),"EmailId  Should not be blank");
                    return;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                //System.out.println(User_EmailId_Text+" "+User_CountryCode+" "+User_MobileNo+" "+User_OTP_Text);
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("EmailID", User_EmailId_Text);
                if(User_CountryCode.matches("null"))
                {

                    params.put("countryCode", "");
                }
                else
                {
                    params.put("countryCode", User_CountryCode);
                }
                if(User_MobileNo.matches("null"))
                {

                    params.put("MobileNumber", "");
                }
                else
                {
                    params.put("MobileNumber", User_MobileNo);
                }

                params.put("OTP", User_OTP_Text);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();     //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public  void Hit_API_DepositorList() {

       // System.out.println("DepositorList  API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getDepositorListRegistrationURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }
*/
                pDialog.hide();
                pDialog.dismiss();
                show_View_VerifyBtnOnClick();
                hide_View_VerifyBtnOnClick();
                show_UnChecked();
                hide_UnChecked();
                Gson gson = new GsonBuilder().setLenient().create();
                DepositorListModel depositorListModel=gson.fromJson(response,DepositorListModel.class);
                //System.out.println(depositorListModel.DepositorList.size());
                //System.out.println(depositorListModel.RefreshTokenValue);
                MyToken.setToken(depositorListModel.RefreshTokenValue);
                MyToken.setAuthorization(depositorListModel.AcsessTokenValue);
                MainScreen.TrackBundle.putParcelable("depositorListModel", depositorListModel);

                DepositorArrayList depositorArrayList=new DepositorArrayList();
                depositorArrayList.DepositorId="0";
                depositorArrayList.DepositorName="-Select-";
                depositorListModel.DepositorList.add(0,depositorArrayList);
                DepositorListCustomAdapter depositorListCustomAdapter=new DepositorListCustomAdapter(getActivity(),depositorListModel.DepositorList);
                Spinner_DepositorCategory.setAdapter(depositorListCustomAdapter);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public  void Hit_API_ControllerList() {

        //System.out.println("ControllerList  API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getControllerListURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();



                Gson gson = new GsonBuilder().setLenient().create();
                ControllerListModel controllerListModel=gson.fromJson(response,ControllerListModel.class);
                //System.out.println(controllerListModel.ControllerList.size());
                //System.out.println(controllerListModel.RefreshTokenValue);
                MyToken.setToken(controllerListModel.RefreshTokenValue);
                MyToken.setAuthorization(controllerListModel.AcsessTokenValue);
                MainScreen.TrackBundle.putParcelable("controllerListModel", controllerListModel);

                ControllerArrayList controllerArrayList=new ControllerArrayList();
                controllerArrayList.ControllerId="0";
                controllerArrayList.ControllerName="-Select-";
                controllerListModel.ControllerList.add(0,controllerArrayList);

                ControllerListCustomAdapter controllerListCustomAdapter=new ControllerListCustomAdapter(getActivity(),controllerListModel.ControllerList);
                Spinner_SelectController.setAdapter(controllerListCustomAdapter);

                SelectController_LinearLayout.setVisibility(View.VISIBLE);
                SelectController_LinearLayout.setEnabled(true);
                OrganizationName_LinearLayout.setVisibility(View.VISIBLE);
                OrganizationName_LinearLayout.setEnabled(true);
                CompanyIdentificationNo_LinearLayout.setVisibility(View.VISIBLE);
                CompanyIdentificationNo_LinearLayout.setEnabled(true);
                Tan_LinearLayout.setVisibility(View.VISIBLE);
                Tan_LinearLayout.setEnabled(true);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

      //  strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public  void Hit_API_BankList() {

        //System.out.println("BankList  API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getBankListURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                BankListModel bankListModel=gson.fromJson(response,BankListModel.class);
                //System.out.println(bankListModel.BankList.size());
                //System.out.println(bankListModel.RefreshTokenValue);
                MyToken.setToken(bankListModel.RefreshTokenValue);
                MyToken.setAuthorization(bankListModel.AcsessTokenValue);
                MainScreen.TrackBundle.putParcelable("bankListModel", bankListModel);

                BankArrayList bankArrayList=new BankArrayList();
                bankArrayList.BankId="0";
                bankArrayList.BankName="-Select-";
                bankListModel.BankList.add(0,bankArrayList);

                BankListCustomAdapter bankListCustomAdapter=new BankListCustomAdapter(getActivity(),bankListModel.BankList);
                Spinner_SelectBankName.setAdapter(bankListCustomAdapter);

                show_Checked();
                hide_Checked();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    public  void Hit_API_CheckUserAvalable() {

        //System.out.println("CheckUserAvalable  API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getCheckUserAvaliabilityURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                CheckUserAvaliabilityModel checkUserAvaliabilityModel=gson.fromJson(response,CheckUserAvaliabilityModel.class);
                //System.out.println(checkUserAvaliabilityModel.ResponseStatus);
                //System.out.println(checkUserAvaliabilityModel.RefreshTokenValue);
                MyToken.setToken(checkUserAvaliabilityModel.RefreshTokenValue);
                MyToken.setAuthorization(checkUserAvaliabilityModel.AcsessTokenValue);
                MainScreen.TrackBundle.putParcelable("checkUserAvaliabilityModel", checkUserAvaliabilityModel);
                RepeatSafeToast.show(getActivity(),checkUserAvaliabilityModel.ResponseStatus);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserName", User_Name_Text);
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    public  void Hit_API_InsertUser() {

        //System.out.println("InsertUser  API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getInsertUserURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                InsertUserModel insertUserModel=gson.fromJson(response,InsertUserModel.class);
                //System.out.println(insertUserModel.ErrorCodes);
                //System.out.println(insertUserModel.ResponseStatus);
                //System.out.println(insertUserModel.RefreshTokenValue);
                MyToken.setToken(insertUserModel.RefreshTokenValue);
                MyToken.setAuthorization(insertUserModel.AcsessTokenValue);
                MainScreen.TrackBundle.putParcelable("insertUserModel", insertUserModel);

                showDialog_UserRegistration(insertUserModel);
                RepeatSafeToast.show(getActivity(),insertUserModel.ResponseStatus);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                if(!IsNeftChecked)
                {
                    params.put("DepositorId", String.valueOf(DepositorNameID));
                    params.put("ControllerId", String.valueOf(ControllerNameID));
                    params.put("FirstName", User_FirstName_Text);
                    params.put("LastName",User_LastName_Text);
                    params.put("UserName", User_Name_Text);
                    String password=  HashingSha256(User_Password_Text);
                    params.put("Password", password);
                    params.put("ConfirmPassword", password);
                    params.put("OrganizationName", User_Organization_Text);
                    params.put("CinNumber", User_CompanyIdentificationNo_Text);
                    params.put("RefreshTokenValue", MyToken.getToken());
                    params.put("Email", User_EmailId_Text);
                    params.put("MobileNumber",User_MobileNo);
                    params.put("CountryCode", User_CountryCode);
                    params.put("UserId", "10");
                    params.put("IsNEFT", "false");
                    params.put("TanNumber", User_TanNumber_Text);

                }
                else
                {
                    params.put("DepositorId", String.valueOf(DepositorNameID));
                    params.put("ControllerId", String.valueOf(ControllerNameID));
                    params.put("FirstName", User_FirstName_Text);
                    params.put("LastName",User_LastName_Text);
                    params.put("UserName", User_Name_Text);
                    String password=  HashingSha256(User_Password_Text);
                    params.put("Password", password);
                    params.put("ConfirmPassword", password);
                    params.put("OrganizationName", User_Organization_Text);
                    params.put("CinNumber", User_CompanyIdentificationNo_Text);
                    params.put("BankId", String.valueOf(BankNameID));
                    params.put("BankAccountNo", User_BankAccountNo_Text);
                    params.put("IFSCCode", User_IFSC_Text);
                    params.put("RefreshTokenValue", MyToken.getToken());
                    params.put("Email", User_EmailId_Text);
                    params.put("MobileNumber",User_MobileNo);
                    params.put("CountryCode", User_CountryCode);
                    params.put("UserId", "10");
                    params.put("IsNEFT", "true");
                    params.put("TanNumber", User_TanNumber_Text);
                }
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    private String HashingSha256(String Input) {
        final HashFunction hashFunction = Hashing.sha256();
        final HashCode hc = hashFunction
                .newHasher()
                .putString(Input, Charsets.UTF_8)
                .hash();
        final String sha256 = hc.toString();
        return sha256;
    }
    private void showDialog_Success(String success) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView success_textview= (TextView) dialog.findViewById(R.id.success_textview);
        success_textview.setText(success);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm = getActivity().getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }
                changeScreen();
                dialog.dismiss();
            }
        });
    }
    private void changeScreen() {

        NewHomeFragment newFragment = new NewHomeFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_frame, newFragment);
        transaction.commit();
    }
    public void showDialog_Blue(String yourText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_blue);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView textView= (TextView) dialog.findViewById(R.id.yourtextview);
        textView.setText(yourText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
    }

    private void showDialog_UserRegistration(InsertUserModel insertUserModel)
    {
        if(insertUserModel.ResponseStatus.matches("User Registered Successfully"))
        {
            showDialog_Success("User Registered Successfully");
            return;
        }
        if(insertUserModel.ResponseStatus.matches("Password And Confirm Password not matched"))
        {
            showDialog_ServerError("Password And Confirm Password not matched");
            return;
        }
        if(insertUserModel.ResponseStatus.matches("Email Id is not in correct format"))
        {
            showDialog_ServerError("Email Id is not in correct format");
            return;
        }
        if(insertUserModel.ResponseStatus.matches("Email Id already exist"))
        {
            showDialog_Blue("Email Id already exist");
            return;
        }
        if(insertUserModel.ResponseStatus.matches("User name is not in correct format"))
        {
            showDialog_ServerError("User name is not in correct format");
            return;
        }
        if(insertUserModel.ResponseStatus.matches("User name already exist"))
        {
            showDialog_Blue("User name already exist");
            return;
        }
        if(insertUserModel.ResponseStatus.matches("User name can not be empty"))
        {
            showDialog_ServerError("User name can not be empty");
            return;
        }
        if(insertUserModel.ResponseStatus.matches("User Registration Failed"))
        {
            showDialog_ServerError(insertUserModel.ErrorCodes);
            return;
        }
    }


}
