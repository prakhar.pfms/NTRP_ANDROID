package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BaseResponse {
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("HdnEmail")
    @Expose
    private String hdnEmail;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("ProductList")
    @Expose
    private List<ProductListModel> productList;
    @SerializedName("ShippingUserTypeList")
    @Expose
    private ArrayList<ShippingUserTypeListModel> shippingUserTypeList;
    @SerializedName("ProductCartList")
    @Expose
    private ArrayList<ProductCartListModel> productCartList;
    @SerializedName("DropDownList")
    @Expose
    private ArrayList<TitleDropDownListModel> dropDownList;
    @SerializedName("ErrorCodes")
    @Expose
    private String errorCodes;
    @SerializedName("ResponseStatus")
    @Expose
    private String responseStatus;
    @SerializedName("XmlDocumentResult")
    @Expose
    private Object xmlDocumentResult;
    @SerializedName("AcsessTokenValue")
    @Expose
    private String acsessTokenValue;
    @SerializedName("RefreshTokenValue")
    @Expose
    private String refreshTokenValue;
    @SerializedName("PaymentPurposeID")
    @Expose
    private int paymentPurposeID;
    @SerializedName("ReceiptEntryId")
    @Expose
    private int receiptEntryId;

    @SerializedName("TotalDiscount")
    @Expose
    private String totalDiscount;
    @SerializedName("ShippingCharges")
    @Expose
    private String shippingCharges;

    public String getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(String shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public ArrayList<ProductCartListModel> getProductCartList() {
        return productCartList;
    }

    public void setProductCartList(ArrayList<ProductCartListModel> productCartList) {
        this.productCartList = productCartList;
    }

    public String getErrorCodes() {
        return errorCodes;
    }

    public void setErrorCodes(String errorCodes) {
        this.errorCodes = errorCodes;
    }

    public ArrayList<ShippingUserTypeListModel> getShippingUserTypeList() {
        return shippingUserTypeList;
    }

    public void setShippingUserTypeList(ArrayList<ShippingUserTypeListModel> shippingUserTypeList) {
        this.shippingUserTypeList = shippingUserTypeList;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getAcsessTokenValue() {
        return acsessTokenValue;
    }

    public void setAcsessTokenValue(String acsessTokenValue) {
        this.acsessTokenValue = acsessTokenValue;
    }

    public String getRefreshTokenValue() {
        return refreshTokenValue;
    }

    public void setRefreshTokenValue(String refreshTokenValue) {
        this.refreshTokenValue = refreshTokenValue;
    }

    public List<ProductListModel> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductListModel> productList) {
        this.productList = productList;
    }

    public int getPaymentPurposeID() {
        return paymentPurposeID;
    }

    public void setPaymentPurposeID(int paymentPurposeID) {
        this.paymentPurposeID = paymentPurposeID;
    }

    public int getReceiptEntryId() {
        return receiptEntryId;
    }

    public void setReceiptEntryId(int receiptEntryId) {
        this.receiptEntryId = receiptEntryId;
    }

    public ArrayList<TitleDropDownListModel> getDropDownList() {
        return dropDownList;
    }

    public void setDropDownList(ArrayList<TitleDropDownListModel> dropDownList) {
        this.dropDownList = dropDownList;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHdnEmail() {
        return hdnEmail;
    }

    public void setHdnEmail(String hdnEmail) {
        this.hdnEmail = hdnEmail;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
