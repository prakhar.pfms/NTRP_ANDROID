package testme.myapp.com.ntrp.module_publicationdivisonstore.fragments;

import static testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment.UNIQUE_ID;
import static testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment.checkoutString;
import static testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment.pTitle;
import static testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment.productCartList;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyCustomToast;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.extra.RepeatSafeToast;
import testme.myapp.com.ntrp.module_publicationdivisonstore.adapter.AttributeSpinnerAdapter;
import testme.myapp.com.ntrp.module_publicationdivisonstore.adapter.MenuAdapter;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.AttributeGetDetailListModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.BaseResponse;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.CartData;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ProductCartListModel;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ProductListModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.utils.AllUrls;


public class ProductDescFragment extends BaseFragment {
    private View mView;
    private ProductListModel data, productListModel;
    private TextView tvItemName, tvPrice, tvAmt, tvQty, tvIsbn, tvAuthor, tvValue, tvQtyAmt, tvProductDesc;
    private ImageView ivImg;
    private Button btnAddToCart, btnAdd, btnMinus, btnCheckout;
    private LinearLayout llAdd, llCheckout, llIssue, llLanguage, llCountry;
    private Spinner spLanguage, spIssueYr, spCountry;
    private EditText etQty;
    private int value = 1, avlQty = 0, userInputQty = 1;
    private ArrayList<AttributeGetDetailListModel> languageList, issueList, countryList;
    private ArrayList<ProductListModel> descListData;
    private AttributeSpinnerAdapter languageSpinnerAdapter, issueSpinnerAdapter, countryAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_product_desc, container, false);
        init();
        return mView;
    }

    @Override
    public void onStart() {
        getActivity().setTitle(pTitle);
        super.onStart();
    }

    @Override
    protected void initView() {
        hideSoftInputKeyboard();
        languageList = new ArrayList<>();
        issueList = new ArrayList<>();
        countryList = new ArrayList<>();
        descListData = new ArrayList<>();
        tvItemName = mView.findViewById(R.id.tvItemName);
        tvPrice = mView.findViewById(R.id.tvPrice);
        tvAmt = mView.findViewById(R.id.tvAmt);
        tvQty = mView.findViewById(R.id.tvQty);
        tvIsbn = mView.findViewById(R.id.tvIsbn);
        tvAuthor = mView.findViewById(R.id.tvAuthor);
        ivImg = mView.findViewById(R.id.ivImg);
        tvValue = mView.findViewById(R.id.tvValue);
        btnAddToCart = mView.findViewById(R.id.btnAddToCart);
        btnAdd = mView.findViewById(R.id.btnAdd);
        btnMinus = mView.findViewById(R.id.btnMinus);
        llAdd = mView.findViewById(R.id.llAdd);
        llCheckout = mView.findViewById(R.id.llCheckout);
        tvQtyAmt = mView.findViewById(R.id.tvQtyAmt);
        btnCheckout = mView.findViewById(R.id.btnCheckout);
        // tvAvlQty = mView.findViewById(R.id.tvAvlQty);
        tvProductDesc = mView.findViewById(R.id.tvProductDesc);
        etQty = mView.findViewById(R.id.etQty);
        spLanguage = mView.findViewById(R.id.spLanguage);
        spIssueYr = mView.findViewById(R.id.spIssueYr);
        spCountry = mView.findViewById(R.id.spCountry);
        llIssue = mView.findViewById(R.id.llIssue);
        llLanguage = mView.findViewById(R.id.llLanguage);
        llCountry = mView.findViewById(R.id.llCountry);
    }

    @Override
    protected void initListener() {

        etQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s.toString())) {
                    int qty = Integer.parseInt(s.toString());
                    if (qty > avlQty) {
                        MyCustomToast.createErrorToast(getActivity(), "Quantity entered must be less than or equal to stock available");
                        etQty.setText("1");
                        etQty.setSelection(etQty.getText().length());
                    } else {
                        userInputQty = qty;
                        double amt = productListModel.getProductItemPrice() * qty;
                        tvAmt.setText("₹ " + amt + "");
                    }
                }

            }
        });
        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userInputQty > avlQty) {
                    MyCustomToast.createErrorToast(getActivity(), "Quantity entered must be less than or equal to stock available");
                    etQty.setText("1");
                    etQty.setSelection(etQty.getText().length());
                    return;
                }
                if (etQty.getText().toString().equalsIgnoreCase("0")) {
                    MyCustomToast.createErrorToast(getActivity(), "Quantity entered must be greater than 0");
                    etQty.setText("1");
                    etQty.setSelection(etQty.getText().length());
                    return;
                }

                if (avlQty > 0) {
                    if (TextUtils.isEmpty(PublicationFragment.AddToCardType)) {
                        HitAddCartProductApi(userInputQty + "", productListModel);
                    } else if (PublicationFragment.AddToCardType.equalsIgnoreCase(PublicationFragment.ModuleType)) {
                        HitAddCartProductApi(userInputQty + "", productListModel);
                    } else {
                        showAlertDialog();
                    }

                    value = 1;
                    tvValue.setText(value + "");
                    // btnAddToCart.setVisibility(View.GONE);
                    //llAdd.setVisibility(View.VISIBLE);
//                    CartData model = new CartData();
//                    model.setImage(data.getProductImage());
//                    model.setProductName(data.getProductName());
//                    model.setPrice(data.getDisplayProductPrice());
//                    model.setQty(value);
//                    model.setTotalPrice(value * data.getDisplayProductPrice());
//                    model.setProductId(data.getProductId());
//                    CartData.cartDataModelList.add(model);
//                    int totalQty = 0, totalPrice = 0;
//                    for (CartData item : CartData.cartDataModelList) {
//                        totalQty += item.getQty();
//                        totalPrice += item.getTotalPrice();
//                    }
//                    tvQtyAmt.setText(totalQty + " Item(s) | ₹ " + totalPrice);
//                    llCheckout.setVisibility(View.VISIBLE);
                }

            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (value == avlQty) {
                    MyCustomToast.createErrorToast(getActivity(), "Quantity entered must be less than or equal to stock available");
                    etQty.setText("1");
                    return;
                }
                value++;
                tvValue.setText(value + "");
                int totalQty = 0, totalPrice = 0;
                for (CartData item : CartData.cartDataModelList) {
                    if (item.getProductId() == data.getProductId()) {
                        item.setQty(value);
                        item.setTotalPrice(value * item.getPrice());
                    }
                    totalQty += item.getQty();
                    totalPrice += item.getTotalPrice();
                }
                tvQtyAmt.setText(totalQty + " Item(s) | ₹ " + totalPrice);
            }
        });
        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value--;
                int totalQty = 0, totalPrice = 0;
                if (value == 0) {
                    for (CartData item : CartData.cartDataModelList) {
                        if (item.getProductId() == data.getProductId()) {
                            CartData.cartDataModelList.remove(item);
                            break;
                        }
                    }
                    llAdd.setVisibility(View.GONE);
                    btnAddToCart.setVisibility(View.VISIBLE);
                    if (CartData.cartDataModelList.size() == 0) {
                        llCheckout.setVisibility(View.GONE);
                    }

                } else {
                    for (CartData item : CartData.cartDataModelList) {
                        if (item.getProductId() == data.getProductId()) {
                            item.setQty(value);
                            item.setTotalPrice(value * item.getPrice());
                        }
                        totalQty += item.getQty();
                        totalPrice += item.getTotalPrice();
                    }
                    tvQtyAmt.setText(totalQty + " Item(s) | ₹ " + totalPrice);
                }
                tvValue.setText(value + "");
            }
        });
        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckoutFragment newFragment = new CheckoutFragment();
                replaceFragment(newFragment, R.id.container_frame, true);
            }
        });

        spIssueYr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AttributeGetDetailListModel model = issueList.get(position);
                for (int i = 0; i < descListData.size(); i++) {
                    if (model.attributeValue.equalsIgnoreCase(descListData.get(i).attributeValue)) {
                        tvPrice.setText("₹ " + descListData.get(i).getDisplayItemPrice() + "");
                        tvQty.setText(descListData.get(i).getProductItemQuantity() + "");
                        // tvAvlQty.setText(descListData.get(i).getProductItemQuantity() + "");
                        avlQty = descListData.get(i).getProductItemQuantity();
                        tvIsbn.setText(descListData.get(i).getiSBN());
                        tvAuthor.setText(descListData.get(i).getWriterName());
                        break;
                    }
                }
//                if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {
//
//                } else {
//                    MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
//                    return;
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AttributeGetDetailListModel model = languageList.get(position);
                for (int i = 0; i < descListData.size(); i++) {
                    if (model.attributeValue.equalsIgnoreCase(descListData.get(i).attributeValue)) {
                        //  tvPrice.setText("₹ " + descListData.get(i).getDisplayItemPrice() + "");
                        tvQty.setText(descListData.get(i).getProductItemQuantity() + "");
                        //tvAvlQty.setText(descListData.get(i).getProductItemQuantity() + "");
                        avlQty = descListData.get(i).getProductItemQuantity();
                        tvIsbn.setText(descListData.get(i).getiSBN());
                        tvAuthor.setText(descListData.get(i).getWriterName());
                        break;
                    }
                }
//                if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {
//
//                } else {
//                    MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
//                    return;
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AttributeGetDetailListModel model = countryList.get(position);
                for (int i = 0; i < descListData.size(); i++) {
                    if (model.attributeValue.equalsIgnoreCase(descListData.get(i).attributeValue)) {
                        // tvPrice.setText("₹ " + descListData.get(i).getDisplayItemPrice() + "");
                        tvQty.setText(descListData.get(i).getProductItemQuantity() + "");
                        // tvAvlQty.setText(descListData.get(i).getProductItemQuantity() + "");
                        avlQty = descListData.get(i).getProductItemQuantity();
                        tvIsbn.setText(descListData.get(i).getiSBN());
                        tvAuthor.setText(descListData.get(i).getWriterName());
                        break;
                    }
                }
//                if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {
//
//                } else {
//                    MyCustomToast.createErrorToast(getActivity(), "Check Your Internet Connection!");
//                    return;
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void bindDataWithUi() {
        Bundle bundle = this.getArguments();
        if (null != bundle) {
            data = bundle.getParcelable("data");
            if (null != data) {
                tvItemName.setText(data.getProductName());
                tvPrice.setText("₹ " + data.getDisplayProductPrice() + "");
                tvAmt.setText("₹ " + data.getDisplayProductPrice() + "");
                tvQty.setText("0");
                Picasso.with(getContext())
                        .load(AllUrls.imagePath + data.getProductImage())
                        .placeholder(R.drawable.book_placeholder)
                        .error(R.drawable.book_placeholder)
                        .into(ivImg);
                if (data.getOutOfStock().equalsIgnoreCase("1")) {
                    // tvAvlQty.setText("0");
                    etQty.setEnabled(false);
                    btnAddToCart.setText("Out of Stock");
                    btnAddToCart.setEnabled(false);
                    btnAddToCart.setBackgroundColor(getContext().getResources().getColor(R.color.light_gray));
                }
            }
        }
        if (!TextUtils.isEmpty(checkoutString)) {
            llCheckout.setVisibility(View.VISIBLE);
            tvQtyAmt.setText(checkoutString);
        }

//        if (productCartList.size() > 0) {
//            int totalQty = 0;
//            double totalPrice = 0;
//            for (ProductCartListModel item : productCartList) {
//                totalQty += item.getTotalQty();
//                totalPrice += item.getTotalPrice();
//            }
//            llCheckout.setVisibility(View.VISIBLE);
//            tvQtyAmt.setText(totalQty + " Item(s) | ₹ " + totalPrice);


//            int totalQty = 0, totalPrice = 0;
//            for (CartData item : CartData.cartDataModelList) {
//                if (item.getProductId() == data.getProductId()) {
//                    btnAddToCart.setVisibility(View.GONE);
//                    llAdd.setVisibility(View.VISIBLE);
//                    value = item.getQty();
//                    tvValue.setText(value + "");
//                }
//                totalQty += item.getQty();
//                totalPrice += item.getTotalPrice();
//                tvQtyAmt.setText(totalQty + " Item(s) | ₹ " + totalPrice);
//                llCheckout.setVisibility(View.VISIBLE);
//            }
//        }

        HitGetProductListApi(data.getCategoryId() + "", data.getProductId() + "");
    }

    public void HitGetProductListApi(String catId, String productId) {
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getProductListURL;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                // Gson gson = new GsonBuilder().setLenient().create();
                Gson gson = new Gson();
                Type type = new TypeToken<ProductListModel>() {
                }.getType();
                ProductListModel responseData = gson.fromJson(response, type);
                if (null != responseData) {
                    if (responseData.getErrorCodes().matches("Success")) {
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        if (responseData.getProductList() != null && responseData.getProductList().size() > 0) {
                            productListModel = responseData.getProductList().get(0);
                            descListData.addAll(responseData.getProductList());
                            avlQty = responseData.getProductList().get(0).getProductItemQuantity();
                            tvQty.setText(responseData.getProductList().get(0).getProductItemQuantity() + "");
                            //tvAvlQty.setText(responseData.getProductList().get(0).getProductItemQuantity() + "");
                            tvIsbn.setText(responseData.getProductList().get(0).getiSBN());
                            tvAuthor.setText(responseData.getProductList().get(0).getWriterName());
                            tvProductDesc.setText(responseData.getProductList().get(0).getProductDescription());

//                        for (int i = 0; i < responseData.getProductList().size(); i++) {
//                            switch (responseData.getProductList().get(i).getReceiptAttributeId()) {
//                                case "1":
//                                    languageList.add(responseData.getProductList().get(i));
//                                    break;
//                                case "2":
//                                    countryList.add(responseData.getProductList().get(i));
//                                    break;
//                                case "3":
//                                    issueList.add(responseData.getProductList().get(i));
//                                    break;
//                            }
//                        }

                            for (int i = 0; i < responseData.getProductList().get(0).getAttributeGetDetailList().size(); i++) {
                                switch (responseData.getProductList().get(0).getAttributeGetDetailList().get(i).getAttributeId()) {
                                    case 1:
                                        languageList.add(responseData.getProductList().get(0).getAttributeGetDetailList().get(i));
                                        break;
                                    case 2:
                                        countryList.add(responseData.getProductList().get(0).getAttributeGetDetailList().get(i));
                                        break;
                                    case 3:
                                        issueList.add(responseData.getProductList().get(0).getAttributeGetDetailList().get(i));
                                        break;
                                }
                            }
                            languageSpinnerAdapter = new AttributeSpinnerAdapter(getContext(), R.layout.single_row_spinner, languageList);
                            issueSpinnerAdapter = new AttributeSpinnerAdapter(getContext(), R.layout.single_row_spinner, issueList);
                            countryAdapter = new AttributeSpinnerAdapter(getContext(), R.layout.single_row_spinner, countryList);
                            spLanguage.setAdapter(languageSpinnerAdapter);
                            spCountry.setAdapter(countryAdapter);
                            spIssueYr.setAdapter(issueSpinnerAdapter);
                            if (issueList.size() > 0)
                                llIssue.setVisibility(View.VISIBLE);
                            if (countryList.size() > 0)
                                llCountry.setVisibility(View.VISIBLE);
                            if (languageList.size() > 0)
                                llLanguage.setVisibility(View.VISIBLE);
                        }
                    } else {
                        MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("Flag", "PD");
                params.put("CategoryId", catId); //2
                params.put("ProductId", productId);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public void HitAddCartProductApi(String qty, ProductListModel productData) {
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getAddCartProduct;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
//                Gson gson = new Gson();
//                Type type = new TypeToken<ProductListModel>() {
//                }.getType();
//                ProductListModel responseData = gson.fromJson(response, type);
                if (null != responseData) {
                    if (responseData.getErrorCodes().matches("Product added to cart Successfully")) {
                        PublicationFragment.AddToCardType = PublicationFragment.ModuleType;
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        tvQtyAmt.setText(responseData.getResponseStatus());
                        checkoutString = responseData.getResponseStatus();
                        llCheckout.setVisibility(View.VISIBLE);
                        btnCheckout.setEnabled(true);
                    } else if (responseData.getResponseStatus().matches("Fail")) {
                        MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
                    } else {
                        MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID);
                params.put("Stock", productData.getProductItemQuantity() + ""); //2
                params.put("Qty", qty);
                params.put("Total", "0");
                params.put("ProductAttributeMappingId", productData.getProductAttributeMappingId() + "");
                params.put("Productname", productData.getProductName());
                params.put("ProductId", productData.getProductId() + "");
                params.put("ProductCode", "");//productData.getProductCode()
                params.put("ProductImage", productData.getProductImage());
                params.put("ReceiptProductItemId", productData.getReceiptProductItemId());
                params.put("CategoryReceiptAccountId", productData.getCategoryReceiptAccountId());
                params.put("ProductCategoryId", productData.getCategoryId() + "");
                params.put("AvailStock", productData.getProductItemQuantity() + "");
                params.put("Price", productData.getProductItemPrice() + "");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void showAlertDialog() {
        TextView tvOk, tvCancel;
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (null != dialog.getWindow())
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.Animations;
        dialog.setContentView(R.layout.dialog_publication_alert);
        tvOk = dialog.findViewById(R.id.tvOk);
        tvCancel = dialog.findViewById(R.id.tvCancel);
        dialog.setCancelable(false);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                tvQtyAmt.setText("");
                btnCheckout.setEnabled(false);
                HitRemoveAllProductFromCartApi();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    public void HitRemoveAllProductFromCartApi() {
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getRemoveAllProductFromCart;
        showLoadingDialog();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideLoadingDialog();
                Gson gson = new GsonBuilder().setLenient().create();
                BaseResponse responseData = gson.fromJson(response, BaseResponse.class);
                if (null != responseData) {
                    if (responseData.getResponseStatus().matches("Success")) {
                        MyCustomToast.show(getContext(), responseData.getErrorCodes());
                        MyToken.setToken(responseData.getRefreshTokenValue());
                        MyToken.setAuthorization(responseData.getAcsessTokenValue());
                        HitAddCartProductApi(userInputQty + "", productListModel);
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                hideLoadingDialog();
                MyCustomToast.createErrorToast(getActivity(), "Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("UniqueUserDeviceId", UNIQUE_ID);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


}