package testme.myapp.com.ntrp.module_nonregisteredusers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FormDetailNext {

    @SerializedName("DoTOfficePAO")
    @Expose
    private Object doTOfficePAO;
    @SerializedName("BankUrl")
    @Expose
    private Object bankUrl;
    @SerializedName("CompleteEncodedKey")
    @Expose
    private Object completeEncodedKey;
    @SerializedName("DigitalCertificate")
    @Expose
    private Object digitalCertificate;
    @SerializedName("ChargesApplicable")
    @Expose
    private Object chargesApplicable;
    @SerializedName("IsPosPurpose")
    @Expose
    private Integer isPosPurpose;
    @SerializedName("DepositorId")
    @Expose
    private Integer depositorId;
    @SerializedName("DepositorName")
    @Expose
    private Object depositorName;
    @SerializedName("TaxDescription")
    @Expose
    private Object taxDescription;
    @SerializedName("TaxValue")
    @Expose
    private Integer taxValue;
    @SerializedName("TaxID")
    @Expose
    private Integer taxID;
    @SerializedName("TaxCalculateValue")
    @Expose
    private Integer taxCalculateValue;
    @SerializedName("IsCurrecnyconversion")
    @Expose
    private Integer isCurrecnyconversion;
    @SerializedName("XMLCurrencycode")
    @Expose
    private Object xMLCurrencycode;
    @SerializedName("XMLAmount")
    @Expose
    private Object xMLAmount;
    @SerializedName("IsAdditionalCharge")
    @Expose
    private Integer isAdditionalCharge;
    @SerializedName("IfExternalIntegration")
    @Expose
    private Integer ifExternalIntegration;
    @SerializedName("DepositorCategoryId")
    @Expose
    private Integer depositorCategoryId;
    @SerializedName("FuncHeadNo")
    @Expose
    private Integer funcHeadNo;
    @SerializedName("FuncHeadDescription")
    @Expose
    private String funcHeadDescription;
    @SerializedName("LobarequestURL")
    @Expose
    private Object lobarequestURL;
    @SerializedName("Address1")
    @Expose
    private Object address1;
    @SerializedName("IsDeleteOfflineSlip")
    @Expose
    private Boolean isDeleteOfflineSlip;
    @SerializedName("Address2")
    @Expose
    private Object address2;
    @SerializedName("AccountShortName")
    @Expose
    private String accountShortName;
    @SerializedName("ServiceDescription")
    @Expose
    private Object serviceDescription;
    @SerializedName("PurposeGroupID")
    @Expose
    private Integer purposeGroupID;
    @SerializedName("PurposeGroupDescription")
    @Expose
    private Object purposeGroupDescription;
    @SerializedName("PurposeRemarks")
    @Expose
    private String purposeRemarks;
    @SerializedName("ModifiedBy")
    @Expose
    private Object modifiedBy;
    @SerializedName("ModifiedDate")
    @Expose
    private String modifiedDate;
    @SerializedName("StateId")
    @Expose
    private Integer stateId;
    @SerializedName("StateName")
    @Expose
    private Object stateName;
    @SerializedName("Salutations")
    @Expose
    private Object salutations;
    @SerializedName("SalutationId")
    @Expose
    private Integer salutationId;
    @SerializedName("UserGroupID")
    @Expose
    private Integer userGroupID;
    @SerializedName("UserGroupDESC")
    @Expose
    private Object userGroupDESC;
    @SerializedName("DistrictId")
    @Expose
    private Integer districtId;
    @SerializedName("DistrictName")
    @Expose
    private Object districtName;
    @SerializedName("Pincode")
    @Expose
    private Object pincode;
    @SerializedName("MobileNo")
    @Expose
    private Object mobileNo;
    @SerializedName("Email")
    @Expose
    private Object email;
    @SerializedName("PanNumber")
    @Expose
    private Object panNumber;
    @SerializedName("TanNumber")
    @Expose
    private Object tanNumber;
    @SerializedName("AadhaarNumber")
    @Expose
    private Object aadhaarNumber;
    @SerializedName("PurposeDescription")
    @Expose
    private String purposeDescription;
    @SerializedName("ControllerId")
    @Expose
    private Integer controllerId;
    @SerializedName("ControllerName")
    @Expose
    private Object controllerName;
    @SerializedName("BankIntegratedToCPSMS")
    @Expose
    private Object bankIntegratedToCPSMS;
    @SerializedName("IntegratedToNPCI4Send")
    @Expose
    private Object integratedToNPCI4Send;
    @SerializedName("FunctionHead")
    @Expose
    private String functionHead;
    @SerializedName("StatusId")
    @Expose
    private Integer statusId;
    @SerializedName("Status")
    @Expose
    private Object status;
    @SerializedName("UTRNoStatus")
    @Expose
    private Object uTRNoStatus;
    @SerializedName("IsUTRVerified")
    @Expose
    private Boolean isUTRVerified;
    @SerializedName("NEFTDate")
    @Expose
    private String nEFTDate;
    @SerializedName("NTRPPurposeId")
    @Expose
    private Integer nTRPPurposeId;
    @SerializedName("MinAmount")
    @Expose
    private Integer minAmount;
    @SerializedName("MaxAmount")
    @Expose
    private Integer maxAmount;
    @SerializedName("AccountId")
    @Expose
    private Integer accountId;
    @SerializedName("AccountGroupId")
    @Expose
    private Integer accountGroupId;
    @SerializedName("ReceiptPurposeMinistryMappingID")
    @Expose
    private Integer receiptPurposeMinistryMappingID;
    @SerializedName("FrequencyId")
    @Expose
    private Integer frequencyId;
    @SerializedName("FrequencyName")
    @Expose
    private Object frequencyName;
    @SerializedName("MinistryDepartment")
    @Expose
    private String ministryDepartment;
    @SerializedName("AggregatorBankId")
    @Expose
    private Integer aggregatorBankId;
    @SerializedName("AggregatorBankName")
    @Expose
    private Object aggregatorBankName;
    @SerializedName("CustomFieldTypeId")
    @Expose
    private Integer customFieldTypeId;
    @SerializedName("FieldTypeName")
    @Expose
    private Object fieldTypeName;
    @SerializedName("HasMasterData")
    @Expose
    private Boolean hasMasterData;
    @SerializedName("CreatedClientIP")
    @Expose
    private String createdClientIP;
    @SerializedName("TypeStatus")
    @Expose
    private Boolean typeStatus;
    @SerializedName("ControlText")
    @Expose
    private Object controlText;
    @SerializedName("ControlValue")
    @Expose
    private Integer controlValue;
    @SerializedName("ControlValues")
    @Expose
    private Object controlValues;
    @SerializedName("MinistryId")
    @Expose
    private Integer ministryId;
    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("MajorHeadDescription")
    @Expose
    private Object majorHeadDescription;
    @SerializedName("MajorHeadId")
    @Expose
    private Object majorHeadId;
    @SerializedName("FunctionHeadId")
    @Expose
    private Object functionHeadId;
    @SerializedName("FunctionHeadNo")
    @Expose
    private Object functionHeadNo;
    @SerializedName("FunctionHeadName")
    @Expose
    private Object functionHeadName;
    @SerializedName("DDOID")
    @Expose
    private Integer ddoid;
    @SerializedName("AgencyID")
    @Expose
    private Integer agencyID;
    @SerializedName("AgencyName")
    @Expose
    private Object agencyName;
    @SerializedName("ObjectHead")
    @Expose
    private String objectHead;
    @SerializedName("PaymentTypeid")
    @Expose
    private Integer paymentTypeid;
    @SerializedName("PaymentTypeDescription")
    @Expose
    private String paymentTypeDescription;
    @SerializedName("DDOName")
    @Expose
    private String dDOName;
    @SerializedName("Year")
    @Expose
    private Integer year;
    @SerializedName("AccountingPeriod")
    @Expose
    private String accountingPeriod;
    @SerializedName("Period")
    @Expose
    private String period;
    @SerializedName("PeriodValue")
    @Expose
    private String periodValue;
    @SerializedName("ReceiptPeriodFrom")
    @Expose
    private Object receiptPeriodFrom;
    @SerializedName("ReceiptPeriodTo")
    @Expose
    private Object receiptPeriodTo;
    @SerializedName("RECDocumentEntities")
    @Expose
    private Object rECDocumentEntities;
    @SerializedName("Totalamount")
    @Expose
    private Integer totalamount;
    @SerializedName("PaymentMode")
    @Expose
    private Object paymentMode;
    @SerializedName("PaymentModeId")
    @Expose
    private Integer paymentModeId;
    @SerializedName("ReceiptNo")
    @Expose
    private Integer receiptNo;
    @SerializedName("ReceiptEntryID")
    @Expose
    private Integer receiptEntryID;
    @SerializedName("ReceiptEntryDetailID")
    @Expose
    private Integer receiptEntryDetailID;
    @SerializedName("TinNumber")
    @Expose
    private Object tinNumber;
    @SerializedName("ChequeNumber")
    @Expose
    private Object chequeNumber;
    @SerializedName("AmountValue")
    @Expose
    private Integer amountValue;
    @SerializedName("DisplayAmount")
    @Expose
    private String displayAmount;
    @SerializedName("Remarks")
    @Expose
    private Object remarks;
    @SerializedName("ChannelName")
    @Expose
    private Object channelName;
    @SerializedName("BankId")
    @Expose
    private Integer bankId;
    @SerializedName("AggregatorID")
    @Expose
    private Integer aggregatorID;
    @SerializedName("AggregatorName")
    @Expose
    private Object aggregatorName;
    @SerializedName("BankName")
    @Expose
    private Object bankName;
    @SerializedName("encrequestparameter")
    @Expose
    private Object encrequestparameter;
    @SerializedName("reqdigitalsignature")
    @Expose
    private Object reqdigitalsignature;
    @SerializedName("TransactionNumber")
    @Expose
    private Object transactionNumber;
    @SerializedName("TransactionDate")
    @Expose
    private Object transactionDate;
    @SerializedName("PayeeName")
    @Expose
    private Object payeeName;
    @SerializedName("ReceiptFile")
    @Expose
    private Object receiptFile;
    @SerializedName("ChallanFile")
    @Expose
    private Object challanFile;
    @SerializedName("ReceiptNumber")
    @Expose
    private Object receiptNumber;
    @SerializedName("TransactionRefNumber")
    @Expose
    private Object transactionRefNumber;
    @SerializedName("PaymentStatus")
    @Expose
    private Object paymentStatus;
    @SerializedName("DigitalSignRequired")
    @Expose
    private Integer digitalSignRequired;
    @SerializedName("PAOId")
    @Expose
    private Integer pAOId;
    @SerializedName("ActiveInActive")
    @Expose
    private Integer activeInActive;
    @SerializedName("ActiveArchive")
    @Expose
    private Boolean activeArchive;
    @SerializedName("CurrencyId")
    @Expose
    private Integer currencyId;
    @SerializedName("CurrencyCode")
    @Expose
    private String currencyCode;
    @SerializedName("CurrentCurrencyValue")
    @Expose
    private Integer currentCurrencyValue;
    @SerializedName("PreviousCurrencyValue")
    @Expose
    private Integer previousCurrencyValue;
    @SerializedName("CurrencyDescription")
    @Expose
    private Object currencyDescription;
    @SerializedName("PAOName")
    @Expose
    private String pAOName;
    @SerializedName("ReceiptPrintDocumentId")
    @Expose
    private Integer receiptPrintDocumentId;
    @SerializedName("ReceiptPrintDocumentName")
    @Expose
    private Object receiptPrintDocumentName;
    @SerializedName("ReportName")
    @Expose
    private Object reportName;
    @SerializedName("ReportVersion")
    @Expose
    private Object reportVersion;
    @SerializedName("ResponseDigitalSignature")
    @Expose
    private Object responseDigitalSignature;
    @SerializedName("ResponseMerchantOrderNo")
    @Expose
    private Object responseMerchantOrderNo;
    @SerializedName("MerchantId")
    @Expose
    private Object merchantId;
    @SerializedName("AggIdVal")
    @Expose
    private Object aggIdVal;
    @SerializedName("ResponseAggregatorRefernceId")
    @Expose
    private Object responseAggregatorRefernceId;
    @SerializedName("ResponseMerchantCustomerID")
    @Expose
    private Object responseMerchantCustomerID;
    @SerializedName("ResponseErrorCode")
    @Expose
    private Object responseErrorCode;
    @SerializedName("ResponseGateWayId")
    @Expose
    private Object responseGateWayId;
    @SerializedName("ResponseStatus")
    @Expose
    private Object responseStatus;
    @SerializedName("ResponseAmount")
    @Expose
    private Integer responseAmount;
    @SerializedName("ResponseCurrency")
    @Expose
    private Object responseCurrency;
    @SerializedName("CurrencyCreatedDate")
    @Expose
    private Object currencyCreatedDate;
    @SerializedName("ResponsePayMode")
    @Expose
    private Object responsePayMode;
    @SerializedName("ResponseOtherDetails")
    @Expose
    private Object responseOtherDetails;
    @SerializedName("ResponseReason")
    @Expose
    private Object responseReason;
    @SerializedName("ResponseBankCode")
    @Expose
    private Object responseBankCode;
    @SerializedName("ResponseBankName")
    @Expose
    private Object responseBankName;
    @SerializedName("ResponseBankTransactionNumber")
    @Expose
    private Object responseBankTransactionNumber;
    @SerializedName("ResponseBankTransactionDate")
    @Expose
    private String responseBankTransactionDate;
    @SerializedName("ReceiptEntryBankDetailId")
    @Expose
    private Integer receiptEntryBankDetailId;
    @SerializedName("ResponseCIN")
    @Expose
    private Object responseCIN;
    @SerializedName("IsNEFT")
    @Expose
    private Boolean isNEFT;
    @SerializedName("AggregatorLogoName")
    @Expose
    private Object aggregatorLogoName;
    @SerializedName("CountryId")
    @Expose
    private Integer countryId;
    @SerializedName("CountryName")
    @Expose
    private Object countryName;
    @SerializedName("ResponseStatusDescription")
    @Expose
    private Object responseStatusDescription;
    @SerializedName("FName")
    @Expose
    private Object fName;
    @SerializedName("MName")
    @Expose
    private Object mName;
    @SerializedName("LName")
    @Expose
    private Object lName;
    @SerializedName("ContactPersonName")
    @Expose
    private Object contactPersonName;
    @SerializedName("UserTypeId")
    @Expose
    private Integer userTypeId;
    @SerializedName("Designation")
    @Expose
    private Object designation;
    @SerializedName("Phone")
    @Expose
    private Object phone;
    @SerializedName("CountryMobileCode")
    @Expose
    private Object countryMobileCode;
    @SerializedName("Username")
    @Expose
    private Object username;
    @SerializedName("Password")
    @Expose
    private Object password;
    @SerializedName("HintQuestionId")
    @Expose
    private Integer hintQuestionId;
    @SerializedName("LastLoginTime")
    @Expose
    private String lastLoginTime;
    @SerializedName("Answer")
    @Expose
    private Object answer;
    @SerializedName("ClientIPAddress")
    @Expose
    private Object clientIPAddress;
    @SerializedName("HistoryType")
    @Expose
    private Object historyType;
    @SerializedName("CreatedBy")
    @Expose
    private Object createdBy;
    @SerializedName("RequirePasswordChange")
    @Expose
    private Boolean requirePasswordChange;
    @SerializedName("EditStatus")
    @Expose
    private Integer editStatus;
    @SerializedName("ReceiptStatusUrl")
    @Expose
    private Object receiptStatusUrl;
    @SerializedName("Flag")
    @Expose
    private Integer flag;
    @SerializedName("ResponseMessage")
    @Expose
    private Object responseMessage;
    @SerializedName("PlainText")
    @Expose
    private Object plainText;
    @SerializedName("City")
    @Expose
    private Object city;
    @SerializedName("CountryCode")
    @Expose
    private Integer countryCode;
    @SerializedName("VerifySignature")
    @Expose
    private Boolean verifySignature;
    @SerializedName("IFSCCode")
    @Expose
    private Object iFSCCode;
    @SerializedName("BranchName")
    @Expose
    private Object branchName;
    @SerializedName("BranchID")
    @Expose
    private Integer branchID;
    @SerializedName("ChallanNo")
    @Expose
    private Object challanNo;
    @SerializedName("UtrnNo")
    @Expose
    private Object utrnNo;
    @SerializedName("AccountNumber")
    @Expose
    private Object accountNumber;
    @SerializedName("DptReference")
    @Expose
    private Object dptReference;
    @SerializedName("LicenceRefNum")
    @Expose
    private Object licenceRefNum;
    @SerializedName("EncryptionKey")
    @Expose
    private Object encryptionKey;
    @SerializedName("ChannelId")
    @Expose
    private Integer channelId;
    @SerializedName("SlabType")
    @Expose
    private Integer slabType;
    @SerializedName("SlabDescription")
    @Expose
    private Object slabDescription;
    @SerializedName("SlabMinAmount")
    @Expose
    private Integer slabMinAmount;
    @SerializedName("SlabMaxAmount")
    @Expose
    private Integer slabMaxAmount;
    @SerializedName("SlabUserCharges")
    @Expose
    private Integer slabUserCharges;
    @SerializedName("IsPercentage")
    @Expose
    private Integer isPercentage;
    @SerializedName("CINNo")
    @Expose
    private Object cINNo;
    @SerializedName("DepartmentName")
    @Expose
    private Object departmentName;
    @SerializedName("LOIDate")
    @Expose
    private Object lOIDate;
    @SerializedName("ComputerizedgeneratedNo")
    @Expose
    private Object computerizedgeneratedNo;
    @SerializedName("ReceiptpaymentTypeId")
    @Expose
    private Integer receiptpaymentTypeId;
    @SerializedName("productdetails")
    @Expose
    private Object productdetails;
    @SerializedName("ValidatedLicenseId")
    @Expose
    private Integer validatedLicenseId;
    @SerializedName("LicenseSetID")
    @Expose
    private Integer licenseSetID;
    @SerializedName("LicenseTypeName")
    @Expose
    private Object licenseTypeName;
    @SerializedName("LicenseTypeId")
    @Expose
    private Integer licenseTypeId;
    @SerializedName("ServiceArea")
    @Expose
    private Integer serviceArea;
    @SerializedName("ServiceAreaName")
    @Expose
    private Object serviceAreaName;
    @SerializedName("ServiceAreaId")
    @Expose
    private Integer serviceAreaId;
    @SerializedName("OrganizationName")
    @Expose
    private Object organizationName;
    @SerializedName("OrganizationId")
    @Expose
    private Integer organizationId;
    @SerializedName("ValidationType")
    @Expose
    private Object validationType;
    @SerializedName("ComputerizeGeneratedNo")
    @Expose
    private Object computerizeGeneratedNo;
    @SerializedName("DoTOffice")
    @Expose
    private Object doTOffice;
    @SerializedName("DoTOfficeDoTId")
    @Expose
    private Integer doTOfficeDoTId;
    @SerializedName("LicenseAgreementNo")
    @Expose
    private Object licenseAgreementNo;
    @SerializedName("LicenseNoDoTId")
    @Expose
    private Integer licenseNoDoTId;
    @SerializedName("LicenseauthorizationNo")
    @Expose
    private Object licenseauthorizationNo;
    @SerializedName("ServiceYear")
    @Expose
    private Integer serviceYear;
    @SerializedName("ServiceYearDescription")
    @Expose
    private Object serviceYearDescription;
    @SerializedName("RBIAccountNumber")
    @Expose
    private Object rBIAccountNumber;
    @SerializedName("ReceiptAccountNumber")
    @Expose
    private Object receiptAccountNumber;
    @SerializedName("AdditionalChargeDescription")
    @Expose
    private Object additionalChargeDescription;
    @SerializedName("AdditionalChargeId")
    @Expose
    private Integer additionalChargeId;
    @SerializedName("AdditionalCharge")
    @Expose
    private Object additionalCharge;
    @SerializedName("AdditionalAmount")
    @Expose
    private Object additionalAmount;
    @SerializedName("FunctionalHead")
    @Expose
    private Object functionalHead;
    @SerializedName("ContentText")
    @Expose
    private Object contentText;
    @SerializedName("ContentValue")
    @Expose
    private Object contentValue;
    @SerializedName("AggregatorwiseSuccessRatio")
    @Expose
    private Integer aggregatorwiseSuccessRatio;
    @SerializedName("receptid")
    @Expose
    private Object receptid;
    @SerializedName("results")
    @Expose
    private Object results;
    @SerializedName("OrderCode")
    @Expose
    private Object orderCode;
    @SerializedName("AtrNo")
    @Expose
    private Object atrNo;
    @SerializedName("UserTypeId_CBEC")
    @Expose
    private Integer userTypeIdCBEC;
    @SerializedName("CBEC_BRNO")
    @Expose
    private Object cbecBrno;
    @SerializedName("CBEC_String")
    @Expose
    private Object cBECString;
    @SerializedName("CBECReceiptDeptId")
    @Expose
    private Integer cBECReceiptDeptId;
    @SerializedName("IsCBEC")
    @Expose
    private Boolean isCBEC;
    @SerializedName("CBEC_PassportNO")
    @Expose
    private Object cBECPassportNO;
    @SerializedName("CBEC_EDICode_EDIDesc")
    @Expose
    private Object cBECEDICodeEDIDesc;
    @SerializedName("CBEC_Admin")
    @Expose
    private Object cBECAdmin;
    @SerializedName("IsLobaTypeorNot")
    @Expose
    private Boolean isLobaTypeorNot;
    @SerializedName("ResponseURL")
    @Expose
    private Object responseURL;
    @SerializedName("NTRPFinYear")
    @Expose
    private Object nTRPFinYear;
    @SerializedName("Additional_PAOName")
    @Expose
    private Object additionalPAOName;
    @SerializedName("Additional_DDOName")
    @Expose
    private Object additionalDDOName;
    @SerializedName("FuncHeadNo_account")
    @Expose
    private Object funcHeadNoAccount;
    @SerializedName("AdditionalChargeName")
    @Expose
    private Object additionalChargeName;
    @SerializedName("AdditionalChargeAmount")
    @Expose
    private Object additionalChargeAmount;
    @SerializedName("AdditionalChargeCurrency")
    @Expose
    private Object additionalChargeCurrency;
    @SerializedName("ReceiptAdditonalChargesId")
    @Expose
    private Object receiptAdditonalChargesId;
    @SerializedName("Quantity")
    @Expose
    private Integer quantity;
    @SerializedName("ProductName")
    @Expose
    private Object productName;
    @SerializedName("ProductPrice")
    @Expose
    private Integer productPrice;
    @SerializedName("DeliveryCharges")
    @Expose
    private Integer deliveryCharges;
    @SerializedName("DiscountAmount")
    @Expose
    private Integer discountAmount;
    @SerializedName("TotalAmount")
    @Expose
    private Integer totalAmount;
    @SerializedName("ShippingAddress1")
    @Expose
    private Object shippingAddress1;
    @SerializedName("ShippingAddress2")
    @Expose
    private Object shippingAddress2;
    @SerializedName("ShippingCity")
    @Expose
    private Object shippingCity;
    @SerializedName("ShippingDistrictID")
    @Expose
    private Object shippingDistrictID;
    @SerializedName("ShippingDistrict")
    @Expose
    private Object shippingDistrict;
    @SerializedName("ShippingState")
    @Expose
    private Object shippingState;
    @SerializedName("ShippingCountry")
    @Expose
    private Object shippingCountry;
    @SerializedName("ShippingPincode")
    @Expose
    private Object shippingPincode;
    @SerializedName("DeliveryChargesID")
    @Expose
    private Object deliveryChargesID;
    @SerializedName("JsonFormat")
    @Expose
    private Object jsonFormat;
    @SerializedName("AppType")
    @Expose
    private Object appType;
    @SerializedName("TotalRecCount")
    @Expose
    private Integer totalRecCount;
    @SerializedName("PageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("DeliverychargesType")
    @Expose
    private Object deliverychargesType;
    @SerializedName("PaoList")
    @Expose
    private Object paoList;
    @SerializedName("PaymentChannel")
    @Expose
    private Object paymentChannel;
    @SerializedName("UserCharges")
    @Expose
    private Object userCharges;
    @SerializedName("TypeFAQ")
    @Expose
    private Object typeFAQ;
    @SerializedName("UniqueUserDeviceId")
    @Expose
    private String uniqueUserDeviceId;

    public Object getDoTOfficePAO() {
        return doTOfficePAO;
    }

    public void setDoTOfficePAO(Object doTOfficePAO) {
        this.doTOfficePAO = doTOfficePAO;
    }

    public Object getBankUrl() {
        return bankUrl;
    }

    public void setBankUrl(Object bankUrl) {
        this.bankUrl = bankUrl;
    }

    public Object getCompleteEncodedKey() {
        return completeEncodedKey;
    }

    public void setCompleteEncodedKey(Object completeEncodedKey) {
        this.completeEncodedKey = completeEncodedKey;
    }

    public Object getDigitalCertificate() {
        return digitalCertificate;
    }

    public void setDigitalCertificate(Object digitalCertificate) {
        this.digitalCertificate = digitalCertificate;
    }

    public Object getChargesApplicable() {
        return chargesApplicable;
    }

    public void setChargesApplicable(Object chargesApplicable) {
        this.chargesApplicable = chargesApplicable;
    }

    public Integer getIsPosPurpose() {
        return isPosPurpose;
    }

    public void setIsPosPurpose(Integer isPosPurpose) {
        this.isPosPurpose = isPosPurpose;
    }

    public Integer getDepositorId() {
        return depositorId;
    }

    public void setDepositorId(Integer depositorId) {
        this.depositorId = depositorId;
    }

    public Object getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(Object depositorName) {
        this.depositorName = depositorName;
    }

    public Object getTaxDescription() {
        return taxDescription;
    }

    public void setTaxDescription(Object taxDescription) {
        this.taxDescription = taxDescription;
    }

    public Integer getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(Integer taxValue) {
        this.taxValue = taxValue;
    }

    public Integer getTaxID() {
        return taxID;
    }

    public void setTaxID(Integer taxID) {
        this.taxID = taxID;
    }

    public Integer getTaxCalculateValue() {
        return taxCalculateValue;
    }

    public void setTaxCalculateValue(Integer taxCalculateValue) {
        this.taxCalculateValue = taxCalculateValue;
    }

    public Integer getIsCurrecnyconversion() {
        return isCurrecnyconversion;
    }

    public void setIsCurrecnyconversion(Integer isCurrecnyconversion) {
        this.isCurrecnyconversion = isCurrecnyconversion;
    }

    public Object getXMLCurrencycode() {
        return xMLCurrencycode;
    }

    public void setXMLCurrencycode(Object xMLCurrencycode) {
        this.xMLCurrencycode = xMLCurrencycode;
    }

    public Object getXMLAmount() {
        return xMLAmount;
    }

    public void setXMLAmount(Object xMLAmount) {
        this.xMLAmount = xMLAmount;
    }

    public Integer getIsAdditionalCharge() {
        return isAdditionalCharge;
    }

    public void setIsAdditionalCharge(Integer isAdditionalCharge) {
        this.isAdditionalCharge = isAdditionalCharge;
    }

    public Integer getIfExternalIntegration() {
        return ifExternalIntegration;
    }

    public void setIfExternalIntegration(Integer ifExternalIntegration) {
        this.ifExternalIntegration = ifExternalIntegration;
    }

    public Integer getDepositorCategoryId() {
        return depositorCategoryId;
    }

    public void setDepositorCategoryId(Integer depositorCategoryId) {
        this.depositorCategoryId = depositorCategoryId;
    }

    public Integer getFuncHeadNo() {
        return funcHeadNo;
    }

    public void setFuncHeadNo(Integer funcHeadNo) {
        this.funcHeadNo = funcHeadNo;
    }

    public String getFuncHeadDescription() {
        return funcHeadDescription;
    }

    public void setFuncHeadDescription(String funcHeadDescription) {
        this.funcHeadDescription = funcHeadDescription;
    }

    public Object getLobarequestURL() {
        return lobarequestURL;
    }

    public void setLobarequestURL(Object lobarequestURL) {
        this.lobarequestURL = lobarequestURL;
    }

    public Object getAddress1() {
        return address1;
    }

    public void setAddress1(Object address1) {
        this.address1 = address1;
    }

    public Boolean getIsDeleteOfflineSlip() {
        return isDeleteOfflineSlip;
    }

    public void setIsDeleteOfflineSlip(Boolean isDeleteOfflineSlip) {
        this.isDeleteOfflineSlip = isDeleteOfflineSlip;
    }

    public Object getAddress2() {
        return address2;
    }

    public void setAddress2(Object address2) {
        this.address2 = address2;
    }

    public String getAccountShortName() {
        return accountShortName;
    }

    public void setAccountShortName(String accountShortName) {
        this.accountShortName = accountShortName;
    }

    public Object getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(Object serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public Integer getPurposeGroupID() {
        return purposeGroupID;
    }

    public void setPurposeGroupID(Integer purposeGroupID) {
        this.purposeGroupID = purposeGroupID;
    }

    public Object getPurposeGroupDescription() {
        return purposeGroupDescription;
    }

    public void setPurposeGroupDescription(Object purposeGroupDescription) {
        this.purposeGroupDescription = purposeGroupDescription;
    }

    public String getPurposeRemarks() {
        return purposeRemarks;
    }

    public void setPurposeRemarks(String purposeRemarks) {
        this.purposeRemarks = purposeRemarks;
    }

    public Object getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Object modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Object getStateName() {
        return stateName;
    }

    public void setStateName(Object stateName) {
        this.stateName = stateName;
    }

    public Object getSalutations() {
        return salutations;
    }

    public void setSalutations(Object salutations) {
        this.salutations = salutations;
    }

    public Integer getSalutationId() {
        return salutationId;
    }

    public void setSalutationId(Integer salutationId) {
        this.salutationId = salutationId;
    }

    public Integer getUserGroupID() {
        return userGroupID;
    }

    public void setUserGroupID(Integer userGroupID) {
        this.userGroupID = userGroupID;
    }

    public Object getUserGroupDESC() {
        return userGroupDESC;
    }

    public void setUserGroupDESC(Object userGroupDESC) {
        this.userGroupDESC = userGroupDESC;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Object getDistrictName() {
        return districtName;
    }

    public void setDistrictName(Object districtName) {
        this.districtName = districtName;
    }

    public Object getPincode() {
        return pincode;
    }

    public void setPincode(Object pincode) {
        this.pincode = pincode;
    }

    public Object getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(Object mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Object getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(Object panNumber) {
        this.panNumber = panNumber;
    }

    public Object getTanNumber() {
        return tanNumber;
    }

    public void setTanNumber(Object tanNumber) {
        this.tanNumber = tanNumber;
    }

    public Object getAadhaarNumber() {
        return aadhaarNumber;
    }

    public void setAadhaarNumber(Object aadhaarNumber) {
        this.aadhaarNumber = aadhaarNumber;
    }

    public String getPurposeDescription() {
        return purposeDescription;
    }

    public void setPurposeDescription(String purposeDescription) {
        this.purposeDescription = purposeDescription;
    }

    public Integer getControllerId() {
        return controllerId;
    }

    public void setControllerId(Integer controllerId) {
        this.controllerId = controllerId;
    }

    public Object getControllerName() {
        return controllerName;
    }

    public void setControllerName(Object controllerName) {
        this.controllerName = controllerName;
    }

    public Object getBankIntegratedToCPSMS() {
        return bankIntegratedToCPSMS;
    }

    public void setBankIntegratedToCPSMS(Object bankIntegratedToCPSMS) {
        this.bankIntegratedToCPSMS = bankIntegratedToCPSMS;
    }

    public Object getIntegratedToNPCI4Send() {
        return integratedToNPCI4Send;
    }

    public void setIntegratedToNPCI4Send(Object integratedToNPCI4Send) {
        this.integratedToNPCI4Send = integratedToNPCI4Send;
    }

    public String getFunctionHead() {
        return functionHead;
    }

    public void setFunctionHead(String functionHead) {
        this.functionHead = functionHead;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getUTRNoStatus() {
        return uTRNoStatus;
    }

    public void setUTRNoStatus(Object uTRNoStatus) {
        this.uTRNoStatus = uTRNoStatus;
    }

    public Boolean getIsUTRVerified() {
        return isUTRVerified;
    }

    public void setIsUTRVerified(Boolean isUTRVerified) {
        this.isUTRVerified = isUTRVerified;
    }

    public String getNEFTDate() {
        return nEFTDate;
    }

    public void setNEFTDate(String nEFTDate) {
        this.nEFTDate = nEFTDate;
    }

    public Integer getNTRPPurposeId() {
        return nTRPPurposeId;
    }

    public void setNTRPPurposeId(Integer nTRPPurposeId) {
        this.nTRPPurposeId = nTRPPurposeId;
    }

    public Integer getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Integer minAmount) {
        this.minAmount = minAmount;
    }

    public Integer getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getAccountGroupId() {
        return accountGroupId;
    }

    public void setAccountGroupId(Integer accountGroupId) {
        this.accountGroupId = accountGroupId;
    }

    public Integer getReceiptPurposeMinistryMappingID() {
        return receiptPurposeMinistryMappingID;
    }

    public void setReceiptPurposeMinistryMappingID(Integer receiptPurposeMinistryMappingID) {
        this.receiptPurposeMinistryMappingID = receiptPurposeMinistryMappingID;
    }

    public Integer getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(Integer frequencyId) {
        this.frequencyId = frequencyId;
    }

    public Object getFrequencyName() {
        return frequencyName;
    }

    public void setFrequencyName(Object frequencyName) {
        this.frequencyName = frequencyName;
    }

    public String getMinistryDepartment() {
        return ministryDepartment;
    }

    public void setMinistryDepartment(String ministryDepartment) {
        this.ministryDepartment = ministryDepartment;
    }

    public Integer getAggregatorBankId() {
        return aggregatorBankId;
    }

    public void setAggregatorBankId(Integer aggregatorBankId) {
        this.aggregatorBankId = aggregatorBankId;
    }

    public Object getAggregatorBankName() {
        return aggregatorBankName;
    }

    public void setAggregatorBankName(Object aggregatorBankName) {
        this.aggregatorBankName = aggregatorBankName;
    }

    public Integer getCustomFieldTypeId() {
        return customFieldTypeId;
    }

    public void setCustomFieldTypeId(Integer customFieldTypeId) {
        this.customFieldTypeId = customFieldTypeId;
    }

    public Object getFieldTypeName() {
        return fieldTypeName;
    }

    public void setFieldTypeName(Object fieldTypeName) {
        this.fieldTypeName = fieldTypeName;
    }

    public Boolean getHasMasterData() {
        return hasMasterData;
    }

    public void setHasMasterData(Boolean hasMasterData) {
        this.hasMasterData = hasMasterData;
    }

    public String getCreatedClientIP() {
        return createdClientIP;
    }

    public void setCreatedClientIP(String createdClientIP) {
        this.createdClientIP = createdClientIP;
    }

    public Boolean getTypeStatus() {
        return typeStatus;
    }

    public void setTypeStatus(Boolean typeStatus) {
        this.typeStatus = typeStatus;
    }

    public Object getControlText() {
        return controlText;
    }

    public void setControlText(Object controlText) {
        this.controlText = controlText;
    }

    public Integer getControlValue() {
        return controlValue;
    }

    public void setControlValue(Integer controlValue) {
        this.controlValue = controlValue;
    }

    public Object getControlValues() {
        return controlValues;
    }

    public void setControlValues(Object controlValues) {
        this.controlValues = controlValues;
    }

    public Integer getMinistryId() {
        return ministryId;
    }

    public void setMinistryId(Integer ministryId) {
        this.ministryId = ministryId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Object getMajorHeadDescription() {
        return majorHeadDescription;
    }

    public void setMajorHeadDescription(Object majorHeadDescription) {
        this.majorHeadDescription = majorHeadDescription;
    }

    public Object getMajorHeadId() {
        return majorHeadId;
    }

    public void setMajorHeadId(Object majorHeadId) {
        this.majorHeadId = majorHeadId;
    }

    public Object getFunctionHeadId() {
        return functionHeadId;
    }

    public void setFunctionHeadId(Object functionHeadId) {
        this.functionHeadId = functionHeadId;
    }

    public Object getFunctionHeadNo() {
        return functionHeadNo;
    }

    public void setFunctionHeadNo(Object functionHeadNo) {
        this.functionHeadNo = functionHeadNo;
    }

    public Object getFunctionHeadName() {
        return functionHeadName;
    }

    public void setFunctionHeadName(Object functionHeadName) {
        this.functionHeadName = functionHeadName;
    }

    public Integer getDdoid() {
        return ddoid;
    }

    public void setDdoid(Integer ddoid) {
        this.ddoid = ddoid;
    }

    public Integer getAgencyID() {
        return agencyID;
    }

    public void setAgencyID(Integer agencyID) {
        this.agencyID = agencyID;
    }

    public Object getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(Object agencyName) {
        this.agencyName = agencyName;
    }

    public String getObjectHead() {
        return objectHead;
    }

    public void setObjectHead(String objectHead) {
        this.objectHead = objectHead;
    }

    public Integer getPaymentTypeid() {
        return paymentTypeid;
    }

    public void setPaymentTypeid(Integer paymentTypeid) {
        this.paymentTypeid = paymentTypeid;
    }

    public String getPaymentTypeDescription() {
        return paymentTypeDescription;
    }

    public void setPaymentTypeDescription(String paymentTypeDescription) {
        this.paymentTypeDescription = paymentTypeDescription;
    }

    public String getDDOName() {
        return dDOName;
    }

    public void setDDOName(String dDOName) {
        this.dDOName = dDOName;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getAccountingPeriod() {
        return accountingPeriod;
    }

    public void setAccountingPeriod(String accountingPeriod) {
        this.accountingPeriod = accountingPeriod;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPeriodValue() {
        return periodValue;
    }

    public void setPeriodValue(String periodValue) {
        this.periodValue = periodValue;
    }

    public Object getReceiptPeriodFrom() {
        return receiptPeriodFrom;
    }

    public void setReceiptPeriodFrom(Object receiptPeriodFrom) {
        this.receiptPeriodFrom = receiptPeriodFrom;
    }

    public Object getReceiptPeriodTo() {
        return receiptPeriodTo;
    }

    public void setReceiptPeriodTo(Object receiptPeriodTo) {
        this.receiptPeriodTo = receiptPeriodTo;
    }

    public Object getRECDocumentEntities() {
        return rECDocumentEntities;
    }

    public void setRECDocumentEntities(Object rECDocumentEntities) {
        this.rECDocumentEntities = rECDocumentEntities;
    }

    public Integer getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(Integer totalamount) {
        this.totalamount = totalamount;
    }

    public Object getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(Object paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Integer getPaymentModeId() {
        return paymentModeId;
    }

    public void setPaymentModeId(Integer paymentModeId) {
        this.paymentModeId = paymentModeId;
    }

    public Integer getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(Integer receiptNo) {
        this.receiptNo = receiptNo;
    }

    public Integer getReceiptEntryID() {
        return receiptEntryID;
    }

    public void setReceiptEntryID(Integer receiptEntryID) {
        this.receiptEntryID = receiptEntryID;
    }

    public Integer getReceiptEntryDetailID() {
        return receiptEntryDetailID;
    }

    public void setReceiptEntryDetailID(Integer receiptEntryDetailID) {
        this.receiptEntryDetailID = receiptEntryDetailID;
    }

    public Object getTinNumber() {
        return tinNumber;
    }

    public void setTinNumber(Object tinNumber) {
        this.tinNumber = tinNumber;
    }

    public Object getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(Object chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public Integer getAmountValue() {
        return amountValue;
    }

    public void setAmountValue(Integer amountValue) {
        this.amountValue = amountValue;
    }

    public String getDisplayAmount() {
        return displayAmount;
    }

    public void setDisplayAmount(String displayAmount) {
        this.displayAmount = displayAmount;
    }

    public Object getRemarks() {
        return remarks;
    }

    public void setRemarks(Object remarks) {
        this.remarks = remarks;
    }

    public Object getChannelName() {
        return channelName;
    }

    public void setChannelName(Object channelName) {
        this.channelName = channelName;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public Integer getAggregatorID() {
        return aggregatorID;
    }

    public void setAggregatorID(Integer aggregatorID) {
        this.aggregatorID = aggregatorID;
    }

    public Object getAggregatorName() {
        return aggregatorName;
    }

    public void setAggregatorName(Object aggregatorName) {
        this.aggregatorName = aggregatorName;
    }

    public Object getBankName() {
        return bankName;
    }

    public void setBankName(Object bankName) {
        this.bankName = bankName;
    }

    public Object getEncrequestparameter() {
        return encrequestparameter;
    }

    public void setEncrequestparameter(Object encrequestparameter) {
        this.encrequestparameter = encrequestparameter;
    }

    public Object getReqdigitalsignature() {
        return reqdigitalsignature;
    }

    public void setReqdigitalsignature(Object reqdigitalsignature) {
        this.reqdigitalsignature = reqdigitalsignature;
    }

    public Object getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(Object transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public Object getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Object transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Object getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(Object payeeName) {
        this.payeeName = payeeName;
    }

    public Object getReceiptFile() {
        return receiptFile;
    }

    public void setReceiptFile(Object receiptFile) {
        this.receiptFile = receiptFile;
    }

    public Object getChallanFile() {
        return challanFile;
    }

    public void setChallanFile(Object challanFile) {
        this.challanFile = challanFile;
    }

    public Object getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(Object receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public Object getTransactionRefNumber() {
        return transactionRefNumber;
    }

    public void setTransactionRefNumber(Object transactionRefNumber) {
        this.transactionRefNumber = transactionRefNumber;
    }

    public Object getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Object paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Integer getDigitalSignRequired() {
        return digitalSignRequired;
    }

    public void setDigitalSignRequired(Integer digitalSignRequired) {
        this.digitalSignRequired = digitalSignRequired;
    }

    public Integer getPAOId() {
        return pAOId;
    }

    public void setPAOId(Integer pAOId) {
        this.pAOId = pAOId;
    }

    public Integer getActiveInActive() {
        return activeInActive;
    }

    public void setActiveInActive(Integer activeInActive) {
        this.activeInActive = activeInActive;
    }

    public Boolean getActiveArchive() {
        return activeArchive;
    }

    public void setActiveArchive(Boolean activeArchive) {
        this.activeArchive = activeArchive;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Integer getCurrentCurrencyValue() {
        return currentCurrencyValue;
    }

    public void setCurrentCurrencyValue(Integer currentCurrencyValue) {
        this.currentCurrencyValue = currentCurrencyValue;
    }

    public Integer getPreviousCurrencyValue() {
        return previousCurrencyValue;
    }

    public void setPreviousCurrencyValue(Integer previousCurrencyValue) {
        this.previousCurrencyValue = previousCurrencyValue;
    }

    public Object getCurrencyDescription() {
        return currencyDescription;
    }

    public void setCurrencyDescription(Object currencyDescription) {
        this.currencyDescription = currencyDescription;
    }

    public String getPAOName() {
        return pAOName;
    }

    public void setPAOName(String pAOName) {
        this.pAOName = pAOName;
    }

    public Integer getReceiptPrintDocumentId() {
        return receiptPrintDocumentId;
    }

    public void setReceiptPrintDocumentId(Integer receiptPrintDocumentId) {
        this.receiptPrintDocumentId = receiptPrintDocumentId;
    }

    public Object getReceiptPrintDocumentName() {
        return receiptPrintDocumentName;
    }

    public void setReceiptPrintDocumentName(Object receiptPrintDocumentName) {
        this.receiptPrintDocumentName = receiptPrintDocumentName;
    }

    public Object getReportName() {
        return reportName;
    }

    public void setReportName(Object reportName) {
        this.reportName = reportName;
    }

    public Object getReportVersion() {
        return reportVersion;
    }

    public void setReportVersion(Object reportVersion) {
        this.reportVersion = reportVersion;
    }

    public Object getResponseDigitalSignature() {
        return responseDigitalSignature;
    }

    public void setResponseDigitalSignature(Object responseDigitalSignature) {
        this.responseDigitalSignature = responseDigitalSignature;
    }

    public Object getResponseMerchantOrderNo() {
        return responseMerchantOrderNo;
    }

    public void setResponseMerchantOrderNo(Object responseMerchantOrderNo) {
        this.responseMerchantOrderNo = responseMerchantOrderNo;
    }

    public Object getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Object merchantId) {
        this.merchantId = merchantId;
    }

    public Object getAggIdVal() {
        return aggIdVal;
    }

    public void setAggIdVal(Object aggIdVal) {
        this.aggIdVal = aggIdVal;
    }

    public Object getResponseAggregatorRefernceId() {
        return responseAggregatorRefernceId;
    }

    public void setResponseAggregatorRefernceId(Object responseAggregatorRefernceId) {
        this.responseAggregatorRefernceId = responseAggregatorRefernceId;
    }

    public Object getResponseMerchantCustomerID() {
        return responseMerchantCustomerID;
    }

    public void setResponseMerchantCustomerID(Object responseMerchantCustomerID) {
        this.responseMerchantCustomerID = responseMerchantCustomerID;
    }

    public Object getResponseErrorCode() {
        return responseErrorCode;
    }

    public void setResponseErrorCode(Object responseErrorCode) {
        this.responseErrorCode = responseErrorCode;
    }

    public Object getResponseGateWayId() {
        return responseGateWayId;
    }

    public void setResponseGateWayId(Object responseGateWayId) {
        this.responseGateWayId = responseGateWayId;
    }

    public Object getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(Object responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Integer getResponseAmount() {
        return responseAmount;
    }

    public void setResponseAmount(Integer responseAmount) {
        this.responseAmount = responseAmount;
    }

    public Object getResponseCurrency() {
        return responseCurrency;
    }

    public void setResponseCurrency(Object responseCurrency) {
        this.responseCurrency = responseCurrency;
    }

    public Object getCurrencyCreatedDate() {
        return currencyCreatedDate;
    }

    public void setCurrencyCreatedDate(Object currencyCreatedDate) {
        this.currencyCreatedDate = currencyCreatedDate;
    }

    public Object getResponsePayMode() {
        return responsePayMode;
    }

    public void setResponsePayMode(Object responsePayMode) {
        this.responsePayMode = responsePayMode;
    }

    public Object getResponseOtherDetails() {
        return responseOtherDetails;
    }

    public void setResponseOtherDetails(Object responseOtherDetails) {
        this.responseOtherDetails = responseOtherDetails;
    }

    public Object getResponseReason() {
        return responseReason;
    }

    public void setResponseReason(Object responseReason) {
        this.responseReason = responseReason;
    }

    public Object getResponseBankCode() {
        return responseBankCode;
    }

    public void setResponseBankCode(Object responseBankCode) {
        this.responseBankCode = responseBankCode;
    }

    public Object getResponseBankName() {
        return responseBankName;
    }

    public void setResponseBankName(Object responseBankName) {
        this.responseBankName = responseBankName;
    }

    public Object getResponseBankTransactionNumber() {
        return responseBankTransactionNumber;
    }

    public void setResponseBankTransactionNumber(Object responseBankTransactionNumber) {
        this.responseBankTransactionNumber = responseBankTransactionNumber;
    }

    public String getResponseBankTransactionDate() {
        return responseBankTransactionDate;
    }

    public void setResponseBankTransactionDate(String responseBankTransactionDate) {
        this.responseBankTransactionDate = responseBankTransactionDate;
    }

    public Integer getReceiptEntryBankDetailId() {
        return receiptEntryBankDetailId;
    }

    public void setReceiptEntryBankDetailId(Integer receiptEntryBankDetailId) {
        this.receiptEntryBankDetailId = receiptEntryBankDetailId;
    }

    public Object getResponseCIN() {
        return responseCIN;
    }

    public void setResponseCIN(Object responseCIN) {
        this.responseCIN = responseCIN;
    }

    public Boolean getIsNEFT() {
        return isNEFT;
    }

    public void setIsNEFT(Boolean isNEFT) {
        this.isNEFT = isNEFT;
    }

    public Object getAggregatorLogoName() {
        return aggregatorLogoName;
    }

    public void setAggregatorLogoName(Object aggregatorLogoName) {
        this.aggregatorLogoName = aggregatorLogoName;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Object getCountryName() {
        return countryName;
    }

    public void setCountryName(Object countryName) {
        this.countryName = countryName;
    }

    public Object getResponseStatusDescription() {
        return responseStatusDescription;
    }

    public void setResponseStatusDescription(Object responseStatusDescription) {
        this.responseStatusDescription = responseStatusDescription;
    }

    public Object getFName() {
        return fName;
    }

    public void setFName(Object fName) {
        this.fName = fName;
    }

    public Object getMName() {
        return mName;
    }

    public void setMName(Object mName) {
        this.mName = mName;
    }

    public Object getLName() {
        return lName;
    }

    public void setLName(Object lName) {
        this.lName = lName;
    }

    public Object getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(Object contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public Integer getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Integer userTypeId) {
        this.userTypeId = userTypeId;
    }

    public Object getDesignation() {
        return designation;
    }

    public void setDesignation(Object designation) {
        this.designation = designation;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public Object getCountryMobileCode() {
        return countryMobileCode;
    }

    public void setCountryMobileCode(Object countryMobileCode) {
        this.countryMobileCode = countryMobileCode;
    }

    public Object getUsername() {
        return username;
    }

    public void setUsername(Object username) {
        this.username = username;
    }

    public Object getPassword() {
        return password;
    }

    public void setPassword(Object password) {
        this.password = password;
    }

    public Integer getHintQuestionId() {
        return hintQuestionId;
    }

    public void setHintQuestionId(Integer hintQuestionId) {
        this.hintQuestionId = hintQuestionId;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Object getAnswer() {
        return answer;
    }

    public void setAnswer(Object answer) {
        this.answer = answer;
    }

    public Object getClientIPAddress() {
        return clientIPAddress;
    }

    public void setClientIPAddress(Object clientIPAddress) {
        this.clientIPAddress = clientIPAddress;
    }

    public Object getHistoryType() {
        return historyType;
    }

    public void setHistoryType(Object historyType) {
        this.historyType = historyType;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean getRequirePasswordChange() {
        return requirePasswordChange;
    }

    public void setRequirePasswordChange(Boolean requirePasswordChange) {
        this.requirePasswordChange = requirePasswordChange;
    }

    public Integer getEditStatus() {
        return editStatus;
    }

    public void setEditStatus(Integer editStatus) {
        this.editStatus = editStatus;
    }

    public Object getReceiptStatusUrl() {
        return receiptStatusUrl;
    }

    public void setReceiptStatusUrl(Object receiptStatusUrl) {
        this.receiptStatusUrl = receiptStatusUrl;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Object getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(Object responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Object getPlainText() {
        return plainText;
    }

    public void setPlainText(Object plainText) {
        this.plainText = plainText;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public Boolean getVerifySignature() {
        return verifySignature;
    }

    public void setVerifySignature(Boolean verifySignature) {
        this.verifySignature = verifySignature;
    }

    public Object getIFSCCode() {
        return iFSCCode;
    }

    public void setIFSCCode(Object iFSCCode) {
        this.iFSCCode = iFSCCode;
    }

    public Object getBranchName() {
        return branchName;
    }

    public void setBranchName(Object branchName) {
        this.branchName = branchName;
    }

    public Integer getBranchID() {
        return branchID;
    }

    public void setBranchID(Integer branchID) {
        this.branchID = branchID;
    }

    public Object getChallanNo() {
        return challanNo;
    }

    public void setChallanNo(Object challanNo) {
        this.challanNo = challanNo;
    }

    public Object getUtrnNo() {
        return utrnNo;
    }

    public void setUtrnNo(Object utrnNo) {
        this.utrnNo = utrnNo;
    }

    public Object getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Object accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Object getDptReference() {
        return dptReference;
    }

    public void setDptReference(Object dptReference) {
        this.dptReference = dptReference;
    }

    public Object getLicenceRefNum() {
        return licenceRefNum;
    }

    public void setLicenceRefNum(Object licenceRefNum) {
        this.licenceRefNum = licenceRefNum;
    }

    public Object getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(Object encryptionKey) {
        this.encryptionKey = encryptionKey;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Integer getSlabType() {
        return slabType;
    }

    public void setSlabType(Integer slabType) {
        this.slabType = slabType;
    }

    public Object getSlabDescription() {
        return slabDescription;
    }

    public void setSlabDescription(Object slabDescription) {
        this.slabDescription = slabDescription;
    }

    public Integer getSlabMinAmount() {
        return slabMinAmount;
    }

    public void setSlabMinAmount(Integer slabMinAmount) {
        this.slabMinAmount = slabMinAmount;
    }

    public Integer getSlabMaxAmount() {
        return slabMaxAmount;
    }

    public void setSlabMaxAmount(Integer slabMaxAmount) {
        this.slabMaxAmount = slabMaxAmount;
    }

    public Integer getSlabUserCharges() {
        return slabUserCharges;
    }

    public void setSlabUserCharges(Integer slabUserCharges) {
        this.slabUserCharges = slabUserCharges;
    }

    public Integer getIsPercentage() {
        return isPercentage;
    }

    public void setIsPercentage(Integer isPercentage) {
        this.isPercentage = isPercentage;
    }

    public Object getCINNo() {
        return cINNo;
    }

    public void setCINNo(Object cINNo) {
        this.cINNo = cINNo;
    }

    public Object getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(Object departmentName) {
        this.departmentName = departmentName;
    }

    public Object getLOIDate() {
        return lOIDate;
    }

    public void setLOIDate(Object lOIDate) {
        this.lOIDate = lOIDate;
    }

    public Object getComputerizedgeneratedNo() {
        return computerizedgeneratedNo;
    }

    public void setComputerizedgeneratedNo(Object computerizedgeneratedNo) {
        this.computerizedgeneratedNo = computerizedgeneratedNo;
    }

    public Integer getReceiptpaymentTypeId() {
        return receiptpaymentTypeId;
    }

    public void setReceiptpaymentTypeId(Integer receiptpaymentTypeId) {
        this.receiptpaymentTypeId = receiptpaymentTypeId;
    }

    public Object getProductdetails() {
        return productdetails;
    }

    public void setProductdetails(Object productdetails) {
        this.productdetails = productdetails;
    }

    public Integer getValidatedLicenseId() {
        return validatedLicenseId;
    }

    public void setValidatedLicenseId(Integer validatedLicenseId) {
        this.validatedLicenseId = validatedLicenseId;
    }

    public Integer getLicenseSetID() {
        return licenseSetID;
    }

    public void setLicenseSetID(Integer licenseSetID) {
        this.licenseSetID = licenseSetID;
    }

    public Object getLicenseTypeName() {
        return licenseTypeName;
    }

    public void setLicenseTypeName(Object licenseTypeName) {
        this.licenseTypeName = licenseTypeName;
    }

    public Integer getLicenseTypeId() {
        return licenseTypeId;
    }

    public void setLicenseTypeId(Integer licenseTypeId) {
        this.licenseTypeId = licenseTypeId;
    }

    public Integer getServiceArea() {
        return serviceArea;
    }

    public void setServiceArea(Integer serviceArea) {
        this.serviceArea = serviceArea;
    }

    public Object getServiceAreaName() {
        return serviceAreaName;
    }

    public void setServiceAreaName(Object serviceAreaName) {
        this.serviceAreaName = serviceAreaName;
    }

    public Integer getServiceAreaId() {
        return serviceAreaId;
    }

    public void setServiceAreaId(Integer serviceAreaId) {
        this.serviceAreaId = serviceAreaId;
    }

    public Object getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(Object organizationName) {
        this.organizationName = organizationName;
    }

    public Integer getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Integer organizationId) {
        this.organizationId = organizationId;
    }

    public Object getValidationType() {
        return validationType;
    }

    public void setValidationType(Object validationType) {
        this.validationType = validationType;
    }

    public Object getComputerizeGeneratedNo() {
        return computerizeGeneratedNo;
    }

    public void setComputerizeGeneratedNo(Object computerizeGeneratedNo) {
        this.computerizeGeneratedNo = computerizeGeneratedNo;
    }

    public Object getDoTOffice() {
        return doTOffice;
    }

    public void setDoTOffice(Object doTOffice) {
        this.doTOffice = doTOffice;
    }

    public Integer getDoTOfficeDoTId() {
        return doTOfficeDoTId;
    }

    public void setDoTOfficeDoTId(Integer doTOfficeDoTId) {
        this.doTOfficeDoTId = doTOfficeDoTId;
    }

    public Object getLicenseAgreementNo() {
        return licenseAgreementNo;
    }

    public void setLicenseAgreementNo(Object licenseAgreementNo) {
        this.licenseAgreementNo = licenseAgreementNo;
    }

    public Integer getLicenseNoDoTId() {
        return licenseNoDoTId;
    }

    public void setLicenseNoDoTId(Integer licenseNoDoTId) {
        this.licenseNoDoTId = licenseNoDoTId;
    }

    public Object getLicenseauthorizationNo() {
        return licenseauthorizationNo;
    }

    public void setLicenseauthorizationNo(Object licenseauthorizationNo) {
        this.licenseauthorizationNo = licenseauthorizationNo;
    }

    public Integer getServiceYear() {
        return serviceYear;
    }

    public void setServiceYear(Integer serviceYear) {
        this.serviceYear = serviceYear;
    }

    public Object getServiceYearDescription() {
        return serviceYearDescription;
    }

    public void setServiceYearDescription(Object serviceYearDescription) {
        this.serviceYearDescription = serviceYearDescription;
    }

    public Object getRBIAccountNumber() {
        return rBIAccountNumber;
    }

    public void setRBIAccountNumber(Object rBIAccountNumber) {
        this.rBIAccountNumber = rBIAccountNumber;
    }

    public Object getReceiptAccountNumber() {
        return receiptAccountNumber;
    }

    public void setReceiptAccountNumber(Object receiptAccountNumber) {
        this.receiptAccountNumber = receiptAccountNumber;
    }

    public Object getAdditionalChargeDescription() {
        return additionalChargeDescription;
    }

    public void setAdditionalChargeDescription(Object additionalChargeDescription) {
        this.additionalChargeDescription = additionalChargeDescription;
    }

    public Integer getAdditionalChargeId() {
        return additionalChargeId;
    }

    public void setAdditionalChargeId(Integer additionalChargeId) {
        this.additionalChargeId = additionalChargeId;
    }

    public Object getAdditionalCharge() {
        return additionalCharge;
    }

    public void setAdditionalCharge(Object additionalCharge) {
        this.additionalCharge = additionalCharge;
    }

    public Object getAdditionalAmount() {
        return additionalAmount;
    }

    public void setAdditionalAmount(Object additionalAmount) {
        this.additionalAmount = additionalAmount;
    }

    public Object getFunctionalHead() {
        return functionalHead;
    }

    public void setFunctionalHead(Object functionalHead) {
        this.functionalHead = functionalHead;
    }

    public Object getContentText() {
        return contentText;
    }

    public void setContentText(Object contentText) {
        this.contentText = contentText;
    }

    public Object getContentValue() {
        return contentValue;
    }

    public void setContentValue(Object contentValue) {
        this.contentValue = contentValue;
    }

    public Integer getAggregatorwiseSuccessRatio() {
        return aggregatorwiseSuccessRatio;
    }

    public void setAggregatorwiseSuccessRatio(Integer aggregatorwiseSuccessRatio) {
        this.aggregatorwiseSuccessRatio = aggregatorwiseSuccessRatio;
    }

    public Object getReceptid() {
        return receptid;
    }

    public void setReceptid(Object receptid) {
        this.receptid = receptid;
    }

    public Object getResults() {
        return results;
    }

    public void setResults(Object results) {
        this.results = results;
    }

    public Object getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(Object orderCode) {
        this.orderCode = orderCode;
    }

    public Object getAtrNo() {
        return atrNo;
    }

    public void setAtrNo(Object atrNo) {
        this.atrNo = atrNo;
    }

    public Integer getUserTypeIdCBEC() {
        return userTypeIdCBEC;
    }

    public void setUserTypeIdCBEC(Integer userTypeIdCBEC) {
        this.userTypeIdCBEC = userTypeIdCBEC;
    }

    public Object getCbecBrno() {
        return cbecBrno;
    }

    public void setCbecBrno(Object cbecBrno) {
        this.cbecBrno = cbecBrno;
    }

    public Object getCBECString() {
        return cBECString;
    }

    public void setCBECString(Object cBECString) {
        this.cBECString = cBECString;
    }

    public Integer getCBECReceiptDeptId() {
        return cBECReceiptDeptId;
    }

    public void setCBECReceiptDeptId(Integer cBECReceiptDeptId) {
        this.cBECReceiptDeptId = cBECReceiptDeptId;
    }

    public Boolean getIsCBEC() {
        return isCBEC;
    }

    public void setIsCBEC(Boolean isCBEC) {
        this.isCBEC = isCBEC;
    }

    public Object getCBECPassportNO() {
        return cBECPassportNO;
    }

    public void setCBECPassportNO(Object cBECPassportNO) {
        this.cBECPassportNO = cBECPassportNO;
    }

    public Object getCBECEDICodeEDIDesc() {
        return cBECEDICodeEDIDesc;
    }

    public void setCBECEDICodeEDIDesc(Object cBECEDICodeEDIDesc) {
        this.cBECEDICodeEDIDesc = cBECEDICodeEDIDesc;
    }

    public Object getCBECAdmin() {
        return cBECAdmin;
    }

    public void setCBECAdmin(Object cBECAdmin) {
        this.cBECAdmin = cBECAdmin;
    }

    public Boolean getIsLobaTypeorNot() {
        return isLobaTypeorNot;
    }

    public void setIsLobaTypeorNot(Boolean isLobaTypeorNot) {
        this.isLobaTypeorNot = isLobaTypeorNot;
    }

    public Object getResponseURL() {
        return responseURL;
    }

    public void setResponseURL(Object responseURL) {
        this.responseURL = responseURL;
    }

    public Object getNTRPFinYear() {
        return nTRPFinYear;
    }

    public void setNTRPFinYear(Object nTRPFinYear) {
        this.nTRPFinYear = nTRPFinYear;
    }

    public Object getAdditionalPAOName() {
        return additionalPAOName;
    }

    public void setAdditionalPAOName(Object additionalPAOName) {
        this.additionalPAOName = additionalPAOName;
    }

    public Object getAdditionalDDOName() {
        return additionalDDOName;
    }

    public void setAdditionalDDOName(Object additionalDDOName) {
        this.additionalDDOName = additionalDDOName;
    }

    public Object getFuncHeadNoAccount() {
        return funcHeadNoAccount;
    }

    public void setFuncHeadNoAccount(Object funcHeadNoAccount) {
        this.funcHeadNoAccount = funcHeadNoAccount;
    }

    public Object getAdditionalChargeName() {
        return additionalChargeName;
    }

    public void setAdditionalChargeName(Object additionalChargeName) {
        this.additionalChargeName = additionalChargeName;
    }

    public Object getAdditionalChargeAmount() {
        return additionalChargeAmount;
    }

    public void setAdditionalChargeAmount(Object additionalChargeAmount) {
        this.additionalChargeAmount = additionalChargeAmount;
    }

    public Object getAdditionalChargeCurrency() {
        return additionalChargeCurrency;
    }

    public void setAdditionalChargeCurrency(Object additionalChargeCurrency) {
        this.additionalChargeCurrency = additionalChargeCurrency;
    }

    public Object getReceiptAdditonalChargesId() {
        return receiptAdditonalChargesId;
    }

    public void setReceiptAdditonalChargesId(Object receiptAdditonalChargesId) {
        this.receiptAdditonalChargesId = receiptAdditonalChargesId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Object getProductName() {
        return productName;
    }

    public void setProductName(Object productName) {
        this.productName = productName;
    }

    public Integer getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Integer productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(Integer deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public Integer getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Object getShippingAddress1() {
        return shippingAddress1;
    }

    public void setShippingAddress1(Object shippingAddress1) {
        this.shippingAddress1 = shippingAddress1;
    }

    public Object getShippingAddress2() {
        return shippingAddress2;
    }

    public void setShippingAddress2(Object shippingAddress2) {
        this.shippingAddress2 = shippingAddress2;
    }

    public Object getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(Object shippingCity) {
        this.shippingCity = shippingCity;
    }

    public Object getShippingDistrictID() {
        return shippingDistrictID;
    }

    public void setShippingDistrictID(Object shippingDistrictID) {
        this.shippingDistrictID = shippingDistrictID;
    }

    public Object getShippingDistrict() {
        return shippingDistrict;
    }

    public void setShippingDistrict(Object shippingDistrict) {
        this.shippingDistrict = shippingDistrict;
    }

    public Object getShippingState() {
        return shippingState;
    }

    public void setShippingState(Object shippingState) {
        this.shippingState = shippingState;
    }

    public Object getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(Object shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public Object getShippingPincode() {
        return shippingPincode;
    }

    public void setShippingPincode(Object shippingPincode) {
        this.shippingPincode = shippingPincode;
    }

    public Object getDeliveryChargesID() {
        return deliveryChargesID;
    }

    public void setDeliveryChargesID(Object deliveryChargesID) {
        this.deliveryChargesID = deliveryChargesID;
    }

    public Object getJsonFormat() {
        return jsonFormat;
    }

    public void setJsonFormat(Object jsonFormat) {
        this.jsonFormat = jsonFormat;
    }

    public Object getAppType() {
        return appType;
    }

    public void setAppType(Object appType) {
        this.appType = appType;
    }

    public Integer getTotalRecCount() {
        return totalRecCount;
    }

    public void setTotalRecCount(Integer totalRecCount) {
        this.totalRecCount = totalRecCount;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getDeliverychargesType() {
        return deliverychargesType;
    }

    public void setDeliverychargesType(Object deliverychargesType) {
        this.deliverychargesType = deliverychargesType;
    }

    public Object getPaoList() {
        return paoList;
    }

    public void setPaoList(Object paoList) {
        this.paoList = paoList;
    }

    public Object getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(Object paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public Object getUserCharges() {
        return userCharges;
    }

    public void setUserCharges(Object userCharges) {
        this.userCharges = userCharges;
    }

    public Object getTypeFAQ() {
        return typeFAQ;
    }

    public void setTypeFAQ(Object typeFAQ) {
        this.typeFAQ = typeFAQ;
    }

    public String getUniqueUserDeviceId() {
        return uniqueUserDeviceId;
    }

    public void setUniqueUserDeviceId(String uniqueUserDeviceId) {
        this.uniqueUserDeviceId = uniqueUserDeviceId;
    }

}
