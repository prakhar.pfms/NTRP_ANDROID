package testme.myapp.com.ntrp.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import java.util.ArrayList;

import java.util.List;

import testme.myapp.com.ntrp.R;

import testme.myapp.com.ntrp.adapters.MainPageButtonsAdapter;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.activity.DonateToSwachhBharatKoshScreen;
import testme.myapp.com.ntrp.extra.ButtonCards;

import testme.myapp.com.ntrp.extra.RecyclerTouchListener;

import testme.myapp.com.ntrp.module_login.activity.LoginScreen;
import testme.myapp.com.ntrp.module_nonregisteredusers.activity.NonRegisteredUsersScreen;
import testme.myapp.com.ntrp.module_publicationdivisonstore.activity.PublicationDivisionEStoreScreen;
import testme.myapp.com.ntrp.module_trackyourpayment.activity.TrackYourPaymentScreen;



/**
 * Created by deepika.s on 3/24/2017.
 */

public class TypesOfPaymentFragment extends Fragment  {

    private View localView;
    private List<ButtonCards> buttonCardsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MainPageButtonsAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_typesofpayment, container, false);
        localView=view;
        CloseKeyBoard();
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mAdapter = new MainPageButtonsAdapter(buttonCardsList);
        RecyclerView.LayoutManager mLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        prepareButtonsData();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.MyClickListener() {
            @Override
            public void onClick(View view, int position) {
                ButtonCards buttonCards = buttonCardsList.get(position);
                selectClickMethod(position);
                System.out.println("hey="+position);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
              getActivity().finish();
                System.exit(0);
                return false;
            }
        });

        return view;
    }
    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
    private void selectClickMethod(int position) {
        switch (position) {
            case 0:
                click_Event_TrackYourPaymentButton();

                break;
            case 1:
                click_Event_LoginButton();
                break;
            case 2:
                click_Event_NonRegisteredUsersButton();
                break;
            case 3:
                click_Event_DonateToSwachhBharatKoshButton();
                break;
            case 4:
                click_Event_PublicationDivisioneStoreButton();
                break;
        }
    }

    private void prepareButtonsData() {
        if(buttonCardsList.size()>0)
        {
            buttonCardsList.clear();
        }
        ButtonCards buttonCards = new ButtonCards("Track Your Payment/payment history");
        buttonCardsList.add(buttonCards);
        buttonCards = new ButtonCards("Login");
        buttonCardsList.add(buttonCards);
        buttonCards = new ButtonCards("Non-Registered Users");
        buttonCardsList.add(buttonCards);
        buttonCards = new ButtonCards("Donate to Swachh Bharat Kosh");
        buttonCardsList.add(buttonCards);
        buttonCards = new ButtonCards("Publication Division e-Store");
        buttonCardsList.add(buttonCards);
    }
    private void click_Event_TrackYourPaymentButton() {

        System.out.println("helllo");
        Intent intent=new Intent(getActivity(), TrackYourPaymentScreen.class);
        getActivity().startActivity(intent);

    }
    private void click_Event_LoginButton()
    {
        Intent intent=new Intent(getActivity(), LoginScreen.class);
        getActivity().startActivity(intent);
    }

    private void click_Event_DonateToSwachhBharatKoshButton() {

        Intent intent=new Intent(getActivity(), DonateToSwachhBharatKoshScreen.class);
        getActivity().startActivity(intent);
        //getActivity().finish();
    }


    private void click_Event_NonRegisteredUsersButton()
    {
        changeActivity_NonRegisteredUsers();
    }
    private void changeActivity_NonRegisteredUsers() {

        Intent intent=new Intent(getActivity(), NonRegisteredUsersScreen.class);
        getActivity().startActivity(intent);
        getActivity().finish();
    }
    private void click_Event_PublicationDivisioneStoreButton()
    {
        Intent intent=new Intent(getActivity(), PublicationDivisionEStoreScreen.class);
        getActivity().startActivity(intent);
        getActivity().finish();
    }

}
