package testme.myapp.com.ntrp.module_current_receipt_available.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReceiptAvailableDatum {

    @SerializedName("Ministry")
    @Expose
    private String ministry;
    @SerializedName("PAOName")
    @Expose
    private String pAOName;
    @SerializedName("NameofReceipts")
    @Expose
    private String nameofReceipts;
    @SerializedName("ReceiptType")
    @Expose
    private String receiptType;

    public String getMinistry() {
        return ministry;
    }

    public void setMinistry(String ministry) {
        this.ministry = ministry;
    }

    public String getPAOName() {
        return pAOName;
    }

    public void setPAOName(String pAOName) {
        this.pAOName = pAOName;
    }

    public String getNameofReceipts() {
        return nameofReceipts;
    }

    public void setNameofReceipts(String nameofReceipts) {
        this.nameofReceipts = nameofReceipts;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

}
