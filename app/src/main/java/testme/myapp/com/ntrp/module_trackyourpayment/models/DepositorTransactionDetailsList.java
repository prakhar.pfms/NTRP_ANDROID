package testme.myapp.com.ntrp.module_trackyourpayment.models;

/**
 * Created by deepika.s on 27-04-2017.
 */

public class DepositorTransactionDetailsList {
    public String DepositorName;
    public String TransactionRefNumber;

    public String ReceiptEntryID;

    /*public String ChallanNo;
    public String OrderCode;
    public String TransactionNumber;
    public String ReceiptEntryID;
    public String TransactionDate;
    public String PayeeName;
    public String Status;
    public String UtrnNo;
    public String UTRNoStatus;*/
    public String MobileNo;
    //public String PaymentMode;
    public String Email;
   /* public String ReceiptNumber;
    public String AggregatorID;
    public String UserId;
    public String Totalamount;
    public String ReceiptFile;
    public String ChallanFile;
    public String NEFTDate;*/
    public String Address1;
    public String Address2;
    public String TanNumber;
    public String TinNumber;
    //public String AggregatorBankName;
    public String Pincode;
    public String PanNumber;
    public String AadhaarNumber;
    public String StateName;
    public String DistrictName;
   /* public String encrequestparameter;
    public String BankUrl;
    public String AggIdVal;
    public String EncryptionKey;
    public String MerchantId;
    public String AdditionalChargeDescription;
    public String AdditionalChargeId;
    public String ControllerName;
    public String ChannelName;
    public String ResponseBankName;
    public String PurposeDescription;
    public String FuncHeadDescription;
    public String DDOName;*/
    public String PaymentStatus;
    public String CurrencyCode;
}
