package testme.myapp.com.ntrp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import testme.myapp.com.ntrp.R;
import androidx.fragment.app.Fragment;



import butterknife.ButterKnife;

public class AboutUsFragment extends Fragment {




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.aboutus_fragment, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle(this.getActivity().getResources().getString(R.string.menu_about_us));
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle(this.getActivity().getResources().getString(R.string.menu_about_us));
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(this.getActivity().getResources().getString(R.string.menu_about_us));
    }
}
