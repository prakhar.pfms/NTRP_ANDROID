package testme.myapp.com.ntrp.module_publicationdivisonstore.activity;

        import android.os.Bundle;
        import com.google.android.material.tabs.TabLayout;
        import androidx.fragment.app.Fragment;
        import androidx.fragment.app.FragmentManager;
        import androidx.fragment.app.FragmentPagerAdapter;
        import androidx.viewpager.widget.ViewPager;
        import androidx.appcompat.app.AppCompatActivity;
        import androidx.appcompat.widget.Toolbar;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.Window;
        import android.widget.TextView;

        import java.util.ArrayList;
        import java.util.List;

        import testme.myapp.com.ntrp.R;
        import testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.OneFragment;
        import testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.ThreeFragment;
        import testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.TwoFragment;


/**
 * Created by deepika.s on 05-05-2017.
 */

public class PublicationDivisionEStoreScreen extends AppCompatActivity{

        private Toolbar toolbar;
        private TabLayout tabLayout;
        private ViewPager viewPager;
        private View view;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.activity_publicationstore_screen);



            viewPager = (ViewPager) findViewById(R.id.viewpager);
            setupViewPager(viewPager);

            tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
            tabLayout.setupWithViewPager(viewPager);

            //setupTabIcons();

            tabLayout.getSelectedTabPosition();




        }

    private void setupTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("ONE");
        tabOne.setBackgroundResource(R.drawable.unfocusededittext);
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_share, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("TWO");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_share, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("THREE");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_share, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OneFragment(), "MAGAZINE");
        adapter.addFragment(new TwoFragment(), "BOOK");
        adapter.addFragment(new ThreeFragment(), "EMPLOYMENT NEWS");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



}
