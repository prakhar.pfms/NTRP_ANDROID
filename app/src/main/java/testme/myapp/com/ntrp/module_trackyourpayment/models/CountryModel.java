package testme.myapp.com.ntrp.module_trackyourpayment.models;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by prakhar.s on 4/12/2017.
 */

@SuppressLint("ParcelCreator")
public class CountryModel implements Parcelable {

    public List<CountryNameList> CountryList;
    public String RefreshTokenValue;

    public CountryModel(Parcel in) {
        this.CountryList = in.readArrayList(CountryModel.class.getClassLoader());
        this.RefreshTokenValue = in.readString();
    }

    public CountryModel() {

    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(CountryList);
        dest.writeString(RefreshTokenValue);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CountryModel createFromParcel(Parcel in) {
            return new CountryModel(in);
        }

        public CountryModel[] newArray(int size) {
            return new CountryModel[size];
        }
    };
}
