package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MenusModel {
    @SerializedName("@xmlns")
    private String xmlns;
    @SerializedName("Menu")
    private ArrayList<MenuModel> menu;

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public ArrayList<MenuModel> getMenu() {
        return menu;
    }

    public void setMenu(ArrayList<MenuModel> menu) {
        this.menu = menu;
    }
}
