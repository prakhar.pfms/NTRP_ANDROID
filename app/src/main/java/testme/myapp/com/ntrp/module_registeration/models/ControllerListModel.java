package testme.myapp.com.ntrp.module_registeration.models;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by deepika.s on 11-07-2017.
 */

@SuppressLint("ParcelCreator")
public class ControllerListModel implements Parcelable {

    public List<ControllerArrayList> ControllerList;
    public String ResponseStatus;
    public String AcsessTokenValue;
    public String RefreshTokenValue;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
