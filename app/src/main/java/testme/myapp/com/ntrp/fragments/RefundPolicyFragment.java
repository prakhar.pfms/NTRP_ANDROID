package testme.myapp.com.ntrp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.fragment.app.Fragment;

import butterknife.BindView;
import testme.myapp.com.ntrp.R;

import butterknife.ButterKnife;

public class RefundPolicyFragment extends Fragment {

    private String html_content=" <!DOCTYPE html> <html lang=\"en\"> <div class=\"container container-l footer-page-heading\"> <h3 class=\"pageHeading\"> Bharatkosh Chargeback/Refund Policy </h3> <hr> <div class=\"websitePolicy\"> </div> <p><strong>1. Bharatkosh as a policy, does not entertain any chargeback claims barring cases of genuine refunds, as outlined in this policy document. </strong> <br><br> <strong>2.</strong> No chargeback claim shall be entertained for payment made on Bharatkosh portal by any payment gateway/card issuer/bank, once the transaction is successfully credited into the bank account (accredited bank) of the PAO (Pay & Accounts Officer) of the respective ministry/department of Government of India (GoI). A genuine refund shall be processed by the respective department (DDO in Ministry)for whom payment (i.e. the Non Tax receipt deposit) was made online. <br><br> <strong>3. </strong>Bharatkosh users need to agree to the terms and conditions (published on the Bharatkosh portal) before making any payment. These terms and conditions would apply to all refund claims. <br><br> <b> What is Chargeback?</b> <br><br> <strong>4. </strong>Chargeback claims simply means a transaction disputed by the credit/debit cardholder. There are many reasons for chargebacks, the most common being:‘service (for which online payment has already been made) not rendered’;‘fraud’ or; ‘errors (in transaction processing)’. In other words, a chargeback occurs when a cardholder decides to formally dispute a charge on his/her card bill, usually because someone else fraudulently used that card or he/she has not received the service for the payment already made by him through the card. <br><br> <strong>5.</strong> Various Card associations such as VISA & MASTERCARD have formulated detailed guidelines for the purpose of chargeback. Globally accepted reasons for carrying out chargebacks are also enumerated in several documents. <br><br> <strong>Bharatkosh Refund Policy: </strong> <br><br> <strong>5.</strong> Bharatkosh provides a common interface for all central ministries and departments for the collection of non-tax payments and provides a link between payer and department for payment, accounting and reconciliation. In other words, Bharatkosh provides first-level User Interface and payment gateway functionalities to the user and accounting functionality to the respective government department/ministry. It also provides MIS support to the ministry/department. However, designated officials/DDO in the department concerned are to provide purpose-linked services and will be officials who will entertain any request of a refund. <br><br> <strong>6 .</strong>This refund policy applies to those cases where a genuine reason for a refund exists on specific grounds as stated below under scenarios 1 & 2, subject to a proof of debit to the user's account; GAR-6 being available and amount having been credited to PAO’saccount. <br><br> <strong>7. Scenario 1:</strong><br> This is a situation wherein a transaction has been completed by the user, GAR6 has been generated (the user has received confirmatory SMS/email) and funds have been credited to PAO account.However, the service department has not delivered the promised service in the given timeframe. <br> Service delivery timeframes shall be dictated by the Rules & Regulations of the Service Department (Ministry).User needs to be aware of and understand those guidelines before making any payment through Bharatkosh. Bharatkosh will not be liable for any refund claims in this regard. <br><br> <strong>Standard Process for claiming Refund: </strong>User will write to Concerned DDO in the Ministry/Department for claiming a refund. DDO in return will acknowledge the receipt of such request through mail to user. DDO shall carry out the due diligence for necessary evidence/proof from the user and will check the merit of the claim and a decision on refund claim will be taken. <br><br> If Refund claim is accepted by concerned DDO in the service department(Ministry), a refund as per Government Financial Rules (GFR), through generation of financial sanction will be initiated. Concerned DDO will generate a sanction and the respective PAO will make the payment to the user on sanction. Refund would be the sole discretion of the respective service department (DDO in Ministry) and Bharatkosh shall not be held responsible for any refund claims. <div style=\"text-align:center\"> <img alt=\"\" src=\"../App_Themes/Receipt/images/flochartforchargebackandefundpolicy.png\"></div> <br> <strong>8. Scenario 2:</strong><br> This is a situation where the cardholder's Card has been fraudulently used by someone else to procure goods and services - without his knowledge or prior agreement.Further, GAR6 has been generated (the user has received confirmatory SMS/email) and funds have been credited to the PAO account. <br><br> <strong>Standard Process for claiming Refund:</strong> User will write to the Concerned DDO in Ministry (e-Mail address will be available on Bharatkosh)within 15 days of the transaction. DDO in return will acknowledge the receipt of such request through mail to the user and Bharatkosh. DDO shall carry out the due diligence for necessary evidence/proof from the user regarding the fraudulent use of the card and will check the merit of the claim and a decision on refund claim will be taken. It shall be the discretion and decision of the DDO to involve any other authority on law and order in this regard. <br><br> If Refund claim is accepted by the concerned DDO in the service department(Ministry), a refund as per Government Financial Rules (GFR), through generation of financial sanction will be initiated. Concerned DDO will generate a sanction and the respective PAO will make the payment to the user on sanction. Refund would be the sole discretion of the respective service department (DDO in Ministry) and Bharatkosh shall not be held responsible for any refund claims. <br><br> <strong>9. In following cases refund Claims shall not be entertained:-</strong><br> <strong>i) </strong>If Services sought by the User in lieu of payment done, have already been provided to the user.<br> <strong>ii)</strong> User no longer wants to avail services where payment has already been credited to PAO account.<br> <strong>iii)</strong> Failed/aborted/refunded transactions where Receipt (GAR 6)has not been generated and the government account has not been credited.<br> <br> <strong>10.</strong> Agreement between user and Non-Tax Receipt Portal (Bharatkosh) is driven by the Terms and Conditions of Bharatkosh and by accepting/ agreeing to these Terms and Conditions, the User expressly agrees that his/ her use of the aforesaid online payment Service is entirely at own risk and responsibility of the User. Any Legal issues coming out of Refund claims shall be suitably addressed in accordance with Bharatkosh Terms and Conditions.</p> </div> </section> </div> </body> </html>";
    @BindView(R.id.charge_back_web_view)
    public WebView charge_back_web_view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_chargeback, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle("Chargeback/Refund Policy");

        charge_back_web_view.setWebViewClient(new WebViewClient());
        WebSettings set = charge_back_web_view.getSettings();
        set.setJavaScriptEnabled(true);
        set.setBuiltInZoomControls(true);
        charge_back_web_view.loadData(html_content, "text/html", "UTF-8");

        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle("Chargeback/Refund Policy");
    }
}
