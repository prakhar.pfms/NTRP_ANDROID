package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuResponse extends BaseResponse {

    @SerializedName("Menus")
    private MenusModel menus;

    public MenusModel getMenus() {
        return menus;
    }

    public void setMenus(MenusModel menus) {
        this.menus = menus;
    }


}

