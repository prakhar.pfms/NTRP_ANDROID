package testme.myapp.com.ntrp.extra;

/**
 * Created by prakhar.s on 4/12/2017.
 */

public class ButtonCards {
    private String button_Text;
    public ButtonCards(String button_Text)
    {
        this.button_Text=button_Text;
    }

    public String getButton_Text() {
        return button_Text;
    }

    public void setButton_Text(String button_Text) {
        this.button_Text = button_Text;
    }
}
