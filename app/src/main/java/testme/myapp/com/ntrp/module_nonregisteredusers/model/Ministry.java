package testme.myapp.com.ntrp.module_nonregisteredusers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ministry {

    @SerializedName("MinistryId")
    @Expose
    private Integer ministryId;
    @SerializedName("MinistryName")
    @Expose
    private String ministryName;

    public Integer getMinistryId() {
        return ministryId;
    }

    public void setMinistryId(Integer ministryId) {
        this.ministryId = ministryId;
    }

    public String getMinistryName() {
        return ministryName;
    }

    public void setMinistryName(String ministryName) {
        this.ministryName = ministryName;
    }


}
