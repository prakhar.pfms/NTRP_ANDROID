package testme.myapp.com.ntrp.module_trackyourpayment.models;

/**
 * Created by deepika.s on 02-05-2017.
 */

public class DepositDetailsList {
    public String PurposeDescription;
    public String FuncHeadDescription;
    public String DDOName;
    public String AmountValue;
    public String PurposeGroupID;
    public String ControllerName;
    public String PAOName;
    public String AccountingPeriod;
    public String CurrencyCode;
    public String AgencyName;

}
