package testme.myapp.com.ntrp.module_trackyourpayment.activity;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.TrackYourPaymentFragment;


/**
 * Created by deepika.s on 05-05-2017.
 */

public class TrackYourPaymentScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_track_your_payment_screen);
        changeFragment_TrackYourPaymentFragment();
    }
    private void changeFragment_TrackYourPaymentFragment() {

        Fragment trackYourPaymentFragment = new TrackYourPaymentFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_frame, trackYourPaymentFragment);
        ft.commit();
    }
}
