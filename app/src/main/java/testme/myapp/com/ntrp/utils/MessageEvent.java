package testme.myapp.com.ntrp.utils;

import testme.myapp.com.ntrp.module_nonregisteredusers.model.PurposeFunctionalMinisty;

public class MessageEvent {

    public String selectedPurpose, selectedPaymentType,selectedFunctionHead,selectedMinistry;

    public PurposeFunctionalMinisty purposeFunctionalMinisty;

    public MessageEvent() {

    }


    public MessageEvent(String selectedPurpose,String selectedPaymentType, String selectedFunctionHead, String selectedMinistry,PurposeFunctionalMinisty purposeFunctionalMinisty) {
        this.selectedPaymentType = selectedPaymentType;
        this.selectedFunctionHead = selectedFunctionHead;
        this.selectedMinistry = selectedMinistry;
        this.selectedPurpose = selectedPurpose;
        this.purposeFunctionalMinisty= purposeFunctionalMinisty;
    }
}
