package testme.myapp.com.ntrp.utils;

import android.content.Context;
import android.graphics.Color;
import android.widget.Toast;

import com.muddzdev.styleabletoast.StyleableToast;

import java.util.HashMap;
import java.util.Map;

public class RepeatSafeToast {

    private static final int DURATION = 500;
    private static StyleableToast styleableToast;
    private static final Map<Object, Long> lastShown = new HashMap<Object, Long>();

    private static boolean isRecent(Object obj) {
        Long last = lastShown.get(obj);
        if (last == null) {
            return false;
        }
        long now = System.currentTimeMillis();
        if (last + DURATION < now) {
            return false;
        }
        return true;
    }

    /*public static synchronized void show(Context context, int resId) {
        if (isRecent(resId)) {
            return;
        }

        StyleableToast.makeText(context, "Hello World!", Toast.LENGTH_LONG, R.style.mytoast).show();
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
        lastShown.put(resId, System.currentTimeMillis());
    }*/

    public static synchronized void show(Context context, String msg) {
        if (isRecent(msg)) {
            return;
        }
        /*styleableToast = new StyleableToast
                .Builder(context)
                .duration(Toast.LENGTH_SHORT)
                .text(msg)
                .textColor(Color.WHITE)
                .cornerRadius(5)
                .strokeWidth(2)
                .strokeColor(Color.parseColor("#52c378"))
                .backgroundColor(Color.parseColor("#49af6c"))
                .build();*/


        new StyleableToast
                .Builder(context)
                .text(msg)
                .textColor(Color.parseColor("#FFFFFF"))
                .backgroundColor(Color.parseColor("#53C378"))
                .stroke(1,Color.parseColor("#53C378"))
                .length(Toast.LENGTH_SHORT)
                .cornerRadius(1)
                .show();
        if (styleableToast != null) {
            styleableToast.show();
            styleableToast = null;
        }
        lastShown.put(msg, System.currentTimeMillis());
    }
}
