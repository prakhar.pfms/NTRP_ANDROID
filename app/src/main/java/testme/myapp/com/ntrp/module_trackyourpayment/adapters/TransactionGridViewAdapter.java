package testme.myapp.com.ntrp.module_trackyourpayment.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainScreen;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.TransactionGridViewFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TransactionList;
import testme.myapp.com.ntrp.module_trackyourpayment.models.ViewSingleTransactionModel;
import testme.myapp.com.ntrp.utils.AllUrls;
import static com.android.volley.VolleyLog.TAG;

/**
 * Created by deepika.s on 25-04-2017.
 */

public class TransactionGridViewAdapter extends RecyclerView.Adapter<TransactionGridViewAdapter.MyViewHolder> {

    private List<TransactionList> transactionLists;
    private TransactionGridViewFragment fragment;
    private Context context;
   // private boolean IsActive;
    String archiveoractive="";

    public TransactionGridViewAdapter(Context context, TransactionGridViewFragment fragment, List<TransactionList> transactionList,String archiveoractive) {
        this.context = context;
        this.fragment = fragment;
        this.transactionLists = transactionList;
        //this.IsActive=IsActive;
        this.archiveoractive=archiveoractive;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_view_track_ur_payment, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        TransactionList transactionList = transactionLists.get(position);
        final String receiptEntryID = transactionList.getReceiptEntryID().toString();
        final String receiptEntryDetailID= transactionList.getReceiptEntryDetailID().toString();
        final String refno = transactionList.getTransactionRefNumber();
        holder.Date_Textview.setText(transactionList.getTransactionDate());
        holder.Transcation_Ref_No_Textview.setText(transactionList.getTransactionRefNumber());
        holder.Challan_No_Textview.setText(transactionList.getChallanNo());
        holder.Bank_Transcation_No_Textview.setText(transactionList.getTransactionNumber());
        holder.Payee_Name_Textview.setText(transactionList.getPayeeName());
        String totalAmount=transactionList.getTotalamount()+" (INR)";
        holder.Total_Amount_Textview.setText(totalAmount);
        holder.Payment_Initiated_Textview.setText(transactionList.getStatus());
        holder.Utrn_No_Textview.setText(transactionList.getUtrnNo());
        if(transactionList.getOrderCode()==null)
        {
            holder.Order_Code_Textview.setText("");

        }
        else
        {
            holder.Order_Code_Textview.setText(""+transactionList.getOrderCode());

        }



       /* SpannableString str = new SpannableString(transactionList.TransactionRefNumber);
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        holder.Transcation_Ref_No_Textview.setText(str);

        str = new SpannableString("Delete");
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        holder.Delete_Textview.setText(str);
        holder.Delete_Textview.setSelected(true);*/
        holder.Delete_Textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragment.Clicked_Delete_Textview(receiptEntryDetailID, holder.Delete_Textview.getText().toString().matches("Active")? "Are you sure want to Active the selected transaction?" : "Are you sure want to Archive the selected transaction?"   );

            }
        });


        if (transactionList.getActiveArchive())
        {
            holder.Delete_Textview.setText("Active");
        }
        else
        {
            holder.Delete_Textview.setText("Archive");
        }

        /*if(archiveoractive.matches("1"))
        {
            holder.Delete_Textview.setText("Active");

        }
        else
        {
            holder.Delete_Textview.setText("Archive");

        }*/
        if(transactionList.getStatus().matches("Transaction Success"))
        {
            holder.Delete_Textview.setVisibility(View.INVISIBLE);
        }
        else
        {
            holder.Delete_Textview.setVisibility(View.VISIBLE);
            holder.Delete_Textview.setEnabled(true);
        }

        holder.Transcation_Ref_No_Textview.setSelected(true);
        holder.Transcation_Ref_No_Textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("clicked.......textview..");
                getDataFromServer_SingleDepositorTransactionDetails(receiptEntryID, refno);
                //fragment.Clicked_TransactionRefNo(receiptEntryID);
            }
        });

    }

    @Override
    public int getItemCount() {
        return transactionLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView Date_Textview, Transcation_Ref_No_Textview, Challan_No_Textview, Bank_Transcation_No_Textview;
        public TextView Payee_Name_Textview, Total_Amount_Textview, Payment_Initiated_Textview;
        public TextView Utrn_No_Textview;
        public Button Delete_Textview;
        public TextView Know_Current_Status_Textview,Order_Code_Textview;

        public MyViewHolder(View itemView) {
            super(itemView);

            Date_Textview = (TextView) itemView.findViewById(R.id.date_textview);
            Transcation_Ref_No_Textview = (TextView) itemView.findViewById(R.id.transcation_ref_no_textview);
            Challan_No_Textview = (TextView) itemView.findViewById(R.id.challan_no_textview);
            Order_Code_Textview = (TextView) itemView.findViewById(R.id.order_code_textview);

            Bank_Transcation_No_Textview = (TextView) itemView.findViewById(R.id.bank_Transcation_no_textview);
            Payee_Name_Textview = (TextView) itemView.findViewById(R.id.payee_name_textview);
            Total_Amount_Textview = (TextView) itemView.findViewById(R.id.total_amount_textview);
            Payment_Initiated_Textview = (TextView) itemView.findViewById(R.id.payment_initiated_textview);
            Know_Current_Status_Textview = (TextView) itemView.findViewById(R.id.know_current_status_textview);
            Utrn_No_Textview = (TextView) itemView.findViewById(R.id.utrn_no_textview);
            Delete_Textview = (Button) itemView.findViewById(R.id.delete_textview);
            Know_Current_Status_Textview = (TextView) itemView.findViewById(R.id.know_current_status_textview);
            Know_Current_Status_Textview.setVisibility(View.GONE);


        }
    }

    private void getDataFromServer_SingleDepositorTransactionDetails(final String receiptEntryID, final String ref) {

        String tag_json_obj = "json_obj_req";

        String url = AllUrls.getSingleDepositorDetailsTransactionURL;
        final ProgressDialog pDialog = new ProgressDialog(context, R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                int maxLogSize = 5000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v(TAG, response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();

                Gson gson = new GsonBuilder().setLenient().create();
                ViewSingleTransactionModel viewSingleTransactionModel = gson.fromJson(response, ViewSingleTransactionModel.class);
                if (viewSingleTransactionModel.DepositerTransactionDetails == null) {

                } else {
                    MyToken.setToken(viewSingleTransactionModel.RefreshTokenValue);
                    MyToken.setAuthorization(viewSingleTransactionModel.AcsessTokenValue);

                    MainScreen.TrackBundle.putParcelable("viewSingleTransactionModel", viewSingleTransactionModel);
                    fragment.Clicked_TransactionRefNo();
                }

            }
        }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryID", receiptEntryID);
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("TransactionRefNumber", ref);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
}


