package testme.myapp.com.ntrp.module_publicationdivisonstore;

public interface IItemListener<T> {
    void onItemClick(T t, int position);
}
