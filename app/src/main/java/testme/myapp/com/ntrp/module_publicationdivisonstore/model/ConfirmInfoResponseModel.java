package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ConfirmInfoResponseModel {
    @SerializedName("DepositerDetailslst")
    public Object depositerDetailslst;
    @SerializedName("SwachhBharatlst")
    public Object swachhBharatlst;
    @SerializedName("PurposeDetaillst")
    public Object purposeDetaillst;
    @SerializedName("AdditionalChargedetailslst")
    public Object additionalChargedetailslst;
    @SerializedName("ShoppinngDepositorDetails")
    public ArrayList<ShoppinngDepositorDetailModel> shoppinngDepositorDetails;
    @SerializedName("ShoppinngDepositorDetailslst")
    public ArrayList<ShoppinngDepositorDetailModel> shoppinngDepositorDetailslst;
    @SerializedName("LstmodProductDetails")
    public ArrayList<LstmodProductDetailModel> lstmodProductDetails;
    @SerializedName("CbecPurposeslst")
    public Object cbecPurposeslst;
    @SerializedName("CbecDetaillst")
    public Object cbecDetaillst;
    @SerializedName("TotalPurposeAmount")
    public double totalPurposeAmount;
    @SerializedName("TotalAddtionalChangesamount")
    public double totalAddtionalChangesamount;
    @SerializedName("SwachhBharatCurrencyCodeInWords")
    public Object swachhBharatCurrencyCodeInWords;
    @SerializedName("PurposeCurrencyCodeInWords")
    public Object purposeCurrencyCodeInWords;
    @SerializedName("AdditionalchargeCurrencyCodeInWords")
    public Object additionalchargeCurrencyCodeInWords;
    @SerializedName("ConversionPurposeCurrencyCode")
    public Object conversionPurposeCurrencyCode;
    @SerializedName("ConversionAddtionalCurrencyCode")
    public Object conversionAddtionalCurrencyCode;
    @SerializedName("ConversionAddtionalXMLAmount")
    public Object conversionAddtionalXMLAmount;
    @SerializedName("XMLAddtionalCurrencycode")
    public Object xMLAddtionalCurrencycode;
    @SerializedName("XMLCurrencycode")
    public Object xMLCurrencycode;
    @SerializedName("ConversionPurposeXMLAmount")
    public Object conversionPurposeXMLAmount;
    @SerializedName("ErrorCodes")
    public Object errorCodes;
    @SerializedName("ResponseStatus")
    public String responseStatus;
    @SerializedName("XmlDocumentResult")
    public Object xmlDocumentResult;
    @SerializedName("AcsessTokenValue")
    public String acsessTokenValue;
    @SerializedName("RefreshTokenValue")
    public String refreshTokenValue;
    @SerializedName("ResponseEmail")
    public Object responseEmail;
    @SerializedName("ResponseMobileNo")
    public Object responseMobileNo;
    @SerializedName("ResponseUserType_CBEC")
    public Object responseUserType_CBEC;
    @SerializedName("ResponseFirstName")
    public Object responseFirstName;
    @SerializedName("ResponseLastName")
    public Object responseLastName;
    @SerializedName("ResponseCountryCode")
    public Object responseCountryCode;
    @SerializedName("ResponseUserId")
    public int responseUserId;
    @SerializedName("PdfFileByte")
    public Object pdfFileByte;
    @SerializedName("XmlDreports")
    public Object xmlDreports;
    @SerializedName("ReceiptEntryId")
    public int receiptEntryId;
    @SerializedName("ShippingCharges")
    public Object shippingCharges;
    @SerializedName("PubUserType")
    public int pubUserType;
    @SerializedName("DisplayPopup")
    public Object displayPopup;
    @SerializedName("RowNo")
    public Object rowNo;
    @SerializedName("PaymentPurposeID")
    public int paymentPurposeID;
    @SerializedName("TotalDiscount")
    public Object totalDiscount;
    @SerializedName("PageCount")
    public int pageCount;

    public Object getDepositerDetailslst() {
        return depositerDetailslst;
    }

    public void setDepositerDetailslst(Object depositerDetailslst) {
        this.depositerDetailslst = depositerDetailslst;
    }

    public Object getSwachhBharatlst() {
        return swachhBharatlst;
    }

    public void setSwachhBharatlst(Object swachhBharatlst) {
        this.swachhBharatlst = swachhBharatlst;
    }

    public Object getPurposeDetaillst() {
        return purposeDetaillst;
    }

    public void setPurposeDetaillst(Object purposeDetaillst) {
        this.purposeDetaillst = purposeDetaillst;
    }

    public Object getAdditionalChargedetailslst() {
        return additionalChargedetailslst;
    }

    public void setAdditionalChargedetailslst(Object additionalChargedetailslst) {
        this.additionalChargedetailslst = additionalChargedetailslst;
    }

    public ArrayList<ShoppinngDepositorDetailModel> getShoppinngDepositorDetails() {
        return shoppinngDepositorDetails;
    }

    public void setShoppinngDepositorDetails(ArrayList<ShoppinngDepositorDetailModel> shoppinngDepositorDetails) {
        this.shoppinngDepositorDetails = shoppinngDepositorDetails;
    }

    public ArrayList<ShoppinngDepositorDetailModel> getShoppinngDepositorDetailslst() {
        return shoppinngDepositorDetailslst;
    }

    public void setShoppinngDepositorDetailslst(ArrayList<ShoppinngDepositorDetailModel> shoppinngDepositorDetailslst) {
        this.shoppinngDepositorDetailslst = shoppinngDepositorDetailslst;
    }

    public ArrayList<LstmodProductDetailModel> getLstmodProductDetails() {
        return lstmodProductDetails;
    }

    public void setLstmodProductDetails(ArrayList<LstmodProductDetailModel> lstmodProductDetails) {
        this.lstmodProductDetails = lstmodProductDetails;
    }

    public Object getCbecPurposeslst() {
        return cbecPurposeslst;
    }

    public void setCbecPurposeslst(Object cbecPurposeslst) {
        this.cbecPurposeslst = cbecPurposeslst;
    }

    public Object getCbecDetaillst() {
        return cbecDetaillst;
    }

    public void setCbecDetaillst(Object cbecDetaillst) {
        this.cbecDetaillst = cbecDetaillst;
    }

    public double getTotalPurposeAmount() {
        return totalPurposeAmount;
    }

    public void setTotalPurposeAmount(double totalPurposeAmount) {
        this.totalPurposeAmount = totalPurposeAmount;
    }

    public double getTotalAddtionalChangesamount() {
        return totalAddtionalChangesamount;
    }

    public void setTotalAddtionalChangesamount(double totalAddtionalChangesamount) {
        this.totalAddtionalChangesamount = totalAddtionalChangesamount;
    }

    public Object getSwachhBharatCurrencyCodeInWords() {
        return swachhBharatCurrencyCodeInWords;
    }

    public void setSwachhBharatCurrencyCodeInWords(Object swachhBharatCurrencyCodeInWords) {
        this.swachhBharatCurrencyCodeInWords = swachhBharatCurrencyCodeInWords;
    }

    public Object getPurposeCurrencyCodeInWords() {
        return purposeCurrencyCodeInWords;
    }

    public void setPurposeCurrencyCodeInWords(Object purposeCurrencyCodeInWords) {
        this.purposeCurrencyCodeInWords = purposeCurrencyCodeInWords;
    }

    public Object getAdditionalchargeCurrencyCodeInWords() {
        return additionalchargeCurrencyCodeInWords;
    }

    public void setAdditionalchargeCurrencyCodeInWords(Object additionalchargeCurrencyCodeInWords) {
        this.additionalchargeCurrencyCodeInWords = additionalchargeCurrencyCodeInWords;
    }

    public Object getConversionPurposeCurrencyCode() {
        return conversionPurposeCurrencyCode;
    }

    public void setConversionPurposeCurrencyCode(Object conversionPurposeCurrencyCode) {
        this.conversionPurposeCurrencyCode = conversionPurposeCurrencyCode;
    }

    public Object getConversionAddtionalCurrencyCode() {
        return conversionAddtionalCurrencyCode;
    }

    public void setConversionAddtionalCurrencyCode(Object conversionAddtionalCurrencyCode) {
        this.conversionAddtionalCurrencyCode = conversionAddtionalCurrencyCode;
    }

    public Object getConversionAddtionalXMLAmount() {
        return conversionAddtionalXMLAmount;
    }

    public void setConversionAddtionalXMLAmount(Object conversionAddtionalXMLAmount) {
        this.conversionAddtionalXMLAmount = conversionAddtionalXMLAmount;
    }

    public Object getxMLAddtionalCurrencycode() {
        return xMLAddtionalCurrencycode;
    }

    public void setxMLAddtionalCurrencycode(Object xMLAddtionalCurrencycode) {
        this.xMLAddtionalCurrencycode = xMLAddtionalCurrencycode;
    }

    public Object getxMLCurrencycode() {
        return xMLCurrencycode;
    }

    public void setxMLCurrencycode(Object xMLCurrencycode) {
        this.xMLCurrencycode = xMLCurrencycode;
    }

    public Object getConversionPurposeXMLAmount() {
        return conversionPurposeXMLAmount;
    }

    public void setConversionPurposeXMLAmount(Object conversionPurposeXMLAmount) {
        this.conversionPurposeXMLAmount = conversionPurposeXMLAmount;
    }

    public Object getErrorCodes() {
        return errorCodes;
    }

    public void setErrorCodes(Object errorCodes) {
        this.errorCodes = errorCodes;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getXmlDocumentResult() {
        return xmlDocumentResult;
    }

    public void setXmlDocumentResult(Object xmlDocumentResult) {
        this.xmlDocumentResult = xmlDocumentResult;
    }

    public String getAcsessTokenValue() {
        return acsessTokenValue;
    }

    public void setAcsessTokenValue(String acsessTokenValue) {
        this.acsessTokenValue = acsessTokenValue;
    }

    public String getRefreshTokenValue() {
        return refreshTokenValue;
    }

    public void setRefreshTokenValue(String refreshTokenValue) {
        this.refreshTokenValue = refreshTokenValue;
    }

    public Object getResponseEmail() {
        return responseEmail;
    }

    public void setResponseEmail(Object responseEmail) {
        this.responseEmail = responseEmail;
    }

    public Object getResponseMobileNo() {
        return responseMobileNo;
    }

    public void setResponseMobileNo(Object responseMobileNo) {
        this.responseMobileNo = responseMobileNo;
    }

    public Object getResponseUserType_CBEC() {
        return responseUserType_CBEC;
    }

    public void setResponseUserType_CBEC(Object responseUserType_CBEC) {
        this.responseUserType_CBEC = responseUserType_CBEC;
    }

    public Object getResponseFirstName() {
        return responseFirstName;
    }

    public void setResponseFirstName(Object responseFirstName) {
        this.responseFirstName = responseFirstName;
    }

    public Object getResponseLastName() {
        return responseLastName;
    }

    public void setResponseLastName(Object responseLastName) {
        this.responseLastName = responseLastName;
    }

    public Object getResponseCountryCode() {
        return responseCountryCode;
    }

    public void setResponseCountryCode(Object responseCountryCode) {
        this.responseCountryCode = responseCountryCode;
    }

    public int getResponseUserId() {
        return responseUserId;
    }

    public void setResponseUserId(int responseUserId) {
        this.responseUserId = responseUserId;
    }

    public Object getPdfFileByte() {
        return pdfFileByte;
    }

    public void setPdfFileByte(Object pdfFileByte) {
        this.pdfFileByte = pdfFileByte;
    }

    public Object getXmlDreports() {
        return xmlDreports;
    }

    public void setXmlDreports(Object xmlDreports) {
        this.xmlDreports = xmlDreports;
    }

    public int getReceiptEntryId() {
        return receiptEntryId;
    }

    public void setReceiptEntryId(int receiptEntryId) {
        this.receiptEntryId = receiptEntryId;
    }

    public Object getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(Object shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public int getPubUserType() {
        return pubUserType;
    }

    public void setPubUserType(int pubUserType) {
        this.pubUserType = pubUserType;
    }

    public Object getDisplayPopup() {
        return displayPopup;
    }

    public void setDisplayPopup(Object displayPopup) {
        this.displayPopup = displayPopup;
    }

    public Object getRowNo() {
        return rowNo;
    }

    public void setRowNo(Object rowNo) {
        this.rowNo = rowNo;
    }

    public int getPaymentPurposeID() {
        return paymentPurposeID;
    }

    public void setPaymentPurposeID(int paymentPurposeID) {
        this.paymentPurposeID = paymentPurposeID;
    }

    public Object getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Object totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }
}
