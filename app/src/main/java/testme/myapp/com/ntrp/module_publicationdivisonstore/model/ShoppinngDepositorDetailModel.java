package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ShoppinngDepositorDetailModel {
    @SerializedName("LblDepositorDetail2")
    public boolean lblDepositorDetail2;
    @SerializedName("DepositorName")
    public String depositorName;
    @SerializedName("MobileNo")
    public String mobileNo;
    @SerializedName("Address1")
    public String address1;
    @SerializedName("Email")
    public String email;
    @SerializedName("Address2")
    public String address2;
    @SerializedName("City")
    public String city;
    @SerializedName("DistrictName")
    public String districtName;
    @SerializedName("StateName")
    public String stateName;
    @SerializedName("CountryName")
    public String countryName;
    @SerializedName("Pincode")
    public String pincode;
    @SerializedName("ShippingAddress1")
    public String shippingAddress1;
    @SerializedName("ShippingDistrict")
    public String shippingDistrict;
    @SerializedName("ShippingAddress2")
    public String shippingAddress2;
    @SerializedName("ShippingCountry")
    public String shippingCountry;
    @SerializedName("ShippingCity")
    public String shippingCity;
    @SerializedName("ShippingPincode")
    public String shippingPincode;
    @SerializedName("ShippingState")
    public String shippingState;
    public boolean lblShoppingDetails;
    @SerializedName("PaymentMode")
    public String paymentMode;
    @SerializedName("ControllerName")
    public String controllerName;
    @SerializedName("DDOName")
    public String dDOName;
    @SerializedName("PAOName")
    public String pAOName;
    @SerializedName("PurposeDescription")
    public String purposeDescription;
    @SerializedName("ProductName")
    public String productName;
    @SerializedName("ProductPrice")
    public double productPrice;
    @SerializedName("DiscountAmount")
    public double discountAmount;
    @SerializedName("DeliveryCharges")
    public double deliveryCharges;
    @SerializedName("TotalAmount")
    public double totalAmount;
    @SerializedName("DeliveryChargesType")
    public String deliveryChargesType;
    public ArrayList<LstmodProductDetailModel> lstmodProductDetails;

    public ArrayList<LstmodProductDetailModel> getLstmodProductDetails() {
        return lstmodProductDetails;
    }

    public void setLstmodProductDetails(ArrayList<LstmodProductDetailModel> lstmodProductDetails) {
        this.lstmodProductDetails = lstmodProductDetails;
    }

    public boolean isLblDepositorDetail2() {
        return lblDepositorDetail2;
    }

    public void setLblDepositorDetail2(boolean lblDepositorDetail2) {
        this.lblDepositorDetail2 = lblDepositorDetail2;
    }

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getShippingAddress1() {
        return shippingAddress1;
    }

    public void setShippingAddress1(String shippingAddress1) {
        this.shippingAddress1 = shippingAddress1;
    }

    public String getShippingDistrict() {
        return shippingDistrict;
    }

    public void setShippingDistrict(String shippingDistrict) {
        this.shippingDistrict = shippingDistrict;
    }

    public String getShippingAddress2() {
        return shippingAddress2;
    }

    public void setShippingAddress2(String shippingAddress2) {
        this.shippingAddress2 = shippingAddress2;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingPincode() {
        return shippingPincode;
    }

    public void setShippingPincode(String shippingPincode) {
        this.shippingPincode = shippingPincode;
    }

    public String getShippingState() {
        return shippingState;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    public boolean isLblShoppingDetails() {
        return lblShoppingDetails;
    }

    public void setLblShoppingDetails(boolean lblShoppingDetails) {
        this.lblShoppingDetails = lblShoppingDetails;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public String getdDOName() {
        return dDOName;
    }

    public void setdDOName(String dDOName) {
        this.dDOName = dDOName;
    }

    public String getpAOName() {
        return pAOName;
    }

    public void setpAOName(String pAOName) {
        this.pAOName = pAOName;
    }

    public String getPurposeDescription() {
        return purposeDescription;
    }

    public void setPurposeDescription(String purposeDescription) {
        this.purposeDescription = purposeDescription;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public double getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(double deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDeliveryChargesType() {
        return deliveryChargesType;
    }

    public void setDeliveryChargesType(String deliveryChargesType) {
        this.deliveryChargesType = deliveryChargesType;
    }
}
