package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.BankListCorrespondingToSelectedAggregatorModel;

public class BankListAdaptorSpinner extends
        ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private BankListCorrespondingToSelectedAggregatorModel aggregatorListModel;
    private Fragment fragment;
    private Context context;
    private int resource;

    private int lastSelectedPosition = -1;

    public BankListAdaptorSpinner(Context context, int resourcs,Fragment fragment, BankListCorrespondingToSelectedAggregatorModel aggregatorListModel, String AggregatorId) {
        super(context,resourcs);
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.resource=resourcs;


        this.fragment = fragment;
        this.aggregatorListModel = aggregatorListModel;

    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }



    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(resource, parent, false);

       // TextView textViewName = (TextView) view.findViewById(R.id.bank_name_text);
        TextView textViewName = (TextView) view.findViewById(R.id.cust_view);


        textViewName.setText(aggregatorListModel.getAggregatorBankList().get(position).getBankName());





        return view;
    }


}
