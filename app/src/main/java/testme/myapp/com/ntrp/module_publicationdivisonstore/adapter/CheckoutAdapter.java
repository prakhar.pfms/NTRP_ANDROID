package testme.myapp.com.ntrp.module_publicationdivisonstore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_publicationdivisonstore.IItemListener;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ProductCartListModel;
import testme.myapp.com.ntrp.utils.AllUrls;

public class CheckoutAdapter extends RecyclerView.Adapter<CheckoutAdapter.MyViewHolder> {
    private List<ProductCartListModel> mList;
    private IItemListener<ProductCartListModel> iItemListener;
    private final Context mContext;

    public CheckoutAdapter(Context mContext, List<ProductCartListModel> mList, IItemListener<ProductCartListModel> iItemListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.iItemListener = iItemListener;
    }

    public void setData(List<ProductCartListModel> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CheckoutAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.checkout_item_row, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull final CheckoutAdapter.MyViewHolder holder, int position) {
        holder.tvName.setText(mList.get(position).getProductName());
        holder.tvPrice.setText("₹ " + mList.get(position).getItemPrice() + "");
        holder.tvQty.setText(mList.get(position).getTotalQty() + "");
        //double subTotal = mList.get(position).getQty() * mList.get(position).getPrice();
        holder.tvSubTotal.setText("₹ " + mList.get(position).getTotalPrice());
        Picasso.with(mContext)
                .load(AllUrls.imagePath + mList.get(position).getProductImage())
                .resize(350, 450)
                .placeholder(R.drawable.book_placeholder)
                .error(R.drawable.book_placeholder)
                .into(holder.ivImg);
        holder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iItemListener.onItemClick(mList.get(holder.getAdapterPosition()), 1);
            }
        });
        holder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iItemListener.onItemClick(mList.get(holder.getAdapterPosition()), 2);
            }
        });
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iItemListener.onItemClick(mList.get(holder.getAdapterPosition()), 3);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvPrice, tvSubTotal, tvLanguage, tvIssueYear, tvQty;
        public ImageView ivImg, ivDelete;
        public Button btnMinus, btnAdd;

        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvPrice = view.findViewById(R.id.tvPrice);
            tvSubTotal = view.findViewById(R.id.tvSubTotal);
            ivImg = view.findViewById(R.id.ivImg);
            tvLanguage = view.findViewById(R.id.tvLanguage);
            tvIssueYear = view.findViewById(R.id.tvIssueYear);
            tvQty = view.findViewById(R.id.tvQty);
            btnMinus = view.findViewById(R.id.btnMinus);
            btnAdd = view.findViewById(R.id.btnAdd);
            ivDelete = view.findViewById(R.id.ivDelete);

        }
    }


}
