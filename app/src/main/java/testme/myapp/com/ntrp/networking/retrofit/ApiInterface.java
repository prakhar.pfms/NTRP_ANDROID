package testme.myapp.com.ntrp.networking.retrofit;

import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.PurposeMinistryFunctionHeadListModel;

public interface ApiInterface {

        // annotation that used with POST type request
    @POST("/ntrpmobileapi/api/NonRegisterUser/GetPurposeMinistryFunctionHeadList") // specify the sub url for our base url
    @FormUrlEncoded
    public void GetPurposeMinistryFunctionHeadList(
            @Header("Content-Type") String appJson,
            @Header("Authorization") String Bearer,
            @Query("RefreshTokenValue") String RefreshTokenValue,
            @Query("ControllerName") String ControllerName,
            @Query("PurposeName") String PurposeName,
            @Query("FunctionHeadNo") String FunctionHeadNo,
            @Query("pageIndex") String pageIndex,
            @Query("userId") String userId,

            @Query("flag") String flag, Callback<PurposeMinistryFunctionHeadListModel> purposeMinistryFunctionHeadListModelCallback);
           // Callback<PurposeMinistryFunctionHeadListModel> purposeMinistryFunctionHeadListModelCallback);

}
