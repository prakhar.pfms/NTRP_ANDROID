package testme.myapp.com.ntrp.module_trackyourpayment.models;

/**
 * Created by prakhar.s on 8/3/2017.
 */

public class AllProductCarts {

    public String TotalQty;

    public String ItemPrice;

    public String TotalPrice;

    public String ProductImage;

    public String ProductName;

    public String DeliveryCharges;

    public String ProductIssues;

    public String ProductAttribute;

    public String ProductLanguage;

    public String ProductCountry;

    public String TotalAmount;

    public String TotalGrandAmount;

    public String DeliveryType;

    public String ProductShippingAmount;

    public String TotalPriceShippingChargesSum;

}
