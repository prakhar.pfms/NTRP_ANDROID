package testme.myapp.com.ntrp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.events.MakeUserLogout;
import testme.myapp.com.ntrp.module_commonreceipts.fragments.CommonReceiptsFragment;
import testme.myapp.com.ntrp.module_contribution_to_ndrf.fragments.ContributionToNDRFFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.DonateToSwachhBharatKoshFragment;
import testme.myapp.com.ntrp.module_login.events.UserLoginEvent;
import testme.myapp.com.ntrp.module_login.fragments.LoginFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsersFragment;
import testme.myapp.com.ntrp.module_publicationdivisonstore.fragments.PublicationFragment;
import testme.myapp.com.ntrp.module_quickpayment.fragments.QuickPaymentFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.TrackYourPaymentFragment;

public class NewHomeFragmentTwo extends Fragment {

    @BindView(R.id.bharat_kosh_logo)
    ImageView bharat_kosh_logo;

    @BindView(R.id.login_linear_layout)
    RelativeLayout login_linear_layout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_home_page, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle(this.getActivity().getResources().getString(R.string.menu_home));

        Picasso.with(this.getContext()).load(R.drawable.bharatkoshlogo).into(bharat_kosh_logo);
        CheckUserLogin();
        return view;
    }
    private void CheckUserLogin() {



        if (MainActivity.IsLogin)
        {
            login_linear_layout.setVisibility(View.GONE);

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle(this.getActivity().getResources().getString(R.string.menu_home));
    }

    @OnClick(R.id.login_linear_layout)
    public void go_To_LoginScreen() {

        LoginFragment newFragment = new LoginFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    @OnClick(R.id.track_your_payment_icon_layout)
    public void go_To_TrackYourPayment() {

        TrackYourPaymentFragment newFragment = new TrackYourPaymentFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @OnClick(R.id.swachh_bharat_kosh_layout)
    public void go_To_SwacchBharatKosh() {

        DonateToSwachhBharatKoshFragment newFragment = new DonateToSwachhBharatKoshFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    @OnClick(R.id.non_register_user_linear_layout)
    public void go_To_NonRegisteredUserPage() {

        NonRegisteredUsersFragment newFragment = new NonRegisteredUsersFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @OnClick(R.id.ndrf_layout)
    public void go_To_NDRFPage() {

        ContributionToNDRFFragment newFragment = new ContributionToNDRFFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    @OnClick(R.id.common_receipt_goi_layout)
    public void go_To_CommonReceiptPage() {

        CommonReceiptsFragment newFragment = new CommonReceiptsFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    //Todo: Bhaskar Joshi
    private void callEStore(String type) {
        Bundle args = new Bundle();
        args.putString("type",type);
        PublicationFragment newFragment = new PublicationFragment();
        newFragment.setArguments(args);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    


    @OnClick(R.id.quick_payment_layout)
    public void callQuickPayment() {
        QuickPaymentFragment newFragment = new QuickPaymentFragment();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        transaction.replace(R.id.container_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @OnClick(R.id.publication_divison_layout)
    public void callPublicationDivision() {
        PublicationFragment.pTitle="Publications Division Ministry of I & B";
        callEStore("3");
    }

    @OnClick(R.id.ministry_of_law_justice_layout)
    public void callLawAndJustice() {
        PublicationFragment.pTitle="Min. of Law & Justice";
        callEStore("4");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserLoginEvent(UserLoginEvent event) {

        login_linear_layout.setVisibility(View.GONE);


    }


}
