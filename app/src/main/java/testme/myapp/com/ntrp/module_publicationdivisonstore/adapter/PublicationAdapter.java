package testme.myapp.com.ntrp.module_publicationdivisonstore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_publicationdivisonstore.IItemListener;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ProductListModel;
import testme.myapp.com.ntrp.utils.AllUrls;

public class PublicationAdapter extends RecyclerView.Adapter<PublicationAdapter.MyViewHolder> {
    private List<ProductListModel> mList;
    private IItemListener<ProductListModel> iItemListener;
    private final Context mContext;

    public PublicationAdapter(Context mContext, List<ProductListModel> mList, IItemListener<ProductListModel> iItemListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.iItemListener = iItemListener;
    }

    public void setData(List<ProductListModel> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PublicationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.publication_item_row, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull final PublicationAdapter.MyViewHolder holder, int position) {
        holder.tvName.setText(mList.get(position).getProductName());
        holder.tvPrice.setText("₹ " + mList.get(position).getDisplayProductPrice() + "");
        if (mList.get(position).getOutOfStock().equalsIgnoreCase("0")) {
            holder.tvInStock.setText("Available");
            holder.tvInStock.setTextColor(mContext.getResources().getColor(R.color.dark_green));
        } else {
            holder.tvInStock.setText("Out of Stock");
            holder.tvInStock.setTextColor(mContext.getResources().getColor(R.color.red));
        }

        Picasso.with(mContext)
                .load(AllUrls.imagePath + mList.get(position).getProductImage())
                .resize(350, 450)
                .placeholder(R.drawable.book_placeholder)
                .error(R.drawable.book_placeholder)
                .into(holder.ivImg);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iItemListener.onItemClick(mList.get(holder.getAdapterPosition()), holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvPrice, tvInStock;
        public ImageView ivImg;

        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvPrice = view.findViewById(R.id.tvPrice);
            tvInStock = view.findViewById(R.id.tvInStock);
            ivImg = view.findViewById(R.id.ivImg);

        }
    }


}
