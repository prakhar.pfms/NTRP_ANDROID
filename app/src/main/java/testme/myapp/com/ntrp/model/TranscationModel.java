package testme.myapp.com.ntrp.model;

/**
 * Created by deepika.s on 24-04-2017.
 */

public class TranscationModel {
    public String Date;
    public String TransactionNumber;
    public String ChallanNo;
    public String ResponseBankTransactionNumber;
    public String PayeeName;
}
