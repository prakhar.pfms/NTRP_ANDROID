package testme.myapp.com.ntrp.module_publicationdivisonstore.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.AttributeGetDetailListModel;

public class AttributeSpinnerAdapter extends ArrayAdapter<AttributeGetDetailListModel> {
    private Context mContext;
    private ArrayList<AttributeGetDetailListModel> mList;

    public AttributeSpinnerAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public AttributeSpinnerAdapter(@NonNull Context context, int resource, ArrayList<AttributeGetDetailListModel> mList) {
        super(context, resource, mList);
        this.mContext = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public AttributeGetDetailListModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setTextColor(mContext.getResources().getColor(R.color.black));
        label.setTextSize(18);
        label.setPadding(30, 2, 30, 2);
        label.setText(mList.get(position).getAttributeValue());
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = new TextView(mContext);
        label.setTextColor(mContext.getResources().getColor(R.color.black));
        label.setTextSize(18);
        label.setPadding(40, 10, 40, 10);
        label.setText(mList.get(position).getAttributeValue());
        label.setTag(mList.get(position).getAttributeId());

        return label;
    }
}
