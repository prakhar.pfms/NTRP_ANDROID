package testme.myapp.com.ntrp.module_publicationdivisonstore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.LstmodProductDetailModel;

public class ProductDetailsConfirmInfoAdapter extends RecyclerView.Adapter<ProductDetailsConfirmInfoAdapter.MyViewHolder> {
    private ArrayList<LstmodProductDetailModel> mList;
    private final Context mContext;

    public ProductDetailsConfirmInfoAdapter(Context mContext, ArrayList<LstmodProductDetailModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public void setData(ArrayList<LstmodProductDetailModel> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductDetailsConfirmInfoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.product_details_confirm_info_item_row, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull final ProductDetailsConfirmInfoAdapter.MyViewHolder holder, int position) {
        holder.tvProduct.setText(mList.get(position).getProductName());
        holder.tvPrice.setText("" + mList.get(position).getProductPrice());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvProduct, tvPrice;

        public MyViewHolder(View view) {
            super(view);
            tvProduct = view.findViewById(R.id.tvProduct);
            tvPrice = view.findViewById(R.id.tvPrice);
        }
    }


}
