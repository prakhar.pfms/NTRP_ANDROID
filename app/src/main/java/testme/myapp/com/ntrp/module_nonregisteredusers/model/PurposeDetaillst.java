package testme.myapp.com.ntrp.module_nonregisteredusers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PurposeDetaillst {

    @SerializedName("ControllerName")
    @Expose
    private String controllerName;
    @SerializedName("PAOName")
    @Expose
    private String pAOName;
    @SerializedName("DDOName")
    @Expose
    private String dDOName;
    @SerializedName("PurposeDescription")
    @Expose
    private String purposeDescription;
    @SerializedName("AccountingPeriod")
    @Expose
    private String accountingPeriod;
    @SerializedName("AmountValue")
    @Expose
    private Double amountValue;
    @SerializedName("XMLAmount")
    @Expose
    private String xMLAmount;
    @SerializedName("AdditionalChargeAmount")
    @Expose
    private Double additionalChargeAmount;
    @SerializedName("XMLCurrencycode")
    @Expose
    private String xMLCurrencycode;
    @SerializedName("CurrencyCode")
    @Expose
    private String currencyCode;
    @SerializedName("DisplayAmount")
    @Expose
    private Object displayAmount;
    @SerializedName("Totalamount")
    @Expose
    private Double totalamount;

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public String getPAOName() {
        return pAOName;
    }

    public void setPAOName(String pAOName) {
        this.pAOName = pAOName;
    }

    public String getDDOName() {
        return dDOName;
    }

    public void setDDOName(String dDOName) {
        this.dDOName = dDOName;
    }

    public String getPurposeDescription() {
        return purposeDescription;
    }

    public void setPurposeDescription(String purposeDescription) {
        this.purposeDescription = purposeDescription;
    }

    public String getAccountingPeriod() {
        return accountingPeriod;
    }

    public void setAccountingPeriod(String accountingPeriod) {
        this.accountingPeriod = accountingPeriod;
    }

    public Double getAmountValue() {
        return amountValue;
    }

    public void setAmountValue(Double amountValue) {
        this.amountValue = amountValue;
    }

    public String getXMLAmount() {
        return xMLAmount;
    }

    public void setXMLAmount(String xMLAmount) {
        this.xMLAmount = xMLAmount;
    }

    public Double getAdditionalChargeAmount() {
        return additionalChargeAmount;
    }

    public void setAdditionalChargeAmount(Double additionalChargeAmount) {
        this.additionalChargeAmount = additionalChargeAmount;
    }

    public String getXMLCurrencycode() {
        return xMLCurrencycode;
    }

    public void setXMLCurrencycode(String xMLCurrencycode) {
        this.xMLCurrencycode = xMLCurrencycode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Object getDisplayAmount() {
        return displayAmount;
    }

    public void setDisplayAmount(Object displayAmount) {
        this.displayAmount = displayAmount;
    }

    public Double getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(Double totalamount) {
        this.totalamount = totalamount;
    }
}
