package testme.myapp.com.ntrp.module_trackyourpayment.adapters;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.DepositorTransactionDetailsFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.OpenPdfUrlFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.TransactionGridViewFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.models.PDFDownloadModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.PdfModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.ViewSingleTransactionModel;
import testme.myapp.com.ntrp.module_trackyourpayment.utils.GetPdfTask;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.utils.AllUrls;


/**
 * Created by prakhar.s on 12/23/2016.
 */

public class PDFAdaptor extends RecyclerView.Adapter<PDFAdaptor.ProductCartModelViewHolder> {

    private List<PdfModel> data;
    private Context ctx;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS =180 ;
    private DepositorTransactionDetailsFragment fragment;
    private String ReceiptEntryId;

    public PDFAdaptor(String ReceiptEntryId,List<PdfModel> data, Context ctx,DepositorTransactionDetailsFragment fragment) {
        this.data = data;
        this.fragment=fragment;
        this.ctx=ctx;
        this.ReceiptEntryId=ReceiptEntryId;
    }


    @Override
    public ProductCartModelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.single_view_pdfdata, parent, false);

        return new ProductCartModelViewHolder(itemView);
    }




    @Override
    public void onBindViewHolder(ProductCartModelViewHolder holder, int i) {

        final PdfModel pdfData = data.get(i);






        holder.ReportName.setText(pdfData.DisplayName);

        holder.pdfDownloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                showDialog_False_OTP_Error("Do You want to download File ?",pdfData.PdfURL,pdfData.ReportName);



            }
        });

    }

    public void showDialog_False_OTP_Error(String warningText, final String PdfURL, final String ReportName) {

        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_delete_records);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.dialogTextMsg);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

//                OpenPdfUrlFragment transactionGridViewFragment = new OpenPdfUrlFragment();
//                transactionGridViewFragment.Url="https://training.pfms.gov.in/Bharatkosh/"+PdfURL;
//                FragmentTransaction ft = fragment.getActivity().getSupportFragmentManager().beginTransaction();
//                ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
//                ft.replace(R.id.container_frame, transactionGridViewFragment);
//                ft.commit();

             //   new GetPdfTask(ctx,"https://training.pfms.gov.in/Bharatkosh/"+PdfURL,ReportName,fragment).execute();

                String[] parts1 = PdfURL.split("\\?");



                Hit_API_PDF_Download(AllUrls.SERVER_ADDRESS+"NTRPMobileApi/API/NTRP/GetPDFFile?"+parts1[1],fragment.getActivity(),fragment);

               // askPermission(AllUrls.SERVER_ADDRESS+"NTRPMobileApi/API/NTRP/GetPDFFile?"+parts1[1],fragment.getActivity(),fragment);

            }
        });

        Button dialogCancelButton = (Button) dialog.findViewById(R.id.cancelbutton);
        dialogCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        Button crossbutton = (Button) dialog.findViewById(R.id.crossbutton);
        crossbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }




    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
    }

    public void Hit_API_PDF_Download(String pdfUrl,final Activity activity, final Fragment trackYourPaymentFragment) {

        System.out.println("Verify Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = pdfUrl;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();


                Gson gson = new GsonBuilder().setLenient().create();
                PDFDownloadModel pdfDownloadModel = gson.fromJson(response, PDFDownloadModel.class);

                if (pdfDownloadModel.getPdfFileByte()!=null)
                {
                    try {
                        WriteBtn(Base64.decode(pdfDownloadModel.getPdfFileByte(), Base64.DEFAULT));
                    } catch (Exception e) {
                      //  e.printStackTrace();
                    }
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               // params.put("ReceiptEntryId", ReceiptEntryId);
               // params.put("IsMobileApi", "1");
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        strReq.setRetryPolicy(new DefaultRetryPolicy(
//                0,
//                0,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    private void WriteBtn( byte[] pdfBytes) throws Exception {



        File root = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            root = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        }
        File dir = new File(root.getAbsolutePath());
        dir.mkdirs();
        File file = new File(dir, "NTRP"+"_"+ SystemClock.currentThreadTimeMillis()+".pdf");

        try {
            FileOutputStream f = new FileOutputStream(file);


            byte[] arr=pdfBytes;
            f.write(arr);
            f.flush();
            f.close();
            MediaScannerConnection.scanFile(
                    ctx,
                    new String[]{file.getAbsolutePath()},
                    null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.v("grokkingandroid",
                                    "file " + path + " was scanned seccessfully: " + uri);
                           String DownloadedPath=path+uri;

                            showDialog("File downloaded successfully to "+DownloadedPath);

                        }
                    });

        } catch (FileNotFoundException e) {
          //  e.printStackTrace();
            android.util.Log
                    .i("tag",
                            "******* File not found. Did you add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } catch (IOException e) {
          //  e.printStackTrace();
        }





    }

    private void askPermission(String url,Activity activity,Fragment fragment)
    {
        if (ContextCompat.checkSelfPermission(fragment.getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(fragment.getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);

        }
        else if(ContextCompat.checkSelfPermission(fragment.getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED)
        {
            Hit_API_PDF_Download(url,activity,fragment);

        }

    }


    private void showDialog(String warningText) {

        final Dialog dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_success);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }
    protected class ProductCartModelViewHolder extends RecyclerView.ViewHolder {

        protected Button pdfDownloadButton;
        protected TextView ReportName;


        protected ProductCartModelViewHolder(View v) {
            super(v);
            pdfDownloadButton =  (Button) v.findViewById(R.id.download_pdf_button);
            ReportName =  (TextView) v.findViewById(R.id.receiptname_textview);

        }

    }

}
