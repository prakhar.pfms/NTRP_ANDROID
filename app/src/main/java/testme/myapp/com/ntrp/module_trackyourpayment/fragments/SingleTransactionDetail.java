package testme.myapp.com.ntrp.module_trackyourpayment.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import testme.myapp.com.ntrp.R;

/**
 * Created by deepika.s on 11-05-2017.
 */

public class SingleTransactionDetail extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_single_transaction_detail, container, false);
        initializeView(view);

        return view;
    }
    private void initializeView(View view) {

    }

}
