package testme.myapp.com.ntrp.activity;

import android.app.Dialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.fragments.TypesOfPaymentFragment;
import testme.myapp.com.ntrp.module_login.events.AskMobileNumberDialogEvent;
import testme.myapp.com.ntrp.module_trackyourpayment.events.PutUserInputEvent;
import testme.myapp.com.ntrp.module_trackyourpayment.models.UserInputDetailModel;

public class MainScreen extends AppCompatActivity {


    public static Bundle TrackBundle = new Bundle();
    public static Bundle UserInputBundle = new Bundle();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_screen);
        put_UserInputData("null","null","null");
        changeFragment_TypesOfPaymentFragment();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {

        final Dialog dialog = new Dialog(MainScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.exit_dialog);
        dialog.setCancelable(false);
        dialog.show();

        Button dialog_yes_button = (Button) dialog.findViewById(R.id.dialog_yes_button);
        dialog_yes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                dialog.dismiss();
                System.exit(0);
            }
        });
        Button dialog_no_button = (Button) dialog.findViewById(R.id.dialog_no_button);
        dialog_no_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }
    private void put_UserInputData(String countryCode,String mobileno,String emailID)
    {
        UserInputDetailModel userInputDetailModel=new UserInputDetailModel();
        userInputDetailModel.CountryCode=null;
        userInputDetailModel.MobileNumber=null;
        userInputDetailModel.EmailID=null;
        UserInputBundle.putParcelable("userInputDetailModel",userInputDetailModel);
    }
    private void changeFragment_TypesOfPaymentFragment() {

        Fragment typesOfPaymentFragment = new TypesOfPaymentFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_frame, typesOfPaymentFragment);
        ft.commit();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAskMobileNumberEvent(PutUserInputEvent event) {

        put_UserInputData(event.CountryCode,event.MobileNumber,event.EmailID);

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}
