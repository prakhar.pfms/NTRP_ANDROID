package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.adaptors.AggregatorAdaptor;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.adaptors.BankListAdaptor;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.adaptors.BankListAdaptorSpinner;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.adaptors.ChannelListAdaptor;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.Aggregator;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.AggregatorListModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.BankListCorrespondingToSelectedAggregatorModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.ChannelListModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.Country;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.CountryModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.PaymentDetailsModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.PaymentWebViewModel;
import testme.myapp.com.ntrp.module_trackyourpayment.HitAPI;
import testme.myapp.com.ntrp.module_trackyourpayment.adapters.TransactionGridViewAdapter;
import testme.myapp.com.ntrp.module_trackyourpayment.fragments.TrackYourPaymentFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.models.SendOTPModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

public class PaymentFragment extends Fragment implements View.OnClickListener{

    private View localView;
    private Button pay_button;
    private Button netBankingButton,debitCardButton,creditCardButton,UPIButton;
    private RecyclerView recyclerView,grid_recyclerview_bank_list,recyclerview_debitcard_paymentmode,recyclerview_creditcard_paymentmode,recyclerview_upi_paymentmode;
    private Spinner bankListSpinner;
    private String Aggregatorid;
    public static String BankId;
    private BankListAdaptorSpinner bankListAdaptorSpinner;
    private Object aggregatorsArrayList;
    private String[] items ;
    private String[] items_id;

    private int NET_BANK=1,CREDIT_CARD=2,DEBIT_CARD=3,UPI=4;

    private String selected_Payment_Mode;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_donate_to_sbk_payment_gateway, container, false);
        localView=view;
        initializeView();
        CloseKeyBoard();
        return view;
    }
    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
    private void initializeView() {

        bankListSpinner= (Spinner) localView.findViewById(R.id.spinner_banklist);
        bankListSpinner.setVisibility(View.GONE);

        recyclerview_debitcard_paymentmode=(RecyclerView) localView.findViewById(R.id.recyclerview_debitcard_paymentmode);
        recyclerview_debitcard_paymentmode.setHasFixedSize(true);
        GridLayoutManager mLayoutManager = new GridLayoutManager(this.getContext(), 2);
        mLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerview_debitcard_paymentmode.setLayoutManager(mLayoutManager);
        recyclerview_debitcard_paymentmode.setVisibility(View.GONE);


        recyclerview_creditcard_paymentmode=(RecyclerView) localView.findViewById(R.id.recyclerview_creditcard_paymentmode);
        recyclerview_creditcard_paymentmode.setHasFixedSize(true);
        GridLayoutManager m2LayoutManager = new GridLayoutManager(this.getContext(), 2);
        m2LayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerview_creditcard_paymentmode.setLayoutManager(m2LayoutManager);
        recyclerview_creditcard_paymentmode.setVisibility(View.GONE);


        recyclerview_upi_paymentmode=(RecyclerView) localView.findViewById(R.id.recyclerview_upi_paymentmode);
        recyclerview_upi_paymentmode.setHasFixedSize(true);
        GridLayoutManager m3LayoutManager = new GridLayoutManager(this.getContext(), 2);
        m3LayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerview_upi_paymentmode.setLayoutManager(m3LayoutManager);
        recyclerview_upi_paymentmode.setVisibility(View.GONE);




        netBankingButton=(Button) localView.findViewById(R.id.netbanking_button);
        debitCardButton=(Button) localView.findViewById(R.id.debitcard_button);
        creditCardButton=(Button) localView.findViewById(R.id.creditcard_button);
        UPIButton=(Button) localView.findViewById(R.id.upi_button);



        netBankingButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
        debitCardButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
        creditCardButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
        UPIButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));



        paymentButtonsClickEvents();

        aggregatorsArrayList=new ArrayList<>();




        pay_button=localView.findViewById(R.id.pay_button);

        recyclerView=localView.findViewById(R.id.grid_recyclerview_paymentgateway_list);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager mLayoutManager1 = new GridLayoutManager(this.getContext(), 2);
        mLayoutManager1.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager1);

        grid_recyclerview_bank_list=localView.findViewById(R.id.grid_recyclerview_bank_list);
        grid_recyclerview_bank_list.setHasFixedSize(true);
        GridLayoutManager mLayoutManager2 = new GridLayoutManager(this.getContext(), 2);
        mLayoutManager2.setOrientation(RecyclerView.VERTICAL);
        grid_recyclerview_bank_list.setLayoutManager(mLayoutManager2);


     //   Hit_API_GETAGGREGATORLIST(this.getActivity(), this,String.valueOf(Prefs.getInt("ReceiptEntryId",0)));

        pay_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  HitPaymentAPIForNetBanking();
                HitPaymentDetailsAPI();
            }
        });

    }

    private void paymentButtonsClickEvents()
    {
        netBankingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                netBankingButton.setBackgroundColor(getResources().getColor(R.color.btn_enabled));
                debitCardButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
                creditCardButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
                UPIButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));

                bankListSpinner.setVisibility(View.VISIBLE);
                recyclerview_debitcard_paymentmode.setVisibility(View.GONE);
                recyclerview_creditcard_paymentmode.setVisibility(View.GONE);
                recyclerview_upi_paymentmode.setVisibility(View.GONE);

                selected_Payment_Mode="NB";
            }
        });

        creditCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                netBankingButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
                debitCardButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
                creditCardButton.setBackgroundColor(getResources().getColor(R.color.btn_enabled));
                UPIButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));

                bankListSpinner.setVisibility(View.GONE);
                recyclerview_debitcard_paymentmode.setVisibility(View.GONE);
                recyclerview_creditcard_paymentmode.setVisibility(View.VISIBLE);
                recyclerview_upi_paymentmode.setVisibility(View.GONE);

                selected_Payment_Mode="CC";
            }
        });

        debitCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                netBankingButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
                debitCardButton.setBackgroundColor(getResources().getColor(R.color.btn_enabled));
                creditCardButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
                UPIButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));

                bankListSpinner.setVisibility(View.GONE);
                recyclerview_debitcard_paymentmode.setVisibility(View.VISIBLE);
                recyclerview_creditcard_paymentmode.setVisibility(View.GONE);
                recyclerview_upi_paymentmode.setVisibility(View.GONE);

                selected_Payment_Mode="DC";
            }
        });

        UPIButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                netBankingButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
                debitCardButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
                creditCardButton.setBackgroundColor(getResources().getColor(R.color.btn_disabled));
                UPIButton.setBackgroundColor(getResources().getColor(R.color.btn_enabled));

                bankListSpinner.setVisibility(View.GONE);
                recyclerview_debitcard_paymentmode.setVisibility(View.GONE);
                recyclerview_creditcard_paymentmode.setVisibility(View.GONE);
                recyclerview_upi_paymentmode.setVisibility(View.VISIBLE);

                selected_Payment_Mode="UPI";
            }
        });
    }



    public void HitGetListForChannelTypeAPI(String aggID) {

        HandleHttpsRequest.setSSL();


        System.out.println("getListForChannelTypeURL..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getListForChannelTypeURL;
        final ProgressDialog pDialog = new ProgressDialog(this.getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                ChannelListModel channelListModel=gson.fromJson(response,ChannelListModel.class);



                if (channelListModel.getErrorCodes().matches("Success"))
                {
                    MyToken.setToken(channelListModel.getRefreshTokenValue());
                    MyToken.setAuthorization(channelListModel.getAcsessTokenValue());

                    recyclerview_debitcard_paymentmode.setAdapter(new ChannelListAdaptor(getActivity(),PaymentFragment.this,channelListModel));

                    HitGetListForChannelTypeAPI_CC(aggID);

                }
                else {
                    showDialog_ServerError("Some Error occured !");

                }



            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("Aggregatorid", aggID);
                params.put("ChannelType", "DC");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }

        };




        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }


    public void HitGetListForChannelTypeAPI_CC(String aggID) {

        HandleHttpsRequest.setSSL();


        System.out.println("HitGetListForChannelTypeAPI_CC..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getListForChannelTypeURL;
        final ProgressDialog pDialog = new ProgressDialog(this.getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                ChannelListModel channelListModel=gson.fromJson(response,ChannelListModel.class);



                if (channelListModel.getErrorCodes().matches("Success"))
                {
                    MyToken.setToken(channelListModel.getRefreshTokenValue());
                    MyToken.setAuthorization(channelListModel.getAcsessTokenValue());

                    recyclerview_creditcard_paymentmode.setAdapter(new ChannelListAdaptor(getActivity(),PaymentFragment.this,channelListModel));

                    HitGetListForChannelTypeAPI_UPI(aggID);

                }
                else {
                    showDialog_ServerError("Some Error occured !");

                }



            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("Aggregatorid", aggID);
                params.put("ChannelType", "CC");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }

        };




       // strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }


    public void HitGetListForChannelTypeAPI_UPI(String aggID) {

        HandleHttpsRequest.setSSL();


        System.out.println("HitGetListForChannelTypeAPI_UPI..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getListForChannelTypeURL;
        final ProgressDialog pDialog = new ProgressDialog(this.getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                ChannelListModel channelListModel=gson.fromJson(response,ChannelListModel.class);



                if (channelListModel.getErrorCodes().matches("Success"))
                {
                    MyToken.setToken(channelListModel.getRefreshTokenValue());
                    MyToken.setAuthorization(channelListModel.getAcsessTokenValue());

                    recyclerview_upi_paymentmode.setAdapter(new ChannelListAdaptor(getActivity(),PaymentFragment.this,channelListModel));



                }
                else {
                    showDialog_ServerError("Some Error occured !");

                }



            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("Aggregatorid", aggID);
                params.put("ChannelType", "UPI");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }

        };




       // strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }


    @Override
    public void onClick(View v) {

    }


    private void HitPaymentAPIForNetBanking() {

        HandleHttpsRequest.setSSL();


        System.out.println("Send Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getReceiptPaymentInfoURL;
        final ProgressDialog pDialog = new ProgressDialog(this.getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                PaymentWebViewModel sendOTPModel=gson.fromJson(response,PaymentWebViewModel.class);






                if (sendOTPModel.getResponseStatus().matches("Success")
                        &&sendOTPModel.getEncryptedRequestparameter()!=null
                        &&sendOTPModel.getReceiptGetTransferToBankId()!=null
                        &&sendOTPModel.getBankUrl()!=null
                        &&sendOTPModel.getDigitalSignature()!=null
                        &&sendOTPModel.getBankMerchantID()!=null
                ) {
                    MyToken.setToken(sendOTPModel.getRefreshTokenValue());
                    MyToken.setAuthorization(sendOTPModel.getAcsessTokenValue());

                   // changeFragment_PaymentWebViewFragment(sendOTPModel);

                    //  return;
                } else {


                    showDialog_ServerError("Some Error occured !");
                    //  return;
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryId", String.valueOf(Prefs.getInt("ReceiptEntryId",0)));
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("Aggregatorid", Aggregatorid);
                params.put("ChannelName", selected_Payment_Mode);
                params.put("ChannelId", BankId);
                params.put("BankId", BankId);
                params.put("TermAndCondition","1");

//                params.put("ReceiptEntryId", String.valueOf(Prefs.getInt("ReceiptEntryId",0)));
//                params.put("RefreshTokenValue", MyToken.getToken());
//                params.put("Aggregatorid", "8");
//                params.put("ChannelName", "DC");
//                params.put("ChannelId", "3");
//                params.put("BankId", "3");
//                params.put("TermAndCondition","1");


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }

        };




        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }

    private void HitPaymentDetailsAPI() {

        HandleHttpsRequest.setSSL();


        System.out.println("Send Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.GET_PAYMENT_DETAILS_URL;
        final ProgressDialog pDialog = new ProgressDialog(this.getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                PaymentDetailsModel sendOTPModel=gson.fromJson(response,PaymentDetailsModel.class);






                if (sendOTPModel.getResponseStatus().matches("Success")
                        &&sendOTPModel.getEncryptedRequestparameter()!=null



                ) {
                    MyToken.setToken(sendOTPModel.getRefreshTokenValue());
                    MyToken.setAuthorization(sendOTPModel.getAcsessTokenValue());

                    changeFragment_PaymentWebViewFragment(sendOTPModel);

                    //  return;
                } else {


                    showDialog_ServerError("Some Error occured !");
                    //  return;
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("ReceiptEntryId", String.valueOf(Prefs.getInt("ReceiptEntryId",0)));
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("Aggregatorid", Aggregatorid);
                params.put("ChannelName", selected_Payment_Mode);
                params.put("ChannelId", BankId);
                params.put("BankId", BankId);
                params.put("TermAndCondition","1");*/

                params.put("ReceiptEntryId", String.valueOf(Prefs.getInt("ReceiptEntryId",0)));
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("Aggregatorid", "8");
                params.put("ChannelName", "DC");
                params.put("ChannelId", "3");
                params.put("BankId", "3");
                params.put("TermAndCondition","1");


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }

        };




        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }




    public void Hit_API_GETAGGREGATORLIST(final Activity activity, final Fragment trackYourPaymentFragment, final String ReceiptEntryId) {


           HandleHttpsRequest.setSSL();


        System.out.println("Send Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getAggregatorListURL;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                AggregatorListModel sendOTPModel=gson.fromJson(response,AggregatorListModel.class);






                if (sendOTPModel.getAggregatorList().size()>0) {
                     MyToken.setToken(sendOTPModel.getRefreshTokenValue());
                     MyToken.setAuthorization(sendOTPModel.getAcsessTokenValue());


                     setDataToRecyclerView(sendOTPModel);
                  //  return;
                } else {


                    showDialog_ServerError("Some Error occured !");
                  //  return;
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryId", ReceiptEntryId);
                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
               // headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }

        };




        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void setDataToRecyclerView(AggregatorListModel sendOTPModel) {

        recyclerView.setAdapter(new AggregatorAdaptor(getActivity(),this,sendOTPModel));

    }

    public void setBankDataToRecyclerView(BankListCorrespondingToSelectedAggregatorModel sendOTPModel, String Aggregatorid) {

        this.Aggregatorid=Aggregatorid;
        grid_recyclerview_bank_list.setAdapter(new BankListAdaptor(getActivity(),this,sendOTPModel,Aggregatorid));


     //  bankListAdaptorSpinner = new BankListAdaptorSpinner(getActivity(), R.layout.single_row_spinner,this,sendOTPModel,Aggregatorid);
      //  bankListSpinner.setAdapter(bankListAdaptorSpinner);

        //bankListSpinner.setAdapter(new BankListAdaptorSpinner(getActivity(),this,sendOTPModel,Aggregatorid));




        String[] items = new String[sendOTPModel.getAggregatorBankList().size()];
        String[] items_id = new String[sendOTPModel.getAggregatorBankList().size()];

        aggregatorsArrayList=sendOTPModel.getAggregatorList();
        for(int i=0;i<sendOTPModel.getAggregatorBankList().size();i++)
        {
            items[i]=sendOTPModel.getAggregatorBankList().get(i).getBankName().toString();
            items_id[i]=sendOTPModel.getAggregatorBankList().get(i).getBankId().toString();

        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, items);
        bankListSpinner.setAdapter(adapter);


        bankListSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              //  String item = bankListSpinner.getItemAtPosition(position).toString();
                BankId=String.valueOf(sendOTPModel.getAggregatorBankList().get(position).getBankId());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }

        });



    }

    /*private void changeFragment_PaymentWebViewFragment(PaymentWebViewModel paymentWebViewModel)
    {
        Bundle bundle=new Bundle();
        bundle.putString("EncryptedRequestparameter",paymentWebViewModel.getEncryptedRequestparameter());

        bundle.putString("BankUrl",paymentWebViewModel.getBankUrl());
        bundle.putString("DigitalSignature",paymentWebViewModel.getDigitalSignature());
        bundle.putString("BankMerchantID",paymentWebViewModel.getBankMerchantID());


        Fragment confirmInfoFragment = new PaymentWebViewFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();


        confirmInfoFragment.setArguments(bundle);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, confirmInfoFragment);
        ft.addToBackStack(null);
        ft.commit();
    }*/
    private void changeFragment_PaymentWebViewFragment(PaymentDetailsModel paymentWebViewModel)
    {
        Bundle bundle=new Bundle();
        bundle.putString("EncryptedRequestparameter",paymentWebViewModel.getEncryptedRequestparameter());



        bundle.putString("ReceiptGetTransferToBankId",paymentWebViewModel.getReceiptGetTransferToBankId().toString());


        Fragment confirmInfoFragment = new PaymentWebViewFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();


        confirmInfoFragment.setArguments(bundle);
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, confirmInfoFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void showDialog_ServerError(String errorText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // getActivity().finish();
                dialog.dismiss();
                //System.exit(0);
            }
        });
    }


}
