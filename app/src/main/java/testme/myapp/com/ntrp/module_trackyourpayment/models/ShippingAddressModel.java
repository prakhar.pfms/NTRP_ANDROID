package testme.myapp.com.ntrp.module_trackyourpayment.models;

/**
 * Created by prakhar.s on 8/2/2017.
 */

public class ShippingAddressModel {


    public String ShippingSalutations;

    public String ShippingAddress1;

    public String ShippingAddress2;

    public String ShippingStateName;

    public String ShippingDistrictName;

    public String ShippingPincode;

    public String ShippingMobileNo;

    public String ShippingEmail;

    public String ShippingCountryName;

    public String ShippingCity;



}
