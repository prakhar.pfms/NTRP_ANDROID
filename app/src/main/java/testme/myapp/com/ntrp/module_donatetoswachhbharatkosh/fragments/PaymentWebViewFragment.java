package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.paynimo.android.payment.PaymentActivity;
import com.paynimo.android.payment.PaymentModesActivity;
import com.paynimo.android.payment.model.Checkout;
import com.paynimo.android.payment.util.Constant;
import com.pixplicity.easyprefs.library.Prefs;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.payments.CheckoutActivity;

public class PaymentWebViewFragment extends Fragment {
    private View localView;
    private WebView webView;

    private String ReceiptGetTransferToBankId,TransactionAmount,Currency,Country,DetailsChallanNumber,EPFBAccountNumber,AmountInOtherDetails;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_donate_to_sbk_payment_gateway_webview, container, false);
        localView=view;
        initializeView();
        CloseKeyBoard();
        try {
           // LoadURL();

            ExtractData();
        } catch (Exception e) {
           // e.printStackTrace();
        }
        return view;
    }




    private void ExtractData()
    {
        Bundle bundle=getArguments();
        if(bundle!=null) {
            String EncryptedRequestparameter=bundle.getString("EncryptedRequestparameter");
            ReceiptGetTransferToBankId=bundle.getString("ReceiptGetTransferToBankId");

            String[] extractedData=EncryptedRequestparameter.split("\\|");
            TransactionAmount=extractedData[4];
            Currency=extractedData[3];
            Country=extractedData[2];

            String[] extractedDataMore=extractedData[5].split("\\^");
            DetailsChallanNumber=extractedDataMore[0];
            EPFBAccountNumber=extractedDataMore[1];
            AmountInOtherDetails=extractedDataMore[2];
            CallPaymentGateway();
        }
    }

    private void CallPaymentGateway()
    {
        Intent myIntent = new Intent(getActivity(), CheckoutActivity.class);
        Bundle mBundle = new Bundle();
        
        mBundle.putString("ReceiptGetTransferToBankId", ReceiptGetTransferToBankId);
        mBundle.putString("TransactionAmount", TransactionAmount);
        mBundle.putString("Currency", Currency);
        mBundle.putString("Country", Country);
        mBundle.putString("DetailsChallanNumber", DetailsChallanNumber);
        mBundle.putString("EPFBAccountNumber", EPFBAccountNumber);
        mBundle.putString("AmountInOtherDetails", AmountInOtherDetails);

        myIntent.putExtras(mBundle);
        getActivity().startActivity(myIntent);
    }


    private void LoadURL() throws Exception {

        Bundle bundle=getArguments();
        if(bundle!=null) {

            String EncryptedRequestparameter=bundle.getString("EncryptedRequestparameter");

            String BankUrl=bundle.getString("BankUrl");
            String DigitalSignature=bundle.getString("DigitalSignature");
            String BankMerchantID=bundle.getString("BankMerchantID");


            /*Map<String, String> params = new HashMap<String, String>();


            params.put("encrequestparameter", EncryptedRequestparameter);
            params.put("reqdigitalsignature", DigitalSignature);
            params.put("merchIdVal", BankMerchantID);*/



           String postData = "encrequestparameter=" + URLEncoder.encode(EncryptedRequestparameter, "UTF-8") + "&reqdigitalsignature=" + URLEncoder.encode(DigitalSignature, "UTF-8")+ "&merchIdVal=" + URLEncoder.encode(BankMerchantID, "UTF-8");


           webView.postUrl(BankUrl,postData.getBytes());


            /*String uri = Uri.parse(BankUrl)
                    .buildUpon()
                    .appendQueryParameter("encrequestparameter", EncryptedRequestparameter)
                    .appendQueryParameter("reqdigitalsignature", DigitalSignature)
                    .appendQueryParameter("merchIdVal", BankMerchantID)
                    .build().toString();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(browserIntent);*/

            /*String url = BankUrl + postData.getBytes();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);*/




          //  startActivity( Intent.createChooser(browserIntent,"Select") );



        }

    }

    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
    private void initializeView() {


        webView = localView.findViewById(R.id.payment_web_view);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                PaymentWebViewFragment.this.getActivity().setTitle("Loading...");
                PaymentWebViewFragment.this.getActivity().setProgress(progress * 100);

                if(progress == 100)
                    PaymentWebViewFragment.this.getActivity().setTitle(R.string.app_name);
            }
        });
        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

    }
}
