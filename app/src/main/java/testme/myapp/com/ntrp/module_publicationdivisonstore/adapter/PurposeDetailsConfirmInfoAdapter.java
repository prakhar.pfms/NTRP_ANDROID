package testme.myapp.com.ntrp.module_publicationdivisonstore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_publicationdivisonstore.IItemListener;
import testme.myapp.com.ntrp.module_publicationdivisonstore.model.ShoppinngDepositorDetailModel;
import testme.myapp.com.ntrp.utils.AllUrls;

public class PurposeDetailsConfirmInfoAdapter extends RecyclerView.Adapter<PurposeDetailsConfirmInfoAdapter.MyViewHolder> {
    private ArrayList<ShoppinngDepositorDetailModel> mList;
    private final Context mContext;

    public PurposeDetailsConfirmInfoAdapter(Context mContext, ArrayList<ShoppinngDepositorDetailModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public void setData(ArrayList<ShoppinngDepositorDetailModel> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PurposeDetailsConfirmInfoAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.purpose_details_confirm_info_item_row, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull final PurposeDetailsConfirmInfoAdapter.MyViewHolder holder, int position) {
        holder.tvMinistry.setText(mList.get(position).getControllerName());
        holder.tvDdo.setText(mList.get(position).getdDOName());
        holder.tvPao.setText(mList.get(position).getpAOName());
        holder.tvPurpose.setText(mList.get(position).getPurposeDescription());
//        holder.tvProduct.setText(mList.get(position).getProductName());
//        holder.tvPrice.setText("" + mList.get(position).getProductPrice());
        holder.rvData.setLayoutManager(new LinearLayoutManager(mContext));
        holder.rvData.setAdapter(new ProductDetailsConfirmInfoAdapter(mContext, mList.get(position).getLstmodProductDetails()));
        holder.rvData.setNestedScrollingEnabled(false);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvMinistry, tvDdo, tvPao, tvPurpose, tvProduct, tvPrice;
        RecyclerView rvData;

        public MyViewHolder(View view) {
            super(view);
            tvMinistry = view.findViewById(R.id.tvMinistry);
            tvDdo = view.findViewById(R.id.tvDdo);
            tvPao = view.findViewById(R.id.tvPao);
            tvPurpose = view.findViewById(R.id.tvPurpose);
//            tvProduct = view.findViewById(R.id.tvProduct);
//            tvPrice = view.findViewById(R.id.tvPrice);
            rvData = view.findViewById(R.id.rvData);
        }
    }


}
