package testme.myapp.com.ntrp.module_trackyourpayment.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.activity.MainScreen;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_trackyourpayment.HitAPI;
import testme.myapp.com.ntrp.module_trackyourpayment.activity.TrackYourPaymentScreen;
import testme.myapp.com.ntrp.module_trackyourpayment.models.DeleteTransactionModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TransactionList;
import testme.myapp.com.ntrp.module_trackyourpayment.models.UserInputDetailModel;
import testme.myapp.com.ntrp.networking.AppController;

import testme.myapp.com.ntrp.module_trackyourpayment.adapters.TransactionGridViewAdapter;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TransctionGridModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.ViewSingleTransactionModel;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.utils.AllUrls;
import static com.android.volley.VolleyLog.TAG;

/**
 * Created by deepika.s on 21-04-2017.
 */

public class TransactionGridViewFragment extends Fragment {

    private View localView;
    private View Track_Button_Linear_Layout,TrackUrPayment_Linear_Layout,Grid_RecyclerView;
    private TextView Change_Mobile_Textview,Change_Email_Textview;
    private TextView Mobile_Textview,Email_Textview,Transaction_Textview,FinancialYear_Textview;
    private EditText Mobile_No_EditText,Email_Id_EditText,Transaction_Ref_No_Edittext;
    private Button TrackButton,BackButton,NextButton;
    private Spinner Spinner_FinancialYear;
    private RecyclerView recyclerView;
    private TransctionGridModel transactionGridModel;
    private boolean TrackButtonClicked;
    private  byte pdfBytes[];

    private SpannableString str;
    private String financialYear_String;
    private RadioGroup radioGroup_transactionType;
   // private boolean IsActive;
    String[] temp;

    @BindView(R.id.mobile_number_and_email_layout)
    public LinearLayout mobile_number_and_email_layout;

    /*@BindView(R.id.radiobtn_archive)
    public RadioButton radiobtn_archive;

    @BindView(R.id.radiobtn_active)
    public RadioButton radiobtn_active;*/



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaction_gridview, container, false);
        localView=view;
        ButterKnife.bind(this,view);
        TrackButtonClicked=false;
        temp=getResources().getStringArray(R.array.financialyear);
        financialYear_String=temp[TrackYourPaymentFragment.financialYearIndex];
        CloseKeyBoard();
        initializeView();
        ManageBackpress();
        return view;
    }
    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
    private void initializeView() {

        initialize_TextViews();

        initialize_EditTextViews();
        initialize_LayoutViews();
        initialize_ButtonViews();
        initialize_SpinnerFinancialYearViews();
        initialize_RecyclerView("0");
        putUnderline_Textviews();
        initialize_RadioGroup();
    }

    private void initialize_RadioGroup() {



        radioGroup_transactionType = (RadioGroup) localView.findViewById(R.id.radioGroup_transactiontype);

        radioGroup_transactionType.check(R.id.radiobtn_active);
        radioGroup_transactionType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if(checkedId == R.id.radiobtn_archive) {




                    if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {

                        Hit_API_GetTrackTransaction("1",true);

                    } else {
                        Toast.makeText(getActivity(), "Check Your Internet Connection!",
                                Toast.LENGTH_LONG).show();
                    }

                } else if(checkedId == R.id.radiobtn_active) {






                    if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {

                        Hit_API_GetTrackTransaction("0",true);

                    } else {
                        Toast.makeText(getActivity(), "Check Your Internet Connection!",
                                Toast.LENGTH_LONG).show();
                    }
                }

            }

        });
    }

    private void initialize_SpinnerFinancialYearViews() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, getResources().getStringArray(R.array.financialyear));

        Spinner_FinancialYear = (Spinner) localView.findViewById(R.id.spinner_financialyear);
        Spinner_FinancialYear.setAdapter(adapter);
        Spinner_FinancialYear.setSelection(TrackYourPaymentFragment.financialYearIndex);

        Spinner_FinancialYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Year= "+temp[position]);
                financialYear_String=temp[position];
                TrackYourPaymentFragment.financialYearIndex=position;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                financialYear_String=temp[0];
                TrackYourPaymentFragment.financialYearIndex=0;
            }
        });
    }

    private void initialize_TextViews()
    {
        Mobile_Textview= (TextView) localView.findViewById(R.id.mobile_textview);
        Email_Textview= (TextView) localView.findViewById(R.id.email_textview);
        Transaction_Textview= (TextView) localView.findViewById(R.id.transcation_ref_no_textview);
        FinancialYear_Textview= (TextView) localView.findViewById(R.id.financial_textview);
        putDoubleRedAsterisk(localView,Mobile_Textview,R.id.mobile_textview);
        putDoubleRedAsterisk(localView,Email_Textview,R.id.email_textview);
        putDoubleRedAsterisk(localView,Transaction_Textview,R.id.transcation_ref_no_textview);
        putDoubleRedAsterisk(localView,FinancialYear_Textview,R.id.financial_textview);
        Change_Mobile_Textview= (TextView) localView.findViewById(R.id.change_mobile_textview);
        Change_Email_Textview= (TextView) localView.findViewById(R.id.change_email_textview);
    }
    private void initialize_EditTextViews()
    {
        Mobile_No_EditText= (EditText) localView.findViewById(R.id.mobile_no_edittext);
        Email_Id_EditText= (EditText) localView.findViewById(R.id.email_id_edittext);
        Transaction_Ref_No_Edittext= (EditText) localView.findViewById(R.id.transaction_ref_no_edittext);
        Mobile_No_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count==10)
                {
                    HitAPI.MobileNo=Mobile_No_EditText.getText().toString();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        set_UserInputData();
    }
    private void initialize_LayoutViews()
    {
        Track_Button_Linear_Layout=localView.findViewById(R.id.track_button_layout);
        TrackUrPayment_Linear_Layout=localView.findViewById(R.id.trackurpayment_layout);
        Track_Button_Linear_Layout.setVisibility(View.VISIBLE);
        Track_Button_Linear_Layout.setEnabled(true);
        TrackUrPayment_Linear_Layout.setVisibility(View.GONE);
    }
    private void initialize_ButtonViews()
    {
        BackButton= (Button) localView.findViewById(R.id.backbutton);
        NextButton= (Button) localView.findViewById(R.id.nextbutton);
        TrackButton= (Button) localView.findViewById(R.id.trackbutton);
        set_TrackButton();
        set_BackButton();
        set_NextButton();
    }
    private void initialize_RecyclerView(String archiveoractive) {
        Grid_RecyclerView=localView.findViewById(R.id.grid_recyclerview);
        recyclerView = (RecyclerView) localView.findViewById(R.id.grid_recyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setVisibility(View.VISIBLE);
        transactionGridModel = (TransctionGridModel) MainScreen.TrackBundle.getParcelable("transactionGridModel");
        assignItemtoRecyclerView(transactionGridModel,archiveoractive);
    }
    private void putUnderline_Textviews()
    {
        str = new SpannableString(Change_Mobile_Textview.getText().toString());
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        Change_Mobile_Textview.setText(str);
        Change_Mobile_Textview.setSelected(true);
        Change_Mobile_Textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeScreen_TrackYourPaymentScreen();

            }
        });

        str = new SpannableString(Change_Email_Textview.getText().toString());
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        Change_Email_Textview.setText(str);
        Change_Email_Textview.setSelected(true);
        Change_Email_Textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeScreen_TrackYourPaymentScreen();

            }
        });
    }

    private void set_TrackButton()
    {
        str = new SpannableString(TrackButton.getText().toString());
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        TrackButton.setText(str);

        if(TrackYourPaymentFragment.IsNormalChecked)
        {
            TrackButton.setVisibility(View.VISIBLE);
        }
        else if(TrackYourPaymentFragment.IsCustomChecked||TrackYourPaymentFragment.IsPOSChecked)
        {
            TrackButton.setVisibility(View.GONE);

        }

        TrackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackButtonClicked=true;
                Track_Button_Linear_Layout.setVisibility(View.GONE);
                TrackUrPayment_Linear_Layout.setVisibility(View.VISIBLE);
                TrackUrPayment_Linear_Layout.setEnabled(true);
                Grid_RecyclerView.setVisibility(View.GONE);

                /*if (MainActivity.IsLogin)
                {
                    mobile_number_and_email_layout.setVisibility(View.GONE);
                }*/

            }
        });
    }
    private void set_BackButton()
    {
        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TrackButtonClicked=false;
                TrackUrPayment_Linear_Layout.setVisibility(View.GONE);
                Track_Button_Linear_Layout.setVisibility(View.VISIBLE);
                Track_Button_Linear_Layout.setEnabled(true);
                Grid_RecyclerView.setVisibility(View.VISIBLE);
                Grid_RecyclerView.setEnabled(true);
                // Hit_API_GetPDF();

                //askPermission();
            }
        });
    }
    private void set_NextButton()
    {
        NextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String receiptEntryID=transactionGridModel.tracktransaction.get(0).ReceiptEntryID;

                TrackButtonClicked=false;
                String transactionno=Transaction_Ref_No_Edittext.getText().toString();
                if(transactionno.matches(""))
                {
                   Hit_API_GetTrackTransaction("0",false);
                    return;
                }
                else
                {
                    Hit_API_GetTransactionRefNumberRecord(transactionno);
                }

            }
        });
    }

    private void set_UserInputData()
    {
        UserInputDetailModel userInputDetailModel = (UserInputDetailModel) MainScreen.UserInputBundle.getParcelable("userInputDetailModel");
        if(userInputDetailModel!=null&&userInputDetailModel.MobileNumber!=null)
        {
            Change_Mobile_Textview.setVisibility(View.VISIBLE);
            Change_Mobile_Textview.setEnabled(true);
            Mobile_No_EditText.setHint(userInputDetailModel.MobileNumber);
        }
        else
        {
            Mobile_No_EditText.setHint("");
            Change_Mobile_Textview.setVisibility(View.INVISIBLE);
        }
        if(userInputDetailModel!=null&&userInputDetailModel.EmailID!=null)
        {
            Change_Email_Textview.setVisibility(View.VISIBLE);
            Change_Email_Textview.setEnabled(true);
            Email_Id_EditText.setHint(userInputDetailModel.EmailID);
        }
        else
        {
            Email_Id_EditText.setHint("");
            Change_Email_Textview.setVisibility(View.INVISIBLE);
        }
    }
    private void changeScreen_TrackYourPaymentScreen() {

        getActivity().finish();
        Intent intent=new Intent(getActivity(), TrackYourPaymentScreen.class);
        getActivity().startActivity(intent);
    }
    private void changeFragment_SingleTransactionDetail() {

        Fragment singleTransactionDetail = new SingleTransactionDetail();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, singleTransactionDetail);
        ft.addToBackStack(null);
        ft.commit();
    }



    private void assignItemtoRecyclerView(TransctionGridModel transctionGridModel,String archiveoractive) {


        recyclerView.setAdapter(new TransactionGridViewAdapter(getActivity(),this,transctionGridModel.tracktransaction,archiveoractive));
    }
    public void Clicked_TransactionRefNo()
    {

        addFragmentForDepositorDetails();

    }
    private void addFragmentForDepositorDetails() {

        Fragment depositorTransactionDetailsFragment = new DepositorTransactionDetailsFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, depositorTransactionDetailsFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void Clicked_Delete_Textview(final String receiptEntryDetailID,String dialogMessage)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_delete_records);
        TextView dialogTextMsg=(TextView) dialog.findViewById(R.id.dialogTextMsg);
        dialogTextMsg.setText(""+dialogMessage);

        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        Button dialogCrossButton = (Button) dialog.findViewById(R.id.crossbutton);
        dialogCrossButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Hit_API_DeleteTransaction(receiptEntryDetailID);
            }
        });
        Button dialogCancelButton = (Button) dialog.findViewById(R.id.cancelbutton);
        dialogCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void putDoubleRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = "**";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }
    private void getDataFromServer_SingleDepositorTransactionDetails(final String receiptEntryID) {

        String tag_json_obj = "json_obj_req";

      //  String url = AllUrls.mainURL + AllUrls.getSingleDepositorDetailsTransactionURL;
        String url =  AllUrls.getSingleDepositorDetailsTransactionURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                /*int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v(TAG, response.substring(start, end));
                }*/

                pDialog.hide();
                pDialog.dismiss();

                Gson gson = new GsonBuilder().setLenient().create();
                ViewSingleTransactionModel viewSingleTransactionModel = gson.fromJson(response, ViewSingleTransactionModel.class);
                if(viewSingleTransactionModel.DepositerTransactionDetails==null)
                {
                    System.out.println("DepositerTransactionDetails is null");
                }
                else
                {
                    System.out.println("hello= "+viewSingleTransactionModel.DepositerTransactionDetails.size());
                    System.out.println("hello= "+viewSingleTransactionModel.PaymentDetails.size());
                    System.out.println("hello= "+viewSingleTransactionModel.DepositorDetails.size());
                    MainScreen.TrackBundle.putParcelable("viewSingleTransactionModel", viewSingleTransactionModel);
                    changeFragment_SingleTransactionDetail();
                }

            }
        }, new Response.ErrorListener() {





            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryID", receiptEntryID);
                params.put("refreshTokenValue", MyToken.getToken());
                System.out.println("ReceiptEntryID"+receiptEntryID);
               ;
                return params;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private void Hit_API_DeleteTransaction(final String receiptEntryDetailID) {

        System.out.println("Delete API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.archiveRecordsURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }


                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                DeleteTransactionModel deleteTransactionModel=gson.fromJson(response,DeleteTransactionModel.class);
                MyToken.setToken(deleteTransactionModel.RefreshTokenValue);
                MyToken.setAuthorization(deleteTransactionModel.AcsessTokenValue);


                if (deleteTransactionModel.ResponseStatus.matches("Record Updated Successfully-Archive")||deleteTransactionModel.ResponseStatus.matches("Record Updated Successfully-Active"))
                {
                    showDialog_Success("Records Updated successfully.");

                    return;
                }
                else
                {

                    showDialog_False_OTP_Error("Unsuccessful Updation..");
                    return;
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());

                pDialog.hide();
                pDialog.dismiss();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryDetailID", receiptEntryDetailID);
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    private void showDialog_Success(String success) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView success_textview= (TextView) dialog.findViewById(R.id.success_textview);
        success_textview.setText(success);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                System.out.println("here...........");
                Hit_API_GetTrackTransaction("0",false);
            }
        });
    }

    private void showDialog_False_OTP_Error(String warningText) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_warning);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    private void Hit_API_GetTrackTransaction(String needArchiveTransaction,boolean callFromRadioGroup) {

        System.out.println("nope...........");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTrackTransactionURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println("response...........");
                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("tyu....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                TransctionGridModel transactionGridModel = gson.fromJson(response, TransctionGridModel.class);

                if(transactionGridModel.ResponseStatus.matches("Success")||transactionGridModel.ResponseStatus.matches("Successfully Verified"))
                {
                    System.out.println("track transactionGridModel is Success");
                    MyToken.setToken(transactionGridModel.RefreshTokenValue);
                    MyToken.setAuthorization(transactionGridModel.AcsessTokenValue);
                    System.out.println(transactionGridModel.tracktransaction);
                    MainScreen.TrackBundle.putParcelable("transactionGridModel", transactionGridModel);

                    if (callFromRadioGroup)
                    {

                        initialize_RecyclerView(needArchiveTransaction);
                    }
                    else
                    {
                        changeFragment_TransactionGridViewFragment();
                    }


                }
                else if(transactionGridModel.ResponseStatus.matches("Fail")||(transactionGridModel.ErrorCodes!=null&&transactionGridModel.ErrorCodes.matches("Record Not Found !")))
                {
                    System.out.println("track transactionGridModel is Record Not Found");
                    MyToken.setToken(transactionGridModel.RefreshTokenValue);
                    MyToken.setAuthorization(transactionGridModel.AcsessTokenValue);
                    showDialog_RecordNotFound("Record not found.");

                   // radiobtn_archive.setChecked(radiobtn_active.isChecked());

                   // radiobtn_active.setChecked(!radiobtn_archive.isChecked());

                    recyclerView.setVisibility(View.GONE);

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                showDialog_ServerError("Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                if (MainActivity.IsLogin==false)
                {

                    UserInputDetailModel userInputDetailModel = (UserInputDetailModel) MainScreen.UserInputBundle.getParcelable("userInputDetailModel");


                    if(HitAPI.MobileNo!=null)
                    {

                        params.put("mobileno", HitAPI.MobileNo);
                        params.put("RefreshTokenValue", MyToken.getToken());
                        params.put("NTRPFY", financialYear_String);
                        params.put("ActiveInActive", needArchiveTransaction);
                        return params;
                    }
                    else if (HitAPI.Email!=null)
                    {
                        params.put("EmailId", HitAPI.Email);
                        params.put("RefreshTokenValue", MyToken.getToken());
                        params.put("NTRPFY", financialYear_String);
                        params.put("ActiveInActive", needArchiveTransaction);
                        return params;
                    }
                    else if (userInputDetailModel.AtrNumber!=null)
                    {
                        params.put("IsPOS", "Yes");
                        params.put("AtrNo", userInputDetailModel.AtrNumber);
                        params.put("RefreshTokenValue", MyToken.getToken());
                        params.put("NTRPFY", financialYear_String);
                        params.put("ActiveInActive", needArchiveTransaction);
                        return params;
                    }
                    else if (userInputDetailModel.BrNumber!=null)
                    {

                        params.put("IsCustom", "Yes");
                        params.put("BrNo", userInputDetailModel.BrNumber);
                        params.put("PassportNo", userInputDetailModel.PassportNumber);
                        params.put("RefreshTokenValue", MyToken.getToken());
                        params.put("NTRPFY", financialYear_String);
                        params.put("ActiveInActive", needArchiveTransaction);
                        return params;
                    }


                }



                else if (MainActivity.IsLogin)
                {

                    params.put("RefreshTokenValue", MyToken.getToken());
                    params.put("NTRPFY", financialYear_String);
                    params.put("ActiveInActive", needArchiveTransaction);
                    params.put("userid", MainActivity.UserID);


                }

                return params;



            }

                @Override
                public Map<String, String> getHeaders () throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    // headers.put("Content-Type", "application/json");
                    headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                    return headers;
                }
            };
        //strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    public void showDialog_ServerError(String errorText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().finish();
                dialog.dismiss();
                System.exit(0);
            }
        });
    }
    private void Hit_API_GetTransactionRefNumberRecord(final String transactionno) {

        System.out.println("Hit_API_GetTransactionRefNumberRecord");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTransactionRefNumberRecordURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println("response...........");
                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("tyu....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                TransctionGridModel transactionGridModel = gson.fromJson(response, TransctionGridModel.class);
                System.out.println(transactionGridModel);
                if(transactionGridModel.ResponseStatus.matches("Success"))
                {
                    System.out.println("track transactionGridModel is Success");
                    MyToken.setToken(transactionGridModel.RefreshTokenValue);
                    MyToken.setAuthorization(transactionGridModel.AcsessTokenValue);
                    System.out.println(transactionGridModel.tracktransaction);
                    MainScreen.TrackBundle.putParcelable("transactionGridModel", transactionGridModel);
                    changeFragment_TransactionGridViewFragment();

                }
                else if(transactionGridModel.ResponseStatus.matches("Record Not Found !"))
                {
                    System.out.println("track transactionGridModel is Record Not Found");
                    MyToken.setToken(transactionGridModel.RefreshTokenValue);
                    MyToken.setAuthorization(transactionGridModel.AcsessTokenValue);
                    showDialog_RecordNotFound("Record not found.");
                    recyclerView.setVisibility(View.GONE);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobileno", HitAPI.MobileNo !=null ? HitAPI.MobileNo : ""  );
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("ReceiptNumber", transactionno);
                params.put("NTRPFY", financialYear_String);
                return params;


            }

            @Override
            public Map<String, String> getHeaders () throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + MyToken.getAuthorization());
                return headers;
            }
        };
        //strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
    public void showDialog_RecordNotFound(String yourText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_blue);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView textView= (TextView) dialog.findViewById(R.id.yourtextview);
        textView.setText(yourText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
    }
    private void changeFragment_TransactionGridViewFragment()
    {

        Fragment transactionGridViewFragment = new TransactionGridViewFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, transactionGridViewFragment);
        //ft.addToBackStack(null);
        ft.commit();
    }



    private void Hit_API_GetPDF() {

        System.out.println("GETPDF API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getAllPDFURL;
        final ProgressDialog pDialog = new ProgressDialog(getActivity(),R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    //Log.v("abc.....", response.substring(start, end));
                }
                pdfBytes=response.getBytes();


                byteArrayToFile();

                pDialog.hide();
                pDialog.dismiss();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryId", "5469");
                params.put("RefreshTokenValue", MyToken.getToken());
                params.put("ReportName", "rptNTRPGAR6Receipt");
                params.put("ReceiptPrintDocumentId", "7");
                params.put("IsSignPdf", "1");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }


    private void byteArrayToFile() {



    }




    public void WriteBtn() {



        File root = android.os.Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        File dir = new File(root.getAbsolutePath());
        dir.mkdirs();
        File file = new File(dir, "PRAKHAR.pdf");

        try {
            FileOutputStream f = new FileOutputStream(file);
            /*PrintWriter pw = new PrintWriter(f);
            pw.println("Hi , How are you");
            pw.println("Hello");
            pw.flush();
            pw.close();*/

            byte[] arr=pdfBytes;
            f.write(arr);
            f.flush();
            f.close();
            MediaScannerConnection.scanFile(
                    getContext(),
                    new String[]{file.getAbsolutePath()},
                    null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.v("grokkingandroid",
                                    "file " + path + " was scanned seccessfully: " + uri);
                        }
                    });

        } catch (FileNotFoundException e) {
          //  e.printStackTrace();
            android.util.Log
                    .i("tag",
                            "******* File not found. Did you add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } catch (IOException e) {
         //   e.printStackTrace();
        }





    }
    private void ManageBackpress()
    {
        localView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(TrackButtonClicked==true&&keyCode==KeyEvent.KEYCODE_BACK)
                {
                    TrackButtonClicked=false;
                    TrackUrPayment_Linear_Layout.setVisibility(View.GONE);
                    Track_Button_Linear_Layout.setVisibility(View.VISIBLE);
                    Track_Button_Linear_Layout.setEnabled(true);
                    Grid_RecyclerView.setVisibility(View.VISIBLE);
                    Grid_RecyclerView.setEnabled(true);

                    return true;
                }
                return false;

            }
        });
    }


}


