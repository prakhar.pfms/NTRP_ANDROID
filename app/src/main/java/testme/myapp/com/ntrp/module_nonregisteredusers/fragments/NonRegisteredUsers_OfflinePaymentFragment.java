package testme.myapp.com.ntrp.module_nonregisteredusers.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.pixplicity.easyprefs.library.Prefs;

import com.sromku.simple.storage.InternalStorage;
import com.sromku.simple.storage.SimpleStorage;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.fragments.NewHomeFragment;
import testme.myapp.com.ntrp.fragments.NewHomeFragmentTwo;
import testme.myapp.com.ntrp.module_trackyourpayment.models.GetAllPdfModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.PDFDownloadModel;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;
import testme.myapp.com.ntrp.utils.AppExternalFileWriter;

public class NonRegisteredUsers_OfflinePaymentFragment extends Fragment {

    private View localView;
    private String ReceiptEntryId,challanNumber,amount;

    @BindView(R.id.challan_no_textview_value)
    public TextView challan_no_textview_value;

    @BindView(R.id.amount_textview_value)
    public TextView amount_textview_value;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS =180 ;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.non_register_user_offline_confirmation_page, container, false);
        localView=view;
        ButterKnife.bind(this,view);
        CloseKeyBoard();
        getActivity().setTitle("Offline");
        NonRegisteredUsers_DepositorDetailsFragment.OfflineWithAccountValidation=false;
        NonRegisteredUsers_DepositorDetailsFragment.OfflineWithLogin=false;

        Bundle bundle = getArguments();
        if (bundle != null) {
            ReceiptEntryId = bundle.getString("ReceiptEntryId");
            challan_no_textview_value.setText(""+bundle.getString("challanNumber"));
            amount_textview_value.setText(""+bundle.getString("amount"));
        }

        return view;
    }
    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public void Hit_API_PDF_Download(String pdfUrl, final Activity activity, final Fragment trackYourPaymentFragment) {

        System.out.println("Verify Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = pdfUrl;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();


                Gson gson = new GsonBuilder().setLenient().create();
                PDFDownloadModel pdfDownloadModel = gson.fromJson(response, PDFDownloadModel.class);

                if (pdfDownloadModel.getPdfFileByte()!=null)
                {
                    try {
                        WriteBtn(Base64.decode(pdfDownloadModel.getPdfFileByte(), Base64.DEFAULT));
                    } catch (Exception e) {
                      //  e.printStackTrace();
                    }
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("ReceiptEntryId", ReceiptEntryId);
                // params.put("IsMobileApi", "1");
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        strReq.setRetryPolicy(new DefaultRetryPolicy(
//                0,
//                0,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void WriteBtn( byte[] pdfBytes) throws Exception {


        File root = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            root = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        }

        File dir =  new File(root.getAbsolutePath());
        dir.mkdirs();
        File file = new File(dir, "NTRP"+"_"+ SystemClock.currentThreadTimeMillis()+".pdf");

        try {
            FileOutputStream f = new FileOutputStream(file);


            byte[] arr=pdfBytes;
            f.write(arr);
            f.flush();
            f.close();
            MediaScannerConnection.scanFile(
                    this.getContext(),
                    new String[]{file.getAbsolutePath()},
                    null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.v("grokkingandroid",
                                    "file " + path + " was scanned seccessfully: " + uri);
                            String DownloadedPath=path+uri;

                            showDialog("File downloaded successfully to "+DownloadedPath);

                        }
                    });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            android.util.Log
                    .i("tag",
                            "******* File not found. Did you add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
        } catch (IOException e) {
            e.printStackTrace();
        }

       /* String Filename="NTRP"+"_"+ SystemClock.currentThreadTimeMillis()+".pdf";

        AppExternalFileWriter appExternalFileWriter=new AppExternalFileWriter(this.getContext());

        appExternalFileWriter.writeDataToFile(Filename,pdfBytes,false);*/






    }
    private void showDialog(String warningText) {

        final Dialog dialog = new Dialog(this.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_warning);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void Hit_API_GetAllPDFLinks() {


        System.out.println("getAllPDFURL API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getAllPDFURL;


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }





                Gson gson = new GsonBuilder().setLenient().create();

                GetAllPdfModel allPdfModel=gson.fromJson(response,GetAllPdfModel.class);
                MyToken.setToken(allPdfModel.RefreshTokenValue);
                MyToken.setAuthorization(allPdfModel.AcsessTokenValue);


                if (allPdfModel.XmlDreports!=null&&allPdfModel.XmlDreports.size()>0)
                {
                    String[] parts1 = allPdfModel.XmlDreports.get(0).PdfURL.split("\\?");



                    Permissions.check(NonRegisteredUsers_OfflinePaymentFragment.this.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE, null, new PermissionHandler() {
                        @Override
                        public void onGranted() {
                            Hit_API_PDF_Download(AllUrls.SERVER_ADDRESS+"NTRPMobileApi/API/NTRP/GetPDFFile?"+parts1[1],NonRegisteredUsers_OfflinePaymentFragment.this.getActivity(),NonRegisteredUsers_OfflinePaymentFragment.this);

                        }
                        @Override
                        public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                            Toast.makeText(NonRegisteredUsers_OfflinePaymentFragment.this.getActivity(), "Need permission to download file!",
                                    Toast.LENGTH_LONG).show();
                        }

                    });


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReceiptEntryId", ReceiptEntryId);
                params.put("RefreshTokenValue", MyToken.getToken());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+MyToken.getAuthorization());
                return headers;
            }
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        strReq.setRetryPolicy(new DefaultRetryPolicy(
//                0,
//                0,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @OnClick(R.id.download_offline_depositor_slip_button)
    public void onDownloadButtonClicked() {

        Hit_API_Token();

       // Hit_API_GetAllPDFLinks();
        //Hit_API_PDF_Download(AllUrls.SERVER_ADDRESS+"NTRPMobileApi/API/NTRP/GetPDFFile?"+ReceiptEntryId,this.getActivity(),this);
    }

    @OnClick(R.id.quit_button)
    public void onQuitButtonClicked() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        changeScreen();

    }

    private void changeScreen() {

        NewHomeFragmentTwo newFragment = new NewHomeFragmentTwo();
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_frame, newFragment);
        transaction.commit();
    }

    private void askPermission()
    {
        if (ContextCompat.checkSelfPermission(this.getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            System.out.println("permisssionnnn/..............");
            ActivityCompat.requestPermissions(this.getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);

        }

    }

    private void Hit_API_Token() {
        HandleHttpsRequest.setSSL();
        System.out.println("Token API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(NonRegisteredUsers_OfflinePaymentFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                TokenModel tokenModel=gson.fromJson(response,TokenModel.class);
                MyToken.setToken(tokenModel.refresh_token);
                MyToken.setAuthorization(tokenModel.access_token);
                if(MyToken.getToken()!=null)
                {

                    if (CheckNetworkConnection.getInstance(NonRegisteredUsers_OfflinePaymentFragment.this.getActivity()).isOnline())
                    {


                       Hit_API_GetAllPDFLinks();

                    }
                    else {
                        Toast.makeText(NonRegisteredUsers_OfflinePaymentFragment.this.getActivity(), "Some error occured!",
                                Toast.LENGTH_LONG).show();

                    }
                }
                else
                {
                    Toast.makeText(NonRegisteredUsers_OfflinePaymentFragment.this.getActivity(), "Some error occured!",
                            Toast.LENGTH_LONG).show();
                }




            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();
                params.put("UserName", Prefs.getString("LoggedInMobileNumber"));
                params.put("AppType", "NTRP-APP");
                params.put("DeviceID", "123");
                params.put("AppVersion", "1.0");
                params.put("password", "123456");
                params.put("OS", "android");
                params.put("Operator", "airtel");
                params.put("grant_type", "password");
                params.put("Flag", "5");
                return params;
            }


        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);

    }

}
