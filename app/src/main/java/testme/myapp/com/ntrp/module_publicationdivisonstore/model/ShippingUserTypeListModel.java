package testme.myapp.com.ntrp.module_publicationdivisonstore.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingUserTypeListModel {
    @SerializedName("Value")
    @Expose
    public String value;
    @SerializedName("Text")
    @Expose
    public String text;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
