package testme.myapp.com.ntrp.receivers;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.activity.SplashScreen;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.utils.AllUrls;

/**
 * Created by deepika.s on 27-04-2017.
 */


public class NetworkChangeReceiver extends BroadcastReceiver {
    public Context context;
    public NetworkChangeReceiver(Context c)
    {
        this.context=c;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.v("11111", "Receieved notification about network status");
        //isNetworkAvailable(context);
    }

    /*private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        if(!MainScreen.isConnected){
                            //Log.v("2222", "Now you are connected to Internet!");

                            MainScreen.isConnected = true;
                            if(CheckNetworkConnection.getInstance(context).isOnline())
                            {
                                HitURLForToken();
                            }
                            else
                            {
                                Toast.makeText(context,"No network Connection!",Toast.LENGTH_LONG).show();
                            }

                        }
                        return true;
                    }
                }
            }
        }

        //MainScreen.isConnected = false;
        return false;
    }*/
    private void HitURLForToken()
    {
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(true);

        pDialog.show();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                pDialog.hide();
                pDialog.dismiss();

                    Gson gson = new GsonBuilder().setLenient().create();
                    TokenModel tokenModel = gson.fromJson(response, TokenModel.class);
                    if(tokenModel==null)
                    {
                        System.out.println("null respnse on splash screen...................");
                        Intent i = new Intent(context, SplashScreen.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    }
                    else
                    {
                        MyToken.setToken(tokenModel.refresh_token);

                    }



            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.hide();
                pDialog.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("grant_type", "password");
                params.put("Operator", "Android");
                params.put("DeviceID", "123");
                params.put("Username", "fatuha.ado");
                params.put("AppType", "ASHA-APP");
                params.put("AppVersion", "1.0");
                params.put("password", "root@1234");
                params.put("OS", "Windows");
                return params;
            }


        };
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }
}

