package testme.myapp.com.ntrp.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
import butterknife.OnClick;
import testme.myapp.com.ntrp.R;

public class UserGuideFragment extends Fragment {



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_user_guide, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle("User Guide");
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle("User Guide");
    }
    @OnClick(R.id.first_text)
    public void openFirstUrl()
    {
        String url = "https://training.pfms.gov.in/Bharatkosh/pdf/UserGuideBharatkosh.pdf";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.second_text)
    public void openSecondUrl()
    {
        String url = "https://training.pfms.gov.in/Bharatkosh/pdf/UserGuideBharatkoshOffLine.pdf";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


}
