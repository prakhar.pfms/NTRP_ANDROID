package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.adaptors;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.PaymentFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.AggregatorListModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.BankListCorrespondingToSelectedAggregatorModel;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

public class BankListAdaptor extends RecyclerView.Adapter<BankListAdaptor.MyViewHolder> {

    private BankListCorrespondingToSelectedAggregatorModel aggregatorListModel;
    private Fragment fragment;
    private Context context;

    private int lastSelectedPosition = -1;


    public BankListAdaptor(Context context, Fragment fragment, BankListCorrespondingToSelectedAggregatorModel aggregatorListModel, String AggregatorId) {
        this.context = context;
        this.fragment = fragment;
        this.aggregatorListModel = aggregatorListModel;

    }

    @Override
    public BankListAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_view_bank_list, parent, false);
        return new BankListAdaptor.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(BankListAdaptor.MyViewHolder holder, int position) {

        holder.bank_radio_button.setChecked(lastSelectedPosition == position);



        holder.bank_name_text.setText(""+aggregatorListModel.getAggregatorBankList().get(holder.getAdapterPosition()).getBankName());

        holder.complete_single_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //PaymentFragment.BankId=String.valueOf(aggregatorListModel.getAggregatorBankList().get(holder.getAdapterPosition()).getBankId());
            }
        });


    }
    public void Hit_API_GetBankList(final Activity activity, final Fragment fragment, final String Aggregatorid) {


        HandleHttpsRequest.setSSL();


        System.out.println("Send Otp API..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getAggregatorListURL;
        final ProgressDialog pDialog = new ProgressDialog(activity,R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();


        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                int maxLogSize = 1000;
                for (int i = 0; i <= response.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > response.length() ? response.length() : end;
                    Log.v("abc.....", response.substring(start, end));
                }



                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();

                AggregatorListModel sendOTPModel=gson.fromJson(response,AggregatorListModel.class);






                if (sendOTPModel.getAggregatorList().size()>0) {
                    MyToken.setToken(sendOTPModel.getRefreshTokenValue());
                    MyToken.setAuthorization(sendOTPModel.getAcsessTokenValue());


                    //  setDataToRecyclerView(sendOTPModel);
                    //  return;
                } else {


                    //  showDialog_ServerError("Some Error occured !");
                    //  return;
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();

            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Aggregatorid", Aggregatorid);
                params.put("RefreshTokenValue", MyToken.getToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }

        };




        strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    @Override
    public int getItemCount() {
        return aggregatorListModel.getAggregatorBankList().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView bank_name_text;
        public RelativeLayout complete_single_view;
        public RadioButton bank_radio_button;
        public RadioGroup bank_radio_group;


        public MyViewHolder(View itemView) {
            super(itemView);
            bank_name_text = (TextView) itemView.findViewById(R.id.bank_name_text);
            complete_single_view = (RelativeLayout) itemView.findViewById(R.id.complete_single_view);
            bank_radio_button = (RadioButton) itemView.findViewById(R.id.bank_radio_button);
            bank_radio_group = (RadioGroup) itemView.findViewById(R.id.bank_radio_group);


            bank_radio_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PaymentFragment.BankId=String.valueOf(aggregatorListModel.getAggregatorBankList().get(getAdapterPosition()).getBankId());

                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();

                }
            });


        }
    }


}


