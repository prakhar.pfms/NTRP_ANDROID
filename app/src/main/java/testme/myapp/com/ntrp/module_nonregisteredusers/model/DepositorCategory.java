package testme.myapp.com.ntrp.module_nonregisteredusers.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DepositorCategory {

    @SerializedName("DepositorCategoryId")
    @Expose
    private Integer depositorCategoryId;
    @SerializedName("DepositorCategoryName")
    @Expose
    private String depositorCategoryName;

    public Integer getDepositorCategoryId() {
        return depositorCategoryId;
    }

    public void setDepositorCategoryId(Integer depositorCategoryId) {
        this.depositorCategoryId = depositorCategoryId;
    }

    public String getDepositorCategoryName() {
        return depositorCategoryName;
    }

    public void setDepositorCategoryName(String depositorCategoryName) {
        this.depositorCategoryName = depositorCategoryName;
    }
}
