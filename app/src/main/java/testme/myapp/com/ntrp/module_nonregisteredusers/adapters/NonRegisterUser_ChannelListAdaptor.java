package testme.myapp.com.ntrp.module_nonregisteredusers.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.adaptors.ChannelListAdaptor;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.fragments.PaymentFragment;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.ChannelListModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_PaymentFragment;

public class NonRegisterUser_ChannelListAdaptor extends RecyclerView.Adapter<NonRegisterUser_ChannelListAdaptor.MyViewHolder> {

    private ChannelListModel channelListModel;
    private NonRegisteredUsers_PaymentFragment fragment;
    private Context context;
    private RadioButton lastCheckedRB = null;

    public NonRegisterUser_ChannelListAdaptor(Context context, NonRegisteredUsers_PaymentFragment fragment, ChannelListModel channelListModel) {
        this.context = context;
        this.fragment = fragment;
        this.channelListModel = channelListModel;

    }

    @Override
    public NonRegisterUser_ChannelListAdaptor.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_view_channel, parent, false);
        return new NonRegisterUser_ChannelListAdaptor.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NonRegisterUser_ChannelListAdaptor.MyViewHolder holder, int position) {



        holder.aggregator_radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton checked_rb = (RadioButton) group.findViewById(checkedId);
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }

                //store the clicked radiobutton
                lastCheckedRB = checked_rb;

            }
        });
        Picasso.with(context)
                .load("https://training.pfms.gov.in/Bharatkosh/App_Themes/Receipt/images/"+channelListModel.getAggregatorCardTypeList().get(position).getAggregatorLogoName())
                .placeholder(R.drawable.loading_error_image)
                .error(R.drawable.loading_error_image)
                .into(holder.payment_gateway_image);


        holder.aggregator_radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NonRegisteredUsers_PaymentFragment.BankId=String.valueOf(channelListModel.getAggregatorCardTypeList().get(holder.getAdapterPosition()).getAggregatorID());


            }
        });




    }






    @Override
    public int getItemCount() {
        return channelListModel.getAggregatorCardTypeList().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public RadioButton aggregator_radio_button;
        public RadioGroup aggregator_radio_group;

        public ImageView payment_gateway_image;
        public MyViewHolder(View itemView) {
            super(itemView);


            aggregator_radio_button = (RadioButton) itemView.findViewById(R.id.aggregator_radio_button);
            payment_gateway_image = (ImageView) itemView.findViewById(R.id.payment_gateway_image);
            aggregator_radio_group = (RadioGroup) itemView.findViewById(R.id.aggregator_radio_group);


        }
    }


}


