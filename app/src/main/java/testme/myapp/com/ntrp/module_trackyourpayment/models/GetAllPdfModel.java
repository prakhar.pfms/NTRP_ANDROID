package testme.myapp.com.ntrp.module_trackyourpayment.models;

import java.util.List;

/**
 * Created by prakhar.s on 8/2/2017.
 */

public class GetAllPdfModel {

    public List<PdfModel> XmlDreports;
    public String AcsessTokenValue;
    public String RefreshTokenValue;

    public String ResponseStatus;
    public String ErrorCodes;
}
