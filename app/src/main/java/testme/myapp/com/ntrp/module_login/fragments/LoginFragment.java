package testme.myapp.com.ntrp.module_login.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixplicity.easyprefs.library.Prefs;

import org.greenrobot.eventbus.EventBus;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainActivity;
import testme.myapp.com.ntrp.extra.MyToken;
import testme.myapp.com.ntrp.extra.UserModel;
import testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models.RegisterUserModel;
import testme.myapp.com.ntrp.module_forgotpassword.fragments.ForgotPasswordFragment;
import testme.myapp.com.ntrp.module_login.events.AskMobileNumberDialogEvent;
import testme.myapp.com.ntrp.module_login.events.UserLoginEvent;
import testme.myapp.com.ntrp.module_login.models.LoginUserModel;
import testme.myapp.com.ntrp.module_nonregisteredusers.events.DeletePurposeEvent;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsers_DepositorDetailsFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.UserInfoModel;
import testme.myapp.com.ntrp.module_registeration.activity.RegistrationScreen;
import testme.myapp.com.ntrp.module_registeration.fragments.RegisterFragment;
import testme.myapp.com.ntrp.module_trackyourpayment.events.PutUserInputEvent;
import testme.myapp.com.ntrp.module_trackyourpayment.models.TokenModel;
import testme.myapp.com.ntrp.module_trackyourpayment.mycaptcha.TextCaptcha;
import testme.myapp.com.ntrp.networking.AppController;
import testme.myapp.com.ntrp.networking.HandleHttpsRequest;
import testme.myapp.com.ntrp.utils.AllUrls;

/**
 * Created by deepika.s on 3/20/2017.
 */

public class LoginFragment extends Fragment {
    private LoginUserModel userInfoModel;
    private View localView;
    private TextCaptcha textCaptcha;
    private ImageView captcha_ImageView;
    private Button Captcha_Retry_Button,Register_As_User_Button,LoginButton;
    private TextView Forgot_Password_Textview;
    private EditText UserName_EditText,Password_EditText,Captcha_EditText;
    private String userNameText,passwordText,captchaText;
    @BindView(R.id.layout_newuser)
    public RelativeLayout layout_newuser;

    @BindView(R.id.layout_forgotpwd)
    public RelativeLayout layout_forgotpwd;

    @BindView(R.id.wrong_credntial_textview)
    public TextView wrong_credntial_textview;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        localView=view;
        ButterKnife.bind(this,view);
        getActivity().setTitle("Login");
        initializeView();
        handle_LoginButton();
        generate_Captcha();
        reload_Captcha_Button();


        if (TextUtils.isEmpty(Prefs.getString("userData"))) {
            EventBus.getDefault().post(new AskMobileNumberDialogEvent());
        } else {
            UserModel model =
            new Gson().fromJson(Prefs.getString("userData"),UserModel.class);
            Hit_RegisterUser_API(model.getMobileNumber());

        }

        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle("Login");
    }
    private void initialize_EditText_Views()
    {
        UserName_EditText= (EditText) localView.findViewById(R.id.username_edittext);
        Password_EditText= (EditText) localView.findViewById(R.id.password_edittext);
        Captcha_EditText= (EditText) localView.findViewById(R.id.captcha_edittext);
    }

    private void initializeView() {
        initialize_EditText_Views();



        Forgot_Password_Textview= (TextView) localView.findViewById(R.id.password_edittext);
        Forgot_Password_Textview.setSelected(true);
        layout_forgotpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFragment_ForgotPasswordFragment();
                return;
            }
        });

    }

    @OnClick(R.id.layout_newuser)
    public void OnNewUserButtonClick()
    {
        changeScreen_RegistrationScreen();
    }

    private void generate_Captcha() {
        textCaptcha = new TextCaptcha(1500, 400, 6, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
        captcha_ImageView = (ImageView) localView.findViewById(R.id.captcha_imageview);
        captcha_ImageView.setImageBitmap(textCaptcha.getImage());
    }

    private void handle_LoginButton()
    {
        LoginButton= (Button) localView.findViewById(R.id.login_button);
        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userNameText=UserName_EditText.getText().toString();
                passwordText=Password_EditText.getText().toString();
                captchaText=Captcha_EditText.getText().toString();
                if(userNameText.matches("")==true&&passwordText.matches("")==true&&captchaText.matches("")==true)
                {
                    UserName_EditText.setError("Please Enter User Name");
                    Password_EditText.setError("Password Required");

                    return;
                }
                else if(userNameText.matches("")==true&&passwordText.matches("")==false&&captchaText.matches("")==false)
                {
                    showDialog_Error("Please Enter User Name");
                    return;
                }
                else if(userNameText.matches("")==false&&passwordText.matches("")==true&&captchaText.matches("")==false)
                {
                    Password_EditText.setError("Password Required");
                    return;
                }
                else if(userNameText.matches("")==false&&passwordText.matches("")==false&&captchaText.matches("")==true)
                {
                    showDialog_Error("Enter the text as shown in the Captcha");
                    return;
                }
                else if(userNameText.matches("")==false&&passwordText.matches("")==false&&captchaText.matches("")==false)
                {
                    if(captchaText.matches(textCaptcha.input_captcha_string))
                    {
                        wrong_credntial_textview.setVisibility(View.GONE);
                        Hit_Submit_User_Login_Details_API(userNameText,passwordText);
                    }
                    else
                    {
                        showDialog_Error("The text you entered does not matches with the Captcha. Please try again.");
                        return;
                    }
                }

            }
        });
    }
    private void showDialog_Error(String errorText) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.error_textview);
        error_textview.setText(errorText);

        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    private void reload_Captcha_Button()
    {
        Captcha_Retry_Button = (Button) localView.findViewById(R.id.captcha_retry_button);
        Captcha_Retry_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                generate_Captcha();
                return;
            }
        });
    }
    private void changeScreen_RegistrationScreen() {

        /*Intent intent=new Intent(getActivity(), RegistrationScreen.class);
        getActivity().startActivity(intent);*/

        Fragment forgotPasswordFragment = new RegisterFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, forgotPasswordFragment);
        ft.addToBackStack(null);
        ft.commit();

    }
    private void changeFragment_ForgotPasswordFragment() {

        Fragment forgotPasswordFragment = new ForgotPasswordFragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, forgotPasswordFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void Hit_Submit_User_Login_Details_API(String UserName, String UserPassword) {

        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.Non_Register_User_Submit_User_Details_URL;
        final ProgressDialog pDialog = new ProgressDialog(LoginFragment.this.getActivity(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                userInfoModel = gson.fromJson(response, LoginUserModel.class);

                MyToken.setToken(userInfoModel.getRefreshTokenValue());
                MyToken.setAuthorization(userInfoModel.getAcsessTokenValue());


                if (userInfoModel.getBankAccount()!=null&&userInfoModel.getBankIfscCode()!=null&&userInfoModel.getAcsessTokenValue()!=null&&userInfoModel.getRefreshTokenValue()!=null)
                {
                    MainActivity.IsLogin=true;
                    MainActivity.UserID= String.valueOf(userInfoModel.getOffLineUserId());
                    MainActivity.UserName=userNameText;


                    EventBus.getDefault().post(new UserLoginEvent(
                            true

                    ));
                    EventBus.getDefault().post(new PutUserInputEvent(
                            "101",userInfoModel.getMobileNo(),userInfoModel.getEmail()

                    ));



                    changeFragment_UserDetails();

                    wrong_credntial_textview.setVisibility(View.GONE);

                }
                else if (userInfoModel.getResponseStatus().matches("Fail")&&userInfoModel.getErrorCodes()!=null)
                {
                    wrong_credntial_textview.setVisibility(View.VISIBLE);
                    //wrong_credntial_textview.setText(""+userInfoModel.getErrorCodes()+" ");
                    wrong_credntial_textview.setText("Wrong Credentials !");
                }
                else
                {
                    wrong_credntial_textview.setVisibility(View.VISIBLE);
                    wrong_credntial_textview.setText("Server Error occured !");
                }





            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError(error.getMessage());
                //showDialog_ServerError("Some error occured !");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("LoginUserName", UserName);

                String password_hashed=HashingSha256(UserPassword);

                String RandomString=getRandomNumberString();

                String password_hashed_hexa=HashingSha256(RandomString+password_hashed.toUpperCase());

                params.put("Password", password_hashed_hexa);

                params.put("newRno", RandomString);


                Gson gson = new GsonBuilder().setLenient().create();
                UserModel userModel = gson.fromJson(Prefs.getString("userData"), UserModel.class);

                params.put("RefreshTokenValue", userModel.getRefreshToken());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                Gson gson = new GsonBuilder().setLenient().create();
                 UserModel userModel = gson.fromJson(Prefs.getString("userData"), UserModel.class);

                headers.put("Authorization", "Bearer "+userModel.getAccessToken());
                // headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }
        };

        //strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);


    }
    private void changeFragment_UserDetails()
    {
        /*Bundle b = new Bundle();
        b.putString("UserName",userNameText);

        Fragment nonRegisteredUsers_depositorDetailsFragment = new UserLogedInHomeFragment();
        nonRegisteredUsers_depositorDetailsFragment.setArguments(b);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, nonRegisteredUsers_depositorDetailsFragment,"UserLogedInHomeFragment");
        ft.addToBackStack(null);
        ft.commit();*/

        Bundle b = new Bundle();
        b.putString("UserName",userNameText);

        Fragment nonRegisteredUsers_depositorDetailsFragment = new WelcomeFragment();
        nonRegisteredUsers_depositorDetailsFragment.setArguments(b);
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, nonRegisteredUsers_depositorDetailsFragment);
        ft.addToBackStack(null);
        ft.commit();


    }

    private void Hit_RegisterUser_API(String mobileNumber) {
        System.out.println("Hit_RegisterUser_API....................................");
        HandleHttpsRequest.setSSL();
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.registerAPIUserURL;
        final ProgressDialog pDialog = new ProgressDialog(getContext(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                System.out.println(response);
                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                RegisterUserModel registerUserModel = gson.fromJson(response, RegisterUserModel.class);

                if(registerUserModel.getErrorCodes().toString().matches("User Already Exists")||
                        registerUserModel.getErrorCodes().toString().matches("User Registered Successfully"))
                {
                    Hit_API_Token(mobileNumber);
                }
                else
                {

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                System.out.println(error.toString());
                pDialog.hide();
                pDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("MobileNo", ""+mobileNumber);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                return headers;
            }
        };

       // strReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    public void Hit_API_Token(String mobileNumber) {
        //System.out.println("Token API for registration..............");
        String tag_json_obj = "json_obj_req";
        String url = AllUrls.getTokenURL;
        final ProgressDialog pDialog = new ProgressDialog(getContext(), R.style.MyTheme);
        pDialog.setMessage("Loading...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                pDialog.hide();
                pDialog.dismiss();
                Gson gson = new GsonBuilder().setLenient().create();
                TokenModel tokenModel = gson.fromJson(response, TokenModel.class);
                MyToken.setToken(tokenModel.refresh_token);
                MyToken.setAuthorization(tokenModel.access_token);
                if (MyToken.getToken() != null) {
                    UserModel model = new UserModel();
                    model.setMobileNumber(mobileNumber);
                    model.setAccessToken(tokenModel.access_token);
                    model.setRefreshToken(tokenModel.refresh_token);
                    Prefs.putString("userData", new Gson().toJson(model));

                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("goterror= ", "Error: " + error.getMessage());
                //System.out.println("Got error= "+error.getMessage());
                pDialog.hide();
                pDialog.dismiss();
                //showDialog_ServerError("Server Error, Please Try again later");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserName", mobileNumber);
                params.put("grant_type", "password");
                params.put("Operator", "airtel");
                params.put("DeviceID", "123");
                params.put("AppType", "NTRP-APP");
                params.put("AppVersion", "1.0");
                params.put("OS", "android");
                // params.put("UserName",mobileNumber );
                //params.put("password", "123456");
                params.put("Flag", "5");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //headers.put("Authorization", "Bearer "+ MyToken.getAuthorization());
                return headers;
            }


        };

      //  strReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        strReq.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private  String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        SecureRandom rnd = new SecureRandom ();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }
    private String HashingSha256(String Input) {
        final HashFunction hashFunction = Hashing.sha256();
        final HashCode hc = hashFunction
                .newHasher()
                .putString(Input, Charsets.UTF_8)
                .hash();
        final String sha256 = hc.toString();
        return sha256;
    }

}