package testme.myapp.com.ntrp.module_donatetoswachhbharatkosh.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DepositerDetailslst {

    @SerializedName("DepositorName")
    @Expose
    private String depositorName;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("District")
    @Expose
    private String district;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("Pincode")
    @Expose
    private String pincode;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("AadhaarNo")
    @Expose
    private String aadhaarNo;
    @SerializedName("PANNo")
    @Expose
    private String pANNo;
    @SerializedName("TANNo")
    @Expose
    private String tANNo;
    @SerializedName("TINNo")
    @Expose
    private String tINNo;
    @SerializedName("PurposeDetails")
    @Expose
    private Object purposeDetails;
    @SerializedName("Paytmentmodes")
    @Expose
    private String paytmentmodes;
    @SerializedName("IfExternalIntegration")
    @Expose
    private Integer ifExternalIntegration;

    public String getDepositorName() {
        return depositorName;
    }

    public void setDepositorName(String depositorName) {
        this.depositorName = depositorName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAadhaarNo() {
        return aadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    public String getPANNo() {
        return pANNo;
    }

    public void setPANNo(String pANNo) {
        this.pANNo = pANNo;
    }

    public String getTANNo() {
        return tANNo;
    }

    public void setTANNo(String tANNo) {
        this.tANNo = tANNo;
    }

    public String getTINNo() {
        return tINNo;
    }

    public void setTINNo(String tINNo) {
        this.tINNo = tINNo;
    }

    public Object getPurposeDetails() {
        return purposeDetails;
    }

    public void setPurposeDetails(Object purposeDetails) {
        this.purposeDetails = purposeDetails;
    }

    public String getPaytmentmodes() {
        return paytmentmodes;
    }

    public void setPaytmentmodes(String paytmentmodes) {
        this.paytmentmodes = paytmentmodes;
    }

    public Integer getIfExternalIntegration() {
        return ifExternalIntegration;
    }

    public void setIfExternalIntegration(Integer ifExternalIntegration) {
        this.ifExternalIntegration = ifExternalIntegration;
    }
}
