package testme.myapp.com.ntrp.module_trackyourpayment.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_trackyourpayment.models.DepositDetailsList;

/**
 * Created by prakhar.s on 8/4/2017.
 */

public class DepositDetailsAdaptor  extends RecyclerView.Adapter<DepositDetailsAdaptor.DepositDetailsViewHolder> {

    private List<DepositDetailsList> data;
    private Context ctx;



    public DepositDetailsAdaptor(List<DepositDetailsList> data, Context ctx) {
        this.data = data;

        this.ctx=ctx;
    }


    @Override
    public DepositDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.single_view_deposit_detail, parent, false);

        return new DepositDetailsViewHolder(itemView);
    }




    @Override
    public void onBindViewHolder(DepositDetailsViewHolder holder, int i) {

        final DepositDetailsList Data = data.get(i);


        holder.Purpose_Value_Textview.setText(Data.PurposeDescription);
        holder.Amount_Value_Textview.setText(Data.AmountValue);
        holder.Ministry_Value_Textview.setText(Data.ControllerName);
        holder.FunctionalHead_Value_Textview.setText(Data.FuncHeadDescription);
        holder.DdoName_Value_Textview.setText(Data.DDOName);
        holder.PaoName_Value_Textview.setText(Data.PAOName);
        holder.PaymentFrequency_Value_Textview.setText(Data.AccountingPeriod);
        holder.DocumentList_Value_Textview.setText(Data.AgencyName);


    }



    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }




    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
    }


    protected class DepositDetailsViewHolder extends RecyclerView.ViewHolder {


        protected TextView Purpose_Value_Textview,Amount_Value_Textview,Ministry_Value_Textview,FunctionalHead_Value_Textview;
        protected TextView DdoName_Value_Textview,PaoName_Value_Textview,PaymentFrequency_Value_Textview,DocumentList_Value_Textview;

        protected DepositDetailsViewHolder(View view) {

            super(view);

            Purpose_Value_Textview= (TextView) view.findViewById(R.id.purpose_value_textview);
            Amount_Value_Textview= (TextView) view.findViewById(R.id.amount_value_textview);
            Ministry_Value_Textview= (TextView) view.findViewById(R.id.ministry_value_textview);
            FunctionalHead_Value_Textview= (TextView) view.findViewById(R.id.functionalhead_value_textview);
            DdoName_Value_Textview= (TextView) view.findViewById(R.id.ddoname_value_textview);
            PaoName_Value_Textview= (TextView) view.findViewById(R.id.paoname_value_textview);
            PaymentFrequency_Value_Textview= (TextView) view.findViewById(R.id.paymentfrequency_value_textview);
            DocumentList_Value_Textview= (TextView) view.findViewById(R.id.documentlist_value_textview);




        }

    }

}
