package testme.myapp.com.ntrp.module_trackyourpayment.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_trackyourpayment.models.DepositAddtionalList;

/**
 * Created by prakhar.s on 8/4/2017.
 */

public class AdditionalChargesAdaptor extends RecyclerView.Adapter<AdditionalChargesAdaptor.AdditionalChargesViewHolder> {

    private List<DepositAddtionalList> data;
    private Context ctx;



    public AdditionalChargesAdaptor(List<DepositAddtionalList> data, Context ctx) {
        this.data = data;

        this.ctx=ctx;
    }


    @Override
    public AdditionalChargesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.single_view_additional_charges, parent, false);

        return new AdditionalChargesViewHolder(itemView);
    }




    @Override
    public void onBindViewHolder(AdditionalChargesViewHolder holder, int i) {

         DepositAddtionalList Data = data.get(i);




        holder.AdditionalCharge_Textview.setText(Data.AdditionalCharge);

        holder.AdditionalAmount_TextView.setText(Data.AdditionalAmount);

        holder.AdditionalFunctinal_TextView.setText(Data.FunctionalHead);

        holder.DDOName_TextView.setText(Data.DDOName);

        holder.PPOName_TextView.setText(Data.PAOName);



    }



    @Override
    public int getItemCount() {
        return data.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }




    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, data.size());
    }


    protected class AdditionalChargesViewHolder extends RecyclerView.ViewHolder {

        protected TextView AdditionalCharge_Textview,
        AdditionalAmount_TextView,AdditionalFunctinal_TextView ,DDOName_TextView,PPOName_TextView;

        protected AdditionalChargesViewHolder(View view) {
            super(view);

            AdditionalCharge_Textview= (TextView) view.findViewById(R.id.additionalcharge_value_textview);
            AdditionalAmount_TextView= (TextView) view.findViewById(R.id.additionalamount_value_textview);
            AdditionalFunctinal_TextView= (TextView) view.findViewById(R.id.additional_functionalhead_value_textview);
            DDOName_TextView= (TextView) view.findViewById(R.id.additionalddoname_value_textview);
            PPOName_TextView= (TextView) view.findViewById(R.id.additionalpponame_value_textview);
        }

    }

}
