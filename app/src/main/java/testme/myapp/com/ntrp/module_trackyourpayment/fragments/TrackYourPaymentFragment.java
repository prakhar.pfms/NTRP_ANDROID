package testme.myapp.com.ntrp.module_trackyourpayment.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.activity.MainScreen;
import testme.myapp.com.ntrp.extra.RepeatSafeToast;
import testme.myapp.com.ntrp.module_trackyourpayment.HitAPI;
import testme.myapp.com.ntrp.module_trackyourpayment.activity.TrackYourPaymentScreen;
import testme.myapp.com.ntrp.module_trackyourpayment.models.UserInputDetailModel;
import testme.myapp.com.ntrp.networking.CheckNetworkConnection;
import testme.myapp.com.ntrp.extra.MyCustomToast;
import testme.myapp.com.ntrp.module_trackyourpayment.mycaptcha.TextCaptcha;


/**
 * Created by deepika.s on 4/10/2017.
 */

public class TrackYourPaymentFragment extends Fragment {

    public View localView;
    private View Country_Linear_Layout, Mobile_Linear_Layout, Email_Linear_Layout, ATR_No_Linear_Layout, BR_No_Linear_Layout;
    private View Passport_No_Linear_Layout;
    private View Otp_Hint_Linear_Layout, Enter_OTP_Code_Linear_Layout, Captcha_Linear_Layout, Security_Code_linear_Layout;
    private View Send_OTP_Linear_Layout, Verify_Btn_Linear_Layout;
    private TextView Country_TextView, Security_Code_TextView, Atr_No_TextView, Br_No_TextView, Password_No_TextView;
    private TextView Mobile_TextView, Email_TextView, Enter_OTP_Code_TextView;
    public TextView OTP_Expires_Textview;
    public EditText Security_Code_EditText, Email_Id_EditText;
    public EditText Mobile_No_EditText,Enter_OTP_Code_EditText;
    public EditText Atr_No_EditText,Br_No_EditText,Passport_No_EditText;
    private RadioButton Pos_Based_Transaction_Checkbox, Custom_Based_Transaction_Checkbox,is_normal_transaction_checkbox;
    private RadioGroup radioGroup;



    private Button Send_OTP_Button, reload, Verify_Button;
    private Spinner country_Spinner;
    private ImageView captcha_ImageView;
    private TextCaptcha textCaptcha;
    public int countryIndex;
    private String captcha_text,email_text,mobile_text;
    public String User_MobileNumber,User_EmailID="",User_CountryCode,User_AtrNumber,User_BrNumber,User_PassportNumber;
    private CountDownTimer countDownTimer;
    private TrackYourPaymentFragment thisFragment;
    public static int financialYearIndex;
    public static boolean IsPOSChecked=false,IsCustomChecked=false,IsNormalChecked=false;
    public static boolean IsCountDownStart=false;
    int hoursToGo = 0;
    int minutesToGo = 5;
    int secondsToGo = 0;
    long milliLeft;
    public int millisToGo = secondsToGo*1000+minutesToGo*1000*60+hoursToGo*1000*60*60;

    private AutoCompleteTextView country_auto_complete_text_view;
    private int AutoComplete_Selected_Country_ID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_trackyourpayment, container, false);
        this.localView = view;
        IsCountDownStart=false;
        getActivity().setTitle("Track Payment");
        initializeView();
        initializeCountrySpinnerView();
        setTextChangeListener_Mobile_No_EditText();
        send_OTP_onClick();
        generate_Captcha();
        reload_Captcha_Button();
        thisFragment=this;
        set_VerifyButton();
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle("Track Payment");
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CloseKeyBoard();
    }
    private void CloseKeyBoard()
    {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(localView.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("On Paused");
        System.out.println("IsCountDownStart in on pause== "+IsCountDownStart);
        if(IsCountDownStart) {

            countDownTimer.cancel();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("On Resume");
        System.out.println("IsCountDownStart in on Resume== "+IsCountDownStart);
        if(IsCountDownStart) {

            setTimerOnResume(OTP_Expires_Textview,milliLeft);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        System.out.println("Detached");
        System.out.println("IsCountDownStart in on Detached== "+IsCountDownStart);
        if(IsCountDownStart) {
            IsCountDownStart=false;
            countDownTimer.cancel();
        }
    }

    private void initializeView() {
        financialYearIndex=0;
        initialize_TextViews();
        initialize_EditTextViews();
        initialize_ImageViews();
        initialize_LayoutViews();
        putSingleRedStar();
        //show_Both_Unchecked();
        //hide_Both_Unchecked();
        //setCheckListeners();
        initialize_CheckBoxViews();

    }
    private void initialize_TextViews() {
        country_auto_complete_text_view= (AutoCompleteTextView) localView.findViewById(R.id.country_auto_complete_text_view);

        Country_TextView = (TextView) localView.findViewById(R.id.country_textview);
        Security_Code_TextView = (TextView) localView.findViewById(R.id.security_code_textview);
        Atr_No_TextView = (TextView) localView.findViewById(R.id.atr_no_textview);
        Br_No_TextView = (TextView) localView.findViewById(R.id.br_no_textview);
        Password_No_TextView = (TextView) localView.findViewById(R.id.passport_no_textview);
        Mobile_TextView = (TextView) localView.findViewById(R.id.mobile_textview);
        Email_TextView = (TextView) localView.findViewById(R.id.email_id_textview);
        Enter_OTP_Code_TextView = (TextView) localView.findViewById(R.id.enter_OTP_Code_textview);
    }
    private void initialize_EditTextViews() {
        Security_Code_EditText = (EditText) localView.findViewById(R.id.security_code_edittext);
        Email_Id_EditText = (EditText) localView.findViewById(R.id.email_id_edittext);
        Enter_OTP_Code_EditText = (EditText) localView.findViewById(R.id.enter_OTP_Code_edittext);
        Mobile_No_EditText = (EditText) localView.findViewById(R.id.mobile_edittext);
        Atr_No_EditText=(EditText) localView.findViewById(R.id.atr_no_edittext);
        Br_No_EditText=(EditText) localView.findViewById(R.id.br_no_edittext);
        Passport_No_EditText=(EditText) localView.findViewById(R.id.passport_no_edittext);

    }
    private void initialize_ImageViews()
    {
        captcha_ImageView = (ImageView) localView.findViewById(R.id.captcha_imageview);
    }
    private void initialize_CheckBoxViews() {

        radioGroup = (RadioGroup) localView.findViewById(R.id.radioGroup);
        radioGroup.clearCheck();
        Pos_Based_Transaction_Checkbox = (RadioButton) localView.findViewById(R.id.is_pos_based_transaction_checkbox);
        Custom_Based_Transaction_Checkbox = (RadioButton) localView.findViewById(R.id.is_custom_based_payment_checkbox);
        is_normal_transaction_checkbox = (RadioButton) localView.findViewById(R.id.is_normal_transaction_checkbox);

        IsNormalChecked=true;
        IsPOSChecked=false;
        IsCustomChecked=false;

        is_normal_transaction_checkbox.setChecked(true);
        Pos_Based_Transaction_Checkbox.setChecked(false);
        Custom_Based_Transaction_Checkbox.setChecked(false);

        show_Both_Unchecked();
        hide_Both_Unchecked();



        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                View radioButton = radioGroup.findViewById(checkedId);
                int index = radioGroup.indexOfChild(radioButton);
                if(index==0)   //Normal
                {
                    HitAPI.MobileNo=null;
                    HitAPI.Email=null;

                    IsNormalChecked=true;
                    IsPOSChecked=false;
                    IsCustomChecked=false;

                    Security_Code_EditText.setText("");
                    generate_Captcha();


                    show_Both_Unchecked();
                    hide_Both_Unchecked();




                }
                else if(index==1) //POS
                {

                    HitAPI.MobileNo=null;
                    HitAPI.Email=null;

                    IsNormalChecked=false;
                    IsPOSChecked=true;
                    IsCustomChecked=false;

                    Security_Code_EditText.setText("");
                    generate_Captcha();
                    show_POS_Checked();
                    hide_POS_Checked();










                }
                else if(index==2)   //Custom
                {
                    HitAPI.MobileNo=null;
                    HitAPI.Email=null;

                    IsNormalChecked=false;
                    IsPOSChecked=false;
                    IsCustomChecked=true;

                    Security_Code_EditText.setText("");
                    generate_Captcha();


                    show_Custom_Checked();
                    hide_Custom_Checked();








                }
            }
        });

    }
    private void initialize_LayoutViews() {
        Country_Linear_Layout = localView.findViewById(R.id.country_linear_layout);
        Mobile_Linear_Layout = localView.findViewById(R.id.mobile_no_linear_layout);
        Email_Linear_Layout = localView.findViewById(R.id.email_linear_layout);
        ATR_No_Linear_Layout = localView.findViewById(R.id.atr_no_linear_layout);
        BR_No_Linear_Layout = localView.findViewById(R.id.br_no_linear_layout);
        Passport_No_Linear_Layout = localView.findViewById(R.id.passport_no_linear_layout);
        Otp_Hint_Linear_Layout = localView.findViewById(R.id.otp_hint_linear_layout);
        Enter_OTP_Code_Linear_Layout = localView.findViewById(R.id.enter_OTP_Code_layout);
        Captcha_Linear_Layout = localView.findViewById(R.id.captcha_layout);
        Security_Code_linear_Layout = localView.findViewById(R.id.security_code_layout);
        Send_OTP_Linear_Layout = localView.findViewById(R.id.send_otp_button);
        Verify_Btn_Linear_Layout = localView.findViewById(R.id.verify_btn_layout);
    }
    private void putSingleRedStar() {
        putRedAsterisk(localView, Country_TextView, R.id.country_textview);
        putRedAsterisk(localView, Security_Code_TextView, R.id.security_code_textview);
        putRedAsterisk(localView, Atr_No_TextView, R.id.atr_no_textview);
        putRedAsterisk(localView, Br_No_TextView, R.id.br_no_textview);
        putRedAsterisk(localView, Password_No_TextView, R.id.passport_no_textview);
        putRedAsterisk(localView, Enter_OTP_Code_TextView, R.id.enter_OTP_Code_textview);
    }
    private void show_Both_Unchecked()
    {
        Country_Linear_Layout.setVisibility(View.VISIBLE);
        Country_Linear_Layout.setEnabled(true);
        Mobile_Linear_Layout.setVisibility(View.VISIBLE);
        Mobile_Linear_Layout.setEnabled(true);
        Email_Linear_Layout.setVisibility(View.VISIBLE);
        Email_Linear_Layout.setEnabled(true);
        Captcha_Linear_Layout.setVisibility(View.VISIBLE);
        Captcha_Linear_Layout.setEnabled(true);
        Security_Code_linear_Layout.setVisibility(View.VISIBLE);
        Security_Code_linear_Layout.setEnabled(true);
        Send_OTP_Linear_Layout.setVisibility(View.VISIBLE);
        Send_OTP_Linear_Layout.setEnabled(true);
    }
    private void hide_Both_Unchecked()
    {
        ATR_No_Linear_Layout.setVisibility(View.GONE);
        BR_No_Linear_Layout.setVisibility(View.GONE);
        Passport_No_Linear_Layout.setVisibility(View.GONE);
        Enter_OTP_Code_Linear_Layout.setVisibility(View.GONE);
        Verify_Btn_Linear_Layout.setVisibility(View.GONE);
        Otp_Hint_Linear_Layout.setVisibility(View.GONE);

    }
    private void setCheckListeners() {
        Pos_Based_Transaction_Checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IsPOSChecked=true;
                    Security_Code_EditText.setText("");
                    generate_Captcha();
                    if (Custom_Based_Transaction_Checkbox.isChecked()) {
                        IsCustomChecked=false;
                        //Custom_Based_Transaction_Checkbox.setChecked(false);
                        show_POS_Checked();
                        hide_POS_Checked();
                    } else {
                        show_POS_Checked();
                        hide_POS_Checked();
                    }

                    //is_normal_transaction_checkbox.setChecked(false);

                    return;
                } else {
                    Security_Code_EditText.setText("");
                    generate_Captcha();
                    IsPOSChecked=false;
                    show_Both_Unchecked();
                    hide_Both_Unchecked();

                    //is_normal_transaction_checkbox.setChecked(true);
                    return;
                }
            }
        });

        Custom_Based_Transaction_Checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IsCustomChecked=true;
                    Security_Code_EditText.setText("");
                    generate_Captcha();
                    if (Pos_Based_Transaction_Checkbox.isChecked()) {
                        //Pos_Based_Transaction_Checkbox.setChecked(false);
                        IsPOSChecked=false;
                        show_Custom_Checked();
                        hide_Custom_Checked();
                    } else {
                        show_Custom_Checked();
                        hide_Custom_Checked();
                    }

                    //is_normal_transaction_checkbox.setChecked(false);
                    return;
                } else {
                    IsCustomChecked=false;
                    Security_Code_EditText.setText("");
                    generate_Captcha();
                    show_Both_Unchecked();
                    hide_Both_Unchecked();

                    //is_normal_transaction_checkbox.setChecked(true);
                    return;
                }
            }
        });


        is_normal_transaction_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    IsCustomChecked=false;
                    IsPOSChecked=false;
                    Security_Code_EditText.setText("");
                    generate_Captcha();
                    if (Pos_Based_Transaction_Checkbox.isChecked()) {
                        //Pos_Based_Transaction_Checkbox.setChecked(false);
                        IsPOSChecked=false;
                        show_Custom_Checked();
                        hide_Custom_Checked();
                    } else {
                        show_Custom_Checked();
                        hide_Custom_Checked();
                    }


                    return;
                } else {
                    IsCustomChecked=false;
                    Security_Code_EditText.setText("");
                    generate_Captcha();
                    show_Both_Unchecked();
                    hide_Both_Unchecked();
                    //is_normal_transaction_checkbox.setEnabled(false);
                    //is_normal_transaction_checkbox.setChecked(true);
                    return;
                }
            }
        });

    }
    private  void setValidation_POS_Transaction()
    {

    }
    private void show_POS_Checked()
    {
        ATR_No_Linear_Layout.setVisibility(View.VISIBLE);
        ATR_No_Linear_Layout.setEnabled(true);
        Captcha_Linear_Layout.setVisibility(View.VISIBLE);
        Captcha_Linear_Layout.setEnabled(true);
        Security_Code_linear_Layout.setVisibility(View.VISIBLE);
        Security_Code_linear_Layout.setEnabled(true);
        Verify_Btn_Linear_Layout.setVisibility(View.VISIBLE);
        Verify_Btn_Linear_Layout.setEnabled(true);
    }
    private void hide_POS_Checked()
    {
        Country_Linear_Layout.setVisibility(View.GONE);
        Mobile_Linear_Layout.setVisibility(View.GONE);
        Email_Linear_Layout.setVisibility(View.GONE);
        Enter_OTP_Code_Linear_Layout.setVisibility(View.GONE);
        Send_OTP_Linear_Layout.setVisibility(View.GONE);
        BR_No_Linear_Layout.setVisibility(View.GONE);
        Passport_No_Linear_Layout.setVisibility(View.GONE);
        Otp_Hint_Linear_Layout.setVisibility(View.GONE);
    }
    private void show_Custom_Checked()
    {
        BR_No_Linear_Layout.setVisibility(View.VISIBLE);
        BR_No_Linear_Layout.setEnabled(true);
        Passport_No_Linear_Layout.setVisibility(View.VISIBLE);
        Passport_No_Linear_Layout.setEnabled(true);
        Captcha_Linear_Layout.setVisibility(View.VISIBLE);
        Captcha_Linear_Layout.setEnabled(true);
        Security_Code_linear_Layout.setVisibility(View.VISIBLE);
        Security_Code_linear_Layout.setEnabled(true);
        Verify_Btn_Linear_Layout.setVisibility(View.VISIBLE);
        Verify_Btn_Linear_Layout.setEnabled(true);
    }
    private void hide_Custom_Checked()
    {
        Country_Linear_Layout.setVisibility(View.GONE);
        Mobile_Linear_Layout.setVisibility(View.GONE);
        Email_Linear_Layout.setVisibility(View.GONE);
        Enter_OTP_Code_Linear_Layout.setVisibility(View.GONE);
        Send_OTP_Linear_Layout.setVisibility(View.GONE);
        ATR_No_Linear_Layout.setVisibility(View.GONE);
        Otp_Hint_Linear_Layout.setVisibility(View.GONE);

    }
    private void initializeCountrySpinnerView() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.single_row_spinner, getResources().getStringArray(R.array.country_list));
        country_Spinner = (Spinner) localView.findViewById(R.id.spinner_country);
        country_Spinner.setAdapter(adapter);

        assignItemArraytoCountrySpinner();


        country_auto_complete_text_view.setThreshold(1);
        //Set the adapter
        country_auto_complete_text_view.setAdapter(adapter);
        country_auto_complete_text_view.setText("India");
        country_Spinner.setSelection(100);
        Mobile_No_EditText.setEnabled(true);
        Mobile_No_EditText.setBackgroundResource(R.drawable.back);
        User_CountryCode="91";
        countryIndex=   100;


        country_auto_complete_text_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (country_auto_complete_text_view.getText().toString().matches("INDIA")) {
                    putDoubleRedAsterisk(localView, Mobile_TextView, R.id.mobile_textview);
                    Email_TextView.setText("Email Id :");
                    putDoubleRedAsterisk(localView, Email_TextView, R.id.email_id_textview);
                    Mobile_No_EditText.setEnabled(true);
                    Mobile_No_EditText.setBackgroundResource(R.drawable.back);
                    User_CountryCode="91";
                    countryIndex=   100;
                } else {

                    Mobile_No_EditText.setText("");
                    Mobile_TextView.setText("Mobile Number:");
                    Mobile_No_EditText.setEnabled(false);
                    Mobile_No_EditText.setBackgroundResource(R.drawable.unfocusededittext);
                    User_CountryCode="null";
                    countryIndex=   23;



                }
            }
        });



    }
    private void assignItemArraytoCountrySpinner() {

        country_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (country_Spinner.getSelectedItem().toString().matches("INDIA")) {
                    putDoubleRedAsterisk(localView, Mobile_TextView, R.id.mobile_textview);
                    Email_TextView.setText("Email Id :");
                    putDoubleRedAsterisk(localView, Email_TextView, R.id.email_id_textview);
                    Mobile_No_EditText.setEnabled(true);
                    Mobile_No_EditText.setBackgroundResource(R.drawable.back);
                    User_CountryCode="91";

                } else {

                    Mobile_No_EditText.setText("");
                    Mobile_TextView.setText("Mobile Number:");
                    Mobile_No_EditText.setEnabled(false);
                    Mobile_No_EditText.setBackgroundResource(R.drawable.unfocusededittext);
                    User_CountryCode="null";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void setTextChangeListener_Mobile_No_EditText()
    {
        Mobile_No_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String enteredString = s.toString();
                if (enteredString.startsWith("0") || enteredString.startsWith("1") || enteredString.startsWith("2") ||
                        enteredString.startsWith("3") || enteredString.startsWith("4") || enteredString.startsWith("5")
                        ) {

                    Mobile_No_EditText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                            String enteredString = s.toString();
                            if (enteredString.startsWith("0") || enteredString.startsWith("1") || enteredString.startsWith("2") ||
                                    enteredString.startsWith("3") || enteredString.startsWith("4") || enteredString.startsWith("5")
                                    ) {

                                RepeatSafeToast.show(getActivity(),"Mobile No. should start with 6,7,8 or 9");


                                if (enteredString.length() > 0) {
                                    Mobile_No_EditText.setText(enteredString.substring(1));
                                } else {
                                    Mobile_No_EditText.setText("");
                                }
                                return;
                            }
                        }
                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                    if (enteredString.length() > 0) {
                        Mobile_No_EditText.setText(enteredString.substring(1));
                    } else {
                        Mobile_No_EditText.setText("");
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setTimer(final TextView textView,long millisToGo)
    {

        countDownTimer=new CountDownTimer(millisToGo,1000) {

            @Override
            public void onTick(long millis) {
                milliLeft=millis;
                int seconds = (int) (millis / 1000) % 60 ;
                int minutes = (int) ((millis / (1000*60)) % 60);
                int hours   = (int) ((millis / (1000*60*60)) % 24);
                String text = String.format("%02d hours, %02d minutes, %02d seconds",hours,minutes,seconds);
                String temp="OTP expires in: "+minutes+" minutes ,"+seconds+" seconds";
                textView.setText(temp);
                System.out.println(text);
                if(minutes==00&&seconds==01)
                {
                    getActivity().finish();
                    Intent intent=new Intent(getActivity(), TrackYourPaymentScreen.class);
                    getActivity().startActivity(intent);
                }
            }
            @Override
            public void onFinish() {

            }
        }.start();
    }
    public void setTimerOnResume(final TextView textView,long milliLeftTemp)
    {

        countDownTimer=new CountDownTimer(milliLeftTemp,1000) {

            @Override
            public void onTick(long millis) {
                milliLeft=millis;
                int seconds = (int) (millis / 1000) % 60 ;
                int minutes = (int) ((millis / (1000*60)) % 60);
                int hours   = (int) ((millis / (1000*60*60)) % 24);
                String text = String.format("%02d hours, %02d minutes, %02d seconds",hours,minutes,seconds);
                String temp="OTP expires in: "+minutes+" minutes ,"+seconds+" seconds";
                textView.setText(temp);
                System.out.println(text);
                if(minutes==00&&seconds==01)
                {
                    getActivity().finish();
                    Intent intent=new Intent(getActivity(), TrackYourPaymentScreen.class);
                    getActivity().startActivity(intent);
                }
            }
            @Override
            public void onFinish() {

            }
        }.start();
    }

    private void send_OTP_onClick() {
        Send_OTP_Button = (Button) localView.findViewById(R.id.send_otp_button);
        Send_OTP_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkValidationsOnOTPButtonClick();

            }
        });
    }
    private void checkValidationsOnOTPButtonClick()
    {
      //  countryIndex = country_auto_complete_text_view.getAdapter().get;
       // countryIndex = country_Spinner.getSelectedItemPosition();
        captcha_text = Security_Code_EditText.getText().toString();
        email_text = Email_Id_EditText.getText().toString();
        mobile_text= Mobile_No_EditText.getText().toString();
        User_EmailID=email_text;
        User_MobileNumber=mobile_text;
        User_CountryCode=countryIndex+"";
        if (countryIndex == 0)
        {
            MyCustomToast.createErrorToast(getActivity(), "Please select your country");
            return;
        }
        else if (countryIndex != 0 && countryIndex != 100)
        {
            // Non-Indian Country
            if (email_text.matches("")) {
                MyCustomToast.createErrorToast(getActivity(), "Email is required");
                return;
            } else if (!isValidEmail(email_text)) {
                MyCustomToast.createErrorToast(getActivity(), "Please Enter Valid Email Id");
                return;
            } else if (captcha_text.matches("")) {
                MyCustomToast.createErrorToast(getActivity(), "Please Enter Security Code");
                return;
            } else if (!captcha_text.matches(textCaptcha.input_captcha_string)) {
                Security_Code_EditText.setText("");
                generate_Captcha();
                showDialog_Captcha_Error("Security Code does not match, try again");
                return;
            }
            else if (captcha_text.matches(textCaptcha.input_captcha_string)) {
                if(email_text.matches(""))
                {
                    email_text=null;
                    User_EmailID=email_text;
                }

                CloseKeyBoard();
                User_CountryCode=User_CountryCode.trim();
                User_MobileNumber=User_MobileNumber.trim();
                User_EmailID=User_EmailID.trim();
                System.out.println(User_CountryCode+"   "+User_EmailID+"    "+User_MobileNumber);
                put_UserInputData(User_CountryCode,User_MobileNumber,User_EmailID,null,null,null);
                HitAPI.Hit_API_Token(getActivity(),thisFragment, Mobile_No_EditText.getText().toString(),Email_Id_EditText.getText().toString(),User_CountryCode);
            }
        }
        else if(countryIndex == 100) //INDIA
        {
            if (mobile_text.length() == 0 && email_text.length() == 0) {
                MyCustomToast.createErrorToast(getActivity(), "Either Email or mobile is required");
                return;
            }
            else if (mobile_text.length() != 0&&mobile_text.length()!=10) {
                MyCustomToast.createErrorToast(getActivity(), "Please Enter valid Mobile No");
                return;
            }
            else if (email_text.length() != 0&&!isValidEmail(email_text)) {
                MyCustomToast.createErrorToast(getActivity(), "Please Enter Valid Email Id");
                return;
            }
            else if (captcha_text.matches("")) {
                MyCustomToast.createErrorToast(getActivity(), "Please Enter Security Code");
                return;
            } else if (!captcha_text.matches(textCaptcha.input_captcha_string)) {
                Security_Code_EditText.setText("");
                generate_Captcha();
                showDialog_Captcha_Error("Security Code does not match, try again");
                return;
            }
            else if (captcha_text.matches(textCaptcha.input_captcha_string)) {

                if(email_text.matches(""))
                {
                    email_text=null;
                    User_EmailID=email_text;
                }
                CloseKeyBoard();

                System.out.println(User_CountryCode+"   "+User_EmailID+"    "+User_MobileNumber);
                put_UserInputData(User_CountryCode,User_MobileNumber,User_EmailID,null,null,null);
                if (CheckNetworkConnection.getInstance(getActivity()).isOnline())
                {
                    HitAPI.Hit_API_Token(getActivity(),thisFragment,Mobile_No_EditText.getText().toString(),Email_Id_EditText.getText().toString(),User_CountryCode);
                }
                else {
                    Toast.makeText(getActivity(), "Check Your Internet Connection!",
                            Toast.LENGTH_LONG).show();
                }

            }
        }
    }
    private void put_UserInputData(String countryCode,String mobileno,String emailID,String AtrNo,String BrNo,String PassportNo)
    {
        UserInputDetailModel userInputDetailModel=new UserInputDetailModel();
        userInputDetailModel.CountryCode=countryCode;
        userInputDetailModel.MobileNumber=mobileno;
        userInputDetailModel.EmailID=emailID;
        userInputDetailModel.AtrNumber=AtrNo;
        userInputDetailModel.BrNumber=BrNo;
        userInputDetailModel.PassportNumber=PassportNo;
        MainScreen.UserInputBundle.putParcelable("userInputDetailModel",userInputDetailModel);
    }
    public void all_Data_Matched() {


        CloseKeyBoard();
        if(countryIndex==100)
        {
            Mobile_No_EditText.setText(mobile_text);
        }
        country_Spinner.setEnabled(false);
        Mobile_No_EditText.setEnabled(false);
        Mobile_No_EditText.setBackgroundResource(R.drawable.unfocusededittext);
        Email_Id_EditText.setEnabled(false);
        Email_Id_EditText.setBackgroundResource(R.drawable.unfocusededittext);
        Enter_OTP_Code_Linear_Layout.setVisibility(View.VISIBLE);
        Enter_OTP_Code_Linear_Layout.setEnabled(true);
        Otp_Hint_Linear_Layout.setVisibility(View.VISIBLE);
        Otp_Hint_Linear_Layout.setEnabled(true);

        OTP_Expires_Textview= (TextView) localView.findViewById(R.id.otp_expires_textview);


        Captcha_Linear_Layout.setVisibility(View.GONE);
        Security_Code_EditText.setText("");
        Security_Code_linear_Layout.setVisibility(View.GONE);

        Send_OTP_Linear_Layout.setVisibility(View.GONE);
        Verify_Btn_Linear_Layout.setVisibility(View.VISIBLE);
        Verify_Btn_Linear_Layout.setEnabled(true);

    }
    private void set_VerifyButton()
    {
        Verify_Button = (Button) localView.findViewById(R.id.verify_button);
        Verify_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CloseKeyBoard();
                if(IsPOSChecked==false&&IsCustomChecked==false)
                {


                    String inputOTP = Enter_OTP_Code_EditText.getText().toString();
                    if (inputOTP.matches("")) {
                        MyCustomToast.createErrorToast(getActivity(),
                                "OTP entered by you, does not matches with the one which was sent to you.");
                        return;
                    }

                    if (CheckNetworkConnection.getInstance(getActivity()).isOnline())
                    {
                        countDownTimer.cancel();
                        IsCountDownStart=false;
                        System.out.println("cancelled timer....");
                        HitAPI.Hit_API_VerifyOTP(getActivity(),thisFragment,inputOTP);

                    }
                    else {
                        Toast.makeText(getActivity(), "Check Your Internet Connection!",
                                Toast.LENGTH_LONG).show();
                    }
                    return;
                }
                else if(IsPOSChecked&&IsCustomChecked==false)
                {

                    String ATRNumber = Atr_No_EditText.getText().toString();
                    captcha_text = Security_Code_EditText.getText().toString();
                    if (ATRNumber.matches("")) {
                        MyCustomToast.createErrorToast(getActivity(), "Please Enter Atr No.");
                        return;
                    }
                    else if (captcha_text.matches("")) {
                        MyCustomToast.createErrorToast(getActivity(), "Please Enter Security Code");
                        return;
                    } else if (!captcha_text.matches(textCaptcha.input_captcha_string)) {
                        Security_Code_EditText.setText("");
                        generate_Captcha();
                        showDialog_Captcha_Error("Security Code does not match, try again");
                        return;
                    }
                    else if (captcha_text.matches(textCaptcha.input_captcha_string)) {


                        CloseKeyBoard();
                        if(!ATRNumber.matches(""))
                        {
                            User_AtrNumber=ATRNumber;
                        }

                        System.out.println(User_CountryCode+"   "+User_EmailID+"    "+User_MobileNumber);
                        put_UserInputData(null,null,null,User_AtrNumber,null,null);
                        if (CheckNetworkConnection.getInstance(getActivity()).isOnline())
                        {
                            HitAPI.Hit_API_Token_POSBased(User_AtrNumber,getActivity(),thisFragment);
                        }
                        else {
                            Toast.makeText(getActivity(), "Check Your Internet Connection!",
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                }
                else if(IsPOSChecked==false&&IsCustomChecked==true)
                {

                    String BrNumber = Br_No_EditText.getText().toString();
                    String PassportNumber = Passport_No_EditText.getText().toString();
                    captcha_text = Security_Code_EditText.getText().toString();
                    if (BrNumber.matches("")) {
                        MyCustomToast.createErrorToast(getActivity(), "Please Enter Br No.");
                        return;
                    }
                    else if (PassportNumber.matches("")) {
                        MyCustomToast.createErrorToast(getActivity(), "Please Enter Passport No.");
                        return;
                    }
                    else if (captcha_text.matches("")) {
                        MyCustomToast.createErrorToast(getActivity(), "Please Enter Security Code");
                        return;
                    } else if (!captcha_text.matches(textCaptcha.input_captcha_string)) {
                        Security_Code_EditText.setText("");
                        generate_Captcha();
                        showDialog_Captcha_Error("Security Code does not match, try again");
                        return;
                    }
                    else if (captcha_text.matches(textCaptcha.input_captcha_string)) {

                        CloseKeyBoard();
                        if(!BrNumber.matches(""))
                        {
                            User_BrNumber=BrNumber;
                        }
                        if(!PassportNumber.matches(""))
                        {
                            User_PassportNumber=PassportNumber;
                        }

                        System.out.println(User_CountryCode+"   "+User_EmailID+"    "+User_MobileNumber);
                        put_UserInputData(null,null,null,null,User_BrNumber,User_PassportNumber);
                        if (CheckNetworkConnection.getInstance(getActivity()).isOnline())
                        {
                            HitAPI.Hit_API_Token_CustomBased(User_PassportNumber,getActivity(),thisFragment);
                        }
                        else {
                            Toast.makeText(getActivity(), "Check Your Internet Connection!",
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                }


            }
        });
    }
    public void verified_OTP(String FinancialYr,String AtrNo, String BrNo, String PassportNo,String needArchiveTransaction)
    {
        if (CheckNetworkConnection.getInstance(getActivity()).isOnline()) {

            HitAPI.Hit_API_GetTrackTransaction(getActivity(),thisFragment,FinancialYr, AtrNo,BrNo,PassportNo,needArchiveTransaction);

        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection!",
                    Toast.LENGTH_LONG).show();
        }
    }
    public void generate_Captcha() {
        textCaptcha = new TextCaptcha(1100, 400, 6, TextCaptcha.TextOptions.NUMBERS_AND_LETTERS);
        captcha_ImageView.setImageBitmap(textCaptcha.getImage());
    }

    private void reload_Captcha_Button()
    {
        reload = (Button) localView.findViewById(R.id.reload_captcha_button);
        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                generate_Captcha();
                return;
            }
        });
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    private void putRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = " *";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }

    private void putDoubleRedAsterisk(View view, TextView textView, int id) {
        textView = (TextView) view.findViewById(id);
        String simple = textView.getText().toString();
        String colored = "**";
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(simple);
        int start = builder.length();
        builder.append(colored);
        int end = builder.length();
        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(builder);
    }
    private void showDialog_Captcha_Error(String errorText) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.error_textview);
        error_textview.setText(errorText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void showDialog_Invalid_Email_Phone(String warningText) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_warning);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void showDialog_False_OTP_Error(String warningText) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_warning);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.warning_textview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void showDialog_Blue_POSDialog(String warningText) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_blue);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView error_textview= (TextView) dialog.findViewById(R.id.yourtextview);
        error_textview.setText(warningText);
        Button dialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        dialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Security_Code_EditText.setText("");
                generate_Captcha();
                dialog.dismiss();
            }
        });
    }

    public void changeFragment_TransactionGridViewFragment()
    {
        Fragment transactionGridViewFragment = new TransactionGridViewFragment();
        FragmentTransaction ft = this.getActivity().getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_enter, R.anim.slide_exit);
        ft.replace(R.id.container_frame, transactionGridViewFragment);
        ft.commit();
    }
    public void showDialog_ServerError(String errorText)
    {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_error);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        TextView Error_TextView= (TextView) dialog.findViewById(R.id.error_textview);
        Error_TextView.setText(errorText);
        Button DialogOKButton = (Button) dialog.findViewById(R.id.okbutton);
        DialogOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // getActivity().finish();
                dialog.dismiss();
               // System.exit(0);
            }
        });
    }
}