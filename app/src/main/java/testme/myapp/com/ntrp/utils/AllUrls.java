package testme.myapp.com.ntrp.utils;

/**
 * Created by deepika.s on 22-04-2017.
 */

public class AllUrls {

    //  public static final String mainURL="http://164.100.129.32/NTRPMobileApi/API/NTRP/";

    public static final String SERVER_ADDRESS = "https://training.pfms.gov.in/";

    //public static final String SERVER_ADDRESS="https://10.249.220.4/";


    // Track Your Payment
    public static final String getTrackTransactionURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRP/GetTrackTransaction";
    public static final String getTokenURL = SERVER_ADDRESS + "NTRPMobileApi/Token";
    public static final String getSingleDepositorDetailsTransactionURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRP/ViewTransactionDetails";
    public static final String sendOTPURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRPWithOutFIlter/SendOTP";
    public static final String verifyOTPURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRP/VerifyOTP";
    public static final String deleteRecordsURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRP/DeleteTrackReceiptDetails";
    public static final String archiveRecordsURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRP/ArchiveTrackReceiptDetails";

    public static final String getAllPDFURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRP/GetPDFReceiptEntry";
    public static final String getTransactionRefNumberRecordURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRP/GetTransactionRefNumberRecord";


    // User Registration APIs
    public static final String sendOTPRegisterUserURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/SendRegisterUserOTP";
    public static final String VeriFyRefisterUserOTPURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/VerifyRegisterUserOTP";
    public static final String getDepositorListRegistrationURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/DepositorList";
    public static final String getControllerListURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/ControllerList";
    public static final String getBankListURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/BankList";
    public static final String getCheckUserAvaliabilityURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/CheckUserAvalable";
    public static final String getInsertUserURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/InsertUser";
    public static final String getUserDetailsURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/UserProfile";
    public static final String getChangePasswordURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/ChangePassword";
    public static final String updateUserProfileURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/UpdateUserProfile";
    public static final String getUserMenuURL = SERVER_ADDRESS + "NTRPMobileApi/API/RegisterUser/GetUserMenuXML";


    //Donate to Swachh Bharat Kosh
    public static final String getSalutationListURL = "https://training.pfms.gov.in/ntrpmobileapi/api/ntrp/GetSalutationList";
    public static final String getCountryListURL = "https://training.pfms.gov.in/NTRPMobileApi/API/NTRPwithoutfilter/CountryList";
    public static final String getStateListURL = "https://training.pfms.gov.in/NTRPMobileApi/API/NTRPwithoutfilter/StateList";
    public static final String getDistrictListURL = "https://training.pfms.gov.in/NTRPMobileApi/API/NTRPwithoutfilter/DistrictList";
    public static final String getSwachhBharatKoshURL = "https://training.pfms.gov.in/ntrpmobileapi/api/ntrp/SwachhBharatKosh";
    public static final String getListForChannelTypeURL = "https://training.pfms.gov.in/ntrpmobileapi/API/NTRP/GetListForChannelType";
    public static final String getAggregatorBankListURL = "https://training.pfms.gov.in/ntrpmobileapi/API/NTRP/GetAggregatorBankList";
    public static final String registerAPIUserURL = "https://training.pfms.gov.in/ntrpmobileapi/API/NTRPwithoutfilter/RegsiterApiUser";
    public static final String getAggregatorListURL = "https://training.pfms.gov.in/ntrpmobileapi/API/NTRP/GetAggregatorList";
    public static final String getDepositorListURL = "https://training.pfms.gov.in/ntrpmobileapi/API/RegisterUser/DepositorList";
    public static final String getDepositorDeatilsSubmitURL = "https://training.pfms.gov.in/ntrpmobileapi/api/ntrp/DepositerInfo";
    public static final String getDepositorDeatilsConfirmURL = "https://training.pfms.gov.in/ntrpmobileapi/api/ntrp/ConfirmInfo";
    public static final String getSubmitConfirmURL = "https://training.pfms.gov.in/ntrpmobileapi/api/ntrp/SubmitConfirmInfo";
    public static final String getBankListForSelectedPaymentGatewayURL = "https://training.pfms.gov.in/ntrpmobileapi/API/NTRP/GetAggregatorBankList";
    //public static final String getListForChannelTypeURL="https://training.pfms.gov.in/ntrpmobileapi/API/NTRP/GetListForChannelType";

    public static final String getReceiptPaymentInfoURL = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/ReceiptPaymentInfo";


    //Non register user

    public static final String Depositor_Category = SERVER_ADDRESS + "ntrpmobileapi/API/NonRegisterUser/Depositorcategory";

    public static final String Ministry_List = SERVER_ADDRESS + "ntrpmobileapi/API/NonRegisterUser/MinistryList";

    public static final String GetPurpose_List = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/GetPurposeMinistryFunctionHeadList";

    public static final String Get_After_Selected_List = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/Purpose_TextChanged";
    public static final String Get_App_Form_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/AddFormDetail";
    public static final String Get_Next_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/btnNext_Click";
    public static final String Non_Register_User_Depositor_URL = SERVER_ADDRESS + "ntrpmobileapi/api/ntrp/DepositerInfo";
    public static final String Non_Register_User_Submit_User_Details_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/SubmitUserDetails";
    public static final String Non_Register_Auto_Complete_Text_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/AutoCompletePurpose";
    public static final String Non_Register_Delete_Purpose_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/DeleteReceiptPurpose";
    public static final String Non_Register_Get_List_Of_Purpose_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/PurposeList";
    public static final String Non_Register_CheckPaymentMode_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/rdbPaymentmode_SelectedIndexChanged";
    public static final String Non_Register_OfflinePaymentMode_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/ResponseStatusOffLine";
    public static final String Non_Register_getBankVlidation_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/ValidateBankDetails";


    //GOI e-Store
    public static final String imagePath = SERVER_ADDRESS + "Bharatkosh/ProductImages/";
    public static final String getProductMenuXMLURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRP/GetProductMenuXML";
    public static final String getProductListURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRP/GetProductList";
    public static final String getAddCartProduct = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/AddCartProduct";
    public static final String getBasket = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/GetBasket";
    public static final String getDropDownList = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/GetDropDownList";
    public static final String getPublicationCheckOut = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/PublicationCheckOut";
    public static final String getTotalDiscount = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/GetTotalDiscount";
    public static final String getShippingCharges = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/GetShippingCharges";
    public static final String getRemoveItem = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/RemoveItem";
    public static final String getRemoveAllProductFromCart = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/RemoveAllProductFromCart";
    public static final String getLogin = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/Login";
    public static final String getTitleDropDown = SERVER_ADDRESS + "ntrpmobileapi/API/NonRegisterUser/GetDropDown";
    public static final String getUserProfile = SERVER_ADDRESS + "ntrpmobileapi/API/RegisterUser/UserProfile";


    //Home Page URLS
    public static final String getFAQURL = SERVER_ADDRESS + "NTRPMobileApi/API/NTRPwithoutfilter/GetFAQ";

    //Current Receipt URL
    public static final String Current_Receipt_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NTRPWithOutFIlter/ReceiptAvailableForDeposits";

    //Contribution to NDRF
    public static final String NDRF_HOME_URL = SERVER_ADDRESS + "ntrpmobileapi/api/NonRegisterUser/DonateToNDRF?Cid=Lc6h5%2Bdr8dw%3D&Pid=9ULJVQojTik%3D&RPT=HeIU6RtSZMg%3D&PD=pUrWJA4yoFN3yqwUOamx2j8tAyNM2zubxbE9iDQi8Dg%3D";

    //Forget Password
    public static final String CHECK_USER_EXIST_URL = SERVER_ADDRESS + "ntrpmobileapi/api/RegisterUser/ForgetPassword";
    public static final String CHANGE_PASSWORD_URL = SERVER_ADDRESS + "ntrpmobileapi/api/RegisterUser/ChangePassword";

    //Payment Details
    public static final String GET_PAYMENT_DETAILS_URL = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/ReceiptPaymentDetail";
    public static final String SEND_PAYMENT_DETAILS_URL = SERVER_ADDRESS + "ntrpmobileapi/API/NTRP/MobileResponseStatus";



}
