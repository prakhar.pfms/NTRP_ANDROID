package testme.myapp.com.ntrp.module_login.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserMenuResponse {

    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("ParentID")
    @Expose
    private Integer parentID;
    @SerializedName("Code")
    @Expose
    private String code;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Text")
    @Expose
    private String text;
    @SerializedName("Url")
    @Expose
    private String url;
    @SerializedName("AppearsInMenu")
    @Expose
    private Integer appearsInMenu;
    @SerializedName("MenuOrder")
    @Expose
    private Integer menuOrder;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getAppearsInMenu() {
        return appearsInMenu;
    }

    public void setAppearsInMenu(Integer appearsInMenu) {
        this.appearsInMenu = appearsInMenu;
    }

    public Integer getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(Integer menuOrder) {
        this.menuOrder = menuOrder;
    }

}
