package testme.myapp.com.ntrp.module_nonregisteredusers.adapters;

import android.app.Dialog;
import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import testme.myapp.com.ntrp.R;
import testme.myapp.com.ntrp.module_nonregisteredusers.fragments.NonRegisteredUsersFragment;
import testme.myapp.com.ntrp.module_nonregisteredusers.model.PurposeFunctionalMinisty;
import testme.myapp.com.ntrp.module_trackyourpayment.models.PurposeList;
import testme.myapp.com.ntrp.utils.MessageEvent;

/**
 * Created by deepika.s on 31-05-2017.
 */

public class ChoosePurposeAdapter extends RecyclerView.Adapter<ChoosePurposeAdapter.MyViewHolder> {

    private List<PurposeFunctionalMinisty> purposeLists;
    private Context context;
    private Fragment nonRegisteredUsersFragment;
    private Dialog ChoosepurposeDialog;
    public ChoosePurposeAdapter(Context context, Fragment nonRegisteredUsersFragment, List<PurposeFunctionalMinisty> purposeList, Dialog ChoosepurposeDialog)
    {
        this.context=context;
        this.nonRegisteredUsersFragment=nonRegisteredUsersFragment;
        this.purposeLists=purposeList;
        this.ChoosepurposeDialog=ChoosepurposeDialog;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_choose_purpose_row,parent, false);
        return new ChoosePurposeAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        PurposeFunctionalMinisty purposeList=purposeLists.get(position);
        holder.PurposeTextView.setText(purposeList.getPurposeDescription());
        holder.PaymentType_TextView.setText(purposeList.getPaymentTypeDescription());
        holder.FunctionHead_TextView.setText(purposeList.getFuncHeadDescription());
        holder.Ministry_TextView.setText(purposeList.getControllerName());

        SpannableString str = new SpannableString(purposeList.getPurposeDescription());
        str.setSpan(new UnderlineSpan(), 0, str.length(), Spanned.SPAN_PARAGRAPH);
        holder.PurposeTextView.setText(str);

        holder.PurposeTextView.setSelected(true);
        holder.PaymentType_TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("clicked.......textview..");
                NonRegisteredUsersFragment.choosenPuposePosition= holder.getAdapterPosition();
                EventBus.getDefault().post(new MessageEvent(
                        purposeList.getPurposeDescription(),
                        purposeList.getPaymentTypeDescription(),
                        purposeList.getFuncHeadDescription(),
                        purposeList.getControllerName(),purposeList
                        ));

                ChoosepurposeDialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return purposeLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        public TextView PurposeTextView,PaymentType_TextView,FunctionHead_TextView,Ministry_TextView;
        public MyViewHolder(View itemView) {
            super(itemView);
            PurposeTextView= (TextView) itemView.findViewById(R.id.purpose_textview);
            PaymentType_TextView= (TextView) itemView.findViewById(R.id.payment_type_textview);
            FunctionHead_TextView= (TextView) itemView.findViewById(R.id.function_head_textview);
            Ministry_TextView= (TextView) itemView.findViewById(R.id.ministry_textview);
        }
    }
}
